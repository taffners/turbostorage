<?php
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/turbostorage/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'turbostorage')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/turbostorage/config.php');
	}

	$tube_id_array['tube_id'] = $_REQUEST['tube_id'];

	$found_tube_info = $db->listAll('select-tube-info', $tube_id_array);
	echo json_encode($found_tube_info);
?>
