<?php

	if(isset($_POST))
	{
		if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
		{
			require_once($_SERVER['DOCUMENT_ROOT'].'/devs/turbostorage/config.php');
		}
		elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'turbostorage')
		{
			require_once($_SERVER['DOCUMENT_ROOT'].'/turbostorage/config.php');
		}

		$user_info = $_POST;

		unset($user_info['form_checked_create']);
		unset($user_info['create_user_submit']);
		unset($user_info['password2']);

		$user_info['role'] = 'regular';
		$user_info['status'] = 'active';

		// encrypt secruity answers and password before adding to db
		$user_info['securityAnswer'] = md5(strtoupper($user_info['securityAnswer']));
		$user_info['securityAnswer2'] = md5(strtoupper($user_info['securityAnswer2']));
		$user_info['password'] = md5($user_info['password']);

		$user_result = $db->addOrModifyRecord('userTable',$user_info);

		if ($user_result[0])
		{
			header('Location: ../index.php?userAdded=1');
		}
		//if it isn't re-load the form with an error message
		else
		{
			header('Location: ../index.php?userAdded=0');
		}
	}
?>
