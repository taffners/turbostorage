<?php
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/turbostorage/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'turbostorage')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/turbostorage/config.php');
	}

	// find all tube info of a box
	$box_info_result = $db->ExportCSV('box-info-csv',$_REQUEST['box_id']);

	$headers = $box_info_result['headers'];
	$box_name = $box_info_result['box_name'];
	$search_result = $box_info_result['search_result'];
	$tube_col_key_num = (int)array_search('color',$headers);

	// make a file
	$of = fopen("php://output", "w");

	// add box name to first line of file
	$curr_array = array_fill('', $num_fields+1, 0);
	$curr_array[0] = 'TurboStorage Box: '.$box_name;
	fputcsv($of,$curr_array);

	// add headers
	fputcsv($of, $headers);

	// array to convert hex color to a human readable
	$colors = array(
					'white' => 'white',
					'red' => 'red',
					'#FF9933' => 'orange',
					'#B0E0E6' => 'blue',
					'#BCE5AF' => 'green',
					'#FFFF4C' => 'yellow',
					'#B87FED' => 'purple',
					'#F493E2' => 'pink',
					'#f5d59a' => 'tan',
					'#c7c7c7' => 'grey'
				);

	// add data to csv
	for($i=0; $i < sizeof($search_result); $i++)
	{
		$curr_row = $search_result[$i];
		// convert color from hex to color name
		$curr_color = $colors[$curr_row[$tube_col_key_num]];

		// change color in current row to human readable from hex
		$curr_row[$tube_col_key_num] = $curr_color;

		fputcsv($of, $curr_row);
	}
	fclose($of);

	header('Content-Type: text/csv');
	header('Content-Disposition: filename="box_info_'.$box_name.'_'.date('m_d_Y_H_i').'.csv"');
	header('Pragma: no-cache');
	header('Expires: 0');

	readfile($of);

?>
