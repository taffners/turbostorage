<?php

	function bytesToSize1024($bytes, $precision = 2)
	{
    		$unit = array('B','KB','MB');
	    	return @round($bytes / pow(1024, ($i = floor(log($bytes, 1024)))), $precision).' '.$unit[$i];
	}

	$upload_ok = false;
	$copy_ok = false;
	$file_ok = false;
	if(isset($_FILES['image_file']))
	{
		// import config file
		if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
		{
			require_once($_SERVER['DOCUMENT_ROOT'].'/devs/turbostorage/config.php');
		}
		elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'turbostorage')
		{
			require_once($_SERVER['DOCUMENT_ROOT'].'/turbostorage/config.php');
		}

		//file types that you will allow to be uploaded
		//if you need to add more, go to https://www.sitepoint.com/web-foundations/mime-types-complete-list/
		$allowed_mimes = array('image/svg+xml','image/gif','image/jpeg','image/png','image/tiff','text/html','text/plain','application/msword','application/excel','application/mspowerpoint','application/pdf');

		//pro tip - always assume it's a bad file type until you know that it isn't :)
		$file_ok = false;

		for($i=0;$i<sizeof($allowed_mimes);$i++)
		{
			if($_FILES['image_file']['type'] === $allowed_mimes[$i])
			{
				$file_ok = true;
				break;
			}
		}

		//if the file is ok
		if($file_ok)
		{
			//read the contents of the file into a string;
			$file_contents = file_get_contents($_FILES['image_file']['tmp_name']);
			//get an md5 hash of the file contents. this will be used as a unique identifier to see if the file has already been uploaded
			$md5 = md5($file_contents);

			$copy_ok = copy($_FILES['image_file']['tmp_name'], '/uploads/'.$_FILES['image_file']['name']);

			if(!$copy_ok)
			{
				echo 'Shit shit shit';
			}

			else
			{
				/*

					Table definition for upload file...
					CREATE TABLE uploads(
					    file_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
					    name VARCHAR(255) NOT NULL,
					    mime VARCHAR(32) NOT NULL,
					    md5 VARCHAR(32) NOT NULL,
					    upload_time INT UNSIGNED NOT NULL
					);
				*/
				$file_info['name'] = $_FILES['image_file']['name'];
				$file_info['size'] = $_FILES['image_file']['size'];
				$file_info['mime'] = $_FILES['image_file']['type'];
				$file_info['md5'] = $md5;
				$file_info['upload_time'] = time();

				$upload_ok = $db->addOrModifyRecord('uploads',$file_info);

			}
		}

		else
		{
			echo 'Danger! Danger!';
		}
	}


	if($upload_ok)
	{
		$sFileName = $_FILES['image_file']['name'];
		$sFileType = $_FILES['image_file']['type'];
		$sFileSize = bytesToSize1024($_FILES['image_file']['size'], 1);

echo <<<HEREDOC
<p>Your file: {$sFileName} has been successfully received.</p>
<p>Type: {$sFileType}</p>
<p>Size: {$sFileSize}</p>
HEREDOC;
	}

	else
	{
echo <<<HEREDOC
SON OF A WHORE!
HEREDOC;
	}
?>
