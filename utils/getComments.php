<?php
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/turbostorage/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'turbostorage')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/turbostorage/config.php');
	}

	$CommentType = $_REQUEST['type'];
	$refID = $_REQUEST['refID'];

	$comments = $db->listAll('comment-'.$CommentType, intval($refID));
	echo json_encode($comments);

?>
