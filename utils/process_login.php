<?php

	if(isset($_POST))
	{
		if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
		{
			require_once($_SERVER['DOCUMENT_ROOT'].'/devs/turbostorage/config.php');
		}
		elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'turbostorage')
		{
			require_once($_SERVER['DOCUMENT_ROOT'].'/turbostorage/config.php');
		}

		$logged_in = $db->login($_POST);

		if(!$logged_in)
		{
			header('Location: ../?page=login&failedLogin=true&attemptNum=1');
		}

		else
		{
			//log in
			header('Location: ../?page=home');

		}

	}


?>
