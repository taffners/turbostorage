<?php
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/turbostorage/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'turbostorage')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/turbostorage/config.php');
	}

     // get all sample type vars for a tube ID
     $sample_type_vars = $db->listAll('sample-type-vars',$_REQUEST['tubeID']);

	if ($_REQUEST['comments'] == 'yes')
	{
		// get comments
		$comments = $db->listAll('comment-sample',$_REQUEST['tubeID']);

		$sample_type_vars += array('all_comments'=> $comments);
	}

     echo json_encode($sample_type_vars);

     exit();

?>
