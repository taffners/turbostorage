<?php
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/turbostorage/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'turbostorage')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/turbostorage/config.php');
	}

	// find if a email address is already added
	$emails = $db->listAll('user-email',$_REQUEST['new_email']);

	// if something was found then halt adding
	if (count($emails) > 0)
	{
		echo 'duplicate email address';
	}

	// otherwise proceed with adding new user
	else
	{
		echo 'Passed Tests';
	}

     exit();

?>
