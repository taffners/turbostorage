<?php
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/turbostorage/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'turbostorage')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/turbostorage/config.php');
	}

	// find if a email address is already added
	$questArray = $db->listAll('user-questions',$_REQUEST['new_email']);
// print_r($questArray);
	// if something was found then halt adding
	if (count($questArray) === 0)
	{
          $questArray['test_status'] = 'Email Address Not Found';
		echo json_encode($questArray);
	}

	// otherwise proceed with adding new user
	else
	{
          $questArray['test_status'] = 'Passed Tests';
		echo json_encode($questArray);
	}

     exit();

?>
