<?php
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/turbostorage/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'turbostorage')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/turbostorage/config.php');
	}

	$groupID = $_REQUEST['action'];

	// if the action is update this group name will already be in the database.  Just pass it.
	if ($groupID === 'update')
	{
		echo 'Passed Tests';
	}

	// Check if the newly added group name is already in the database
	else
	{
		// find if group name already exists
		$group_names = $db->listAll('group-name',$_REQUEST['group_name']);

		if ($group_names[0]['groupCount'] != 0)
		{
			echo 'group name already used';
		}
		else
		{
			echo 'Passed Tests';
		}
	}

     exit();
?>
