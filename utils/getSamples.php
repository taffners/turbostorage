<?php
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/turbostorage/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'turbostorage')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/turbostorage/config.php');
	}

	$boxID = $_REQUEST['boxID'];

	// Get all of the tubes in the selected box
	$tubes = $db->listAll('tubes-box',intval($boxID));

	for($i=0; $i<sizeof($tubes) ;$i++)
	{
		$tubeVariables = $db->listAll('sampleTypeVariables',$tubes[$i]['tubeID']);

		$tubeInfo = array();

		for($k=0; $k<sizeof($tubeVariables);$k++)
		{
			$temp = $tubeVariables[$k];

			$key = $temp['variableName'];

			$tubeInfo[$key] = $temp['variableValue'];
		}
		$tubes[$i]['sampleInfo'] = $tubeInfo;
	}

	echo json_encode($tubes);
	exit();
?>
