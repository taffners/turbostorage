<?php
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/turbostorage/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'turbostorage')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/turbostorage/config.php');
	}

	// find all tube info of a box
	$grid_result = $db->ExportGridCSV('box-grid',$_REQUEST['box_id']);

	$box_size = (int)$grid_result['box_size'];
     $grid = $grid_result['grid'];
     $box_name = $grid_result['box_name'];

	// make a file
	$of = fopen("php://output", "w");

	// add box name to first line of file
	$curr_array = array_fill('',$box_size+1, 0);
	$curr_array[0] = 'TurboStorage Box: '.$box_name;
	fputcsv($of,$curr_array);

	// add data to csv
	for($i=0; $i < $box_size+1; $i++)
	{
		fputcsv($of, $grid[$i]);
	}
	fclose($of);

	header('Content-Type: text/csv');
	header('Content-Disposition: filename="box_layout_'.$box_name.'_'.date('m_d_Y_H_i').'.csv"');
	header('Pragma: no-cache');
	header('Expires: 0');

	readfile($of);
?>
