<?php
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/turbostorage/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'turbostorage')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/turbostorage/config.php');
	}

	$search_array = array();

     $search_array['search_term'] = $_REQUEST['search_term'];

	$search_array['userID'] = $_SESSION['user']['userID'];

	// if page is admin search for all tubes otherwise just search for the users tubes
	if ($_REQUEST['page'] == 'admin')
	{
		$found_tubes = $db->listAll('admin-search-tubes', $search_array);
	}
	else
	{
		$found_tubes = $db->listAll('search-tubes', $search_array);
	}



	echo json_encode($found_tubes);
?>
