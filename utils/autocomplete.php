<?php

	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/turbostorage/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'turbostorage')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/turbostorage/config.php');
	}

	// find a list of data for autocomplete
	$search_array = array();

	$search_array['query_name'] = $_POST['request']['term'];

	$search_array['userID'] = $_SESSION['user']['userID'];

	// if page is admin autocomplete with all ever query name not just the users
	if($_POST['page'] == 'admin')
	{
		$results = $db->listAll('admin-autocomplete-'.$_POST['query_name'], $search_array);
	}
	else
	{
		$results = $db->listAll('autocomplete-'.$_POST['query_name'], $search_array);
	}

     echo json_encode($results);

?>
