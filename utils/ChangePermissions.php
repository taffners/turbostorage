<?php

	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/turbostorage/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'turbostorage')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/turbostorage/config.php');
	}

	// if checked add value to database
	if($_POST['db_task'] == 'add')
	{
		$role_array = array();
		$role_array['userID'] = $_POST['input_ids'][1];
		$role_array['roleID'] = $_POST['input_ids'][2];
		$role_array['createrUserID'] = $_POST['user_id'];

		$role_response = $db->addOrModifyRecord('userRoleTable',$role_array);
	}


	// if not checked delete value from database
	elseif($_POST['db_task'] == 'delete')
	{
		$delete_role = array();
		$delete_role['userRoleID'] = $_POST['input_ids'][0];

		$result = $db->deleteRecord('userRoleTable',$delete_role);
	}


?>
