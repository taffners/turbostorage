<?php
	class DateFuncs
	{
		public function HumanReadableDateTime($unixTime)
		{
   			$dt = new DateTime("@$unixTime");
   			return $dt->format('Y-m-d H:i:s');
		}

		public function ChangeDateFormatUS($in_date, $check_array)
		{
			// Purpose: Convert date format from YYYY-MM-DD to MM/DD/YYYY

			$is_empty = $this->CheckEmptyDate($in_date, $check_array);

			return $is_empty ? '' : date('m/d/Y', strtotime($in_date));
		}

		private function CheckEmptyDate($in_date, $check_array)
		{
			// Purpose: check if date is equal to '' or '00/00/0000'.  If it is return true otherwise return false

			if($in_date === '' || $in_date === '0000-00-00' || $in_date === '00/00/0000')
			{
				return TRUE;
			}
			elseif($check_array)
			{
				return !isset($in_date);
			}
			else
			{
				return FALSE;
			}
		}

		public function ChangeDateFormatEuropean($in_date)
		{
			// Purpose: Convert date format from MM/DD/YYYY to YYYY-MM-DD

		}


	}

?>
