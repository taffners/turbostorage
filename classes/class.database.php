<?php

	class DataBase
	{
		private $conn;

		public function DataBase($conn)
		{
			$this->conn = $conn;
		}

		public function CreateRow()
		{

			$botFound = $this->formatPost();//clean up $_POST and find if bot was there

			//make sure a bot was not found
			if ($botFound == 'no')
			{
				//make sure a duplicate was not found
				$alreadyAdded = $this->findDuplicate();

				if($alreadyAdded > 0) //aready present in the colName
				{
					return 'Already Added';
				}
				else
				{
					$result = $this->addOrModifyRecord();
					if($result > 0 )
					{
						return 'Added';
					}
					else
					{
						return 'error adding';
					}
				}

			}
			else //bot found
			{
				return 'abort';
			}

		}

		public function formatPost()
		{
			//removes submit and adds a creation time to every Post
			//also checks if every Post is posted by a bot.

			unset($this->postInput[$this->submitButtonName]);

			$botFound ='no';

			if($this->postInput['creationTime'] == '')
			{
				$this->postInput['creationTime'] = time();
			}

			//redirect to sign in and log out if bot found
			if($this->postInput['botCheck'] != '')
			{
				$botFound = 'yes';
			}
			else
			{
				unset($this->postInput['botCheck']);
			}

			return $botFound;
		}

		public function addOrModifyRecord($table, $inputs)
		{
			$count = 0;
			$replace = 'REPLACE '.$this->sanitize($table).' SET ';

			foreach($inputs as $name => $value)
			{
				$replace .= $this->sanitize($name).' = "'.$this->sanitize($value).'"';

				$count++;

				if($count < sizeof($inputs))
				{
					$replace .= ', ';
				}
			}

			$result = mysqli_query($this->conn, $replace) or die('Oops!! Died '.$replace);

			// get the Id of the inserted record
			$lastId = mysqli_insert_id($this->conn);

			return [$result, $lastId];
		}

		public function addOrModifySampleVariables($table, $inputs, $sampleID)
		{

			foreach($inputs as $name => $value)
			{
				$replace = 'REPLACE '.$this->sanitize($table).' SET ';

				// if this is an update the name will have a - in it.
				// it will be variableName - sampleTypeVariableID.
				// seperate variableName and sampleTypeVariableID
				if (strpos($name, '-') !== false)
				{
					$name_ID = str_split($name, strpos($name, '-'));

					$replace .= 'variableName = "'.$this->sanitize($name_ID[0]).'", ';
					$replace .= 'sampleTypeVariableID = "'.$this->sanitize(str_replace('-', '',$name_ID[1])).'", ';
				}
				else
				{
					$replace .= 'variableName = "'.$this->sanitize($name).'", ';
				}



				$replace .= 'variableValue = "'.$this->sanitize($value).'", ';
				$replace .= 'tubeID = '.$this->sanitize($sampleID);
				$count++;

				$result = mysqli_query($this->conn, $replace) or die($replace);
				$lastId = mysqli_insert_id($this->conn);
			}


			return [$result, $lastId];
		}

		public function deleteRecord($table, $inputs)
		{
			$count = 0;
			$delete = 'DELETE FROM '.$this->sanitize($table).' WHERE ';

			foreach($inputs as $name => $value)
			{
				$delete .= $this->sanitize($name).' = "'.$this->sanitize($value).'"';

				$count++;

				if($count < sizeof($inputs))
				{
					$delete .= ' AND ';
				}
			}

			$result = mysqli_query($this->conn, $delete) or die($delete);
			$lastId = mysqli_insert_id($this->conn);

			return [$result, $lastId];
		}

		public function listAll($query, $where=NULL)
		{

			switch($query)
			{
				case 'roles':
					$select =
					'
					SELECT 		*
					FROM		     roleTable
					ORDER BY 	     roleID
					';
					break;

				case 'all-user-roles':
					$select =
					'
					SELECT         CONCAT(ut.firstName," " ,ut.lastName) AS userName,
					               urt.userID,
					               rt.roleName,
					               rt.roleID,
					               urt.userRoleID

					FROM           userRoleTable urt

					LEFT JOIN      userTable ut
					ON             urt.userID = ut.userID

					LEFT JOIN      roleTable rt
					ON             rt.roleID = urt.roleID
					';
					break;

				case 'user-names':
					$select =
					'
					SELECT         CONCAT(ut.firstName," " ,ut.lastName) AS userName,
								ut.userID

					FROM          	userTable ut
					';
					break;

				case 'user-roles':
					$select =
					'
					SELECT        	rt.roleName

					FROM           userRoleTable urt

					LEFT JOIN      userTable ut
					ON             urt.userID = ut.userID

					LEFT JOIN      roleTable rt
					ON             rt.roleID = urt.roleID

					WHERE 		urt.userID = "'.$this->sanitize($where).'"
					';
					break;

				case 'irbs':
					$select =
					'
					SELECT 	*
					FROM		irbTable
					';
					break;

				case 'using-freezer':
					$select =
					'
					SELECT DISTINCT     bt.userID,
					                    CONCAT(ut.firstName," " ,ut.lastName) AS userName,
					                    bt.freezerID,
					                    bt.groupID,
					                    agmv.allMembers,
					                    agmv.groupName,
					                    ft.freezerName,
					                    ft.roomNum,
					                    ft.temp

					FROM                boxTable bt

					LEFT JOIN           userTable ut
					ON                  ut.userID = bt.userID

					LEFT JOIN          all_group_members_vw agmv
					ON                 bt.groupID = agmv.groupID

					LEFT JOIN           freezerTable ft
					ON                  ft.freezerID = bt.freezerID

					';
					break;

				case 'remove-log':
					$select =
					'
					SELECT 	rrl.*,
							CONCAT(ut.firstName," ",ut.lastName) AS userName,
							tt.sampleType

					FROM 	requestRemoveLog rrl

					LEFT JOIN userTable ut
					ON		ut.userID = rrl.userID

					LEFT JOIN tubeTable tt
					ON		tt.tubeID = rrl.tubeID

					';
					break;

				case 'box-tubeids':
					$select =
					'
					SELECT DISTINCT	tubeID
					FROM 			tube_info_vw
					WHERE 	boxID = "'.$this->sanitize($where).'"
					';
					break;

				// get all irbs
				case 'all-irbs':
					$select =
					'
					SELECT 	*
					FROM		irbTable
					';
					break;
				// get all users
				case 'select-tube-info':
							$select =
							'
							SELECT DISTINCT 	tt.sampleName,
											tt.sampleType,
											tt.freezeDate,
											tt.experimentName,
											tt.labBook,
											tt.pageNum,
											tt.published,
											tt.intialNumTubes,
											tt.mta,
											tt.frozeBy,
											tt.intialNumTubes,
											tt.consentYesNo,
											tt.consentApprovalDate,
											tt.locConsent,
											tt.consentNotes,
											tt.openAccess,
											tt.irbID,
											bt.boxName,
											it.irbProjectTitle,
											it.irbPI,
											it.irbNum

							FROM 	tubeTable tt

							INNER JOIN 	spaceTable st
							USING	(tubeID)

							INNER JOIN 	boxTable bt
							USING	(boxID)

							LEFT JOIN	irbTable it
							USING 	(irbID)

							WHERE tubeID = "'.$this->sanitize($where['tube_id']).'"
							';
							break;
				case 'autocomplete-sampleName':
					$searchTerm = $this->sanitize($where['query_name']);

					$userID = $this->sanitize($where['userID']);
					$select =
					'
					SELECT DISTINCT    sampleName

					FROM
						(

							SELECT DISTINCT     tiv.tubeID,
											tiv.spaceID,
											tiv.boxID,
											tiv.sampleName,
											b.boxName

							FROM           tube_info_vw tiv

							INNER JOIN     boxTable b ON tiv.boxID = b.boxID

							WHERE         	tiv.sampleName LIKE "%'.$searchTerm.'%"

							AND             tiv.boxID IN
								(
																														SELECT    pub.boxID
																FROM      public_boxes_vw pub
																UNION
																														SELECT    priv.boxID
																														FROM      private_boxes_vw priv
																														WHERE     priv.userID = '.$userID.'
																														UNION
																														SELECT    gr.boxID
																														FROM      group_boxes_vw gr
																														WHERE     gr.groupID IN
																															(
																																SELECT    groupID
																																FROM      groupMemberTable
																																WHERE     userID = '.$userID.'
																															)
								)

						)  results
					GROUP BY tubeID
					'
					;
					break;

				case 'admin-autocomplete-sampleName':
					$searchTerm = $this->sanitize($where['query_name']);

					$userID = $this->sanitize($where['userID']);
					$select =
					'
					SELECT DISTINCT    sampleName

					FROM
						(

							SELECT DISTINCT     tiv.tubeID,
											tiv.spaceID,
											tiv.boxID,
											tiv.sampleName,
											b.boxName

							FROM           tube_info_vw tiv

							INNER JOIN     boxTable b ON tiv.boxID = b.boxID

							WHERE         	tiv.sampleName LIKE "%'.$searchTerm.'%"

							AND             tiv.boxID IN
								(
																														SELECT    pub.boxID
																FROM      public_boxes_vw pub
																UNION
																														SELECT    priv.boxID
																														FROM      private_boxes_vw priv
																														UNION
																														SELECT    gr.boxID
																														FROM      group_boxes_vw gr
																														WHERE     gr.groupID IN
																															(
																																SELECT    groupID
																																FROM      groupMemberTable

																															)
								)

						)  results
					GROUP BY tubeID
					'
					;
					break;

				case 'users':
								$select =
								'
								SELECT 	*

								FROM 	userTable
								'
								;
								break;


				case 'user-email':
								$select =
								'
								SELECT 	emailAddress

								FROM 	userTable

								WHERE 	emailAddress = "'.$this->sanitize($where).'"
								'
								;
								break;

				case 'user-questions':
								$select =
								'
								SELECT 	userID,
										emailAddress,
										securityQuestion,
										securityQuestion2

								FROM 	userTable

								WHERE 	emailAddress = "'.$this->sanitize($where).'"
								'
								;
								break;
				case 'user-answers':
								$select =
								'
								SELECT 	*

								FROM 	userTable

								WHERE 	userID = "'.$this->sanitize($where).'"
								'
								;
								break;
				case 'box':
								$select =
									'SELECT * FROM boxTable bt
									LEFT JOIN freezerTable ft
									USING (freezerID)';
								break;

				case 'box-notubes':
								$select =
								'
								SELECT    pub.boxID,
										pub.boxName,
										pub.privacyLevel,
										"" AS reqRequired,
										"" AS groupName,
										pub.freezerName,
										pub.roomNum,
										pub.shelfNum,
										pub.rackNum,
										pub.xSize,
										pub.ySize,
										pub.rackSpaceNum,
										pub.emptySpaces

								FROM      public_boxes_vw pub

								WHERE	(pub.xSize * pub.ySize) = pub.emptySpaces

								UNION

								SELECT    priv.boxID,
										priv.boxName,
										priv.privacyLevel,
										"" AS reqRequired,
										"" AS groupName,
										priv.freezerName,
										priv.roomNum,
										priv.shelfNum,
										priv.rackNum,
										priv.xSize,
										priv.ySize,
										priv.rackSpaceNum,
										priv.emptySpaces


								FROM      private_boxes_vw priv

								WHERE     priv.userID ="'.$this->sanitize($where).'"
								AND 		(priv.xSize * priv.ySize) = priv.emptySpaces

								UNION

								SELECT    gr.boxID,
										gr.boxName,
										gr.privacyLevel,
										gr.reqRequired,
										gr.groupName,
										gr.freezerName,
										gr.roomNum,
										gr.shelfNum,
										gr.rackNum,
										gr.xSize,
										gr.ySize,
										gr.rackSpaceNum,
										gr.emptySpaces


								FROM      group_boxes_vw gr

								WHERE     (gr.xSize * gr.ySize) = gr.emptySpaces
								AND		gr.groupID IN  (
														SELECT    groupID
														FROM      groupMemberTable
														WHERE     userID ="'.$this->sanitize($where).'"
													)

								'
								;
								break;

				// get all freezer infromation
				case 'freezer':
								$select =
								'
								SELECT 	*
								FROM 	freezerTable
								'
								;
								break;

				case 'box-freezer':
								$select =
									'SELECT * FROM boxTable
									LEFT JOIN freezerTable
									USING (freezerID)';
								break;

				// find all freezers with no boxes in them
				case 'freezer-nobox':
								$select =
								'
								SELECT         f.*


								FROM           freezerTable f

								LEFT JOIN      boxTable b
								ON             f.freezerID = b.freezerID

								WHERE          b.freezerID IS NULL
								'
								;
								break;

				// get all the for making visual tube in box
				case 'tubes-box':
								$select =
									'
									SELECT         *

									FROM           tube_info_vw ti

									WHERE          ti.boxID =  '.$this->sanitize($where);
								break;

				case 'sampleTypeVariables':
								$select =
									'SELECT * FROM sampleTypeVariables
									WHERE sampleID = '.$this->sanitize($where);
								break;
				// comments
				// Get all the comments for a particular freezer
				case 'comment-freezer':
								$select =
									'
									SELECT 		*
									FROM 		freezerTable f
									LEFT JOIN 	commentTable c
									ON 			f.freezerID=c.commentRef
									WHERE 		f.freezerID ="'.$this->sanitize($where).'"
									AND 			c.commentType = "freezer"';
								break;

				// get allt he comments for a particular box
				case 'comment-box':
								$select =
									'
									SELECT 		*

									FROM 		boxTable b

									LEFT JOIN 	commentTable c

									ON 			b.boxID=c.commentRef

									WHERE 		b.boxID ="'.$this->sanitize($where).'"
									AND 			c.commentType = "box"';
								break;
				// get the comments for a particular tube
				case 'comment-sample':
								$select =
								'
								SELECT 			ct.comment,
												ct.commentRef,
												tt.sampleName

								FROM				commentTable ct

								LEFT JOIN			tubeTable tt
								ON				tt.tubeID = ct.commentRef

								WHERE			ct.commentRef = "'.$this->sanitize($where).'"

								And 				ct.commentType = "tube"
								'
								;

								break;


				// get the comment for a particular request
				case 'comment-request':
								$select =
								'
								SELECT 			ct.comment,
												ct.commentRef

								FROM				commentTable ct

								WHERE			ct.commentRef = "'.$this->sanitize($where).'"

								And 				ct.commentType = "request"
								'
								;
								break;

				// find all comments for a group
				case 'comment-group':
								$select =
								'
								SELECT 			ct.comment,
												ct.commentRef,
												gt.groupName


								FROM				commentTable ct

								LEFT JOIN			groupTable gt
								ON				gt.groupID = ct.commentRef

								WHERE			ct.commentRef = "'.$this->sanitize($where).'"

								And 				ct.commentType = "group"
								';
								break;


				// use to find if a group of user ids already exists
				case 'group-exists':
								$select  = 'SELECT DISTINCT groupID ';
								$select .= 'FROM groupMemberTable ';
								$select .= 'WHERE ';

								foreach($where as $key => $value)
								{
									// skip if $key == group_name
									if (is_int($key))
									{
										// add another select statement if key is greater than 0
										if($key > 0)
										{
											$select.= ' AND ';
										}

										$select .= 'groupID IN (';
										$select .= 'SELECT DISTINCT groupID ';
										$select .= 'FROM groupMemberTable ';
										$select .= ' WHERE userID = '.$this->sanitize($value['userID']).')';
									}
								}

								break;

				// find the group name passed
				case 'group-name':
								$select  =
								'
								SELECT 	COUNT(groupName) AS groupCount
								FROM 	groupTable
								WHERE 	groupName = "'.$this->sanitize($where).'"';

								break;

				// get all of the group names for a user
				case 'members-in-group':
								$select  =
								'
								SELECT 		g.groupName,
											g.groupID,
											u.*

								FROM 		groupMemberTable gm

								-- find groups user is in
								INNER JOIN 	groupTable g
								ON             gm.groupID = g.groupID

								-- get all groupID for all users and user name
								INNER JOIN 	user_name_groupID_vw u
								ON             g.groupID = u.groupID

								WHERE 		gm.userID = "'.$this->sanitize($where).'"

								';
								break;
				case 'admin-members-in-group':
								$select  =
								'
								SELECT DISTINCT	g.groupName,
											g.groupID,
											u.*

								FROM 		groupMemberTable gm

								-- find groups user is in
								INNER JOIN 	groupTable g
								ON             gm.groupID = g.groupID

								-- get all groupID for all users and user name
								INNER JOIN 	user_name_groupID_vw u
								ON             g.groupID = u.groupID
								';
								break;
				// get all groups not used


				// get all of the group names for a user
				case 'groups-user-in':
								$select  =
								'
								SELECT 		g.groupName,
											g.groupID

								FROM 		groupMemberTable gm

								LEFT JOIN 	groupTable g
								ON 			gm.groupID = g.groupID

								WHERE 	gm.userID = "'.$this->sanitize($where).'"
								'
								;
								break;
				case 'admin-groups':
								$select  =
								'
								SELECT DISTINCT		g.groupName,
											g.groupID

								FROM 		groupMemberTable gm

								LEFT JOIN 	groupTable g
								ON 			gm.groupID = g.groupID

								'
								;
								break;
				// find all comments for groups user is in
				case 'groups-comments':
								$select =
								'
								SELECT 		g.groupName,
											g.groupID,
											ct.commentID,
											ct.comment,
											ct.commentType


								FROM 		groupMemberTable gm

								LEFT JOIN 	groupTable g
								ON 			gm.groupID = g.groupID

								LEFT JOIN	commentTable ct
								ON			gm.groupID = ct.commentRef

								WHERE 	gm.userID = "'.$this->sanitize($where).'"
								';
								break;

				// find if a box exists
				case 'box-boxName':
								$select  = 'SELECT boxID, boxName ';
								$select .= 'FROM boxTable ';
								$select .= 'WHERE boxName = "';
								$select .= $this->sanitize($where).'"';

								break;

				// Find if box already in loc
				case 'box-loc':
					$select =
					'
					SELECT 	freezerID,
							rackNum,
							shelfNum,
							boxName,
							boxID

					FROM 	boxTable

					WHERE 	freezerID ="'.$this->sanitize($where['freezerID']).'"

					AND 		rackNum = "'.$this->sanitize($where['rackNum']).'"

					AND shelfNum =
					"'.$this->sanitize($where['numShelf']).'"

					AND rackSpaceNum =
					"'.$this->sanitize($where['rackSpaceNum']).'"
					'
					;

					break;

				// find boxes available to user
				case 'box-user':
								$select =
								'
								SELECT    pub.boxID,
								          pub.boxName,
								          pub.privacyLevel,
								          "" AS reqRequired,
								          "" AS groupName,
								          pub.freezerName,
								          pub.roomNum,
								          pub.shelfNum,
								          pub.rackNum,
								          pub.xSize,
								          pub.ySize,
								          pub.rackSpaceNum,
										pub.emptySpaces

								FROM      public_boxes_vw pub

								UNION

								SELECT    priv.boxID,
								          priv.boxName,
								          priv.privacyLevel,
								          "" AS reqRequired,
								          "" AS groupName,
								          priv.freezerName,
								          priv.roomNum,
								          priv.shelfNum,
								          priv.rackNum,
								          priv.xSize,
								          priv.ySize,
								          priv.rackSpaceNum,
										priv.emptySpaces


								FROM      private_boxes_vw priv

								WHERE     priv.userID ="'.$this->sanitize($where).'"

								UNION

								SELECT    gr.boxID,
								          gr.boxName,
								          gr.privacyLevel,
								          gr.reqRequired,
								          gr.groupName,
								          gr.freezerName,
								          gr.roomNum,
								          gr.shelfNum,
								          gr.rackNum,
								          gr.xSize,
								          gr.ySize,
								          gr.rackSpaceNum,
										gr.emptySpaces


								FROM      group_boxes_vw gr

								WHERE     gr.groupID IN  (
								                              SELECT    groupID
								                              FROM      groupMemberTable
								                              WHERE     userID ="'.$this->sanitize($where).'"
								                         )

								';

								break;

				case 'admin-all-boxes':
					$select =
					'
					SELECT    pub.boxID,
								          pub.boxName,
								          pub.privacyLevel,
								          "" AS reqRequired,
								          "" AS groupName,
								          pub.freezerName,
								          pub.roomNum,
								          pub.shelfNum,
								          pub.rackNum,
								          pub.xSize,
								          pub.ySize,
								          pub.rackSpaceNum,
										pub.emptySpaces

								FROM      public_boxes_vw pub

								UNION

								SELECT    priv.boxID,
								          priv.boxName,
								          priv.privacyLevel,
								          "" AS reqRequired,
								          "" AS groupName,
								          priv.freezerName,
								          priv.roomNum,
								          priv.shelfNum,
								          priv.rackNum,
								          priv.xSize,
								          priv.ySize,
								          priv.rackSpaceNum,
										priv.emptySpaces


								FROM      private_boxes_vw priv


								UNION

								SELECT    gr.boxID,
								          gr.boxName,
								          gr.privacyLevel,
								          gr.reqRequired,
								          gr.groupName,
								          gr.freezerName,
								          gr.roomNum,
								          gr.shelfNum,
								          gr.rackNum,
								          gr.xSize,
								          gr.ySize,
								          gr.rackSpaceNum,
										gr.emptySpaces


								FROM      group_boxes_vw gr

								WHERE     gr.groupID IN  (
								                              SELECT    groupID
								                              FROM      groupMemberTable

								                         )

					';
					break;


				// find if freezer exists
				case 'freezer-freezerName':
					$select = 'SELECT freezerID, freezerName ';
					$select .= 'FROM freezerTable ';
					$select .= 'WHERE freezerName = "';
					$select .= $this->sanitize($where).'"';
					break;


				// Get user Name
				case 'user-info';
					$select = 'SELECT firstName, lastName ';
					$select .= 'FROM userTable ';
					$select .= 'WHERE userID = "';
					$select .= $this->sanitize($where).'" ';
					break;

				// get all sample type vars for a tube ID
				case 'sample-type-vars':
					$select =
					'
					SElECT		stv.variableName,
								stv.variableValue,
								stv.sampleTypeVariableID

					FROM 		sampleTypeVariables stv

					WHERE		stv.tubeID = "'.$this->sanitize($where).'"
					';
					break;

				// get all unique numbers for a user
				case 'unique-nums':
					$select =
					'
					SELECT 		*

					FROM			uniqueTable

					WHERE		userID = 	"'.$this->sanitize($where).'"
					'
					;
					break;

				case 'search-tubes':
					$searchTerm = $this->sanitize($where['search_term']);
					// $searchTerm = str_replace(' ', '%', $searchTerm);
					// $searchTerm = str_replace('-', '%', $searchTerm);

					$userID = $this->sanitize($where['userID']);

					$select =
					'

					SELECT    COUNT(tubeID) AS "tubeCount",
							tubeID,
							spaceID,
							boxID,
							sampleName,
							sampleType,
							freezeDate,
							frozeBy,
							intialNumTubes,
							boxName

					FROM (

							SELECT DISTINCT     tiv.tubeID,
											tiv.spaceID,
											tiv.boxID,
											tiv.sampleName,
											tiv.sampleType,
											tiv.freezeDate,
											tiv.frozeBy,
											tiv.intialNumTubes,
											b.boxName

							FROM           tube_info_vw tiv

							LEFT JOIN      sampleTypeVariables stv
							ON             tiv.tubeID = stv.tubeID

							LEFT JOIN      commentTable ct
							ON             tiv.tubeID = ct.commentRef
							AND            ct.commentType = "tube"


							INNER JOIN     boxTable b ON tiv.boxID = b.boxID

							WHERE          tiv.boxID IN   (
														SELECT    pub.boxID

														FROM      public_boxes_vw pub

														UNION

														SELECT    priv.boxID

														FROM      private_boxes_vw priv

														WHERE     priv.userID = '.$userID.'

														UNION

														SELECT    gr.boxID

														FROM      group_boxes_vw gr

														WHERE     gr.groupID IN  (
																				SELECT    groupID
																				FROM      groupMemberTable
																				WHERE     userID = '.$userID.'
																			)
													)

										AND
										(
											stv.variableValue LIKE ("%'.$searchTerm.'%")
											OR

											ct.comment LIKE ("%'.$searchTerm.'%")
											OR

											tiv.sampleName LIKE "%'.$searchTerm.'%"
											OR

											tiv.sampleType LIKE "%'.$searchTerm.'%"
											OR

											tiv.freezeDate LIKE "%'.$searchTerm.'%"
											OR

											tiv.experimentName LIKE "%'.$searchTerm.'%"
											OR
											b.boxName LIKE "%'.$searchTerm.'%"
										)

						)  results
					GROUP BY tubeID
					'
					;

					break;

					case 'admin-search-tubes':
						$searchTerm = $this->sanitize($where['search_term']);
						// $searchTerm = str_replace(' ', '%', $searchTerm);
						// $searchTerm = str_replace('-', '%', $searchTerm);

						$select =
						'

						SELECT    COUNT(tubeID) AS "tubeCount",
								tubeID,
								spaceID,
								boxID,
								sampleName,
								sampleType,
								freezeDate,
								frozeBy,
								intialNumTubes,
								boxName

						FROM (

								SELECT DISTINCT     tiv.tubeID,
												tiv.spaceID,
												tiv.boxID,
												tiv.sampleName,
												tiv.sampleType,
												tiv.freezeDate,
												tiv.frozeBy,
												tiv.intialNumTubes,
												b.boxName

								FROM           tube_info_vw tiv

								LEFT JOIN      sampleTypeVariables stv
								ON             tiv.tubeID = stv.tubeID

								LEFT JOIN      commentTable ct
								ON             tiv.tubeID = ct.commentRef
								AND            ct.commentType = "tube"


								INNER JOIN     boxTable b ON tiv.boxID = b.boxID

								WHERE          tiv.boxID IN   (
															SELECT    pub.boxID

															FROM      public_boxes_vw pub

															UNION

															SELECT    priv.boxID

															FROM      private_boxes_vw priv

															UNION

															SELECT    gr.boxID

															FROM      group_boxes_vw gr

															WHERE     gr.groupID IN  (
																					SELECT    groupID
																					FROM      groupMemberTable
																				)
														)

											AND
											(
												stv.variableValue LIKE ("%'.$searchTerm.'%")
												OR

												ct.comment LIKE ("%'.$searchTerm.'%")
												OR

												tiv.sampleName LIKE "%'.$searchTerm.'%"
												OR

												tiv.sampleType LIKE "%'.$searchTerm.'%"
												OR

												tiv.freezeDate LIKE "%'.$searchTerm.'%"
												OR

												tiv.experimentName LIKE "%'.$searchTerm.'%"
												OR
												b.boxName LIKE "%'.$searchTerm.'%"
											)

							)  results
						GROUP BY tubeID
						'
						;

						break;
				// get all requests which have not been filled
				case 'outstanding-requests':
					$select =
					'
					SELECT 	*

					FROM 	requestTable

					WHERE	requestFilled = 0
					'
					;
					break;

				// get all requests which have not been filled but are approved
				case 'outstanding-approved-requests':
								$select =
								'
								SELECT 	*

								FROM 	requestTable

								WHERE	requestFilled = 0

								and		approved = 1
								'
								;
								break;

				//  get request log information that equals the reqID
				case 'requestRemoveLog-reqID':
				 				$select =
								'
								SELECT 		rrl.*,
											CONCAT(ut.firstName, " ", ut.lastName) AS fullName


								FROM 		requestRemoveLog rrl

								LEFT JOIN		userTable ut
								ON			rrl.userID = ut.userID

								WHERE		rrl.reqID = "'.$this->sanitize($where).'"
								'
								;
								break;

				// find all requests already filled
				case 'filled-requests':
								$select =
								'
								SELECT 	*

								FROM 	requestTable

								WHERE	requestFilled = 1
								'
								;
								break;


				case 'space-info':
							$select =
							'
							SELECT 		*

							FROM 		spaceTable

							WHERE		tubeID = "'.$this->sanitize($where).'"
							';
							break;

				case 'login-attempts':
							$select =
							'
							SELECT 	COUNT(*) AS count

							FROM 	loginAttemps

							WHERE 	(creationTime > now() - INTERVAL 10 MINUTE)
							AND emailAddress = "'.$this->sanitize($where).'"
							'
							;
							break;

				case 'consent-info':
							$select =
							'
							SELECT 	*

							FROM 	consentTable
							'
							;
							break;


			}


			if(isset($select))
			{

				$results = array();

				// perform query
				$searchResult = mysqli_query($this->conn, $select);

				if(!$searchResult)
				{
					return false;
				}

				// make results array
				else
				{

					while($row = mysqli_fetch_array($searchResult))
					{
						$results[] = $row;
					}

					return $results;
				}
			}
		}

		public function ExportCSV($query, $where=NULL)
		{
			switch($query)
			{
				case 'box-info-csv':

					$select =
					'

					SELECT DISTINCT     tt.sampleName,
									tt.sampleType,
									tt.freezeDate,
									tt.experimentName,
									tt.labBook,
									tt.pageNum,
									tt.published,
									tt.mta,
									tt.color,
									tt.frozeBy,
									tt.intialNumTubes,
									CASE WHEN tt.ConsentYesNo = 0 THEN "No" ELSE "Yes" END AS ConsentYesNo,
									tt.locConsent,
									tt.consentNotes,
									CASE tt.openAccess WHEN 0 THEN "Opt Out" WHEN 1 THEN "Opt In" WHEN 2 THEN "Question Not Asked" END AS openAccess,
									it.irbProjectTitle,
									it.irbPI,
									it.irbNum,
									CONCAT(ut.firstName," ",ut.lastName) AS userName,
									bt.boxName,
									bt.privacyLevel,
									bt.rackNum,
									bt.shelfNum,
									bt.rackSpaceNum,
									gt.groupName,
									gt.reqRequired,
									MAX(
										CASE WHEN stv.variableName = "Total_Number_per_million_cells"
										THEN stv.variableValue ELSE NULL END
									) AS Total_Number_per_million_cells,
									MAX(
										CASE WHEN stv.variableName = "Expiration_Date"
										THEN stv.variableValue ELSE NULL END
									) AS Expiration_Date,
									MAX(
										CASE WHEN stv.variableName = "Species"
										THEN stv.variableValue ELSE NULL END
									) AS Species,
									MAX(
										CASE WHEN stv.variableName = "Genetic_Manipulation"
										THEN stv.variableValue ELSE NULL END
									) AS Genetic_Manipulation,
									MAX(
										CASE WHEN stv.variableName = "Vector"
										THEN stv.variableValue ELSE NULL END
									) AS Vector,
									MAX(
										CASE WHEN stv.variableName = "Growing_Conditions"
										THEN stv.variableValue ELSE NULL END
									) AS Growing_Conditions,
									MAX(
										CASE WHEN stv.variableName = "collection_time"
										THEN stv.variableValue ELSE NULL END
									) AS collection_time,
									MAX(
										CASE WHEN stv.variableName = "freeze_time"
										THEN stv.variableValue ELSE NULL END
									) AS freeze_time,
									MAX(
										CASE WHEN stv.variableName = "Protein"
										THEN stv.variableValue ELSE NULL END
									) AS Protein,
									MAX(
										CASE WHEN stv.variableName = "Isotype"
										THEN stv.variableValue ELSE NULL END
									) AS Isotype,
									MAX(
										CASE WHEN stv.variableName = "Clone"
										THEN stv.variableValue ELSE NULL END
									) AS Clone,
									MAX(
										CASE WHEN stv.variableName = "Tube_Color"
										THEN stv.variableValue ELSE NULL END
									) AS Tube_Color,
									MAX(
										CASE WHEN stv.variableName = "Total_Vol"
										THEN stv.variableValue ELSE NULL END
									) AS Total_Vol,
									MAX(
										CASE WHEN stv.variableName = "Unit_Of_Measure_Total"
										THEN stv.variableValue ELSE NULL END
									) AS Unit_Of_Measure_Total,
									MAX(
										CASE WHEN stv.variableName = "Quantiy_Per_Tube"
										THEN stv.variableValue ELSE NULL END
									) AS Quantiy_Per_Tube,
									MAX(
										CASE WHEN stv.variableName = "concentration"
										THEN stv.variableValue ELSE NULL END
									) AS concentration,
									MAX(
										CASE WHEN stv.variableName = "Unit_Of_Measure_Concentration"
										THEN stv.variableValue ELSE NULL END
									) AS Unit_Of_Measure_Concentration,
									MAX(
										CASE WHEN stv.variableName = "Tissue"
										THEN stv.variableValue ELSE NULL END
									) AS Tissue


					FROM	     tubeTable tt

					LEFT JOIN	userTable ut
					ON		ut.userID = tt.userID

					LEFT JOIN irbTable it
					ON        it.irbID = tt.irbID

					LEFT JOIN spaceTable st
					ON        st.tubeID = tt.tubeID

					LEFT JOIN boxTable bt
					ON        st.boxID = bt.boxID

					LEFT JOIN groupTable gt
					ON        gt.groupId = bt.groupID

					LEFT JOIN sampleTypeVariables stv
					ON        tt.tubeID = stv.tubeID

					WHERE     tt.tubeID
					IN

							(
								SELECT DISTINCT	tubeID
								FROM 			tube_info_vw
								WHERE 			boxID ="'.$this->sanitize($where).'"
							)

					GROUP BY stv.tubeID
					';
					break;
			}

			if(isset($select))
			{
				$results = array();

				// perform query
				$searchResult = mysqli_query($this->conn, $select);

				if (!$searchResult) die(mysqli_error($conn));

				// Add search results
				$search_result = array();

				while ($row = mysqli_fetch_row($searchResult))
				{
					$search_result[] = $row;
				}

				$results['search_result'] = $search_result;

				// reset mysqli_fetch_row data pointer
				mysqli_data_seek($searchResult, 0);

				// Find number of fields in result
				$num_fields = mysqli_num_fields($searchResult);

				// Make header
				$headers = array();

				for ($i = 0; $i < $num_fields; $i++)
				{
				       $headers[] = $this->mysqli_field_name($searchResult , $i);
				}

				$results['headers'] = $headers;

				// Get box Name
				$box_name = mysqli_fetch_array($searchResult)['boxName'];

				$results['box_name'] = $box_name;

				return $results;
			}
		}

		public function ExportGridCSV($query, $where=NULL)
		{
			switch($query)
			{

				case 'box-grid':
					$select =
					'
					SELECT         	ti.sampleName,
									ti.xLoc,
									ti.yLoc,
									bt.boxName,
									bt.xSize

					FROM           tube_info_vw ti

					LEFT JOIN 		boxTable bt
					ON				ti.boxID = bt.boxID

					WHERE          ti.boxID = "'.$this->sanitize($where).'"
					ORDER BY ti.xLoc, ti.yLoc
					';
					break;
			}
			if(isset($select))
			{
				// perform query
				$searchResult = mysqli_query($this->conn, $select);

				if(!$searchResult)
				{
					return false;
				}

				// make grid array
				else
				{
					$search_array = array();
					$results = array();
					$i = 0;
					while ($row = mysqli_fetch_array($searchResult))
					{
						if ($i == 0)
						{
							$box_size = (int)$row['xSize'];
							$results['box_size'] = $box_size;
							$box_name = $row['boxName'];
							$results['box_name'] = $box_name;

						}

						$i++;

						$search_array[] = $row;
					}

					// make an empty square matrix of the size of the box
					$square_matrix = array();
					$alphabet = range('A', 'Z');

					for($i=0; $i < $box_size+1; $i++)
					{
						// if $i is 0 change index make the first row list of numbers the size of the box
						if ($i === 0)
						{
							$curr_array = range(0,$box_size);
						}
						// Make empty array the size of the size of the box
						else
						{
							$curr_array = array_fill(0, $box_size+1, 0);
							$curr_array[0] = $alphabet[$i-1];

							for ($k=0; $k<sizeof($search_array); $k++)
							{
								$curr_samp = $search_array[$k];

								if ($curr_samp['xLoc'] == $i)
								{
									$curr_array[$curr_samp['yLoc']] = $curr_samp['sampleName'];
								}
							}
						}
						$square_matrix[$i] = $curr_array;
					}
					// add grid to results
					$results['grid'] = $square_matrix;

					return $results;
				}
			}
		}


		public function get($table, $id)
		{
/*
			$count = 0;
			$results = array();

			$clean_query = $this->sanit

								SELECT    COUNT(tubeID) AS "tubeCount",
										tubeID,
										spaceID,
										boxID,
										sampleName,
										sampleType,
										freezeDate,
										frozeBy,
										intialNumTubes,
										boxName

								FROM (

										SELECT DISTINCT     tiv.tubeID,
														tiv.spaceID,
														tiv.boxID,
														tiv.sampleName,
														tiv.sampleType,
														tiv.freezeDate,
														tiv.frozeBy,
														tiv.intialNumTubes,
														b.boxName

										FROM           tube_info_vw tiv

										LEFT JOIN      sampleTypeVariables stv
										ON             tiv.tubeID = stv.tubeID

										LEFT JOIN      commentTable ct
										ON             tiv.tubeID = ct.commentRef
										AND            ct.commentType = "tube"


										INNER JOIN     boxTable b ON tiv.boxID = b.boxID

										WHERE          stv.variableValue LIKE ("%'.$searchTerm.'%")
										OR             ct.comment LIKE ("%'.$searchTerm.'%")

										OR            (
														tiv.sampleName LIKE "%'.$searchTerm.'%"

														OR

														tiv.sampleType LIKE "%'.$searchTerm.'%"

														OR

														tiv.freezeDate LIKE "%'.$searchTerm.'%"


														OR

														tiv.experimentName LIKE "%'.$searchTerm.'%"

														OR

														b.boxName LIKE "%'.$searchTerm.'%"
													)
										AND             tiv.boxID IN   (
																	SELECT    pub.boxID

																	FROM      public_boxes_vw pub

																	UNION

																	SELECT    priv.boxID

																	FROM      private_boxes_vw priv

																	WHERE     priv.userID = '.$userID.'

																	UNION

																	SELECT    gr.boxID

																	FROM      group_boxes_vw gr

																	WHERE     gr.groupID IN  (
																																								SELECT    groupID
																																								FROM      groupMemberTable
																																								WHERE     userID = '.$userID.'
																						)
																)

									)  results
								GROUP BY tubeID
								'
								;
print_r($select);
								break;

				// get all requests which have not been filled
				case 'outstanding-requests':
								$select =
								'
								SELECT 	*

								FROM 	requestTable

								WHERE	requestFilled = 0
								'
								;
								break;

				// get all requests which have not been filled but are approved
				case 'outstanding-approved-requests':
								$select =
								'
								SELECT 	*

								FROM 	requestTable

								WHERE	requestFilled = 0

								and		approved = 1
								'
								;
								break;

				//  get request log information that equals the reqID
				case 'requestRemoveLog-reqID':
				 				$select =
								'
								SELECT 		rrl.*,
											CONCAT(ut.firstName, " ", ut.lastName) AS fullName


								FROM 		requestRemoveLog rrl

								LEFT JOIN		userTable ut
								ON			rrl.userID = ut.userID

								WHERE		rrl.reqID = "'.$this->sanitize($where).'"
								'
								;
								break;

				// find all requests already filled
				case 'filled-requests':
								$select =
								'
								SELECT 	*

								FROM 	requestTable

								WHERE	requestFilled = 1
								'
								;
								break;


				case 'space-info':
							$select =
							'
							SELECT 		*

							FROM 		spaceTable

							WHERE		tubeID = "'.$this->sanitize($where).'"
							';
							break;

				case 'login-attempts':
							$select =
							'
							SELECT 	COUNT(*) AS count

							FROM 	loginAttemps

							WHERE 	(creationTime > now() - INTERVAL 10 MINUTE)
							AND emailAddress = "'.$this->sanitize($where).'"
							'
							;
							break;

				case 'consent-info':
							$select =
							'
							SELECT 	*

							FROM 	consentTable
							'
							;
							break;

			}


			if(isset($select))
			{

				$results = array();

				// perform query
				$searchResult = mysqli_query($this->conn, $select);

				if(!$searchResult)
				{
					return false;
				}

				// make results array
				else
				{

					while($row = mysqli_fetch_array($searchResult))
					{
						$results[] = $row;
					}

					return $results;
				}
			}
		}

		public function get($table, $id)
		{
/*
			$count = 0;
			$results = array();

			$clean_query = $this->sanitize($query);

			$searchResult = mysqli_query($conn, $clean_query);

			if(!$searchResult)
			{
				return false;

								SELECT    COUNT(tubeID) AS "tubeCount",
										tubeID,
										spaceID,
										boxID,
										sampleName,
										sampleType,
										freezeDate,
										frozeBy,
										intialNumTubes,
										boxName

								FROM (

										SELECT DISTINCT     tiv.tubeID,
														tiv.spaceID,
														tiv.boxID,
														tiv.sampleName,
														tiv.sampleType,
														tiv.freezeDate,
														tiv.frozeBy,
														tiv.intialNumTubes,
														b.boxName

										FROM           tube_info_vw tiv

										LEFT JOIN      sampleTypeVariables stv
										ON             tiv.tubeID = stv.tubeID

										LEFT JOIN      commentTable ct
										ON             tiv.tubeID = ct.commentRef
										AND            ct.commentType = "tube"


										INNER JOIN     boxTable b ON tiv.boxID = b.boxID

										WHERE          stv.variableValue LIKE ("%'.$searchTerm.'%")
										OR             ct.comment LIKE ("%'.$searchTerm.'%")

										OR            (
														tiv.sampleName LIKE "%'.$searchTerm.'%"

														OR

														tiv.sampleType LIKE "%'.$searchTerm.'%"

														OR

														tiv.freezeDate LIKE "%'.$searchTerm.'%"


														OR

														tiv.experimentName LIKE "%'.$searchTerm.'%"

														OR

														b.boxName LIKE "%'.$searchTerm.'%"
													)
										AND             tiv.boxID IN   (
																	SELECT    pub.boxID

																	FROM      public_boxes_vw pub

																	UNION

																	SELECT    priv.boxID

																	FROM      private_boxes_vw priv

																	WHERE     priv.userID = '.$userID.'

																	UNION

																	SELECT    gr.boxID

																	FROM      group_boxes_vw gr

																	WHERE     gr.groupID IN  (
																																								SELECT    groupID
																																								FROM      groupMemberTable
																																								WHERE     userID = '.$userID.'
																						)
																)

									)  results
								GROUP BY tubeID
								'
								;
print_r($select);
								break;

				// get all requests which have not been filled
				case 'outstanding-requests':
								$select =
								'
								SELECT 	*

								FROM 	requestTable

								WHERE	requestFilled = 0
								'
								;
								break;

				// get all requests which have not been filled but are approved
				case 'outstanding-approved-requests':
								$select =
								'
								SELECT 	*

								FROM 	requestTable

								WHERE	requestFilled = 0

								and		approved = 1
								'
								;
								break;

				//  get request log information that equals the reqID
				case 'requestRemoveLog-reqID':
				 				$select =
								'
								SELECT 		rrl.*,
											CONCAT(ut.firstName, " ", ut.lastName) AS fullName


								FROM 		requestRemoveLog rrl

								LEFT JOIN		userTable ut
								ON			rrl.userID = ut.userID

								WHERE		rrl.reqID = "'.$this->sanitize($where).'"
								'
								;
								break;

				// find all requests already filled
				case 'filled-requests':
								$select =
								'
								SELECT 	*

								FROM 	requestTable

								WHERE	requestFilled = 1
								'
								;
								break;


				case 'space-info':
							$select =
							'
							SELECT 		*

							FROM 		spaceTable

							WHERE		tubeID = "'.$this->sanitize($where).'"
							';
							break;

				case 'login-attempts':
							$select =
							'
							SELECT 	COUNT(*) AS count

							FROM 	loginAttemps

							WHERE 	(creationTime > now() - INTERVAL 10 MINUTE)
							AND emailAddress = "'.$this->sanitize($where).'"
							'
							;
							break;

				case 'consent-info':
							$select =
							'
							SELECT 	*

							FROM 	consentTable
							'
							;
							break;

			}


			if(isset($select))
			{

				$results = array();

				// perform query
				$searchResult = mysqli_query($this->conn, $select);

				if(!$searchResult)
				{
					return false;
				}

				// make results array
				else
				{

					while($row = mysqli_fetch_array($searchResult))
					{
						$results[] = $row;
					}

					return $results;
				}
			}
		}

		public function get($table, $id)
		{
/*
			$count = 0;
			$results = array();

			$clean_query = $this->sanitize($query);

			$searchResult = mysqli_query($conn, $clean_query);

			if(!$searchResult)
			{
				return false;y($conn, $clean_query);

			if(!$searchResult)
			{
				return false;
			}

			else
			{
				$row = mysqli_fetch_array($searchResult);

				return $row;
			}
*/
			switch($table)
			{
				// get a freezer
				case 'freezer':
							$select =
							'
							SELECT 	*

							FROM 	freezerTable

							WHERE 	freezerID = '.intval($id);
							break;

				case 'irb':
							$select =
							'
							SELECT 	*

							FROM 	irbTable

							WHERE 	irbID = '.intval($id);
							break;
				case 'box':
							$select =
							'
							SELECT 		*

							FROM 		boxTable bt

							LEFT JOIN 	freezerTable ft

							USING 		(freezerID)

							WHERE 		bt.boxID='.intval($id);
							break;

				case 'group':

							$select =
							'
							SELECT 		g.groupName,
										g.groupID,
										g.reqRequired

							FROM 		groupTable g

							WHERE 		g.groupID ="'.$this->sanitize($id).'"
							'
							;
							break;

				case 'member':
							$select =
							'
							SELECT 		*

							FROM 		groupMemberTable

							WHERE 		groupID = "'.$this->sanitize($id).'"
							'
							;
							break;

				case 'request':
							$select =
							'
							SELECT 		*

							FROM 		requestTable

							WHERE		reqID = "'.$this->sanitize($id).'"
							'
							;
							break;
				case 'tube-info':
							$select =
							'
							SELECT 		*

							FROM 		tube_info_vw

							WHERE		tubeID = "'.$this->sanitize($id).'"
							'
							;
							break;

				case 'sampleVars-info':
							$select =
							'
							SELECT 		*

							FROM 		sampleTypeVariables

							WHERE		tubeID = "'.$this->sanitize($id).'"
							'
							;
							break;

			}

			if(isset($select))
			{
				$results = array();

				$searchResult = mysqli_query($this->conn, $select);

				if(!$searchResult)
				{
					return false;
				}

				else
				{
					while($row = mysqli_fetch_array($searchResult))
					{
						$results[] = $row;

					}

					return $results[0];
				}
			}

		}

		public function login($inputs)
		{
			$emailAddress = $inputs['emailAddress'];
			$password = md5($inputs['password']);

			$select =
			'
			SELECT 	*

			FROM 	userTable

			WHERE 	emailAddress = "'.$this->sanitize($emailAddress).'"
			AND 		password = "'.$password.'"
			';

			$result = array();

			$searchResult = mysqli_query($this->conn, $select);

			if($searchResult->num_rows == 0)
			{
				return false;
			}

			else
			{
				// !!!!!!!!!!!!!Why aren't all cols in the user table present here??????
				$row = mysqli_fetch_array($searchResult);

				// !!!!!!!!!!!NEED TO CHECK OF ACCOUNT IS STATUS LOCKED BEFORE ADDING COOKIE
				return $this->setLoginCookie($row);

			}
/*
			else
			{
				// Add a login attempt to loginAttemps table
				$newAttempt =  array();
				$newAttempt['ipAddress'] = $this->findIp();
				$newAttempt['emailAddress'] = $inputs['emailAddress'];

				$new_attempt_results = $this->addOrModifyRecord('loginAttemps', $newAttempt);



				// find out how many loggins have happened in the last 5 minutes from this ip address
				$num_attempts = $this->('login-attempts',$newAttempt['emailAddress']);
				// if num of attemps is greater than 5 lock the account
				// how to do this?  since I'm using ipAddress. Do I just look to see if the email address is in the database? and set that to locked status
				if ($num_attempts[0]['count'] >= 5)
				{
					$loggedAttempts = array();
					$loggedAttempts['logged_in'] = false;
					$loggedAttempts['attemptsCount'] = $num_attempts[0]['count'];

					$user_info = $this->('user-info',$newAttempt['emailAddress']);
					$user_info[0]['status'] = 'locked';

					$updated_user = $user_info[0];
					$updated_user['status'] = 'locked';

					$userLocked = $this->addOrModifyRecord('userTable', $updated_user);

					return $loggedAttempts;
				}

				// return an array of including logged_in = false and attempts
				else
				{
					$loggedAttempts = array();
					$loggedAttempts['logged_in'] = false;
					$loggedAttempts['attemptsCount'] = $num_attempts[0]['count'];

					return $loggedAttempts;
				}

			}
*/

		}


		public function setLoginCookie($user_info, $create = true)
		{
			if($create)
			{
				session_start();

					$_SESSION['user'] = $user_info;

				session_write_close();
			}

			else
			{

				$_SESSION = array();

				if(ini_get('session.use_cookies'))
				{
					$params = session_get_cookie_params();
					setcookie(session_name(), '', time() - 42000, $params['path'], $params['domain'],$params['secure'], $params['httponly']);
				}

				session_destroy();
			}
/*
			foreach($user_info as $name => $value)
			{
				// creates cookie to login
			     if($create)
			     {
                         $time = time() + 3600;
			     }

				// kills cookie for logging out
			     else
			     {
			          $time = time() - 3600;
			     }

				setcookie('user["'.$name.'"]', $value , $time);
			}

*/
		     return true;
		}

		public function findDuplicate()
		{
			$searchQuery = 'SELECT '.$this->colName.' FROM '.$this->tableName.' WHERE '.$this->colName.' = "'.$this->postInput[$this->colName].'";';

			$searchResult = mysqli_num_rows(mysqli_query($this->conn, $searchQuery));
			return $searchResult;
		}

		private function sanitize($dirty_string)
		{
			$no_ticks_string = str_replace("`","'",$dirty_string);
			$clean_string = mysqli_real_escape_string($this->conn, $no_ticks_string);

			return $clean_string;
		}

		private function findIp()
		{

			$ip_address = '';
			if (getenv('HTTP_CLIENT_IP'))
			{
			    $ip_address = getenv('HTTP_CLIENT_IP');
		    	}
			else if(getenv('HTTP_X_FORWARDED_FOR'))
			{
			    $ip_address = getenv('HTTP_X_FORWARDED_FOR');
		    	}
			else if(getenv('HTTP_X_FORWARDED'))
			{
			    $ip_address = getenv('HTTP_X_FORWARDED');
		    	}
			else if(getenv('HTTP_FORWARDED_FOR'))
			{
			    $ip_address = getenv('HTTP_FORWARDED_FOR');
		    	}
			else if(getenv('HTTP_FORWARDED'))
			{
			   $ip_address = getenv('HTTP_FORWARDED');
		   	}
			else if(getenv('REMOTE_ADDR'))
			{
			    $ip_address = getenv('REMOTE_ADDR');
		    	}
			else
			{
			    $ip_address = 'UNKNOWN';
		    	}

			return $ip_address;
		}
		function mysqli_field_name($result, $field_offset)
		{
		    $properties = mysqli_fetch_field_direct($result, $field_offset);
		    return is_object($properties) ? $properties->name : null;
		}



	}

?>
