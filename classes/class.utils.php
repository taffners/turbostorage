<?php

	class Utils
	{
		public function DoubleAscSort($row_1_nam, $row_2_nam, $in_array)
		{
			// sort an array of arrays
			foreach ($in_array as $key => $row)
			{
			    $row_1[$key] = $row[$row_1_nam];
			    $row_2[$key] = $row[$row_2_nam];
			}

			array_multisort($row_1, SORT_ASC, $row_2, SORT_ASC, $in_array);

			return $in_array;
		}

		public function TimeDayGreeting()
		{
			// () -> str
			// purpose: get random greeting

			$hour = date('H');

			/* If the time is less than 1200 hours, show good morning */
			if ($hour < '12')
			{
				$time = 'Good morning';
			}

			/* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
			elseif ($hour >= '12' && $hour < '17')
			{
				$time = 'Good afternoon';
			}

			/* Should the time be between or equal to 1700 and 1900 hours, show good evening */
			elseif ($hour >= '17' && $hour < '19')
			{
			   $time = 'Good evening';
			}

			/* Finally, show good night if the time is greater than or equal to 1900 hours */
			elseif ($hour >= '19')
			{
				$time = 'Good night';
			}

			// pick one of the possible greetings below.
			$poss_greetings = array('Welcome to Turbo Storage', 'Howdy', 'Hello there,', 'Bonjour', 'Hello! Welcome to Turbo Storage', 'Hi there', 'Nice to see you', $time);
			$i = rand(0, count($poss_greetings)-1);
			$greeting = $poss_greetings[$i];

			return $greeting;
		}


		public function PickRandomFileFromFolder($folder_address)
		{
			// (str) -> str
			// Purpose: pick a random file from a folder.  Use this to pick a a random img from a folder of img

			// get all files in folder removing hidden folders and files
			$files = array_diff(scandir($folder_address), array('.', '..'));

			// pick random number starting from the first index in array
			$i = rand(array_keys($files)[0], count($files)-1);

			// provide random address
			return $folder_address.'/'.$files[$i];
		}

     }
?>
