define(['models/tube_search', 'models/box_funcs'], function(tube_search, box_funcs)
{
     function _RunTests()
     {
          // Purpose: Test functions in the tube_seach_module Module
          tube_search.Search('10396', 'insert_search_results_here','clone_row',  'tube_change');
          tube_search.Search('cell', 'insert_search_results_here','clone_row',  'home');
          require(['views/events/RedirectEvents'], function(RedirectEvents)
          {
               RedirectEvents.StartEvents();

          });

     }

     return {
          RunTests:_RunTests
     }
});
