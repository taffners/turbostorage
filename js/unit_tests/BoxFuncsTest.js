define(['models/box_funcs', 'models/date_funcs'], function(box_funcs, date_funcs)
{
     function _RunTests()
     {
          // Purpose: Test functions in the tube_seach_module Module



          QUnit.test('Box Funcs Test: BuildZeroSquareMatix', function(assert)
          {
               var  _size = 2,
                    _test_val = box_funcs.BuildZeroSquareMatix(_size),
                    _expected = [[0,0],[0,0]];

               assert.deepEqual(_test_val, _expected, 'A square zero matrix is returned');
          });


          QUnit.test('Box Funcs Test: InsertIntoMatrix', function(assert)
          {
               var  _x_key = 'xLoc',
                    _y_key = 'yLoc',
                    _empty_matrix = box_funcs.BuildZeroSquareMatix(3),
                    _input_data =
                         [
                              {
                                   'xLoc':1,
                                   'yLoc':2,
                                   'insertData':'Data'
                              },
                              {
                                   'xLoc':2,
                                   'yLoc':1,
                                   'insertData':'Data'
                              }
                         ]
                    _test_val = box_funcs.InsertIntoMatrix(_x_key, _y_key, _empty_matrix, _input_data),
                    _expected = [[0,0,0],[0,0,
                         {
                              "insertData": "Data",
                              "xLoc": 1,
                              "yLoc": 2
                         }],
                         [0,
                         {
                              "insertData": "Data",
                              "xLoc": 2,
                              "yLoc": 1
                         },0]];

               assert.deepEqual(_test_val, _expected, 'Add data to matrix');
          });

          //
          // box_funcs.MakeTubeInfoTable(32, {
          //      'button_status': 'on',
          //      'on_buttons': ['dup-tube-button', 'update-tube-button', 'delete-tube-info-table']
          // });

          // QUnit.test('Box Funcs Test: AddBoxGridToHTML', function(assert)
          // {
          //      var  _box_size = 10,
          //           _box_id = 12,
          //           _expected;
          //
          //      // run test function
          //      box_funcs.AddBoxGridToHTML(_box_size, _box_id);
          //
          //      // Test that
          //
          //
          //
          //
          //      // Test that a header is added
          // });

     }

     return {
          RunTests:_RunTests
     }
});
