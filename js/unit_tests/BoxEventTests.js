define(['models/utils'], function(utils)
{
     function _RunTests(page_action)
     {
          // Purpose: Test functions in the Utils Module

          var  xSize,
               ySize;


          if (page_action === 'add')
          {
               // ONly test these if page has no data already present.  Meaning not updating
               QUnit.test('Box Events Tests', function(assert)
               {
                    // Test that when typing in xsize ysize equals xsize
                    $('#xSize').val(9).trigger('keyup');

                    ySize = $('#ySize').val();

                    assert.equal(ySize, '9', 'Test that when typing in xsize ysize equals xsize');

                    // Reset values
                    $('#xSize').val('');
                    $('#xSize').val('');

                    // Test that when typing in xsize ysize equals xsize
                    $('#ySize').val(9).trigger('keyup');

                    xSize = $('#xSize').val();

                    assert.equal(xSize, '9', 'Test that when typing in ysize xsize equals xsize');

                    // Reset values
                    $('#xSize').val('');
                    $('#xSize').val('');

                    // Test changing privacy level to group which is under a change event
                    $('#privacyLevel').val('Group').trigger('change');

                    // Test to see if group_div class is show
                    assert.equal($('#group_div').attr('class'), 'form-group row show', 'Test changing privacy level to group which is under a change event');
               });
          }

          // Test update only actions
          else if (page_action == 'update')
          {
               QUnit.test('Box Events Tests', function(assert)
               {
                    // check that boxName is greyed out
                    assert.ok($('#boxName').is('[readonly]'),'test if boxName is readonly');

               });

          }


     }

     return {
          RunTests:_RunTests
     }
});
