define(function()
{
     function _RunLoginTests(php_vars)
     {
          // test Utils
          require(['unit_tests/UtilsTest'], function(UtilsTest)
          {
               UtilsTest.RunUtilsTests();
          });

          // test turbo_toggle
          require(['unit_tests/TurboToggleTest'], function(TurboToggleTest)
          {
               TurboToggleTest.RunTurboToggleTests(php_vars);
          });
     }

     return {
          RunLoginTests:_RunLoginTests
     }
})
