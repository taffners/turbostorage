define(['models/table_funcs'], function(table_funcs)
{
     function _RunTests()
     {
          // Purpose: Test functions in the table_funcs module

          QUnit.test('Table Funcs Test: PasteStrData', function(assert)
          {

               var _str_pieces =
                         [
                              '?page=tube_form&action=updateTube&tubeID=',
                              '&boxID=',
                              '&boxName='
                         ],
                    _data_keys = ['tubeID','boxID','boxName'],
                    _input_data =
                         {
                              boxID: "12",
                              boxName:"test box",
                              tubeID:"20"
                         },
                    _test_val = table_funcs.PasteStrData(_str_pieces, _input_data, _data_keys),
                    _expected = '?page=tube_form&action=updateTube&tubeID=20&boxID=12&boxName=test_box';

               assert.equal(_test_val, _expected, 'check the str is pasted together correctly');
          });

          // var  _clone_cell_id = 'clone_grid_thead_column',
          //      _insert_id = 'grid_thead_row',
          //      _input_data = {boxID:"12",boxName:"test box",color:"#BCE5AF",consent_approval_date:"0000-00-00",consent_notes:"",consent_yes_no:"0",experimentName:"",freezeDate:"0000-00-00",frozeBy:"samantha",groupID:null,intialNumTubes:"2",labBook:"",loc_consent:"",mta:"0",pageNum:"0",privacyLevel:"Private",published:"0",reqRequired:null,sampleName:"test",sampleType:"",spaceID:"163",tubeID:"24",userID:"5",xLoc:"1",yLoc:"1"},
          //      _test_val = table_funcs.AddTableCloneCell(_clone_cell_id, _insert_id, _input_data)
          //      _expected = '?page=tube_form&action=updateTube&tubeID=20&boxID=12&boxName=test_box';

          // // test no data to add
          // var  _clone_cell_id = 'clone_grid_thead_column',
          //      _insert_id = 'grid_thead_row',
          //      _input_data = {r:'0',c:'3',sampleName:'testData'}
          //      _test_val = table_funcs.AddTableCloneCell(_clone_cell_id, _insert_id, _input_data)
          //      _expected = '?page=tube_form&action=updateTube&tubeID=20&boxID=12&boxName=test_box';
     }

     return {
          RunTests:_RunTests
     }
});
