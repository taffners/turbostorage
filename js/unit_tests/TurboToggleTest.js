define(['models/turbo_toggle'], function(turbo_toggle)
{
     function _RunTurboToggleTests(php_vars)
     {
          // Purpose: Test functions in the Utils Module

          var  _curr_val,
               _returned,
               _expected;

          if (php_vars['page'] == 'login')
          {
               // Test security_questions
               QUnit.test('turbo toggle Test: security questions', function(assert)
               {
                    // find out if selection of question 1 val 1 disables question 2 val 2
                    $('#securityQuestion').val('1').trigger('change');

                    assert.ok($('#securityQuestion2 option[value="1"]').is(':disabled'), 'Clicking on question 1 val 1 disables question 2 val 2');

                    // find out if selection of question 2 val 2 disables question 1 val 2
                    $('#securityQuestion2').val('2').trigger('change');

                    assert.ok($('#securityQuestion option[value="2"]').is(':disabled'), 'find out if selection of question 2 val 2 disables question 1 val 2');

                    // select question 1 val 3 find out if question 2 val 1 is enabled. find out if enabling of previosly disabled value works
                    $('#securityQuestion1').val('3').trigger('change');

                    assert.ok(!$('#securityQuestion option[value="1"]').is(':disabled'), 'Enabling of previosly disabled value works');

               });

               // Test HideShowType
               QUnit.test('turbo toggle Test: Hide Show Type', function(assert)
               {
                    // check if the input #password_create changes type to text
                    turbo_toggle.HideShowType('#password_create', '#show_password_create');

                    assert.equal($('#password_create').attr('type'), 'text', 'check if the input #password_create changes type to text');

                    // check if the type switches back to password
                    turbo_toggle.HideShowType('#password_create', '#show_password_create');

                    assert.equal($('#password_create').attr('type'), 'password', 'check if the type switches back to password');

               });
          }
          else if (php_vars['page'] == 'tube_form' && php_vars['action'] == 'add' && php_vars['status'] != 'dup')
          {
               // test toggle sample type variables
               QUnit.test('Turbo toggle test:  Toggle sample type fields', function(assert)
               {
                    _returned = turbo_toggle.FindSampleTypeGroup('Antibody');
                    _expected = 'Group1';
                    assert.equal(_returned, _expected, 'Check Antibody Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('Bacterial_Cultures');
                    _expected = 'Group2';
                    assert.equal(_returned, _expected, 'Check Bacterial_Cultures Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('Blood');
                    _expected = 'Group4';
                    assert.equal(_returned, _expected, 'Check Blood Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('Body_fluids');
                    _expected = 'Group4';
                    assert.equal(_returned, _expected, 'Check Body_fluids Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('Bone');
                    _expected = 'Group4';
                    assert.equal(_returned, _expected, 'Check Bone Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('Cell_Lines');
                    _expected = 'Group3';
                    assert.equal(_returned, _expected, 'Check Cell_Lines Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('Cell_Pellet');
                    _expected = 'Group10';
                    assert.equal(_returned, _expected, 'Check Cell_Pellet Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('Chemical');
                    _expected = 'Group5';
                    assert.equal(_returned, _expected, 'Check Chemical Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('Cytokine');
                    _expected = 'Group6';
                    assert.equal(_returned, _expected, 'Check Cytokine Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('DNA');
                    _expected = 'Group7';
                    assert.equal(_returned, _expected, 'Check DNA Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('PBMC');
                    _expected = 'Group9';
                    assert.equal(_returned, _expected, 'Check Bacterial_Cultures Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('Plasma');
                    _expected = 'Group4';
                    assert.equal(_returned, _expected, 'Check Plasma Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('RNA');
                    _expected = 'Group7';
                    assert.equal(_returned, _expected, 'Check RNA Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('Serum');
                    _expected = 'Group4';
                    assert.equal(_returned, _expected, 'Check Serum Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('Urine');
                    _expected = 'Group4';
                    assert.equal(_returned, _expected, 'Check Urine Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('Virus');
                    _expected = 'Group8';
                    assert.equal(_returned, _expected, 'Check Virus Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('Whole_Tissue');
                    _expected = 'Group4';
                    assert.equal(_returned, _expected, 'Check Whole_Tissue Group #');

                    _returned = turbo_toggle.FindSampleTypeGroup('Primary_Cells');
                    _expected = 'Group11';
                    assert.equal(_returned, _expected, 'Check Primary_Cells Group #');

               });

               // Test hiding all of the sample type variables groups
               QUnit.test('Turbo Toggle Test: Test hiding all sample type groups', function(assert)
               {
                    // show the PBMC group 9
                    $('#Group9').attr('class', 'show');

                    // test the class for group 9 after running hide function
                    turbo_toggle.HideAllSampleTypeGroups();

                    _returned = $('#Group9').attr('class');
                    _expected = 'hide';

                    assert.equal(_returned, _expected, 'Check if group 9 is hidden');

                    // Show multiple groups
                    $('#Group9').attr('class', 'show');
                    $('#Group1').attr('class', 'show');

                    turbo_toggle.HideAllSampleTypeGroups();

                    var  _is_same;

                    _is_same = $('#Group9').attr('class') == $('#Group1').attr('class');

                    if (_is_same)
                    {
                         _returned = $('#Group9').attr('class');
                    }
                    else
                    {
                         _returned = 'not same';
                    }

                    _expected = 'hide';

                    assert.equal(_returned, _expected, 'multiple group hide');


               });

          }


     }

     return {
          RunTurboToggleTests:_RunTurboToggleTests
     }
})
