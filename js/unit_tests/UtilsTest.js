define(['models/utils'], function(utils)
{
     function _RunUtilsTests()
     {
          // Purpose: Test functions in the Utils Module

          var  _non_empty_ids =
               {
                    'firstName':'Fill out your first name.',
                    'lastName':'Fill out your last name.'
               };

          // Test Check If Inputs are Empty
          QUnit.test('Utils Test: Check If Inputs are Empty', function(assert)
          {
               // check if it picks up the first one being empty
               assert.equal(utils.CheckIfInputsEmpty(_non_empty_ids), 'firstName', 'check if it picks up the first one being empty');

               // set firstName to test. and check if it will pick up the last name to be empty
               $('#firstName').val('test');

               assert.equal(utils.CheckIfInputsEmpty(_non_empty_ids), 'lastName', 'check if it picks up the first one being empty');

               // set lastName to test and check if passed will be returned
               $('#lastName').val('test');

               assert.equal(utils.CheckIfInputsEmpty(_non_empty_ids), 'passed', 'check if it picks up the first one being empty');

          });

          // Test Validate Email
          QUnit.test('Utils Test: Check Validate Email', function(assert)
          {
               // Test a valid email address
               assert.ok(utils.ValidateEmail('taffners@gmail.com'), 'Test a valid email address');

               // Test an foreign email address
               assert.ok(utils.ValidateEmail('taffners@gmail.ca'), 'Test a valid foriegn email address');

               assert.ok(utils.ValidateEmail('firstname.lastname@domain.com'), 'Email contains dot in the address field');

               assert.ok(utils.ValidateEmail('email@subdomain.domain.com'), 'Email contains dot with subdomain');

               assert.ok(utils.ValidateEmail('firstname+lastname@domain.com'), 'Plus sign is considered valid character');

               assert.ok(utils.ValidateEmail('email@[123.123.123.123]'), '	Square bracket around IP address is considered valid');

               assert.ok(utils.ValidateEmail('“email”@domain.com'), 'Quotes around email is considered valid');

               assert.ok(utils.ValidateEmail('1234567890@domain.com'), 'Digits in address are valid');

               assert.ok(utils.ValidateEmail('email@domain-one.com'), 'Dash in domain name is valid');

               assert.ok(utils.ValidateEmail('_______@domain.com'), '	Underscore in the address field is valid');

               assert.ok(utils.ValidateEmail('email@domain.name'), '.name is valid Top Level Domain name');

               assert.ok(utils.ValidateEmail('email@domain.co.jp'), 'Dot in Top Level Domain name also considered valid (use co.jp as example here)');

               assert.ok(utils.ValidateEmail('firstname-lastname@domain.com'), 'Dash in address field is valid');

               // Not valid email addresses
               assert.ok(!utils.ValidateEmail('email@123.123.123.123'), 'Domain is valid IP address');

               assert.ok(!utils.ValidateEmail('taffnersgmail.com'), '	Missing @');

               assert.ok(!utils.ValidateEmail('taffners@gmailcom'), '	Missing top level domain (.com/.net/.org/etc)');

               assert.ok(!utils.ValidateEmail('plainaddress'), '	Missing @ sign and domain');

               assert.ok(!utils.ValidateEmail('#@%^%#$@#$@#.com'), 'Garbage');

               assert.ok(!utils.ValidateEmail('@domain.com'), 'Missing username');

               assert.ok(!utils.ValidateEmail('Joe Smith <email@domain.com>'), 'Encoded html within email is invalid');

               assert.ok(!utils.ValidateEmail('email@domain@domain.com'), 'Two @ sign');

               assert.ok(!utils.ValidateEmail('.email@domain.com'), '	Leading dot in address is not allowed');

               assert.ok(!utils.ValidateEmail('email.@domain.com'), '	Trailing dot in address is not allowed');

               assert.ok(!utils.ValidateEmail('email..email@domain.com'), 'Multiple dots');

               assert.ok(!utils.ValidateEmail('email@domain.com (Joe Smith)'), 'Text followed email is not allowed');

               assert.ok(!utils.ValidateEmail('email@domain..com'), '	Multiple dot in the domain portion is invalid');
          });

          // Test Validate only ints
          QUnit.test('Utils Test: Check Validate only ints', function(assert)
          {
               // vaild only ints
               assert.ok(utils.ValidateOnlyInts('12345678911'), '11 only ints');

               assert.ok(!utils.ValidateOnlyInts('12345a8b11'), 'letters included');

               assert.ok(!utils.ValidateOnlyInts('1!@#$11'), 'special chars included');
          });

          // test Validate Only Int PhoneNum
          QUnit.test('Utils Test: Check Validate Only Int PhoneNum', function(assert)
          {
               // vaild only ints
               assert.ok(utils.ValidateOnlyIntPhoneNum('12345678911'), '11 only ints');

               assert.ok(!utils.ValidateOnlyIntPhoneNum('1234567891'), 'Not enough ints');

               assert.ok(!utils.ValidateOnlyIntPhoneNum('12345a8b11'), 'letters included');

               assert.ok(!utils.ValidateOnlyIntPhoneNum('1!@#$11'), 'special chars included');
          });

          QUnit.test('Utils New line to HTML', function(assert)
          {
               // \r\n|\r|\n
               assert.equal(utils.NewLineToHTML('1st line\r\nsecond line'), '1st line<br>second line', 'test \r\n');
               assert.equal(utils.NewLineToHTML('1st line\rsecond line'), '1st line<br>second line', 'test \r');
               assert.equal(utils.NewLineToHTML('1st line\nsecond line'), '1st line<br>second line', 'test \n');
               assert.equal(utils.NewLineToHTML('1st line second line'), '1st line second line', 'test no new line');
          });

          QUnit.test('Utils Replace Caplital letters with dash & lower case ', function(assert)
          {
               assert.equal(utils.ReplaceCapWithDash('firstSecond'), 'first-second', 'test one cap');

               assert.equal(utils.ReplaceCapWithDash('tubeID'), 'tube-i-d', 'test two cap');
          });

          QUnit.test('Utils Find a word in a str and cut it at the str', function(assert)
          {
               assert.equal(utils.FindWordInStrAndCut('pop-up-accordion group_num_20', 'group_num_'), '20', 'test1');

               assert.equal(utils.FindWordInStrAndCut('pop-up-accordion group_num_20', 'pop-up-'), 'accordion', 'test2');
          });
     }

     return {
          RunUtilsTests:_RunUtilsTests
     }
});
