define(function()
{
     function _BindEvents()
     {
          require(['views/events/ChangePasswordEvents'], function(ChangePasswordEvents)
          {
               ChangePasswordEvents.StartEvents();

          });
     }

     function _RunTests()
     {

     }

     function _start(test_status, php_vars)
     {
          _BindEvents();

          // finally load all tests if testing is on
          if (test_status === 'testing_on')
          {
               // Show unit test results
               $('#qunit_testing').toggleClass('show hide');

               _RunTests();
          }
     }

     return {
          start:_start
     };
});
