define(function()
{
     function _BindEvents(php_vars)
     {
          // add an event to all the comment buttons to open the comment modal.
          require(['views/events/AddCommentEvents'], function(AddCommentEvents)
          {
               AddCommentEvents.StartEvents();
          });

          // Add searching for tubes while pressing enter  to an input which has a class of .searchPressEnter, if the button has an id of searchTubes search for the database for a tube containing the value of the input TubeSearchTerm.  Also add to the input with id TubeSearchTerm an autocompletion of sample Names after 2 chars are entered
          require(['views/events/SearchEvents'], function(SearchEvents)
          {
               SearchEvents.StartEvents(php_vars);
          });

          // Add event to the table row of the table listing all boxes which addes the grid showing the inside of the box.
          require(['views/events/AddBoxEvents'], function(AddBoxEvents)
          {
               AddBoxEvents.StartEvents(php_vars);
          });

          // Add all the events related to the tube info table and adding a tube to an empty space in the box.
          require(['views/events/CellEvents'], function(CellEvents)
          {
               CellEvents.StartEvents(php_vars);
          });

          require(['views/events/PrintEvents'], function(PrintEvents)
          {
               PrintEvents.StartEvents(php_vars);
          });

          require(['views/events/ToolTipEvents'], function(ToolTipEvents)
          {
               ToolTipEvents.StartEvents();
          });

          require(['views/events/ModalEvents'], function(ModalEvents)
          {
               ModalEvents.StartEvents();
          });

          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          // Used to filter the box list table.
          require(['views/events/TableBoxFilterEvents'], function(TableBoxFilterEvents)
          {
               TableBoxFilterEvents.StartEvents();
          });

          require(['views/events/RedirectEvents'], function(RedirectEvents)
          {
               RedirectEvents.StartEvents();

          });
     }

     function _RunTests()
     {
          // test tube Search
          require(['unit_tests/TubeSearchTest'], function(TubeSearchTest)
          {
               TubeSearchTest.RunTests();
          });

          // test Table Funcs
          require(['unit_tests/TableFuncsTest'], function(TableFuncsTest)
          {
               TableFuncsTest.RunTests();
          });

          // test Box Funcs
          require(['unit_tests/BoxFuncsTest'], function(BoxFuncsTest)
          {
               BoxFuncsTest.RunTests();
          });
     }

     function _start(test_status, php_vars)
     {
          _BindEvents(php_vars);

          // finally load all tests if testing is on
          if (test_status === 'testing_on')
          {
               // Show unit test results
               $('#qunit_testing').toggleClass('show hide');

               _RunTests();
          }
     }

     return {
          start:_start
     };
});
