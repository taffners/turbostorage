define(function()
{
     function _BindEvents(php_vars)
     {
          require(['views/events/SearchEvents'], function(SearchEvents)
          {
               SearchEvents.StartEvents(php_vars);
          });

          require(['views/events/RedirectEvents'], function(RedirectEvents)
          {
               RedirectEvents.StartEvents();

          });

          require(['views/events/AddCommentEvents'], function(AddCommentEvents)
          {
               AddCommentEvents.StartEvents();

          });
     }

     function _RunTests()
     {

     }

     function _start(test_status, php_vars)
     {
          _BindEvents(php_vars);

          // finally load all tests if testing is on
          if (test_status === 'testing_on')
          {
               // Show unit test results
               $('#qunit_testing').toggleClass('show hide');

               _RunTests();
          }
     }

     return {
          start:_start
     };
});
