define(function()
{
     function _BindEvents()
     {
          require(['views/events/RedirectEvents'], function(RedirectEvents)
          {
               RedirectEvents.StartEvents();

          });

          require(['views/events/AddCommentEvents'], function(AddCommentEvents)
          {
               AddCommentEvents.StartEvents();

          });

          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          require(['views/events/ToolTipEvents'], function(ToolTipEvents)
          {
               ToolTipEvents.StartEvents();
          });

          // Used to filter the box list table.
          require(['views/events/TableBoxFilterEvents'], function(TableBoxFilterEvents)
          {
               TableBoxFilterEvents.StartEvents();
          });
     }

     function _RunTests()
     {

     }

     function _start(test_status, php_vars)
     {
          _BindEvents();

          // finally load all tests if testing is on
          if (test_status === 'testing_on')
          {
               // Show unit test results
               $('#qunit_testing').toggleClass('show hide');

               _RunTests();
          }
     }

     return {
          start:_start
     };
});
