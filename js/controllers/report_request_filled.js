define(function()
{
     function _BindEvents(php_vars)
     {
          require(['views/events/ReportRequestFilledEvents'], function(ReportRequestFilledEvents)
          {
               ReportRequestFilledEvents.StartEvents(php_vars);
          });

          require(['views/events/ToolTipEvents'], function(ToolTipEvents)
          {
               ToolTipEvents.StartEvents();
          });

          require(['views/events/RedirectEvents'], function(RedirectEvents)
          {
               RedirectEvents.StartEvents();

          });

          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });
     }

     function _start(test_status, php_vars)
     {
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
