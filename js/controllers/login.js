define(['models/utils'],function(utils)
{
     function _BindEvents()
     {
          require(['views/events/LoginEvents'], function(LoginEvents)
          {
               LoginEvents.StartEvents();
          });
     }

     function _RunTests(php_vars)
     {
          // test Utils
          require(['unit_tests/LoginTests'], function(LoginTests)
          {
               LoginTests.RunLoginTests(php_vars);
          });
     }

     function _start(test_status, php_vars)
     {
          // Check if the browser is approved
          if (utils.CheckBrowser())
          {
               _BindEvents();

               // finally load all tests if testing is on
               if (test_status === 'testing_on')
               {
                    // Show unit test results
                    $('#qunit_testing').toggleClass('show hide');

                    _RunTests(php_vars);
               }
          }
     }

     return {
          start:_start
     };
});
