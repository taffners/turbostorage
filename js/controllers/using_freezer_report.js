define(function()
{
     function _BindEvents()
     {
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });
     }

     function _RunTests()
     {

     }

     function _start(test_status, php_vars)
     {
          _BindEvents();
     }

     return {
          start:_start
     };
});
