define(['views/events/TubeFormEvents', 'models/utils', 'models/turbo_toggle'], function(TubeFormEvents, utils, turbo_toggle)
{
     function _BindEvents(php_vars, test_status)
     {
          // Add events toggle sample type variables, show irbs available, and submit tube form
          TubeFormEvents.StartEvents(php_vars);

          // Add event to the table row of the table listing all boxes which addes the grid showing the inside of the box.
          require(['views/events/AddBoxEvents'], function(AddBoxEvents)
          {
               AddBoxEvents.StartEvents(php_vars);
          });

          // Add all the events related to the tube info table and adding a tube to an empty space in the box.
          require(['views/events/CellEvents'], function(CellEvents)
          {
               CellEvents.StartEvents(php_vars);
          });

          // Add an event to all buttons to add todays date.
          require(['views/events/DateEvents'], function(DateEvents)
          {
               DateEvents.StartEvents(php_vars);
          });

          // add an event to all the comment buttons to open the comment modal.
          require(['views/events/AddCommentEvents'], function(AddCommentEvents)
          {
               AddCommentEvents.StartEvents();

          });

          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          // finally load all tests if testing is on
          if (test_status === 'testing_on')
          {
               // Show unit test results
               $('#qunit_testing').toggleClass('show hide');

               _RunTests(php_vars);
          }

          // Used to filter the box list table.
          require(['views/events/TableBoxFilterEvents'], function(TableBoxFilterEvents)
          {
               TableBoxFilterEvents.StartEvents();
          });
     }

     function _RunTests(php_vars)
     {
          // test turbo_toggle
          require(['unit_tests/TurboToggleTest'], function(TurboToggleTest)
          {
               TurboToggleTest.RunTurboToggleTests(php_vars);
          });
     }

     function _SetView(php_vars, action_status)
     {
          // Change the view if the page is for updating
          var  _sample_type = $('#sampleType').val(),
               _tube_id = php_vars['tubeID'] || 'broken',
               _group_num = turbo_toggle.FindSampleTypeGroup(_sample_type);

          require(['views/change_form_view'], function(change_form_view)
          {
               change_form_view.Render(php_vars, action_status);

               // disable sample type select list if action is to update.  Because of the way the database is set up updating this is difficult
               if (action_status === 'update')
               {
                    change_form_view.UpdateTubeLayout(_sample_type);
               }

               if (_tube_id != 'broken')
               {

                    change_form_view.AddSampleVariables(_tube_id, _group_num);
               }
               else if (action_status != 'dup')
               {
                    utils.dialog_window('A tube id is not present.  Do not proceed.  Something is wrong');
               }
          });
     }

     function _SetPageAction(php_vars)
     {
          // Purpose: Set the action status of the page.  Default is add.  If action is set to update the view will be altered to have more restrictions

          if ((php_vars.hasOwnProperty('action') && php_vars['action'] === 'updateTube') )
          {
               _SetView(php_vars, 'update');
          }
          else if (php_vars.hasOwnProperty('action') && php_vars['action'] === 'add' && php_vars.hasOwnProperty('status') && php_vars['status'] === 'dup')
          {
               // get tube id and add it to php_vars
               _SetView(php_vars, 'dup');
          }
     }

     function _start(test_status, php_vars)
     {
          _BindEvents(php_vars, test_status);

          _SetPageAction(php_vars);

          // set color the drop down menu what color is your tube to the last tube selected
          if(localStorage.getItem('LAST_COLOR_SELECTION') !== null && 'action' in php_vars && php_vars['action'] == 'add')
          {
               // set current last color to the color stored in local storage
               $('select#tube_color').val(localStorage.LAST_COLOR_SELECTION).css('background-color', localStorage.LAST_COLOR_SELECTION);
          }

          // if the localStorage var does not exist set it to white
          else if ('action' in php_vars && php_vars['action'] == 'add')
          {
               // set a default color of white
               localStorage.setItem('LAST_COLOR_SELECTION', 'white');
          }

          // if this is an update set background color to color
          else if ('action' in php_vars && php_vars['action'] != 'add')
          {
               var  _color = $('select#tube_color').val();

               // set background color
               $('select#tube_color').css('background-color', _color);
          }

     }

     return {
          start:_start
     };
});
