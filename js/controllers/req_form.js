define(function()
{
     function _BindEvents(php_vars, test_status)
     {
          require(['views/events/ReqFormEvents'], function(ReqFormEvents)
          {
               ReqFormEvents.StartEvents();
          });

          // add todays date
          require(['views/events/DateEvents'], function(DateEvents)
          {
               DateEvents.StartEvents(php_vars);
          });

          require(['views/events/AddCommentEvents'], function(AddCommentEvents)
          {
               AddCommentEvents.StartEvents();

          });
     }

     function _RunTests()
     {

     }

     function _ChangeView()
     {
          $(':input').attr('readonly','readonly');

          $('#CreateRequestSubmit').remove();
     }

     function _start(test_status, php_vars)
     {
          _BindEvents(php_vars, test_status);

          // change form to view only
          if (php_vars['action'] == 'view')
          {
               _ChangeView();
          }

     }

     return {
          start:_start
     };
});
