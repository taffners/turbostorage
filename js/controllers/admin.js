define(['models/utils'],function(utils)
{
     function _BindEvents(php_vars)
     {
          $(':checkbox').on('change', function()
          {
               // Purpose: Every time a value is checked in the admin permissions update value in database.  Id is set up likeadd id for each td is input_userRoleID_userID_role_ID.

               var  _input_ids = $(this)
                         .attr('id')
                         .replace('input_', '')
                         .split('_'),
                    _db_task = $(this).is(":checked") ? 'add': 'delete';

               //change value in database
               $.ajax(
               {
                    type:'POST',
                    url: 'utils/ChangePermissions.php?',

                    dataType: 'text',
                    data: {'input_ids': _input_ids, 'user_id':php_vars['userID'], 'db_task': _db_task},
                    success: function(response)
                    {


                    },
                    error: function(textStatus, errorThrown)
                    {
                         dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus, '#permissions-legend');
                    }
               });
          });


          // hide all menus
          $('#add-nav-drop-down').attr('class', 'hide');
          $('#update-nav-drop-down').attr('class', 'hide');
          $('#remove-nav-drop-down').attr('class', 'hide');
          $('#reports-nav-drop-down').attr('class', 'hide');

          // add an event to all the comment buttons to open the comment modal.
          require(['views/events/AddCommentEvents'], function(AddCommentEvents)
          {
               AddCommentEvents.StartEvents();
          });

          // Add searching for tubes while pressing enter  to an input which has a class of .searchPressEnter, if the button has an id of searchTubes search for the database for a tube containing the value of the input TubeSearchTerm.  Also add to the input with id TubeSearchTerm an autocompletion of sample Names after 2 chars are entered
          require(['views/events/SearchEvents'], function(SearchEvents)
          {
               SearchEvents.StartEvents(php_vars);
          });

          // Add event to the table row of the table listing all boxes which addes the grid showing the inside of the box.
          require(['views/events/AddBoxEvents'], function(AddBoxEvents)
          {
               AddBoxEvents.StartEvents(php_vars);
          });

          // Add all the events related to the tube info table and adding a tube to an empty space in the box.
          require(['views/events/CellEvents'], function(CellEvents)
          {
               CellEvents.StartEvents(php_vars);
          });

          require(['views/events/PrintEvents'], function(PrintEvents)
          {
               PrintEvents.StartEvents(php_vars);
          });

          require(['views/events/ToolTipEvents'], function(ToolTipEvents)
          {
               ToolTipEvents.StartEvents();
          });

          require(['views/events/ModalEvents'], function(ModalEvents)
          {
               ModalEvents.StartEvents();
          });

          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          // Used to filter the box list table.
          require(['views/events/TableBoxFilterEvents'], function(TableBoxFilterEvents)
          {
               TableBoxFilterEvents.StartEvents();
          });

          require(['views/events/RedirectEvents'], function(RedirectEvents)
          {
               RedirectEvents.StartEvents();

          });
     }

     function _start(test_status, php_vars)
     {
          _BindEvents(php_vars);

     }

     return {
          start:_start
     };
});
