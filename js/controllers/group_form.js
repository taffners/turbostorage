define(function()
{
     function _BindEvents(php_vars, test_status)
     {
          require(['views/events/GroupFormEvents'], function(GroupFormEvents)
          {
               GroupFormEvents.StartEvents(php_vars);
          });

          require(['views/events/AddCommentEvents'], function(AddCommentEvents)
          {
               AddCommentEvents.StartEvents();

          });

          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });
     }

     function _RunTests()
     {

     }

     function _SetView(php_vars)
     {
          // Change the view if the page is for updating

          require(['views/change_form_view'], function(change_form_view)
          {
               change_form_view.Render(php_vars, php_vars['action']);
          });

     }

     function _start(test_status, php_vars)
     {
          _BindEvents(php_vars, test_status);

          if (php_vars['action'] === 'update')
          {
               _SetView(php_vars);
          }

     }

     return {
          start:_start
     };
});
