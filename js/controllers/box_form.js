define(['views/events/BoxFormEvents', 'views/events/AddCommentEvents'], function(BoxFormEvents, AddCommentEvents)
{
     var  _page_action = 'add';

     function _BindEvents(php_vars, test_status)
     {
          BoxFormEvents.StartEvents(php_vars);

          // add an event to all the comment buttons to open the comment modal.
          AddCommentEvents.StartEvents();

          // finally load all tests if testing is on
          if (test_status === 'testing_on')
          {
               // Show unit test results
               $('#qunit_testing').toggleClass('show hide');

               _RunTests();
          }

          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });
     }

     function _RunTests()
     {
          // test box events
          require(['unit_tests/BoxEventTests'], function(BoxEventTests)
          {
               BoxEventTests.RunTests(_page_action);
          });
     }

     function _SetView(php_vars)
     {
          // Change the view if the page is for updating

          require(['views/change_form_view'], function(change_form_view)
          {
               change_form_view.Render(php_vars, php_vars['action']);
          });

     }

     function _SetPageAction(php_vars)
     {
          // Purpose: Set the action status of the page.  Default is add.  If action is set to update the view will be altered to have more restrictions

          if (php_vars.hasOwnProperty('action') && php_vars['action'] === 'update')
          {
               _page_action = 'update';
          }
          else
          {
               _page_action = 'add';
          }
     }

     function _start(test_status, php_vars)
     {
          _BindEvents(php_vars, test_status);

          _SetPageAction(php_vars);

          if (_page_action === 'update')
          {
               _SetView(php_vars);
          }
     }

     return {
          start:_start
     };
});
