define(['models/utils'],function(utils)
{
     function _BindEvents(php_vars)
     {
          // CSV Buttons are run directly by php GetBoxInfoCSV.php & GetGridCSV.php in utils

          require(['views/events/AddCommentEvents'], function(AddCommentEvents)
          {
               AddCommentEvents.StartEvents();
          });

          // show save all button
          $('#save-all-box-info').attr('class', 'btn btn-primary btn-lg show');



          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          require(['views/events/ToolTipEvents'], function(ToolTipEvents)
          {
               ToolTipEvents.StartEvents();
          });

          // Used to filter the box list table.
          require(['views/events/TableBoxFilterEvents'], function(TableBoxFilterEvents)
          {
               TableBoxFilterEvents.StartEvents();
          });

          // Add event to the table row of the table listing all boxes which addes the grid showing the inside of the box.
          require(['views/events/AddBoxEvents'], function(AddBoxEvents)
          {
               AddBoxEvents.StartEvents(php_vars);
          });

          // Add all the events related to the tube info table and adding a tube to an empty space in the box.
          require(['views/events/CellEvents'], function(CellEvents)
          {
               CellEvents.StartEvents(php_vars);
          });

          require(['views/events/RedirectEvents'], function(RedirectEvents)
          {
               RedirectEvents.StartEvents();

          });
     }

     function _start(test_status, php_vars)
     {
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
