define(['views/events/TubeFormEvents', 'models/utils', 'models/turbo_toggle'], function(TubeFormEvents, utils, turbo_toggle)
{
     function _BindEvents(php_vars, test_status)
     {
          // 
          require(['views/events/RemoveTubeFormEvents'], function(RemoveTubeFormEvents)
          {
               RemoveTubeFormEvents.StartEvents();
          });

          // Add event to the table row of the table listing all boxes which addes the grid showing the inside of the box.
          require(['views/events/AddBoxEvents'], function(AddBoxEvents)
          {
               AddBoxEvents.StartEvents(php_vars);
          });

          // Add all the events related to the tube info table and adding a tube to an empty space in the box.
          require(['views/events/CellEvents'], function(CellEvents)
          {
               CellEvents.StartEvents(php_vars);
          });

          // add an event to all the comment buttons to open the comment modal.
          require(['views/events/AddCommentEvents'], function(AddCommentEvents)
          {
               AddCommentEvents.StartEvents();
          });

          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          // Add searching for tubes while pressing enter  to an input which has a class of .searchPressEnter, if the button has an id of searchTubes search for the database for a tube containing the value of the input TubeSearchTerm.  Also add to the input with id TubeSearchTerm an autocompletion of sample Names after 2 chars are entered
          require(['views/events/SearchEvents'], function(SearchEvents)
          {
               SearchEvents.StartEvents(php_vars);
          });

          // During removal of a tube and the table row of a table with id addedRequestsList is clicked add the id of the clicked request to clicked-request-id
          require(['views/events/RequestEvents'], function(RequestEvents)
          {
               RequestEvents.StartEvents(php_vars);
          });
     }

     function _RunTests(php_vars)
     {
          // test turbo_toggle
          require(['unit_tests/TurboToggleTest'], function(TurboToggleTest)
          {
               TurboToggleTest.RunTurboToggleTests(php_vars);
          });
     }

     function _SetView(php_vars, action_status)
     {
          // Change the view if the page is for updating
          var  _sample_type = $('#sampleType').val(),
               _tube_id = php_vars['tubeID'] || 'broken',
               _group_num = turbo_toggle.FindSampleTypeGroup(_sample_type);

          require(['views/change_form_view'], function(change_form_view)
          {
               change_form_view.Render(php_vars, action_status);

               // disable sample type select list if action is to update.  Because of the way the database is set up updating this is difficult
               if (action_status === 'update')
               {
                    change_form_view.UpdateTubeLayout(_sample_type);
               }

               if (_tube_id != 'broken')
               {

                    change_form_view.AddSampleVariables(_tube_id, _group_num);
               }
               else if (action_status != 'dup')
               {
                    utils.dialog_window('A tube id is not present.  Do not proceed.  Something is wrong');
               }
          });
     }

     function _SetPageAction(php_vars)
     {
          // Purpose: Set the action status of the page.  Default is add.  If action is set to update the view will be altered to have more restrictions

          if ((php_vars.hasOwnProperty('action') && php_vars['action'] === 'updateTube') )
          {
               _SetView(php_vars, 'update');
          }
          else if (php_vars.hasOwnProperty('action') && php_vars['action'] === 'add' && php_vars.hasOwnProperty('status') && php_vars['status'] === 'dup')
          {
               // get tube id and add it to php_vars
               _SetView(php_vars, 'dup');
          }
     }

     function _start(test_status, php_vars)
     {
          _BindEvents(php_vars, test_status);

          _SetPageAction(php_vars);
     }

     return {
          start:_start
     };
});
