define(function()
{
     function _BindEvents()
     {

          require(['views/events/irbFormEvents'], function(irbFormEvents)
          {
               irbFormEvents.StartEvents();

          });
     }

     function _start(test_status, php_vars)
     {
          _BindEvents();


     }

     return {
          start:_start
     };
});
