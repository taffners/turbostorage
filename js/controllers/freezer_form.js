define(function()
{
     function _BindEvents(php_vars)
     {
          require(['views/events/FreezerFormEvents'], function(FreezerFormEvents)
          {
               FreezerFormEvents.StartEvents(php_vars);

          });

          require(['views/events/AddCommentEvents'], function(AddCommentEvents)
          {
               AddCommentEvents.StartEvents();

          });

          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });
     }

     function _RunTests()
     {

     }

     function _SetView(php_vars)
     {
          // Change the view if the form is for updating
          if (php_vars.hasOwnProperty('action') && php_vars['action'] === 'update')
          {
               // change updating to true so the check to see if freezer name is present is removed
               require(['views/change_form_view'], function(change_form_view)
               {
                    change_form_view.Render(php_vars, php_vars['action']);
               });
          }
     }

     function _start(test_status, php_vars)
     {
          _BindEvents(php_vars);

          _SetView(php_vars);

          // finally load all tests if testing is on
          if (test_status === 'testing_on')
          {
               // Show unit test results
               $('#qunit_testing').toggleClass('show hide');

               _RunTests();
          }
     }

     return {
          start:_start
     };
});
