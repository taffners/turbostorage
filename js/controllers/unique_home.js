define(['models/utils'],function(utils)
{
     function _BindEvents(php_vars)
     {
          $(':radio').on('change', function()
          {
               // purpose: Every time a value is checked change update value in database

               // vars
               var  _val_checked = $(this).val(),
                    _uniqueID = $(this).attr('id').split('_')[2],
                    _php_vars = utils.PhpVars(),
                    _new_class = _val_checked == '1' ? 'yes-button-val': 'no-button-val';

               // Change class of row to new value so filters work
               $(this).parent().closest('tr').attr('class', _new_class);

               // change value in database with ajax
               $.ajax(
               {
                    type:'POST',
                    url: 'utils/updateUniqueID.php?',

                    dataType: 'text',
                    data: {'used': _val_checked, 'uniqueID':_uniqueID, 'userID':_php_vars['userID']},
                    success: function(response)
                    {


                    },
                    error: function(textStatus, errorThrown)
                    {
                         dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus, '#new_nums_legend');
                    }
               });
          });

          require(['views/events/ToolTipEvents'], function(ToolTipEvents)
          {
               ToolTipEvents.StartEvents();
          });

          // make filter buttons
          $('#unique-number-yes-filter').click(function()
          {
               // show all the yes's
               $('.yes-button-val').each(function()
               {
                    $(this).attr('class', 'yes-button-val')
               });

               // hide all the no's
               $('.no-button-val').each(function()
               {
                    $(this).attr('class', 'no-button-val collapse')
               });
          });

          $('#unique-number-no-filter').click(function()
          {
               // hide all the yes's
               $('.yes-button-val').each(function()
               {
                    $(this).attr('class', 'yes-button-val collapse')
               });

               // show all the no's
               $('.no-button-val').each(function()
               {
                    $(this).attr('class', 'no-button-val')
               });
          });

          $('#unique-number-all-filter').click(function()
          {
               // show all the yes's
               $('.yes-button-val').each(function()
               {
                    $(this).attr('class', 'yes-button-val')
               });

               // show all the no's
               $('.no-button-val').each(function()
               {
                    $(this).attr('class', 'no-button-val')
               });
          });
     }

     function _start(test_status, php_vars)
     {
          _BindEvents(php_vars);

     }

     return {
          start:_start
     };
});
