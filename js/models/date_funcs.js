define(function()
{

     // Purpose: store all functions dealing with dates

	var 	_consent_info = _SetConsentDateInfo();

	function _GetTodaysDate()
	{
		// purpose: get today's date

		// vars
		var 	today = new Date(),
			dd = today.getDate(),
			mm = today.getMonth()+1, //January is 0!
			yyyy = today.getFullYear();

		// add a zero before days and months under ten
		if(dd < 10)
		{
		    dd = '0' + dd
		}

		if(mm < 10)
		{
		    mm = '0' + mm
		}

		today = mm+'/'+dd+'/'+yyyy;

		return today;
	}

	function _ValidateDate(in_date)
	{
		// Purpose: check if date format is MM/DD/YYYY between 1900 and 2099.  Return true if date format is correct and false if not
		var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;
		return date_regex.test(in_date);
	}

	function _ChangeDateFormatEuropean(in_date)
	{
		// Purpose: Convert date format from MM/DD/YYYY to YYYY-MM-DD

		var 	split_date = in_date.split('/'),
			new_date = split_date[2] + '-' +  split_date[0] + '-' + split_date[1];

		return new_date;
	}

	function _ChangeDateFormatUS(in_date)
	{
		// Purpose: Convert date format from YYYY-MM-DD to MM/DD/YYYY

		var 	split_date = in_date.split('-'),
			new_date = split_date[1] + '/' + split_date[2] + '/' + split_date[0]

		return new_date;
	}

	function _SetConsentDateInfo()
	{
		// Purpose: Get all consent date information

		$.ajax(
		{
			type:'POST',
			url: 'utils/getConsentInfo.php',
			async: false,
			dataType:'json',
			success: function(response)
			{
				_consent_info = response;
			},
			error: function()
			{
				alert('Ajax call failed (utils/getConsentInfo.php)');
			}
		});

		return _consent_info;
	}

	function _GetAllConsentInfo()
	{
		// Purpose: get what the current consent info is

		return _consent_info;
	}

	function _NearestConsentDateInfo(approv_date)
	{
		// Purpose: Find what date consent info applies to the approval date of current consent
		// return _consent_info[0]['consentDate'];
		var 	_len_consent_info = _consent_info.length,
			i,
			_approv_date = new Date(approv_date),
			_consent_date,
			_result_info;

		// find if approv date is after the last consent info date.  If so include last date as the result date
		if (_approv_date > new Date(_consent_info[_len_consent_info-1]['consentDate']))
		{
			_result_info =_consent_info[_len_consent_info-1];
		}

		// if it falls somewhere else find where the date falls
		else
		{
			for (i = 0; i < _len_consent_info; i++)
			{
				_consent_date = new Date(_consent_info[i]['consentDate']);
				if (_consent_date > _approv_date)
				{
					if (i === 0)
					{
						_result_info = _consent_info[0];
					}
					else
					{
						_result_info = _consent_info[i-1];
					}
					break;
				}
			}
		}
		return _result_info;
	}

	return {
		GetTodaysDate:_GetTodaysDate,
		ValidateDate:_ValidateDate,
		ChangeDateFormatEuropean:_ChangeDateFormatEuropean,
		ChangeDateFormatUS:_ChangeDateFormatUS,
		SetConsentDateInfo:_SetConsentDateInfo,
		GetAllConsentInfo:_GetAllConsentInfo,
		NearestConsentDateInfo:_NearestConsentDateInfo
	}
});
