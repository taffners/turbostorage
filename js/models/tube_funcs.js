define(['models/utils'], function(utils)
{
     var  _remove_inputs = ['spaceID', 'tubeID', 'xLoc', 'yLoc', 'boxID', 'tubeName', 'freezerName', 'boxName', 'shelfNum', 'rackNum', 'rackSpaceNum'],
          _box_info_inputs = ['freezerName', 'boxName', 'shelfNum', 'rackNum', 'rackSpaceNum'];

     function _PrepareToRemoveTube(tube_id)
     {
          // Purpose: information on what tube is clicked and clear out anything which was previously in the div.

          // Called by clicking on tube to remove

               // inputs include:
                    // spaceID
                    // tubeID
                    // xLoc
                    // yLoc
                    // boxID

          var  _remove_tube_div = document.createElement('div'),
               _curr_input,
               _i,
               _data_attr_name,
               _box_id = $('#' + tube_id).attr('data-box-i-d');

          // empty out clicked-tubes div
          $('#clicked-tubes').empty();

          // get box row information
          _box_data = $('tr#box_' + _box_id).data();

          // for every element in remove_inputs append an input with the data from that data_attr.  (data attributes change all capitials to lowercase so this has to be accounted for)
          for (_i = 0; _i < _remove_inputs.length; _i++)
          {
               // reset curr_input to new input
               _curr_input = _remove_tube_div.appendChild(document.createElement('input'));

               // add name with capitals for database addition
               _curr_input.setAttribute('name', _remove_inputs[_i]);

               // Add an id to the input
               _curr_input.id = _remove_inputs[_i];

               //  for any box inputs found in the box row of the table addedBoxesList add them as a remove input for adding the remove log
               if (_box_info_inputs.indexOf(_remove_inputs[_i]) > -1)
               {
                    _curr_input.setAttribute('value', _box_data[_remove_inputs[_i]]);
               }
               // add value from input data attributes.  The case fo' + _remove_inputs[_i]r the data attributes is different.  For example tubeID == tube-i-d.  However xLoc == r & yLoc == c
               else if (_remove_inputs[_i] == 'xLoc')
               {
                    _curr_input.setAttribute('value', $('#' + tube_id).attr('data-r'));
               }
               else if (_remove_inputs[_i] == 'yLoc')
               {
                    _curr_input.setAttribute('value', $('#' + tube_id).attr('data-c'));
               }
               else if (_remove_inputs[_i] == 'tubeName')
               {
                    _curr_input.setAttribute('value', $('#' + tube_id).html());
               }
               else
               {
                    _data_attr_name = utils.ReplaceCapWithDash(_remove_inputs[_i]);
                    _curr_input.setAttribute('value', $('#' + tube_id).attr('data-' + _data_attr_name));
               }
          }

          // Add all _remove_inputs to clicked-tubes
          $('#clicked-tubes').append(_remove_tube_div);
     }

     function _CheckVaildClickedLoc()
     {
          // () -> bool
          // Purpose: Check if all the inputs made by _PrepareToRemoveTube are actually there.  If the value is not there then return false otherwise return true
          var  _i;

          for (_i = 0; _i < _remove_inputs.length; _i++)
          {
               if (!$('#' + _remove_inputs[_i]).val())
               {
                    return false;
               }
          }
          return true;
     }

     return {
          PrepareToRemoveTube:_PrepareToRemoveTube,
          CheckVaildClickedLoc:_CheckVaildClickedLoc
     }
});
