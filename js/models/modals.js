define(function()
{

     function _CleanModal(title_id, body_id)
     {
          // Purpose: Remove all previously added data to the modal
          // remove title from modal
          _CleanTitle(title_id);

          // empty body of modal
          _CleanBody(body_id);
     }

     function _CleanTitle(title_id)
     {
          // Purpose: remove title from modal
          $('#' + title_id).empty();
     }

     function _CleanBody(body_id)
     {
          // Purpose: empty body of modal
          $('#' + body_id).empty();
     }

     function _AddTitle(title_id, modal_title)
     {
          // Purpose: Add a title to the modal

          _CleanTitle(title_id);

          $('#' + title_id).append(modal_title);
     }

     function _AddListElmToBody(body_id, elm)
     {
          // Purpose: append a list elmement to the body of the modal

          var  _elm_line = document.createElement('li');

          _elm_line.innerHTML = elm;

          $('#' + body_id).append(_elm_line);
     }

     function _ToggleModal(modal_id)
     {
          // purpose: toggle showing and hiding of the modal

          $('#' + modal_id).modal('toggle');
     }

     return {
          CleanModal:_CleanModal,
          AddTitle:_AddTitle,
          AddListElmToBody:_AddListElmToBody,
          ToggleModal:_ToggleModal
     }
});
