define(['models/utils', 'models/date_funcs'],function(utils, date_funcs)
{
     var  _hrefs =
          [
               {
                    'page':'tube_change',
                    'href':
                         [
                              '?page=tube_form&action=updateTube&tubeID=',
                              '&boxID=',
                              '&boxName='
                         ],
                    'insert_ids':
                         [
                              'tubeID','boxID','boxName'
                         ],
                    'tr_class': 'update-redirect notselected hover_row'
               },

               // Make sure the default page is always at the index 1
               {
                    'page':'default',
                    'href':'none',
                    'tr_class': 'view-box-grid-redirect notselected hover_row'
               },
               {
                    'page':'home',
                    'href':'none',
                    'tr_class': 'view-box-grid-redirect notselected hover_row'
               }
          ],
          _button_tool_tips =
          {
               'dup-tube-button': 'Duplicate Tube',
               'update-tube-button':'Update These Tubes'
          };

     function _AddTableBodyCloneRow(response, clone_insert_id, clone_row_id, clean_out, page_name)
     {
          // Purpose:  Clone the a row of a hidden tbody element.  Then add new text from a json with keys the same as the data-row-name in the cloned row.  Also add build a data-href attr based on the page_name.

          var  _i,
               _row_name,
               _row,
               _row_text,
               _clone,
               _href_pieces,
               _keys,
               _final_href,
               _insert_id_val;

               // find relevant page data for the page if not found set to default in _hrefs[1]
               _page_href = _hrefs.find(function(o)
               {
                    return o.page === page_name;
               }) || _hrefs[1];


          if (clean_out)
          {
               // Clean out insert tbody
               $('#' + clone_insert_id).empty();
          }

          // find each object in JSON
          for (_i in response)
          {
               _row = (response[_i]);

               // clone a new row from the html id clone insert id
               _clone = $('#' + clone_row_id).clone(true);

               // add an id to tr row
               _clone.attr('id', 'clone_row_id_' + _i);

               // add class to tr row
               _clone.attr('class', _page_href['tr_class']);

               // add href to redirect to if it is necessary for the page
               if (_page_href['href'] !== 'none')
               {
                    // build the href and put row data where related to insert Ids between every piece in the href
                    _href_pieces = _page_href['href'];
                    _keys = _page_href['insert_ids'];

                    // build a href to redirect to from an array of strs, json for data, and an array of keys
                    _final_href = _PasteStrData(_href_pieces, _row, _keys);

                    // add new built href to a data attribute
                    _clone.attr('data-href', _final_href);
               }

               // find all td elements and replace text based on keys of data row-name
               _clone.find('td').each(function()
               {
                    _row_name = $(this).data('row-name')

                    // exit if the data element row name is not present
                    // This is used for td's like:
                    // <td>Box Name</td>
                    // <td data-row-name="boxName"></td>
                    if (_row_name === '')
                    {
                         return $(this);
                    }

                    // if row name is a date convert to US Format for displaying in table
                    else if (_row_name.indexOf('Date') !== -1 || _row_name.indexOf('date') !== -1)
                    {
                         _row_text = date_funcs.ChangeDateFormatUS(_row[_row_name]);

                         // Replace 00/00/0000 with ''
                         _row_text = _row_text === '00/00/0000'? '': _row_text;
                    }

                    // othewise add the response from database
                    else
                    {
                         _row_text = _row[_row_name];
                    }

                    // Replace underscores with a space
                    _row_text = utils.ReplaceUnderScores(_row_text);
                    return $(this).text(_row_text);
               });

               _clone.appendTo('#' + clone_insert_id);
          }
     }

     function _AddTableRowFirstTdRowName(response, clone_insert_id, clone_row_id)
     {
          var  _clone = $('#' + clone_row_id).clone(true),
               _row_count = 0,
               _key_name,
               _skip_row = false,
               _row_text,
               _format_text_return,
               _irb_consent_info;

          // change id of cloned row
          _clone.attr('id', clone_row_id.replace('clone', 'data'));

          _clone.find('td').each(function()
          {
               // find the td has a data-row-name.  If it does alter the contents
               if ($(this).data('row-name') != undefined)
               {
                    _key_name = $(this).data('row-name');

                    _key_val = response[_key_name];

                    // Make sure value isn't empty
                    if ((_key_val != undefined && _key_val != '0000-00-00' && _key_val != '' && _key_val != null) || !(_key_name === 'openAccess' && response['consentYesNo'] == 0))
                    {
                         _format_text_return = utils.FormatTextForInput(_key_name, _key_val, 'str_out');

                         _skip_row = _format_text_return[1];
                         _row_text = _format_text_return[0];
                         return $(this).text(_row_text);
                    }

                    // do not add rows which are empty
                    else
                    {
                         _skip_row = true
                    }
               }
               _row_count += 1;
          });

          if (!_skip_row)
          {
               _clone.appendTo('#' + clone_insert_id);
          }

          // add information about the consent here
          if (_key_name === 'consentApprovalDate' && !_skip_row)
          {
               _irb_consent_info = date_funcs.NearestConsentDateInfo(_key_val);

               _AddTableRowFirstTdRowName(_irb_consent_info, clone_insert_id, 'table-info-clone-IRBQuestion');

               _AddTableRowFirstTdRowName(_irb_consent_info, clone_insert_id, 'table-info-clone-answer');
          }
     }

     function _PasteStrData(str_pieces, input_data, data_keys)
     {
          // (array of strs, json, array of strs) -> str
          // Purpose: Paste together a string by inserting the value in the input_data related to the positional key related to k.

          // Example:
               // input:
                    // str_pieces =
                         // [
                              // '?page=tube_form&action=updateTube&tubeID=',
                              // '&boxID=',
                              // '&boxName='
                         // ]
                    // data_keys =
                         //['tubeID','boxID','boxName']
                    // input_data =
                         // {
                              // boxID: "12",
                              // boxName:"test box",
                              // tubeID:"20"
                         // }

               // example output = '?page=tube_form&action=updateTube&tubeID=20&boxID=12&boxName=test_box'
          var  _k = 0,
               _final_str = str_pieces.reduce(function(_end_str, _str_piece)
               {
                    // find the data to be inserted
                   _insert_id_val = utils.ReplaceSpaces(input_data[data_keys[_k]]);


                   _end_str += _str_piece + _insert_id_val;

                   _k += 1;
                   return _end_str;
               }, '');

          return _final_str;
     }

     function _AddTableCloneCell(clone_cell_id, insert_id, input_data, cell_id, group_num)
     {
          // (str, str, JSON, int, int) -> new cell in html appended to insert_id
          // Purpose: Clone a cell iterate thru all data-attr's and replace value with value of data attr with input_data[orginal_data_attr_data]

          var  _clone = $('#' + clone_cell_id).clone(false),
               _data_attrs = _clone.data(),
               _data_attr_keys = Object.keys(_data_attrs),
               _data_attr_len = _data_attr_keys.length,
               _i,
               _new_data_val,
               _orginal_data_val,
               _attr_name_camel_case,
               _attr_name_dash_syntax,
               _curr_class = _clone.attr('class'),
               _new_class;

          // iterate over all _data_attrs
          for (_i = 0; _i < _data_attr_len; _i++)
          {
               // get the name of the attribute.  Unfortuantley this name has been converted from a dash syntax to a camel case syntax example tube-i-d -> is now tubeID.  This needs to be converted back to tube-i-d to change the data attribute in the clone :( fucking data attributes why why does this happend
               _attr_name_camel_case = _data_attr_keys[_i];

               // convert attribute name back to the actual attribute name
               _attr_name_dash_syntax = utils.ReplaceCapWithDash(_attr_name_camel_case);

               // Original data stored in the data attribute.  Should be equal to the key in the JSON (input_data)
               _orginal_data_val = _data_attrs[_attr_name_camel_case];

               _new_data_val = utils.ReplaceSpaces(input_data[_orginal_data_val]);

               _clone.attr('data-'+ _attr_name_dash_syntax, _new_data_val);

               // add an id
               _clone.attr('id', cell_id);

               // add a group class to keep track of cells which are full. When clicking on them all of them can be highlighted
               if (group_num === undefined)
               {
                    _new_class = _curr_class;
               }
               else
               {
                    _new_class = _curr_class + ' group_num_' + group_num;
               }

               // add a class to keep track of grouped cells
               _clone.attr('class',_new_class);

               if (_attr_name_camel_case === 'color')
               {
                    _clone.css({'background-color':_new_data_val});
               }
          }

          // add text to clone
          _clone.text(input_data['sampleName']);

          _clone.appendTo('#' + insert_id);

     }

     function _CloneDivAddTable(clone_id, clone_num, table_class, insert_id, button_status, table_type, head_attr)
     {
          // (int, int, json, str, str) -> table insert + list(th_id, tbody_id)

          // Purpose: Clone a div and append a table to it.  Add table class to table.  Include thead and tbody id's. The class trying to find in the buttons needs to be listed first in clone

          var  _clone = $('#' + clone_id).clone(true),
               _tube_info_table = document.createElement('table'),
               _thead,
               _tbody,
               _tr,
               _th,
               _tbody_id,
               _th_id,
               _on_buttons,
               _num_buttons,
               _button_col_class_offset,
               _button_count,
               _new_class,
               _new_id,
               _new_div_id;

          // change div id
          _new_div_id = 'div-'+ table_type+'-' + clone_num;
          _clone.attr('id', _new_div_id);

          // add an id to both buttons
          if (button_status['button_status'] === 'on')
          {
               // get the array of buttons to unhide & add an id to
               _on_buttons = button_status['on_buttons'];

               // Depending on number of buttons to turn on change the col offset.  Each button gets a col size of 2
               _num_buttons = _on_buttons.length;

               _button_col_class_offset = utils.ColClassSizes(_num_buttons, 2, 'left');

               _button_col_class_no_offset = utils.ColClassSizes(_num_buttons, 2, 'right');

               _button_count = 0;

               // find the divs where the buttons are.  unhide the buttons of interest.  Set the class of the first button to have an offset
               _clone.find('div').each(function()
               {
                    // Set the class which is interesting

                    _insteresting_class = $(this).attr('class').split(' ')[0];

                    // find if the current div is in the list of buttons wanting to turn on
                    if (_on_buttons.indexOf(_insteresting_class) > -1)
                    {
                         // if this is the first button include an offset
                         if (_button_count === 0)
                         {
                              _new_class = _button_col_class_offset + ' ' + _insteresting_class;
                         }
                         else
                         {
                              _new_class = _button_col_class_no_offset + ' ' + _insteresting_class;
                         }

                         // make id
                         _new_id = _insteresting_class + '-' + clone_num;

                         // increase the button count so the rest of the buttons do not have an offset in the class
                         _button_count += 1;
                         return $(this).attr('class', _new_class)
                              .attr('id', _new_id)
                              .attr('data-toggle', 'tooltip')
                              .attr('data-original-title', _button_tool_tips[_insteresting_class]).
                              attr('data-placement', 'top').tooltip();
                    }
               });
          }

          _clone.appendTo('#' + insert_id);

          // make a table
          _tube_info_table.className = "QueryInfo";
          _tube_info_table.id = 'table-'+table_type+'-' + clone_num;

          _thead = _tube_info_table.appendChild(document.createElement('thead'));
          _thead.id = 'thead-'+table_type+'-' + clone_num;

          _tr = _thead.appendChild(document.createElement('tr'))

          _th = _tr.appendChild(document.createElement('th'));
          _th.setAttribute('data-row-name', head_attr);
          _th.colSpan =2;
          _th_id = 'thead-'+head_attr+'-' + clone_num;
          _th.id = _th_id;

          _tbody = _tube_info_table.appendChild(document.createElement('tbody'));
          _tbody_id = 'tbody-'+table_type+'-' + clone_num;
          _tbody.id = _tbody_id;

          // add table to clone
          $('#' + _new_div_id).append(_tube_info_table);

          return [_tbody_id, _th_id];
     }

     return {
          AddTableBodyCloneRow:_AddTableBodyCloneRow,
          PasteStrData:_PasteStrData,
          AddTableCloneCell:_AddTableCloneCell,
          CloneDivAddTable:_CloneDivAddTable,
          AddTableRowFirstTdRowName:_AddTableRowFirstTdRowName
     }
});
