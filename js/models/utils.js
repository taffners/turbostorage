define(['models/date_funcs'],function(date_funcs)
{
     var  _min_tested_ffversion = 38,
          _min_ffversion = 34,
          _min_chromeversion = 50;

     function _GetCurrURL()
     {
          // Purpose:  Get the full current url
          return window.location.href;
     }

     function _GetDevelopmentStatus()
     {
          var  _url_split = _GetCurrURL().split('/');

          if (_url_split.indexOf('devs') != -1)
          {
               return 'development';
          }
          else
          {
               return 'active';
          }
     }

     function _scroll(div_ID)
     {
     	// purose: scroll to a div

     	// scroll to div
     	$('html, body').animate(
     	{
     		scrollTop: $(div_ID).offset().top
     	}, 50);
     }

     function _dialog_window(error_message, div_ID)
     {
     	// Purpose: display an alert window and scroll to a div

     	// add message
     	$('#AlertId').html(error_message);

     	// make dialog window
     	$('#AlertId').dialog(
     	{

     		height: 300,
     		width: 400,
     		modal: true,
     		position:
     		{
     			my: 'center bottom',
     			at: 'center top',
     			of: window
     		},
     		buttons:
     		{
     			'ok': function()
     			{
     				$(this).dialog('close');

                         // Make sure div exists before scrolling to it
                         if ($(div_ID).length > 0)
                         {
                              _scroll(div_ID);
                         }
     			}
     		}
     	});
     };

     function _confirmPopUp(confrimPopUpHTML, buttonArray)
     {
          // Purpose: display a confirm pop up

     	$('#ConfirmId').html(confrimPopUpHTML)

     	$('#ConfirmId').dialog(
     	{
     		resizable: false,
     		height: 400,
     		width: 500,
     		modal: true,
     		buttons: buttonArray
     	});

          // change title
          $('#ConfirmId').dialog('option', 'title', 'WARNING!!!');
     };

     function _PhpVars()
     {
          // Purpose: Get all the php vars if they exist

          // vars
               var	_i,
                    _all_php_vars,
                    _split_php_vars,
                    _split_var,
                    _url = _GetCurrURL(),
                    _curr_php_var = {};

          // find if php vars are present in url
          if (_url.indexOf('?') != -1)
          {
               // Get all of the php vars
               _all_php_vars = _url.split('?')[1];

               // get individual vars in a array

                    // if multiple vars are present
                    if (_all_php_vars.indexOf('&') != -1)
                    {
                         _split_php_vars = _all_php_vars.split('&');
                    }

                    // only page is present
                    else
                    {
                         _split_php_vars = [_all_php_vars];
                    }

               // add all vars in the split_php_vars array to the json php_vars as
               // key = var name and value = value of var
               for (_i=0; _i<_split_php_vars.length; _i++)
               {
                    _split_var = _split_php_vars[_i].split('=');
                    _curr_php_var[_split_var[0]] = _split_var[1];
               }
          }
          return _curr_php_var;
     }

     function _CheckBrowser()
     {
          // Purpose: Check if the browser is ok for turbostorage

          // figure out which browser the user is using. If the browser is not chrome or firefox remove all login and replace with a warning.
          if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent))
          {
               var _ffversion = new Number(RegExp.$1) ;

               if (_ffversion < _min_ffversion)
               {
                    $('#all_login_fields').attr('class', 'hide');
                    $('#browser_warning').attr('class', 'show');

                    _dialog_window('Testing for on the firefox web browser was performed on version 39.  You are currently running version ' + ffversion + '. It is recommended to update your browser.', '#loginOutsideRow');
                    return false;


               }

               else if (_ffversion < _min_tested_ffversion)
               {
                    _dialog_window('Testing for on the firefox web browser was performed on version 39.  You are currently running version ' + ffversion + '. It is recommended to update your browser.', '#loginOutsideRow');
               }
               return true;
          }

          else if (/Chrome[\/\s](\d+\.\d+)/.test(navigator.userAgent))
          {
               var chromeversion = new Number(RegExp.$1);

               if (chromeversion < _min_chromeversion)
               {
                    _dialog_window('Testing for on the chrome web browser was performed on version 50.  You are currently running version ' + chromeversion + '. It is recommended to update your browser.', '#loginOutsideRow');
               }
               return true;
               // capture x.x portion and store as a number
          }

          // otherwise hide login
          else
          {
               $('#all_login_fields').attr('class', 'hide');
               $('#browser_warning').attr('class', 'show');
               return false;
          }
     }

     function _CheckIfInputsEmpty(array_fields)
     {
          // Purpose: pass an array of ids and the response if empty and see if the input is empty.  If it is empty open or null break loop and return the problem id. if all ids entered are not empty return 'passed'
          // (array) -> str
          // Input example:
               //   {
               //        'firstName':'Fill out your first name.',
               //        'lastName':'Fill out your last name.'
               //   }
          var _id,
               _val;

          // iterate over the array
          for (_id in array_fields)
          {
               _val = $('#' + _id).val();
               // find if input is empty or null
               if (_val === '' || _val === null)
               {
                    return _id;
                    break;
               }
          }
          return 'passed';
     }

     function _ValidateEmail(input_email)
     {
          // Purpose: check if the string is formated like an email address

          var _re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return _re.test(input_email);
     }

     function _ValidateOnlyInts(input_val)
     {
          // Purpose: check if the string is formated like an Phone number

          var  _re = /^\d+$/;
          return _re.test(input_val);
     }

     function _ValidateOnlyIntPhoneNum(input_phone)
     {
          // Purpose: check if the string is only ints and at least 10 digits long
          var  _only_ints = _ValidateOnlyInts(input_phone);
               _input_ok = _only_ints && input_phone.length >= 10 ? true: false;
               return _input_ok;
     }

     function _AddComments(comment_type, ref_id)
     {
          // Purpose: Add all comments of a ref_id to a modal

          require(['ajax_calls/GetComments'], function(GetComments)
          {
               GetComments.StartAJAX(comment_type, ref_id).done(function(response)
               {
                    GetComments.CheckResponse(response, comment_type);
               });
          });
     }

     function _NewLineToHTML(input_str)
     {
          // Purpose:  Take all new lines and returns and replace with <br>

          return input_str.replace(/(?:\r\n|\r|\n)/g, '<br>');
     }

     function _FormatTextForInput(key_name, key_val, check_box_type)
     {
          // (str, str, str (int_out, str_out)) -> [str, bool]
          // Purpose:  Refomat a value based on the key and check box type.  For instance change dates to us format, change yes no from int to strs, and change open access from int to str

          var  _yes_no_keys = ['consentYesNo', 'published', 'mta'],
               _skip_row = false,
               _row_text = key_val;

          // Make sure value isn't empty
          if (key_val != undefined && key_val != '0000-00-00' && key_val != '' && key_val != null)
          {
               // Some fields are either yes or no answer no = 0, yes = 1, opt out = 0, opt in = 1, question not asked = 2, or if it's a date convert to us format

               // if row name is a date convert to US Format for displaying in table
               if (key_name.indexOf('Date') !== -1 || key_name.indexOf('date') !== -1)
               {
                    _row_text = date_funcs.ChangeDateFormatUS(_row_text);

                    // Replace 00/00/0000 with ''
                    _row_text = _row_text === '00/00/0000'? '': _row_text;
               }

               // for variables where the format needs to be changed from int. Used for making tube info table.
               else if (check_box_type === 'str_out')
               {
                    // change values yes no questions from numeric
                    if (_yes_no_keys.indexOf(key_name) !== -1)
                    {
                         _row_text = _row_text == 1 ? 'Yes': 'No';
                    }

                    // change openAccess
                    else if(key_name === 'openAccess')
                    {
                         switch (_row_text)
                         {
                              case '0':
                                   _row_text = 'opt out';
                                   break;
                              case '1':
                                   _row_text = 'opt in';
                                   break;
                              case '2':
                                   _row_text = 'question not asked';
                                   break;
                         }
                    }
                    // Some samples such as sample type use underscores in the name replace them
                    else
                    {
                         _row_text = _ReplaceUnderScores(_row_text)
                    }
               }

               else if (check_box_type === 'int_out')
               {
                    // set the variable equal to
                    if (key_name == 'pageNum' && key_val == 0)
                    {
                         _row_text = '';
                         _skip_row = true;
                    }

                    else if (key_name === 'published' && key_val != 1)
                    {
                         _row_text = '';
                         _skip_row = true;
                    }

                    else if (key_name === 'mtal' && key_val != 1)
                    {
                         _row_text = '';
                         _skip_row = true;
                    }
               }
               return [_row_text, _skip_row];
          }
          else
          {
               return ['', true];
          }

     }

     function _ReplaceUnderScores(input_str)
     {
          // Purpose: input has an underscore replace it with a space

          return _ReplaceAll(input_str, '_', ' ');
     }

     function _ReplaceSpaces(input_str)
     {
          // Purpose: input has an spaces replace all with an underscore

          return _ReplaceAll(input_str, ' ', '_');
     }

     function _ReplaceCapWithDash(input_str)
     {
          // (str) -> str
          // Purpose: Take a string and replace all Capital letters with a dash then a lower case letter
          // Example ('tubeID') -> 'tube-i-d'

          var  _char_array = input_str.split(''),
               _convert_str = _char_array.reduce(function(_end_str, _c)
               {
                    _end_str += _c.toLowerCase() === _c ?  _c:'-'+ _c.toLowerCase();

                    return _end_str;
               }, '');

          return _convert_str;
     }

     function _FindWordInStrAndCut(input_str,find_str)
     {
          // (str, str) -> str
          // Purpose:  Take a str of many words.  Find the first occurance of a find_str in a word and return a the remaining part of the word
          // Example:
          // (fullCell pop-up-accordion group_num_20, group_num_) ->
          // 20
          var  _found_word = _FindWord(input_str, find_str);

          if (_found_word != undefined)
          {
               return _found_word.replace(find_str, '');
          }
          else
          {
               return false;
          }

     }

     function _FindWord(input_str, find_str)
     {
          // (str, str) -> str
          // Purpose: Take a str of many words and find the first stir which contains part of the word

          return input_str.split(' ')
               .filter(function(each_word)
               {
                    if(each_word.indexOf(find_str) > -1)
                    {
                         return each_word;
                    }
               })[0];
     }

     function _ReplaceAll(input_str, search, replacement)
     {
          if (input_str != null || input_str != undefined)
          {
               input_str = input_str.toString();
               return input_str.split(search).join(replacement);
          }
          else
          {
               return '';
          }
     }

     function _CopyWhileTyping(copy_from_id, copy_to_id)
     {
          // Purpose:  While typing in the input of copy_from_id add it automatically to copy_to_id input

          $('#' + copy_from_id).keyup(function()
          {
               var  _curr_str = $(this).val();

               $('#' + copy_to_id).val(_curr_str);
          });
     }

     function _ColClassSizes(num_cols, col_size, alignment)
     {
          // (int, int, str(left,right)) -> str
          // Purpose: Build a class for column sizes including a offset if alignment === left

          var  _i,
               _offset = 12 - (num_cols * col_size),
               _new_class = '',
               _col_sizes = ['xs', 'sm', 'md'];

          for (_i in _col_sizes)
          {
               _new_class += 'col-' + _col_sizes[_i] + '-' + col_size + ' ';

               if (alignment === 'left')
               {
                    _new_class += 'col-' + _col_sizes[_i] + '-offset-' + _offset + ' ';
               }
          }
          return _new_class;
     }

     function _ReloadJs(js_address)
     {
          // (str) -> reload js
          // Purpose: remove the js script and then reload it with a random num to show it has been reloaded.

          var  _random = 'reload_random=' + (Math.floor(Math.random() * 200).toString());

          require.undef(js_address);
          require.config(
          {
               urlArgs: _random
          });
          require([js_address], function(RedirectEvents)
          {
               RedirectEvents.StartEvents(_PhpVars());
          });
     }

     function _SaveAsPNG(default_save_name, selection_id)
     {
          //Purpose: Save selection_id as png

          html2canvas($('#' + selection_id),
          {
               onrendered: function(canvas)
               {
                    theCanvas = canvas;
                    document.body.appendChild(canvas);

                    // Convert and download as image
                    Canvas2Image.saveAsPNG(canvas);
                    // $("#img-out").append(canvas);
                    // Clean up
                    //document.body.removeChild(canvas);
               }
          });
     }

     function _SaveAsPDF(selection_id)
     {
          var  quotes = document.getElementById(selection_id);

          html2canvas(quotes,
          {
               onrendered: function(canvas)
               {
                    //! MAKE YOUR PDF
                    var pdf = new jsPDF('p', 'pt', 'letter');

                    for (var _pages_nums = 0; _pages_nums <= quotes.clientHeight/980; _pages_nums++)
                    {
                         //! This is all just html2canvas stuff
                         var srcImg  = canvas;
                         var sX      = 0;
                         var sY      = 980*_pages_nums; // start 980 pixels down for every new page
                         var sWidth  = 900;
                         var sHeight = 980;
                         var dX      = 0;
                         var dY      = 0;
                         var dWidth  = 900;
                         var dHeight = 980;

                         window.onePageCanvas = document.createElement("canvas");
                         onePageCanvas.setAttribute('width', 900);
                         onePageCanvas.setAttribute('height', 980);
                         var ctx = onePageCanvas.getContext('2d');
                         // details on this usage of this function:
                         // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images#Slicing
                         ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);

                         // document.body.appendChild(canvas);
                         var canvasDataURL = onePageCanvas.toDataURL("image/png", 1.0);

                         var width         = onePageCanvas.width;
                         var height        = onePageCanvas.clientHeight;

                         //! If we're on anything other than the first page,
                         // add another page
                         if (_pages_nums > 0)
                         {
                              pdf.addPage(612, 791); //8.5" x 11" in pts (in*72)
                         }
                         //! now we declare that we're working on that page
                         pdf.setPage(_pages_nums+1);
                         //! now we add content to that page!
                         pdf.addImage(canvasDataURL, 'PNG', 20, 40, (width*.62), (height*.62));

                    }
                    //! after the for loop is finished running, we save the pdf.

                    saveName = window.prompt('Enter file name', 'box');
                    pdf.save(saveName + '.pdf');

                    // reload page
                    location.reload();
               }
          });



     }

     return {
          GetCurrURL:_GetCurrURL,
          GetDevelopmentStatus:_GetDevelopmentStatus,
          CheckBrowser:_CheckBrowser,
          dialog_window:_dialog_window,
          confirmPopUp:_confirmPopUp,
          CheckIfInputsEmpty:_CheckIfInputsEmpty,
          ValidateEmail:_ValidateEmail,
          ValidateOnlyInts:_ValidateOnlyInts,
          ValidateOnlyIntPhoneNum:_ValidateOnlyIntPhoneNum,
          AddComments:_AddComments,
          NewLineToHTML:_NewLineToHTML,
          ReplaceUnderScores:_ReplaceUnderScores,
          ReplaceSpaces:_ReplaceSpaces,
          ReplaceCapWithDash:_ReplaceCapWithDash,
          CopyWhileTyping:_CopyWhileTyping,
          FindWordInStrAndCut:_FindWordInStrAndCut,
          ColClassSizes:_ColClassSizes,
          ReloadJs:_ReloadJs,
          SaveAsPNG:_SaveAsPNG,
          SaveAsPDF:_SaveAsPDF,
          PhpVars:_PhpVars,
          scroll:_scroll,
          FormatTextForInput:_FormatTextForInput,
          FindWord:_FindWord,
          ReplaceAll:_ReplaceAll
     }

});
