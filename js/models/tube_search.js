define(['ajax_calls/TubeSearch'], function(TubeSearch)
{
     // Purpose: Module to activate searching for tubes and making html tables of the results

     function _Search(search_term, clone_insert_id, clone_row_id, page_name)
     {

          TubeSearch.StartAJAX(search_term, page_name).done(function(response)
          {

               TubeSearch.CheckResponse(response, clone_insert_id, clone_row_id, page_name);
          });
     }

     function _AutoComplete(input_id, query_name, page)
     {
          // purpose: set up autocomplete on input_id using a query eqal to query_name

          $(input_id).autocomplete(
          {
               source: function(request, response)
               {
                    $.ajax(
                    {
                         type:'POST',
                         url: 'utils/autocomplete.php',
                         dataType: 'json',
                         data: {'query_name':query_name, 'request': request, 'page':page},
                         success: function(data)
                         {
                              var  i,
                                   arr =[];

                              for (i=0; i<data.length; i++)
                              {
                                   arr.push(data[i][query_name]);
                              }

                              response(arr);
                         },
                         error: function(textStatus, errorThrown)
                         {
                              dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus, '#add_new_box_fieldset');
                         }
                    });
               },
               minLength: 2
          });
     }

     return {
          Search:_Search,
          AutoComplete:_AutoComplete
     }
});
