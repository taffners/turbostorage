define(function()
{
     var  _past_quest_answ = {'1':0, '2':0},
          _security_questions =
               {
                    1: 'What was the name of your first pet?',
                    2: 'What High School did you attend?',
                    3: 'In which City where you born?',
                    4: "What is your mother's maddien name?",
                    5: 'What was your childhood nickname?',
                    6: 'What is the name of your favorite childhood friend?',
                    7: 'What school did you attend for sixth grade?',
                    8: 'What street did you live on in third grade?',
                    9: 'In what city or town did your mother and father meet?',
                    10: 'In what city or town was your first job?',
                    11: "What is your maternal grandmother's maiden name?",
                    12: 'In what city does your nearest sibling live?'
               };

     function _SecurityQuestions(quest_num, quest_val, common_id_start)
     {
          // purpose: Currently there are two security questions.  However, this function has the possiblity of adding more in the future. The purpose of this function is to disable the same question in the other question list
          var  _i;

          // iterate over all possible question nums and find all the ones not equal to quest_num
          for (_i in _past_quest_answ)
          {
               if (_i != quest_num)
               {
                    // since the id securityQuestion for the first one doesn't have a 1 at the end reset _i to the empty string
                    _i = _i === '1' ? '':_i;

                    // Make sure a value was previously selected for the clicked question before disabling the old answer
                    if (_past_quest_answ[quest_num] !== 0)
                    {
                         $('#'+ common_id_start +_i +' option[value="' + _past_quest_answ[quest_num] + '"]').prop('disabled', false);
                    }

                    // disable current selection
                    $('#' + common_id_start +_i +' option[value="' + quest_val + '"]').prop('disabled', true);
               }
          }

          // set the currently selected question to the currenly selected value
          _past_quest_answ[quest_num] = quest_val;

     }

     function _SecurityQuestionsNumToQuest(input_num)
     {
          // Purpose: return the question related to the number input.  If the number is not in _security_questions then return error 404 Not Found

          if (input_num in _security_questions)
          {
               return _security_questions[input_num];
          }
          else
          {
               return 'Error 404: NOT FOUND';
          }
     }

     function _HideShowType(input_changing, button_changing)
     {
          // purpose: Use to hide and show input fields (toggle between type password and type text)

          // vars
               var  curr_type;

          // get current type of input of input_changing
          curr_type = $(input_changing).attr('type');

          // change type
          switch (curr_type)
          {
               case 'password':
                              $(input_changing).attr('type', 'text');
                              $(button_changing).html('hide');
                              break;
               case 'text':
                              $(input_changing).attr('type', 'password');
                              $(button_changing).html('show');
                              break;
               default:
                              $(input_changing).attr('type', 'password');
                              $(button_changing).html('show');
                              break;
          }
     }

     function _ToggleGroupList(privacy_level)
     {
          // Purpose: Toggle showing the group list depending on if privacy level === Group or not.  If it isn't equal to Group remove selected of group list

          // change if group list is visable
          switch (privacy_level)
          {
               //when privacy level is group show the groups list
               case 'Group':
                         $('#group_div').attr('class', 'form-group row show');
                         break;

               // Otherwise hide group and removed selected
               default:
                         $('#groups option:selected').removeAttr('selected');
                         $('#group_div').attr('class', 'form-group row hide');
          }
     }

     function _FindSampleTypeGroup(sample_type)
     {
          // (str) -> str
          // Purpose: change which group of extra fields are show depending on the sample type choosen
          // Change group which was clicked to be shown

          var  _curr_group;

          switch (sample_type)
          {
               // Group 1
               case 'Antibody':
                              _curr_group = 'Group1';
                              break;

               // Group 2
               case 'Bacterial_Cultures':
                              _curr_group = 'Group2';
                              break;

               // Group 3
               case 'Cell_Lines':
                         _curr_group = 'Group3';
                         break;

               // Group 4
               case 'Blood':
               case 'Body_fluids':
               case 'Plasma':
               case 'Serum':
               case 'Urine':
               case 'Bone':
               case 'Whole_Tissue':
                         _curr_group = 'Group4';
                         break;

               // Group 5
               case 'Chemical':
                         _curr_group = 'Group5';
                         break;

               // Group 6
               case 'Cytokine':
                         _curr_group = 'Group6';
                         break;

               // Group 7
               case 'DNA':
               case 'RNA':
                         _curr_group = 'Group7';
                         break;

               // Group 8
               case 'Virus':
                         _curr_group = 'Group8';
                         break;

               // Group 9
               case 'PBMC':
                         _curr_group = 'Group9';
                         break;

               // Group 10
               case 'Cell_Pellet':
                         _curr_group = 'Group10';
                         break;

               // Group 11
               case 'Primary_Cells':
                         _curr_group = 'Group11';
                         break;

               default:
                         break;
          }
          return _curr_group;
     }

     function _HideAllSampleTypeGroups()
     {
          // () -> hides all groups and deletes any values already choosen

          // vars
               var	i,
                    numGroups,
                    moreGroups = true;

          // find out how many group classes are present
          for (i = 1; moreGroups; i ++)
          {
               moreGroups = $('#Group'+i).length > 0 ? true:false;
          }

          // set the number of groups found to i - 1
          numGroups = i -1;

          // Hide all current groups
          for (i = 1; i <= numGroups; i++)
          {
               // find if the group class was show if it was set all inputs back to ''
               if ($('#Group' + i).attr('class') == 'show')
               {
                    // find all inputs and change them to ''
                    $('#Group' + i).find('input').each(function()
                    {
                         $(this).val('');
                    });

                    // find all lists and change them to ''
                    $('#Group' + i).find('select').each(function()
                    {
                         $(this).val('');
                    });

                    // now hide the group
                    $('#Group' + i).attr('class', 'hide');
               }
          }
     }

     function _ToggleSampleTypeFields(group_num)
     {
          // Show the current group
          $('div#' + group_num).attr('class', 'show');
     }

     return {
          SecurityQuestions:_SecurityQuestions,
          HideShowType:_HideShowType,
          SecurityQuestionsNumToQuest:_SecurityQuestionsNumToQuest,
          ToggleGroupList:_ToggleGroupList,
          ToggleSampleTypeFields:_ToggleSampleTypeFields,
          HideAllSampleTypeGroups:_HideAllSampleTypeGroups,
          FindSampleTypeGroup:_FindSampleTypeGroup
     }
});
