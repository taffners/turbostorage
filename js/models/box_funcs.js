define(['ajax_calls/GetSamplesInBox', 'models/table_funcs', 'models/utils'], function(GetSamplesInBox, table_funcs, utils)
{
     function _AddBoxGridToHTML(box_size, box_id)
     {
          // (int, int) -> array matrix
          // Purpose: Find all samples currently in the box with the input id.  Then return a square array the size of box_size of the spaces available and full in the box.  A zero represents space is empty and an object represents a full space.

          var  _i, _r, _c,
               _cellData,
               _empty_matrix = _BuildZeroSquareMatix(box_size +1),
               _clone_header_cell_id = 'clone_grid_thead_column',
               _insert_header_id = 'grid_thead_row',
               _insert_body_id = 'grid_tbody',
               _header_data = {r:'0', c:'0', sampleName:''},
               _table_data = {},
               _row,
               _row_id;

          // Find all samples in the box with the box_id in input
          GetSamplesInBox.StartAJAX(box_id).done(function(response)
          {
               // Iterate over the response and insert into _cellData at the appropriate xLoc and yLoc location

               _cellData = _InsertIntoMatrix('xLoc', 'yLoc', _empty_matrix, response);

               // Clean out header and body of table
               $('#grid_thead_row').empty();
               $('#grid_tbody').empty();

               // Add Header row to the box grid in the html
               for (_i = 0; _i <= box_size; _i++)
               {
                    _header_data['c'] = _i;

                    _header_data['sampleName'] = _i !== 0 ? _i:'';

                    table_funcs.AddTableCloneCell(_clone_header_cell_id, _insert_header_id, _header_data, 'grid_header_' +_i, 'none')
               }

               // Add data rows to box grid in the html

               // iterate over rows
               for (_r = 1; _r < box_size + 1; _r++)
     		{
                    // make a new row to add all the cells to
                    _row = document.createElement('tr');
                    _row_id = 'table_row_' + _r;
                    _row.id = _row_id;
                    $('#grid_tbody').append(_row);

                    // iterate over all the columns in the row
                    for (_c = 0; _c < box_size + 1; _c++)
                    {
                         _table_data = {};

                         // Default clone type is a fullcell unless a zero is found in the matrix
                         _clone_body_cell_id = 'clone_grid_tbody_fullCell';

                         // if the data in the matrix is equal to zero find out if it's the
                         if (_cellData[_r][_c] === 0)
                         {
                              if (_c === 0)
                              {
                                   _clone_body_cell_id = 'clone_grid_tbody_headerCell';
                                   _table_data['sampleName'] = String.fromCharCode(65 + _r -1);
                              }
                              else
                              {
                                   _clone_body_cell_id = 'clone_grid_tbody_emptyCell'
                                   _table_data['sampleName'] = '';
                              }
                         }
                         else
                         {
                              _table_data = _cellData[_r][_c];
                         }
                         _table_data['c'] = _c;
                         _table_data['r'] = _r;

                         // Clone a cell iterate thru all data-attr's and replace value with value of data attr with input_data[orginal_data_attr_data]
                         table_funcs.AddTableCloneCell(_clone_body_cell_id, _row_id, _table_data, 'grid_cell_' + _c.toString() + '_' + _r.toString(), _table_data['tubeID'])
                    }
               }

               // reload CellEvents
               utils.ReloadJs('views/events/CellEvents');
          });
     }

     function _BuildZeroSquareMatix(size)
     {
          // Purpose: Build a square matrix of zeros the size the input size
          //      _size = 2,
          //      _test_val = box_funcs.BuildZeroSquareMatix(_size),
          //      _expected = [[0,0],[0,0]];
          var  _matrix = math.zeros(size, size);
          return _matrix._data;
     }

     function _InsertIntoMatrix(x_key, y_key, empty_matrix, input_data)
     {
          // (str, str, array matrix, json) -> array matrix
          // Purpose: Take an input square matrix (empty_matrix), a json (input_data) of data to add at spefic spots related to the values of the input_data[x_key][y_key]
          // _test_val = box_funcs.InsertIntoMatrix(_x_key, _y_key, _empty_matrix, _input_data),
          // _expected = [[0,0,0],[0,0,
          //      {
          //           "insertData": "Data",
          //           "xLoc": 1,
          //           "yLoc": 2
          //      }],
          //      [0,
          //      {
          //           "insertData": "Data",
          //           "xLoc": 2,
          //           "yLoc": 1
          //      },0]];

          var  _i,
               _x,
               _y,
               _input_len = input_data.length;

          for (_i = 0; _i < _input_len; _i++)
          {
               _x = input_data[_i][x_key];
               _y = input_data[_i][y_key];

               empty_matrix[_x][_y] = input_data[_i];
          }

          return empty_matrix;
     }

     function _MakeTubeInfoTable(tube_id, button_status)
     {
          // (int, json) -> Make a tube info table
          // button_status example:
               // {
               //      'button_status' : 'on',
               //      'on_buttons': ['dup-tube-button', 'update-tube-button', 'delete-tube-info-table']
               // }
          // Purpose: find all the following information about the sample with the input tube_id: Fields in the tubeTable
          // (
               // sampleName,
               // sampleType,
               // freezeDate,
               // experimentName,
               // labBook,
               // pageNum,
               // published,
               // intialNumTubes,
               // mta,
               // frozeBy,
               // intialNumTubes,
               // consentYesNo,
               // consentApprovalDate,
               // locConsent,
               // consentNotes,
               // openAccess

          // ),
          //box_name (in the box table), all comments with the commentRef, and all sampleTypeVariables,

          var  _table_ids,
               _response_keys,
               _i,
               _all_comments,
               _is_empty,
               _each_vars,
               _response_for_local_storage;

          // Clone div (tube-info-clone-div) Change div id to tube-info-tube-id-num Append a table with class="QueryInfo" thead with ids (thead-tube-num) and tbody with id(tbody-tube-num)
          _table_ids = table_funcs.CloneDivAddTable('tube-info-clone-div', tube_id, 'QueryInfo', 'insert-tube-info-tables-here', button_status, 'tube-info', 'sampleName');

          // Get the tube info
          require(['ajax_calls/GetTubeTableInfo'], function(GetTubeTableInfo)
          {
               GetTubeTableInfo.StartAJAX(tube_id).done(function(response)
               {
                    _response_for_local_storage = {};

                    // since tube id is not in response add tube id
                    _response_for_local_storage['tubeID'] = tube_id;

                    // iterate over all of the keys in the response and clone the
                    _response_keys = Object.keys(response[0]);

                    for (_i in _response_keys)
                    {
                         _curr_key = _response_keys[_i];
                         // Get only keys and not index keys from the response
                         if (isNaN(_curr_key))
                         {
                              // add every key to the local storage var except the index keys
                              _response_for_local_storage[_curr_key] = response[0][_curr_key] === null ? '': response[0][_curr_key] ;

                              // if key is equal to sampleName change the th value
                              if (_curr_key === 'sampleName')
                              {
                                   $('#' + _table_ids[1]).text(response[0]['sampleName']);
                              }

                              // clone the row and append to new table body
                              else
                              {
                                   table_funcs.AddTableRowFirstTdRowName(response[0], _table_ids[0], 'table-info-clone-' + _curr_key);
                              }
                         }
                    }
                    // store response in local storage
                    localStorage.setItem('CURR_TUBE_INFO',
                         JSON.stringify(_response_for_local_storage));
               });
          });

          // Get the comments
          require(['ajax_calls/GetComments'], function(GetComments)
          {
               GetComments.StartAJAX('sample',tube_id).done(function(response)
               {
                    // iterate over all of the comments and make them into a numbered list
                    _all_comments = ''
                    _is_empty = true;

                    for (_i in response)
                    {
                         if ('comment' in response[_i])
                         {
                              if (_i == response.length -1)
                              {
                                   _all_comments += ' ' + response[_i]['comment'];
                              }
                              else
                              {
                                   _all_comments += ' ' + response[_i]['comment'] +', ';
                              }
                              _is_empty = false;
                         }
                    }

                    // add no comments if no comments could be found
                    if (_is_empty)
                    {
                         _all_comments += 'no comments';

                    }

                    // add a row to the tube info table
                    table_funcs.AddTableRowFirstTdRowName({'comments':_all_comments}, _table_ids[0], 'table-info-clone-comments');
               });
          });

          // Get the sampleTypeVariables
          require(['ajax_calls/GetSampleTypeVars'], function(GetSampleTypeVars)
          {
               GetSampleTypeVars.StartAJAX(tube_id).done(function(response)
               {
                    _each_vars = {};
                    _response_for_local_storage = {};
                    _response_for_local_storage[tube_id] = response;

                    // store response in local storage
                    localStorage.setItem('SAMPLE_TYPE_SPECFIC',
                         JSON.stringify(_response_for_local_storage));

                    for (_i in response)
                    {
                         _each_vars['variableName'] = utils.ReplaceUnderScores(response[_i]['variableName']);
                         _each_vars['variableValue'] = response[_i]['variableValue'];

                         table_funcs.AddTableRowFirstTdRowName(_each_vars, _table_ids[0], 'table-info-clone-sampleTypeVars');
                    }
               });
          });
     }

     return {
          AddBoxGridToHTML:_AddBoxGridToHTML,
          BuildZeroSquareMatix:_BuildZeroSquareMatix,
          InsertIntoMatrix:_InsertIntoMatrix,
          MakeTubeInfoTable:_MakeTubeInfoTable
     }
});
