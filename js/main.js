// append a version to the end of all the script urls for cache busting.
require.config(
{
     urlArgs:"ver=v1.6"
});

require(['router','models/utils'], function(router, utils)
{
     var  _testing_types = {'development': 'testing_on', 'active':'testing_off'},
          _code_status = utils.GetDevelopmentStatus(),
          _testing_status = _testing_types[_code_status];

     // start the router
     router.startRouting(_testing_status);
});
