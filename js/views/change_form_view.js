define(['models/turbo_toggle', 'models/date_funcs', 'models/utils'],function(turbo_toggle, date_funcs, utils)
{

	function _ActionBasedLayoutToggle(action_type)
	{
		// Purpose: Show form fields which need to be shown for the type of action performing.  Example if updating change legend from new_legend to update legend

		switch (action_type)
		{
			case 'update':
			case 'updateTube':
				// change legend
				$('#new_legend').attr('class','hide');
				$('#update_legend').attr('class','show');

				// change submit button
				$('#new_item').attr('class','hide');
				$('#update_item').attr('class','show');

				// change label for comments
				$('label#commentLabel').text('Add a New Comment');
				break;
			case 'dup':
				var 	_dup_tube = JSON.parse(localStorage.CURR_TUBE_INFO),
					_curr_sample_spec =  JSON.parse(localStorage.SAMPLE_TYPE_SPECFIC),
					_format_text_return,
					_key,
					_skip_row,
					_row_text,
					_group_num;

				// show the group of variables that the sampleName belongs to.
				_group_num = turbo_toggle.FindSampleTypeGroup(_dup_tube['sampleType']);

				_AddSampleVariables(_dup_tube['tubeID'], _group_num);

				for (_key in _dup_tube)
				{
					// skip tube id
					if (_key != 'tubeID' && _key != 'intialNumTubes')
					{
						_format_text_return = utils.FormatTextForInput(_key, _dup_tube[_key], 'int_out');

						_skip_row = _format_text_return[1];
	                         _row_text = _format_text_return[0];

						if (!_skip_row)
						{
							if (_key == 'consentYesNo' && _dup_tube[_key] == 1)
							{
								$('#consent_yes').prop('checked', true);
							}
							else if (_key == 'consentYesNo' && _dup_tube[_key] == 0)
							{
								$('#consent_no').prop('checked', true);
							}
							else if (_key == 'openAccess')
							{
								if (_dup_tube[_key] == 0)
								{
									$('#openAccess_opt_out').prop('checked', true);
								}
								else if( _dup_tube[_key] == 1)
								{
									$('#openAccess_opt_in').prop('checked', true);
								}
								else if (_dup_tube[_key] == 2)
								{
									$('#openAccess_not_asked').prop('checked', true);
								}
							}
							else if (_key == 'mta' && _dup_tube[_key] == 1)
							{
								$('#mta').prop('checked', true);
							}
							else if (_key == 'published' && _dup_tube[_key] == 1)
							{
								$('#published').prop('checked', true);
							}
							else
							{
								$('#' + _key).val(_row_text);
							}
						}
					}
				}
				break;
		}
	}

	function _FormBasedLayoutToggle(form_name)
	{
		switch (form_name)
		{
			case 'freezer_form':

				// turn of editing of freezer name because the check of duplicate names has already been performed
				$('#freezerName').prop('readonly','true');
				break;

			case 'box_form':
				var 	_freezer_id = $('input#freezerID').val(),
					_privacy_level = $('#privacyLevel').val();

				// highlight freezer choosen
				if (_freezer_id != '')
				{
					$('tr#freezer_'+ _freezer_id).toggleClass('selected notselected');
				}

				// Show the group list if privacy list is equal to group
				turbo_toggle.ToggleGroupList(_privacy_level);

				// turn off editing of fields which are checked if they already exist in db
				$('#boxName').prop('readonly','true');
				break;

			case 'tube_form':

				break;

			case 'group_form':
				$('#groupName').prop('readonly', 'true');

				// add all group members already in group
				var	_php_vars = utils.PhpVars(),
					_group_members = _php_vars['members_userIDs'].split(','),
					_member_id,
					_user_id,
					_id_pairs,
					_i,
					_tr_row_id;

				// iterate over all of the members in the group and highlight row and add user_id and member_id
				for (_i = 0; _i < _group_members.length-1; _i++)
				{
					// find current id pair of user id and member id
					_id_pairs = _group_members[_i].split('_');

					// find each id
					_user_id = _id_pairs[0];
					_member_id = _id_pairs[1];

					_tr_row_id = 'tr#userID_' + _user_id;

					// click on the row to add it to the group
					$(_tr_row_id).trigger('click');

					// make it so member can not be deleted
					$(_tr_row_id).attr('class', 'selected');

					// add member id to hidden clicked users
					$('input#user_' + _user_id).val(_user_id + '_' + _member_id);
				}



				break;
		}
	}

	function _Render(php_vars, action_status)
	{
		// Purpose: Change the view based the form and action (or status)

		_FormBasedLayoutToggle(php_vars['page']);
		_ActionBasedLayoutToggle(action_status);
	}

	function _AddSampleVariables(tube_id, group_num)
	{
		// (int, str) ->
		// Purpose: Get all of the sample type variables associated with the input tube id.  Find the group number that the sample type belongs to.  Then add the value of the add all of the variable data to the group.

		var 	_var_name,
			_var_val,
			_i;

		// show the group number
		// show the sample type variables associated with the sampleType
		turbo_toggle.ToggleSampleTypeFields(group_num);

		require(['ajax_calls/GetSampleTypeVars'], function(GetSampleTypeVars)
          {
               GetSampleTypeVars.StartAJAX(tube_id).done(function(response)
               {
				// fill in information for the currGroup
				for (_i=0; _i<response.length; _i++)
				{
					_var_name = response[_i]['variableName'];
					_var_val = response[_i]['variableValue'];

					// change any dates to us format
					if(_var_name.indexOf('date') >= 0 || _var_name.indexOf('Date') >= 0)
					{
						_var_val = date_funcs.ChangeDateFormatUS(_var_val);
					}

					// select the current input and change the value to what it was. add the id of the variable to the name
					$('#' + group_num + '_' + _var_name)
						.val(_var_val)
						.attr('name', group_num + '[' + _var_name + '-'+ response[_i]['sampleTypeVariableID'] + ']');
				}
			});
		});
	}

	function _UpdateTubeLayout(sample_type)
	{
		// grey out sampleType update cannot change sample type
		// change to disabled but since it will not submit add a hidden field
		var _add_sample_type_input = document.createElement('input');

		_add_sample_type_input.setAttribute('name', 'sampleType');
		_add_sample_type_input.value = sample_type;
		_add_sample_type_input.className = 'hide';

		// disable sample type
		$('select#sampleType').attr('disabled',true);

		// add hidden sample type input
		$('#create_tube').append(_add_sample_type_input);
	}

	return {
		Render:_Render,
		AddSampleVariables:_AddSampleVariables,
		UpdateTubeLayout:_UpdateTubeLayout
	}
});
