define(['models/box_funcs', 'models/table_funcs', 'models/utils'],function(box_funcs, table_funcs,utils)
{
     function _StartEvents(php_vars)
     {
          // Purose: Add event to the table row of the table listing all boxes which addes the grid showing the inside of the box.

          $('#addedBoxesList >tbody > tr').click(function()
          {
               var  _i,
                    _box_size = $(this).data('x-size'),
                    _box_id = $(this).data('box-id'),
                    _all_data = $(this).data(),
                    _req_required_status,  // 0 : no, 1 : yes
                    _where_clause = '';


               // Delete tube info table already present
               $('#insert-tube-info-tables-here').empty();

               box_funcs.AddBoxGridToHTML(_box_size, _box_id);

               // show the table showing where the tubes are going to be added to
               $('#print_table_div').attr('class', 'row show');

               // scroll to the div where the box was just made
               utils.scroll('#choosen_print_sheet');

               // show the table and directions for adding tubes
               if ((php_vars['page'] === 'tube_form' && php_vars['action'] === 'add') || php_vars['page'] === 'home' || php_vars['page'] === 'remove_tube_form')
               {
                    // php makes different messages under the same select_spots id depending on page.  For tube_form it notifies to select an empty spot or spots to add a sample to.  For remove_tube_form it Select the tube you would like to remove.
                    $('#select_spots').attr('class', 'alert alert-success show');

                    // show the warning These Samples are going to be added to the following box
                    $('#going_to_be_added').attr('class', 'alert alert-warning show');

                    // add information about the location of where the tube is going to be added to here in id #insert_print_info_here

                    // clean out table first
                    $('#insert_print_info_here').empty();

                    table_funcs.AddTableRowFirstTdRowName(_all_data, 'insert_print_info_here', 'clone_row_print_info');

                    if (php_vars['page'] === 'tube_form')
                    {
                         // Empty Clicked_Locs div to make sure no samples are still added
                         $('#Clicked_Locs').empty();

                         // add box id to hidden input fields from box_list.php
                         $('#BoxID').val(_box_id);
                    }
                    else if (php_vars['page'] === 'home')
                    {
                         $('#save-button-home').attr('class', 'col-xs-1 col col-sm-1 col-md-1 show');

                         $('#box-location-modal-button').attr('class', 'col-xs-1 col col-xs-offset-10 col-sm-1 col-sm-offset-10 col-md-offset-10 col-md-1 show');
                    }
               }

               // for removing tube only. If req is required add a 1 to a hidden field reqRequired-id and show the div holding all the otherwise add a 0 and hide div
               if (php_vars['page'] === 'remove_tube_form' && php_vars['action'] === 'deleteTube')
               {
                    // Get status of the box related to if a requisition is required to remove the tube 0:no, 1:yes
                    _req_required_status = $(this).data('reqrequired');

                    switch(_req_required_status)
                    {
                         // req not required
                         case 0:
                              $('#request_form_table_div').attr('class', "col-xs-12 col-sm-12 col-md-12 hide");

                              // change reqRequired-id to 0 so a req will not be looked for durning removal submission
                              $('#reqRequired-id').val(_req_required_status);
                              break;

                         default:
                              $('#request_form_table_div').attr('class', "col-xs-12 col-sm-12 col-md-12 show");

                              // change reqRequired-id to 1 so a req will be required durning removal submission
                              $('#reqRequired-id').val(_req_required_status);
                              break;
                    }
               }
          });
     }

     return {
          StartEvents:_StartEvents
     }
});
