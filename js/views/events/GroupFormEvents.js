define(['models/utils'],function(utils)
{
     function _StartEvents(php_vars)
     {
          // Add/delete a user to the hidden input field Clicked_Users
          $('.add-delete-toggle-user').on('click', function()
          {
               var  _user_id = $(this).attr('id').replace('userID_', ''),
                    _user_status = utils.FindWord($(this).attr('class'), '-user-event'),
                    _new_user_input = document.createElement('input');;

               // change class to delete user to prevent adding user again, change class to selected to turn row yellow
               if (_user_status === 'add-user-event')
               {
                    $(this).attr('class', 'hover_row delete-user-event add-delete-toggle-user selected');

                    // add user to clicked users
                    _new_user_input.id = 'user_' + _user_id;
                    _new_user_input.setAttribute('type', 'hidden');
                    _new_user_input.setAttribute('value', _user_id +'_');
                    _new_user_input.setAttribute('name', 'group[]');
                    $('#Clicked_Users').append(_new_user_input);
               }
               // change class to add user to be able to add user again and class to notselected to turn remove yellow color
               else if (_user_status === 'delete-user-event')
               {
                    $(this).attr('class', 'hover_row add-user-event add-delete-toggle-user notselected');

                    // remove user from clicked users
                    $('input#user_' + _user_id).remove();
               }
          });

          $('form#create_group').submit(function(e)
          {
               var  _non_empty_ids =
                    {
                         'groupName':'Fill out a Group Name.',
                         'reqRequired': 'Fill out Sample Request Form Required'

                    },
                    _empty_status,
                    _user_found = false,
                    _all_users,
                    _group_data = {};

               // Find all users
               _all_users = [];

               $('#Clicked_Users').find('input')
                    .each(function(index, value)
                    {
                         _all_users.push($(this).val()
                              .split('_')[0]); // because data has the format userID_memberID
                    });

               // Find if user is in all users
               _user_found = _all_users.indexOf(php_vars['userID']) > -1 ? true: false;

               // check if form is validated
               if ($('input#form_checked').val() == 0)
               {
                    _empty_status = utils.CheckIfInputsEmpty(_non_empty_ids);

                    // produce a dialog message if anything required not to be empty or equal to null is
                    if (_empty_status !== 'passed')
                    {
                         utils.dialog_window(_non_empty_ids[_empty_status],'#' + _empty_status);
                         return false;
                    }

                    // make sure the current user is choosen in group
                    else if (!_user_found)
                    {
                         utils.dialog_window('Make sure you add yourself to the group', '#Users_List');
     				return false;
                    }

                    // Make sure at least 2 people have been choosen
                    else if (_all_users.length < 2)
     			{
     				utils.dialog_window('Make sure you choose at least 2 people to be in the group', '#Users_List');
     				return false;
     			}

                    // Make sure sample request form question is filled in
                    else if (!$('#reqRequired_no').is(':checked') && !$('#reqRequired_yes').is(':checked'))
                    {
                         utils.dialog_window('Please answer the Sample Request Form Required Question.', '#reqRequired_yes');
                         return false;
                    }

                    else
                    {
                         // build an object for ajax call.  include group to see if this is an update based on action
                         _group_data['group_name'] = $('#groupName').val();
          			_group_data['action'] = php_vars['action'];

                         require(['ajax_calls/CheckDupGroupName'], function(CheckDupGroupName)
                         {

                              CheckDupGroupName.StartAJAX(_group_data).done(function(response)
                              {
                                   CheckDupGroupName.CheckResponse(response, $('#groupName').val());

                              });
                         });
                         return false;
                    }
               }

          });

     }

     return {
          StartEvents:_StartEvents
     }
});
