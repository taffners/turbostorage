define(['models/box_funcs', 'models/table_funcs', 'models/utils'],function(box_funcs, table_funcs,utils)
{
     function _StartEvents(php_vars)
     {
          // Purpose:  Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap

          // make all tables sortable
          $('.dataTable').dataTable(
          {
               // turns off keeping a limited number of records on page.
               paging:false
          });
     }

     return {
          StartEvents:_StartEvents
     }
});
