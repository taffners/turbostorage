define(['models/utils', 'models/turbo_toggle'],function(utils, turbo_toggle)
{
     function _StartEvents()
     {
          $('#show_security_answer_create').on('click', function()
          {
               turbo_toggle.HideShowType('#securityAnswer', '#show_security_answer_create');
          });

          $('#show_security_answer_create2').on('click', function()
          {
               turbo_toggle.HideShowType('#securityAnswer2', '#show_security_answer_create2');
          });

          $('#show_password_create').on('click', function()
          {
               turbo_toggle.HideShowType('#password_create', '#show_password_create');
          });

          $('#show_password').on('click', function()
          {
               turbo_toggle.HideShowType('#inputPassword', '#show_password');
          });

          $('#show_password_create2').on('click', function()
          {
               turbo_toggle.HideShowType('#password_create2', '#show_password_create2');
          });

          $('#get_security_quest_button').on('click', function()
          {
               var  email_address = $(input_emailAddress).val();
               // check that a email address was entered into #input_emailAddress
               if (utils.ValidateEmail(email_address))
               {
                    // call get_secruity_questions ajax
                    require(['ajax_calls/GetSecurityQuestions'], function(GetSecurityQuestions)
                    {
                         GetSecurityQuestions.StartAJAX(email_address).done(function(response)
                         {
                              GetSecurityQuestions.CheckResponse(response);
                              return false;
                         });
                    });


               }
               else
               {
                    utils.dialog_window('Fill in an email address.', '#emailAddress');
                    return false;
               }
               return false;
          });

          $('button#check_security_answers_submit').on('click', function()
          {
               var  _non_empty_ids =
                    {
                         'userID': 'ERROR 404: User Not found',
                         'securityQuestionVal': 'ERROR 404: Security question 1 value',
                         'securityAnswer': 'Fill in Security Answer 1',
                         'securityQuestion2Val': 'ERROR 404: Security question 2 value',
                         'securityAnswer2': 'Fill in Security Answer 2',
                         'password_create': 'Fill in the new password',
                         'password_create2': 'Fill in the confirm password',
                    },
                    _empty_status,
                    _pass1 = $('#password_create').val(),
                    _pass2 = $('#password_create2').val();
               // Make sure everything is not empty
               _empty_status = utils.CheckIfInputsEmpty(_non_empty_ids);

               if (_empty_status !== 'passed')
               {
                    utils.dialog_window(_non_empty_ids[_empty_status],'#' + _empty_status);
                    return false;
               }

               // make sure password is at least 8 charcters long
               else if (_pass1.length < 8)
               {
                    utils.dialog_window('Your password is too short.', '#password_create');
                    return false;
               }

               // Make sure Passwords match
               else if (_pass1 != _pass2)
               {
                    utils.dialog_window('Your password and confirm password do not match.', '#password_create');
                    return false;
               }

               // Perform an ajax call to change the password
               else
               {

                    if ($('#form_checked').val() == 0)
                    {
                         // allows php form to submit
                         $('#form_checked').val('1');

                         // submit form
                         $('form#change_password_form').submit();

                         return false;
                    }
               }

               return false;
          });
     }

     return {
          StartEvents:_StartEvents
     }
});
