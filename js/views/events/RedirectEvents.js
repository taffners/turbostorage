define(['models/utils'],function(utils)
{
     function _StartEvents()
     {
          var  _php_vars = utils.PhpVars(),
               _page = _php_vars['page'] || 'home',
               _action = _php_vars['action'] || '',
               _tube_id,
               _box_id,
               _box_name,
               _clicked_tube_info;

          // activate redirect for update list
          if (_page.indexOf('_change') >= -1 && _action == 'update')
          {
               $('.update-redirect').click(function()
               {
                    // redirect to update
                    window.document.location = $(this).data('href');
               });

               // Deactivate redirect if also clicking on add_comment_button
               $('.update-redirect .add_comment_button').click(function(e)
               {
                    e.stopPropagation();
               });
          }
          else if (_page.indexOf('_change') >= -1 && _action == 'delete')
          {

               $('.delete-redirect').click(function()
               {

                    // delete item using php $_REQUEST
                    var  _item_href = $(this).data('href'),
                         _item_name = $(this).attr('data-itemName'),
                         _buttonArray =
                         {
                             cancel: function()
                             {
                                 $(this).dialog('close');
                             },
                             'yes': function()
                             {
                                 $(this).dialog('close');
                                 window.document.location = _item_href;
                             }
                         };

                    utils.confirmPopUp('<h1>Are you sure you would like to delete ' + _item_name +'. Deletion is Permanent</h1>', _buttonArray);
               });

               $('.delete-redirect .add_comment_button').click(function(e)
               {
                    e.stopPropagation();
               });
          }
          else if (_page == 'home' || _page == 'box_info')
          {
               $('.dup-tube-button').on('click', function()
               {
                    document.location.href = "?page=tube_form&action=add&status=dup";
               });

               $('.update-tube-button').on('click', function()
               {
                    _clicked_tube_info = JSON.parse(localStorage.CURR_TUBE_INFO);

                    _tube_id = _clicked_tube_info['tubeID'];
                    _box_id = _clicked_tube_info['boxID'];
                    _box_name = _clicked_tube_info['boxName'];

                    document.location.href = "?page=tube_form&action=updateTube&tubeID=" + _tube_id + "&boxName=" + _box_name;
               });

          }

          // look at the data only
          $('.view-redirect').click(function()
          {

               // redirect to update
               window.document.location = $(this).data('href');
          });

          $('.view-box-grid-redirect').click(function()
          {
               // Get the boxID of the clicked row
               $(this).find('td').each(function()
               {
                    if ($(this).attr('data-row-name') == 'boxID')
                    {
                         _box_id = $(this).html();
                         return false;
                    }
               });
               // open that box
               $('#box_' + _box_id).click()
          });
     }

     return {
          StartEvents:_StartEvents
     }
});
