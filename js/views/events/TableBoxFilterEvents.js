define(['models/utils'],function(utils)
{
     function _StartEvents(php_vars)
     {
          // Purpose:  Used to filter the box list table.

          var  _curr_class;

          // make filters for box list
          $('#private-box-filter-button').click(function()
          {
               _BoxFilter('private', ['public', 'group']);
          });

          $('#group-box-filter-button').click(function()
          {
               _BoxFilter('group', ['public', 'private']);
          });

          $('#public-box-filter-button').click(function()
          {
               _BoxFilter('public', ['group', 'private']);
          });

          $('#full-box-filter-button').click(function()
          {
               _BoxFilter('full', ['unfull']);
          });

          $('#unfull-box-filter-button').click(function()
          {
               _BoxFilter('unfull', ['full']);
          });

          $('#all-box-filter-button').click(function()
          {
               _BoxFilter('all', []);
          });
     }

     function _BoxFilter(show_class, hide_classes)
     {
          // (str, array)
          var  _curr_class,
               _i;

          $('table#addedBoxesList > tbody > tr').show();

          $('.' + show_class + '-filter').each(function()
          {
               _curr_class = $(this).attr('class');
               _curr_class = utils.ReplaceAll(_curr_class, 'hide', '');

               $(this).attr('class', _curr_class);
          });

          for (_i = 0; _i < hide_classes.length; _i++)
          {
               $('.' + hide_classes[_i] + '-filter').each(function()
               {
                    _curr_class = $(this).attr('class');
                    _curr_class = utils.ReplaceAll(_curr_class, 'hide', '');
                    _curr_class = _curr_class +' hide';

                    $(this).attr('class', _curr_class);
               });

               $(this).hide();
          }
     }

     return {
          StartEvents:_StartEvents
     }
});
