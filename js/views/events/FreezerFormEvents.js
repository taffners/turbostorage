define(['models/utils'],function(utils)
{
     var  _updating;

     function _StartEvents(php_vars)
     {
          _SetUpdatingStatus(php_vars);

          // if -196 is choosen change num of shelfs to 1
          $('#temp').on('change', function()
          {
               if($('#temp').val() == -196)
               {
                    $('#numShelf').val('1');
               }
          });

          // Make sure all of the fields are filled out correctly
          $('form#create_freezer').submit(function(e)
          {
               var  _freezer_name = $('#freezerName').val(),
                    _non_empty_ids =
                    {
                         'freezerName':'Fill out a freezer Name.',
                         'roomNum':'Fill out a room number.',
                         'numShelf':'Fill out number of shelves.'
                    },
                    _empty_status;

               // check if form is validated
               if ($('input#form_checked').val() == 0)
               {
                    _empty_status = utils.CheckIfInputsEmpty(_non_empty_ids);

                    // produce a dialog message if anything required not to be empty or equal to null is
                    if (_empty_status !== 'passed')
                    {
                         utils.dialog_window(_non_empty_ids[_empty_status],'#' + _empty_status);
                         return false;
                    }

                    // make sure freezer name is unique
                    else
                    {

                         require(['ajax_calls/CheckDupFreezerName'], function(CheckDupFreezerName)
                         {

                              CheckDupFreezerName.StartAJAX(_freezer_name).done(function(response)
                              {
                                   CheckDupFreezerName.CheckResponse(response, _updating, _freezer_name);
                                   return false;
                              });
                         });
                         return false;
                    }

               }
          });
     }

     function _SetUpdatingStatus(php_vars)
     {
          if (php_vars.hasOwnProperty('action') && php_vars['action'] === 'update')
          {
               _updating = true;
          }
          else
          {
               _updating = false;
          }
     }

     return {
          StartEvents:_StartEvents
     }
});
