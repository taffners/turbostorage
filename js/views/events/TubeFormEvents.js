define(['models/utils', 'models/turbo_toggle', 'models/date_funcs', 'models/table_funcs'],function(utils, turbo_toggle, date_funcs, table_funcs)
{
     var  _updating;

     function _StartEvents(php_vars)
     {
          // Purpose: Add events toggle sample type variables, show irbs available, change the color of select list What color is your tube label?, and submit tube form

          // Set the variable _updating so the view can be set correctly.
          _SetUpdatingStatus(php_vars);

          // Activate the sample type extra field listener
          $('#sampleType').change(function()
          {
               var  _group = turbo_toggle.FindSampleTypeGroup($(this).val());

               // hide all sample type variables already choosen
               turbo_toggle.HideAllSampleTypeGroups();

               // un hide the new sample type variables
               turbo_toggle.ToggleSampleTypeFields(_group);
          });

          // Show the correct information about IRB Approval
          $('#consentApprovalDate').change(function()
          {
               var  _consent_date = $('#consentApprovalDate').val(),
                    _irb_info;

               // check that a date was entered in us format
               if (date_funcs.ValidateDate(_consent_date))
               {
                    // clean out place to insert table
                    $('#insert-consent-irb-info-here').empty();

                    // use date_funcs to find what irb information pertains
                    _irb_info = date_funcs.NearestConsentDateInfo(_consent_date);

                    // copy table
                    _table_ids = table_funcs.CloneDivAddTable('consent-irb-info-clone-div', 1, 'QueryInfo', 'insert-consent-irb-info-here',
                    {'button_status' : 'off'}, 'consent-irb-info', 'clone-header');

                    // then clone consent info and and too div
                    table_funcs.AddTableRowFirstTdRowName(_irb_info, _table_ids[0], 'consent-irb-info-clone-IRBQuestion');

                    table_funcs.AddTableRowFirstTdRowName(_irb_info,_table_ids[0], 'consent-irb-info-clone-answer');

                    // add a header to the table
                    $('#thead-clone-header-1').text('IRB Approval Information');

                    // show table
                    $('#insert-consent-irb-info-here').attr('class', 'show');

               }
               // otherwise hide the table
               else
               {
                    $('#insert-consent-irb-info-here').attr('class', 'hide');

                    utils.dialog_window('Make sure a valid consent date was entered in MM/DD/YYYY format', '#consentApprovalDate');
               }
          });

          // Make sure all the fields are filled out correctly and then submit the tube
          $('form#create_tube').submit(function(e)
          {
               var  _non_empty_ids =
                    {
                         'sampleName':'Fill out Sample Name.',
                         'BoxID':'Make sure you choose a box.',
                         'sampleType':'Make sure you choose a sample type.'
                    },
                    _empty_status,
                    _box_data = {};

               // check if form is validated
               if ($('input#form_checked').val() == 0)
               {
                    _empty_status = utils.CheckIfInputsEmpty(_non_empty_ids);

                    // produce a dialog message if anything required not to be empty or equal to null is
                    if (_empty_status !== 'passed')
                    {
                         utils.dialog_window(_non_empty_ids[_empty_status],'#' + _empty_status);
                         return false;
                    }
                    // make sure a space was clicked
     			else if ($('div#Clicked_Locs:empty').is(':empty'))
     			{
     				utils.dialog_window('Make sure you click on the grid to add your tubes', '#insertBoxHere');
     				return false;
     			}
                    // Make sure date formats are correct
                    else if ($('#freezeDate').val() != '' & !date_funcs.ValidateDate($('#freezeDate').val()))
                    {
                         utils.dialog_window('Make sure a valid freeze date was entered in MM/DD/YYYY format', '#freezeDate');
                         return false;

                    }

                    else if ($('#consentApprovalDate').val() != '' & !date_funcs.ValidateDate($('#consentApprovalDate').val()))
                    {
                         utils.dialog_window('Make sure a valid consent date was entered in MM/DD/YYYY format', '#consentApprovalDate');
                         return false;

                    }

                    else if ($('#Group1_Expiration_Date').val() != '' & !date_funcs.ValidateDate($('#Group1_Expiration_Date').val()))
                    {
                         utils.dialog_window('Make sure a valid expiration date was entered in MM/DD/YYYY format', '#Group1_Expiration_Date');
                         return false;

                    }

                    else if ($('#Group5_Expiration_Date').val() != '' & !date_funcs.ValidateDate($('#Group5_Expiration_Date').val()))
                    {
                         utils.dialog_window('Make sure a valid expiration date was entered in MM/DD/YYYY format', '#Group5_Expiration_Date');
                         return false;

                    }

                    else if ($('#Group6_Expiration_Date').val() != '' & !date_funcs.ValidateDate($('#Group6_Expiration_Date').val()))
                    {
                         utils.dialog_window('Make sure a valid expiration date was entered in MM/DD/YYYY format', '#Group6_Expiration_Date');
                         return false;

                    }
                    // if everything is filled out change form_checked to 1 so php can run
                    else
                    {
                         // allows php form to submit
                         $('input#form_checked').val('1');

                         // change all date formats from mm/dd/yyyy to YYYY-MM-DD
                         $('.date-picker').each(function()
                         {
                              var curr_date_format = $(this).val();

                              // Make sure field is not empty
                              if (curr_date_format != '')
                              {
                                   $(this).val(date_funcs.ChangeDateFormatEuropean(curr_date_format));
                              }
                         });

                         // submit form
                         $('form#create_tube').submit();
                    }
               }
               return true;
          });

     }

     function _SetUpdatingStatus(php_vars)
     {
          if (php_vars.hasOwnProperty('action') && php_vars['action'] === 'update')
          {
               _updating = true;
          }
          else
          {
               _updating = false;
          }
     }

     return {
          StartEvents:_StartEvents
     }
});
