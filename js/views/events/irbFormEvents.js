define(['models/utils'],function(utils)
{
     function _StartEvents()
     {
          $('form#create_irb').submit(function()
          {
               var  _non_empty_ids =
                    {
                         'irbProjectTitle':'Fill out IRB project Title.',
                         'irbPI':'Fill out a PI.'
                    },
                    _empty_status;
               // check if form is validated
               if ($('input#form_checked').val() == 0)
               {
                    _empty_status = utils.CheckIfInputsEmpty(_non_empty_ids);

                    // produce a dialog message if anything required not to be empty or equal to null is
                    if (_empty_status !== 'passed')
                    {
                         utils.dialog_window(_non_empty_ids[_empty_status],'#' + _empty_status);
                         return false;
                    }
                    else
                    {

                         // allows php form to submit
                         $('input#form_checked').val('1');

                         // submit form
                         $('form#create_irb').submit();

                    }
               }

          });
     }
     return {
          StartEvents:_StartEvents
     }
});
