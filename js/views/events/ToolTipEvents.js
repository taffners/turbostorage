define(function()
{
     function _StartEvents()
     {
          $('[data-toggle="tooltip"]').tooltip();
     }

     return {
          StartEvents:_StartEvents
     }
});
