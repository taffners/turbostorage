define(['models/turbo_toggle', 'models/utils'], function(turbo_toggle, utils)
{
     function _StartEvents()
     {
          // create New user button hides user login and unhides create a user
          $('#create_user_button').on('click', function()
          {
               // hide user login
               $('#user_login_form').attr('class','form-horizontal hide');

               // show create user form
               $('#create_user').attr('class','form-horizontal show');
          });

          // activate cancel of user create form to reset page
          $('#cancel_creating_user').on('click',function()
          {
               window.location.href = "index.php";
          });

          // bring up sucks to be u with forgot password
          $('#forgot_password').on('click', function()
          {
               // change page to change_password
               window.location.href = "?page=change_password";
          });

          // Activate Security question change.  If either security Question list was clicked disable selected that value in the other security question list
          $('#securityQuestion').on('change', function()
          {
               turbo_toggle.SecurityQuestions('1', $(this).val(), 'securityQuestion');
          });

          $('#securityQuestion2').on('change', function()
          {
               turbo_toggle.SecurityQuestions('2', $(this).val(), 'securityQuestion');
          });

          // Activate all of the show buttons to toggle from show to hide
          // change fields from hidden to shown when clicking show
          $('#show_security_answer_create').on('click', function()
          {
               turbo_toggle.HideShowType('#securityAnswerCreate', '#show_security_answer_create');
          });

          $('#show_security_answer_create2').on('click', function()
          {
               turbo_toggle.HideShowType('#securityAnswerCreate2', '#show_security_answer_create2');
          });

          $('#show_password_create').on('click', function()
          {
               turbo_toggle.HideShowType('#password_create', '#show_password_create');
          });

          $('#show_password').on('click', function()
          {
               turbo_toggle.HideShowType('#inputPassword', '#show_password');
          });

          $('#show_password_create2').on('click', function()
          {
               turbo_toggle.HideShowType('#password_create2', '#show_password_create2');
          });


          // check to see if everything is filled incorrectly before submitting create user
          $('form#create_user').submit(function(e)
          {
               // vars
               var  email_address = $('#emailAddress').val(),
                    tel_num  = $('#phoneNumber').val(),
                    pass1 = $('#password_create').val(),
                    pass2 = $('#password_create2').val(),
                    _non_empty_ids =
                         {
                              'firstName':'Fill out your first name.',
                              'lastName':'Fill out your last name.',
                              'emailAddress': 'Fill in an email address.',
                              'tel_num': 'Fill in a phone number.',
                              'labName': 'Fill in a lab name.',
                              'securityQuestion': 'Choose a security Question 1.', 'securityAnswerCreate':'Answer the security question 1.',
                              'securityQuestion2': 'Choose a security Question 2.',
                              'securityAnswerCreate2': 'Answer the security question 2.'
                         },
                    _empty_status;

               // check if form is validated
               if ($('input#form_checked_create').val() == 0)
               {
                    _empty_status = utils.CheckIfInputsEmpty(_non_empty_ids);

                    // produce a dialog message if anything required not to be empty or equal to null is
                    if (_empty_status !== 'passed')
                    {
                         utils.dialog_window(_non_empty_ids[_empty_status],'#' + _empty_status);
                         return false;
                    }
                    // makes sure email address is formated as an email address
                    else if (!utils.ValidateEmail(email_address))
                    {
                         utils.dialog_window('Fill in an email address.', '#emailAddress');
                         return false;
                    }

                    // Make sure phone Number is filled in and is the length of 11
                    else if (!utils.ValidateOnlyIntPhoneNum(tel_num))
                    {
                         utils.dialog_window('Fill in a correct format of a phone number. Include at least 10 digits and include only ints', '#phoneNumber');
                         return false;
                    }

                    // make sure password is at least 8 charcters long
                    else if (pass1.length < 8)
                    {
                         utils.dialog_window('Your password is too short.', '#password_create');
                         return false;
                    }

                    // make sure the confirm password is the same as the orginal
                    else if (pass1 != pass2)
                    {
                         utils.dialog_window('Your password and confirm password do not match.', '#password_create');
                         return false;
                    }

                    // perform ajax to check if email address is already in database
                    else
                    {
                         // perform an ajax call to see if email address is already in the database.  If it is notify user.  If not add new user.
                         require(['ajax_calls/CheckDupEmail'], function(CheckDupEmail)
                         {
                              CheckDupEmail.StartAJAX(email_address).done(function(response)
                              {
                                   CheckDupEmail.CheckResponse(response);
                                   return false;
                              });
                         });
                         return false;
                    }
               }
          });

          // check to make sure email and password is filled in before trying to login
          $('form#user_login_form').submit(function(e)
          {
               var  email_address = $('#inputEmailAddress').val(),
                    _non_empty_ids =
                         {
                              'inputEmailAddress':'Fill out the email address',
                              'inputPassword':'Fill out the password'
                         },
                    _empty_status;
               // check if form is validated
               if ($('input#form_checked_login').val() == 0)
               {
                    _empty_status = utils.CheckIfInputsEmpty(_non_empty_ids);

                    // // check if emailAddress is filled in
                    if (_empty_status !== 'passed')
                    {
                         utils.dialog_window(_non_empty_ids[_empty_status],'#' + _empty_status);
                         return false;
                    }

                    // makes sure email address is formated as an email address
                    else if (!utils.ValidateEmail(email_address))
                    {
                         utils.dialog_window('Fill in an email address.', '#emailAddress');
                         return false;
                    }
                    else
                    {
                         return true;
                    }
               }
          });

     }

     return {
          StartEvents:_StartEvents
     }

});
