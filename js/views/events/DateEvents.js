define(['models/date_funcs'],function( date_funcs)
{
     function _StartEvents(php_vars)
     {
          // Purpose: Add an event to all buttons to add todays date.

          // add today's date to input
          $('.todays-date').click(function()
          {
               // how to use:  on button with a class todays-date add a data-input-id equal to the id of the input where you would like today's date added.  This function adds today's date in us format.
               var  _input_id = $(this).data('input-id');

               $('#' + _input_id).val(date_funcs.GetTodaysDate());
          });
     }

     return {
          StartEvents:_StartEvents
     }
});
