define(['models/utils'],function(utils)
{
     function _StartEvents()
     {
          // Purpose: add an event to all the comment buttons to open the comment modal.

          $('.add_comment_button').on('click', function()
          {
               // how to use:  on button with a class add_comment_button add data-comment-type equal to type of comment (freezer, box, sample, request, group).  If a different type of comment is required add to class.database.php.  Also add data-comment-id equal to the comment id under the button with add_comment_button as class.

               var  _comment_type = $(this).attr('data-comment-type'),
                    _comment_id = $(this).attr('data-comment-id');

               utils.AddComments(_comment_type, _comment_id);
          });
     }

     return {
          StartEvents:_StartEvents
     }
});
