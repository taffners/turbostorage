define(['models/utils', 'models/date_funcs'],function(utils, date_funcs)
{

     function _StartEvents(php_vars)
     {
          $('form#create_request').submit(function(e)
          {
               var  _freezer_name = $('#freezerName').val(),
                    _non_empty_ids =
                    {
                         'piName':'Fill out a PI Name.',
                         'piEmail':'Fill out a PI Email Address.',
                         'requestDate':'Fill out Request Date.',
                         'requestors_irb_num':'Fill out a Requestors IRB Num.',
                         'sampleType':'Fill out sample Type.',
                         'researchAbstract':'Fill out a research abstract.',
                         'data_contribution':'Fill Out Contribution of Data.',
                         'quantiy': 'Fill out quanity requested',
                         'quantiyUnits': 'Fill out quanity units',
                         'sampleIDsRequested': "Fill out sample ID's Requested"
                    },
                    _empty_status;

               // check if form is validated
               if ($('input#form_checked').val() == 0)
               {
                    _empty_status = utils.CheckIfInputsEmpty(_non_empty_ids);

                    // produce a dialog message if anything required not to be empty or equal to null is
                    if (_empty_status !== 'passed')
                    {
                         utils.dialog_window(_non_empty_ids[_empty_status],'#' + _empty_status);
                         return false;
                    }

                    // Make sure dates are in vaild format
                    else if ($('#ApproveDate').val() != '' && !date_funcs.ValidateDate($('#ApproveDate').val()))
     			{
     				utils.dialog_window('Make sure a valid approval date was entered in MM/DD/YYYY format', '#ApproveDate');
     				return false;

     			}

     			else if ($('#requestDate').val() != '' && !date_funcs.ValidateDate($('#requestDate').val()))
     			{
     				utils.dialog_window('Make sure a valid request date was entered in MM/DD/YYYY format', '#requestDate');
     				return false;

     			}

                    // makes sure email address is formated as an email address
                    else if (!utils.ValidateEmail($('#piEmail').val()))
                    {
                         utils.dialog_window('Fill in an email address.', '#piEmail');
                         return false;
                    }

                    else if ($('#labContactEmail').val() != '' && !utils.ValidateEmail($('#labContactEmail').val()))
                    {
                         utils.dialog_window('Fill in an email address.', '#labContactEmail');
                         return false;
                    }

                    // Make sure phone Number is filled in and is the length of 11
                    else if ($('#labContactPhone').val() != '' && !utils.ValidateOnlyIntPhoneNum($('#labContactPhone').val()))
                    {
                         utils.dialog_window('Fill in a correct format of a phone number. Include at least 10 digits and include only ints', '#labContactPhone');
                         return false;
                    }

                    else
                    {
                         $('.date-picker').each(function()
                         {
                              var curr_date_format = $(this).val();

                              // Make sure field is not empty
                              if (curr_date_format != '')
                              {
                                   $(this).val(date_funcs.ChangeDateFormatEuropean(curr_date_format));
                              }
                         });

                         // allows php form to submit
                         $('input#form_checked').val('1');

                         // submit form
                         $('form#create_request').submit();
                    }
               }
          });

     }

     return {
          StartEvents:_StartEvents
     }
});
