define(['models/utils', 'models/tube_funcs'],function(utils, tube_funcs)
{
     function _StartEvents()
     {
          // Make sure all of the fields are filled out correctly
          $('form#remove_tube').submit(function(e)
          {
               // check if form is validated
               if ($('input#form_checked').val() == 0)
               {
                    // Make sure a request was clikced if it is required for the box
                    if ($('#reqRequired-id').val() == 1 && ($('#clicked-request-id').val() == ''))
                    {

                         utils.dialog_window('Please click on the request form that this removal is linked to.  The request needs to be approved before the form will be found here and before you can remove any of these samples.', '#outstanding-requests-fieldset');
                         return false;
                    }

                    // Make sure they clicked on a tube to remove
                    else if  ( $('#clicked-tubes').is(':empty'))
                    {
                         utils.dialog_window('Please click on a tube you would like to remove.', '#choosen_print_sheet');

                         return false;
                    }

                    // Make sure all of the fields in clicked-tubes exist
                    else if (!tube_funcs.CheckVaildClickedLoc())
                    {
                         utils.dialog_window('Something went wrong with selecting the tube', '#choosen_print_sheet');
                    }

                    // otherwise submit for removal
                    else
                    {
                         // ask the user if they would really like to remove this sample. Submit for removal if they choose yes
                         var  _button_array =
                              {
                                   cancel: function()
                                   {
                                        $(this).dialog('close');

                                   },
                                   'yes': function()
                                   {
                                        $(this).dialog('close');

                                        // allows php form to submit
                                        $('input#form_checked').val('1');

                                        // submit form
                                        $('form#remove_tube').submit();

                                   }
                              },
                              _tube_name = JSON.parse(localStorage.CURR_TUBE_INFO)['sampleName'];

                         // ask user if they would like to remove
                         utils.confirmPopUp('<h1>Are you sure you would like to delete the tube '+ _tube_name +'. Deletion is Permanent</h1>', _button_array);

                         return false;
                    }
               }
          });
     }

     return {
          StartEvents:_StartEvents
     }
});
