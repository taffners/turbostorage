define(['models/tube_search', 'models/utils'],function(tube_search,utils)
{
     var  _updating;

     function _StartEvents(php_vars)
     {
          // Purose: Add searching for tubes while pressing enter  to an input which has a class of .searchPressEnter, if the button has an id of searchTubes search for the database for a tube containing the value of the input TubeSearchTerm.  Also add to the input with id TubeSearchTerm an autocompletion of sample Names after 2 chars are entered

          var  _page = php_vars['page'],
               _search_term;

          // activate search if enter is pressed also inside search field
          $('.searchPressEnter').keypress(function(e)
          {
               _search_term = $(this).val();

               // Search if enter was pressed
               if(e.which == 13 && _search_term !== '')
               {
                    _SearchEvent(_search_term, _page);
               }
          });

          $('#searchTubes').on('click', function()
          {
               _search_term = $('#TubeSearchTerm').val();

               if (_search_term !== '')
               {
                    _SearchEvent(_search_term, _page);
               }
          });

          // Set autocomplete on TubeSearchTerm
          tube_search.AutoComplete('#TubeSearchTerm', 'sampleName', _page);
     }

     function _SearchEvent(search_term, page)
     {
          // Purpose: Activate search when either enter is pushed or the search button is push          {ed

          tube_search.Search(search_term, 'insert_search_results_here','clone_row', page);

          // add all redirect events to the newly made table
          // utils.ReloadJs('views/events/RedirectEvents');
     }

     return {
          StartEvents:_StartEvents
     }
});
