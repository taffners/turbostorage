define(['models/box_funcs', 'models/utils', 'models/tube_funcs'],function(box_funcs, utils, tube_funcs)
{
     function _StartEvents(php_vars)
     {
          // Purpose: Add all the events related to the tube info table and adding a tube to an empty space in the box.

          // if the page is equal to tube_form add a clicking for adding a tube to it
          if (php_vars['page'] === 'tube_form' && php_vars['action'] === 'add')
          {
               // show a list of all the boxes
               $("#boxTable_div").attr('class', 'show');

               $('.emptyCell').click(function()
               {
                    var  _curr_class = $(this).attr('class'),
                         _id = $(this).attr('id');

                    if (_curr_class === 'clickedEmpty')
                    {
                         // remove the div from the html since this cell was unclicked
                         $('#space_' + _id).remove();
                    }
                    else if (_curr_class === 'emptyCell')
                    {
                         var  _x, _y,
                              _r = $(this).attr('data-r'),
                              _c = $(this).attr('data-c'),
                              _div_space = document.createElement('div');

                         // Add a div to store all of the inputs for the clicked cell
                         _div_space.id = 'space_' +_id;

                         _x = _div_space.appendChild(document.createElement('input'));

                         _x.type = 'hidden';
                         _x.name = 'space[' + _id + '][x]';
                         _x.value = _r;

                         _y = _div_space.appendChild(document.createElement('input'));

                         _y.type = 'hidden';
                         _y.name = 'space[' + _id + '][y]';
                         _y.value = _c;

                         $('#Clicked_Locs').append(_div_space);
                    }

                    $(this).toggleClass('emptyCell clickedEmpty');
               });
          }

          // show the tube info table
          $('.fullCell').click(function()
          {
               var  _curr_class = $(this).attr('class'),
                    _tube_id = utils.FindWordInStrAndCut(_curr_class, 'group_num_')

               // Delete table already present
               $('#insert-tube-info-tables-here').empty();

               // add tube table
               box_funcs.MakeTubeInfoTable(_tube_id, {
                    'button_status': 'on',
                    'on_buttons': ['dup-tube-button', 'update-tube-button']
               });

               // Unhighlight all tubes already clicked with class activeFullCell
               $('.activeFullCell').each(function()
               {
                    var  _c = $(this).attr('class').replace('activeFullCell', '');

                    // reset class
                    $(this).attr('class', _c);
               })

               // hightlight with a blue line only the tube clicked on because this is the tube which will be removed.  Also add that tube the clicked-tubes div for removal
               if (php_vars['page'] === 'remove_tube_form')
               {
                    $(this).attr('class', 'activeFullCell ' +_curr_class);

                    // call tube_funcs to set currenlty clicked tube for removal and remove any previously clicked tubes.  Only one tube at a time can be removed.
                    tube_funcs.PrepareToRemoveTube($(this).attr('id'));
               }

               // hightlight all of the tubes related to the tube info table
               else
               {
                    $('.group_num_' + _tube_id).each(function()
                    {

                         $(this).attr('class', 'activeFullCell ' +_curr_class);
                    });
               }
          });

          // delete the tube info table
          $('.delete-tube-info-table').click(function()
          {
               var  _tube_id = $(this).attr('id').replace('delete-tube-info-table-', '');

               // delete div with tube info table in it
               $('#tube-info-div-' +_tube_id ).remove();
          });
     }

     return {
          StartEvents:_StartEvents
     }
});
