define(['models/modals'],function(modals)
{
     function _StartEvents()
     {
          $('#box-location-modal-button').on('click',function()
          {
               modals.ToggleModal('modal-box-location-info');
          });
     }

     return {
          StartEvents:_StartEvents
     }
});
