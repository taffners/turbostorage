define(['models/utils','ajax_calls/GetBoxInfoCSV'],function(utils, GetBoxInfoCSV)
{
     function _StartEvents(php_vars)
     {
          $('#print-box-info-tube-report-button').click(function()
          {
               // select all of the delete-tube-info-table buttons and hide them
               $('.delete-tube-info-table').each(function()
               {
                    // if the button is not the the clone button hide the button
                    if ($(this).attr('id')!= 'delete-tube-info-table-clone')
                    {
                         $(this).attr('class', 'hide');
                    }
               });

               // change column to be larger for printing
               $('#insertBoxHere').attr('class', 'col-xs-12 col-sm-12 col-md-8')

               //
               $('#turbo-storage-icon-printing').attr('class', 'container-fluid TopName show');

               // change the size of the grid
               $('td.headerCell').each(function()
               {
                    $(this).css('width', '75px');
                    $(this).css('height', '75px');
               });

               // save
               utils.SaveAsPDF('insertBoxHere'); // just box
               // utils.SaveAsPDF('choosen_print_sheet'); // full sheet

               // change class back to normal
               $('#insertBoxHere').attr('class', 'col-xs-12 col-sm-12 col-md-6')

               // change the size of the grid back
               $('td.headerCell').each(function()
               {
                    $(this).css('width', '50px');
                    $(this).css('height', '50px');
               });

               $('#turbo-storage-icon-printing').attr('class', 'container-fluid TopName hide');
          });

          // save a csv of the grid and all the tube info inside the box for all box rows clicked
          if (php_vars['page'] === 'box_info' && php_vars['action'] === 'report')
          {
               $('#addedBoxesList >tbody > tr').click(function()
               {
                    var _box_id = $(this).data('box-id');

                    GetBoxInfoCSV.StartAJAX(_box_id);

               });

               $('#save-all-box-info').click(function()
               {
                    $('#addedBoxesList').find('tr').each(function()
                    {
                         var _box_id = $(this).data('box-id');
                         // Get all tubes in a box and print

                         GetBoxInfoCSV.StartAJAX(_box_id);
                    });
               });
          }
     }

     return {
          StartEvents:_StartEvents
     }
});
