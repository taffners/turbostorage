define(['models/utils', 'models/turbo_toggle'],function(utils, turbo_toggle)
{
     var  _updating;


     function _StartEvents(php_vars)
     {
          // Set the variable _updating so the view can be set correctly.
          // Reasons why:
               // turn off checking if box name is already there because obviously it is cause we are updating it.
          _SetUpdatingStatus(php_vars);

          // show or hide group list depending on privacy level
          $('#privacyLevel').on('change', function()
          {
               var  _privacy_level = $(this).val();

               turbo_toggle.ToggleGroupList(_privacy_level);
          });

          // When typing in the xSize box add the same thing to ySize since the box can only be square.  Also do the same for ySize typing.
          utils.CopyWhileTyping('xSize', 'ySize');
          utils.CopyWhileTyping('ySize', 'xSize');

          // hightlight the freezer info row in the table and add freezer id to hidden input #freezerID
          $('.freezer_info_row').on('click', function()
          {
               var  _freezer_id = $(this).attr('id').split('_')[1],
				_num_shelf = $('#freezer_'+ _freezer_id).data('numshelves');

               // set max of numShelfs depending on clicked
			$('#numShelf').attr('max', _num_shelf);

               // set freezer id
               $('#freezerID').val(_freezer_id);

               // row color section
               // change all rows to grey
               $('#FreezerInfo tbody').children('tr')
                    .each(function(index, value)
                    {
                         $(this).attr('class','hover_row freezer_info_row notselected');
                    });

               //change Selected color of clicked row
               $(this).toggleClass('hover_row freezer_info_row selected', 'hover_row freezer_info_row notselected');
          });

          // if the comment button is clicked stop highlighting of row
          $('.freezer_info_row .add_comment_button').click(function(e)
          {
               e.stopPropagation();
          });

          // Make sure all of the fields are filled out correctly
          $('form#create_box').submit(function(e)
          {
               var  _non_empty_ids =
                    {
                         'boxName':'Fill out a box Name.',
                         'xSize':'Make sure you fill in the x and y size.',
                         'ySize':'Make sure you fill in the x and y size.',
                         'freezerID':'Please click on a freezer to add the box to',
                         'numShelf':'Please add a shelf number',
                         'rackNum':'Please add a rack number',
                         'rackSpaceNum':'Please add a rack space number',
                         'privacyLevel':'Please check a privacy level',
                    },
                    _empty_status,
                    _box_data = {};

               // check if form is validated
               if ($('input#form_checked').val() == 0)
               {
                    _empty_status = utils.CheckIfInputsEmpty(_non_empty_ids);

                    // produce a dialog message if anything required not to be empty or equal to null is
                    if (_empty_status !== 'passed')
                    {
                         utils.dialog_window(_non_empty_ids[_empty_status],'#' + _empty_status);
                         return false;
                    }

                    // Make sure x and y equal to each other
                    else if ($('#xSize').val() == '' || $('#ySize').val() == '')
                    {
                         // error message
                         dialog_window('Make sure you fill in the x and y size.', '#box_size_fieldset');
                         return false;
                    }

                    // make sure box name & box loc is unique
                    else
                    {
                         // make dataset to send in ajax
                         _box_data['boxID'] = $('#boxID').val();
					_box_data['box_name'] = $('#boxName').val();
					_box_data['freezerID'] = $('#freezerID').val();
					_box_data['numShelf'] = $('#numShelf').val();
					_box_data['rackNum'] = $('#rackNum').val();
					_box_data['rackSpaceNum'] = $('#rackSpaceNum').val();

                         require(['ajax_calls/CheckDupBox'], function(CheckDupBox)
                         {

                              CheckDupBox.StartAJAX(_box_data).done(function(response)
                              {
                                   CheckDupBox.CheckResponse(response, _updating, _box_data['box_name']);
                                   return false;
                              });
                         });
                         return false;
                    }
               }
          });
     }


     function _SetUpdatingStatus(php_vars)
     {
          if (php_vars.hasOwnProperty('action') && php_vars['action'] === 'update')
          {
               _updating = true;
          }
          else
          {
               _updating = false;
          }
     }


     return {
          StartEvents:_StartEvents
     }
});
