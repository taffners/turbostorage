define(function()
{
     function _StartEvents()
     {
          // Purpose: During removal of a tube and the table row of a table with id addedRequestsList is clicked add the id of the clicked request to clicked-request-id

          var  _req_id;

          $('#addedRequestsList > tbody > tr').on('click', function()
          {
               // Change color of all selected request form row back to grey
                    // change all rows to grey
                    $('#addedRequestsList > tbody').children('tr')
                         .each(function(index, value)
                         {
                              $(this).attr('class','notselected');
                         });

                    // change color to yellow of selected row
                    $(this).toggleClass('selected notselected');

               // Now add an input to clicked-request div

                    // add new reqID val
                         _req_id = $(this).attr('id').split('_')[1];

                         $('#clicked-request-id').val(_req_id);        
          });
     }

     return {
          StartEvents:_StartEvents
     }
});
