define(function()
{
     function _StartAJAX(box_id)
     {
          // Purpose: use an ajax call to all the samples in a box
          return $.ajax({
               cache:    false,
               url:      'utils/getSamples.php?',
               dataType: 'json',
               data: {boxID:box_id}
          });
     }



     return {
          StartAJAX:_StartAJAX          
     }

});
