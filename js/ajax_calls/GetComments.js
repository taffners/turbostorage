define(['models/utils', 'models/modals'],function(utils, modals)
{
     function _StartAJAX(comment_type, ref_id)
     {
          // Purpose: use an ajax call to find if the email address is already in the database as a use.  Return an ajax object.
          return $.ajax({
               cache:    false,
               url:      'utils/getComments.php',
               dataType: 'json',
               data: {'type': comment_type, 'refID':ref_id}
          });
     }

     function _CheckResponse(response, comment_type)
     {
          var  _comment_text = '',
               _object_nam,
               _modal_title,
               _modal_body_list;

          modals.CleanModal('modalInfoTitle', 'modal_body_id_ul');

          // add comment to modal if there is a comment
          if (response.length > 0)
          {
               // Add title to modal

               // Get the Name of the item or set it to the commentRef number
               _object_nam = response[0][comment_type+'Name'] === undefined ? response[0]['commentRef']: response[0][comment_type+'Name'];

               _modal_title = 'Comments for the ' + comment_type + ': <b>' + _object_nam+'</b>'

               // add modal title
               modals.AddTitle('modalInfoTitle', _modal_title);

               // Add response to body of modalcomment_text
               // iterate over all comments and add to modal body
               for (i=0; i<response.length; i++)
               {

                    // if if comment is equal to NULL
                    if (response[i]['comment'] == null)
                    {
                         modals.AddListElmToBody('modal_body_id_ul', 'This '+ comment_type + ' Does not have any comments.');
                    }
                    else
                    {
                         modals.AddListElmToBody('modal_body_id_ul', utils.NewLineToHTML(response[i]['comment']));
                    }
               }
          }
          else
          {
               // Add title to modal
               modals.AddTitle('modalInfoTitle', 'No Comments found for this ' + comment_type);

               // add to body that no comments were found
               modals.AddListElmToBody('modal_body_id_ul', 'No Comments found');
          }

          // toggle modal
          modals.ToggleModal('modalInfo');
     }

     return {
          StartAJAX:_StartAJAX,
          CheckResponse:_CheckResponse
     }

});
