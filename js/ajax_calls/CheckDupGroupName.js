define(['models/utils'],function(utils)
{
     function _StartAJAX(group_data)
     {
          // Purpose: use an ajax call to find if the email address is already in the database as a use.  Return an ajax object.
          return $.ajax({
               cache:    false,
               url:      'utils/getGroups.php',
               dataType: 'text',
               data: group_data
          });
     }

     function _CheckResponse(response, group_name)
     {
          // If the group name was already added and action is not update error out

          if (response === 'group name already used')
          {
               utils.dialog_window('The Group Name ' + group_name + ' has already been used before.  Choose another name.', '#groupName');
          }

          else if (response === 'Passed Tests')
          {  
               // allows php form to submit
               $('input#form_checked').val('1');

               // submit form
               $('form#create_group').submit();
          }

          else
          {
               utils.dialog_window('Something went wrong with ajax call', '#groupName');
          }
     }

     return {
          StartAJAX:_StartAJAX,
          CheckResponse:_CheckResponse
     }

});
