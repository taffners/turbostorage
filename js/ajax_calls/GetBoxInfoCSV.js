define(function()
{
     function _StartAJAX(box_id)
     {
          // Purpose: use an ajax call to find all tube_ids in a box in the database.  Return an ajax object.

          return $.ajax({
               cache:    false,
               url:      'utils/GetBoxInfoCSV.php',
               dataType: 'json',
               data: {'box-id': box_id}
          });
     }

     return {
          StartAJAX:_StartAJAX
     }

});
