define(['models/utils', 'models/modals'],function(utils, modals)
{
     function _StartAJAX(tube_id)
     {
          // Purpose: use an ajax call to find if the email address is already in the database as a use.  Return an ajax object.
          return $.ajax({
               cache:    false,
               url:      'utils/getSampleTypeVariables.php',
               dataType: 'json',
               data: {'tubeID':tube_id}
          });
     }

     return {
          StartAJAX:_StartAJAX
     }

});
