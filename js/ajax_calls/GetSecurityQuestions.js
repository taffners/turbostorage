define(['models/utils', 'models/turbo_toggle'],function(utils, turbo_toggle)
{
     function _StartAJAX(email_address)
     {
          // Purpose: use an ajax call to find if the email address is already in the database as a use.  Return an ajax object.
          return $.ajax({
               cache:    false,
               url:      'utils/GetSecurityQuestions.php',
               dataType: 'json',
               data: {'new_email': email_address}
          });
     }

     function _CheckResponse(response)
     {
          // Purpose: Check if response from the above ajax call and see if the email address was already in the database.  If so tell the user.  If not submit form.

          // check if email address is already in user table
          if (response['test_status'] === 'Email Address Not Found')
          {
               utils.dialog_window('This email address is not registered in Turbo Storage.', '#emailAddress');
               return false;
          }
          // if not submit form
          else if (response['test_status'] === 'Passed Tests')
          {
               // add userID to hidden field
               $('#userID').val(response[0]['userID']);

               // add security question number to hidden fields that will submit
               $('#securityQuestionVal').val(response[0]['securityQuestion']);
               $('#securityQuestion2Val').val(response[0]['securityQuestion2']);

               $('#security_question_1_written').val(function()
               {
                    return turbo_toggle.SecurityQuestionsNumToQuest(response[0]['securityQuestion']);
               });

               $('#security_question_2_written').val(function()
               {
                    return turbo_toggle.SecurityQuestionsNumToQuest(response[0]['securityQuestion2']);
               });

               // show change password form will the security questions already filled in
               $('#get_security_quest_div').attr('class', 'hide');
               $('#check_security_answers_div').attr('class', 'show');

               return false;
          }
          return false;
     }

     return {
          StartAJAX:_StartAJAX,
          CheckResponse:_CheckResponse
     }

});
