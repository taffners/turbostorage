define(['models/table_funcs', 'models/utils'],function(table_funcs, utils)
{
     function _StartAJAX(search_term, page_name)
     {
          // Purpose: use an ajax call to find all tubes which match the search term

          return $.ajax({
               cache:    false,
               url:      'utils/searchTubes.php',
               dataType: 'json',
               data: {'search_term':search_term, 'page': page_name},
          });
     }

     function _CheckResponse(response, clone_insert_id, clone_row_id, page_name)
     {
          // Purpose: Clone a row with data-row-name equal to the key of response.  Also add an data-href attr based on the page.

          table_funcs.AddTableBodyCloneRow(response, clone_insert_id, clone_row_id, true, page_name);

          utils.ReloadJs('views/events/RedirectEvents');

     }

     return {
          StartAJAX:_StartAJAX,
          CheckResponse:_CheckResponse
     }

});
