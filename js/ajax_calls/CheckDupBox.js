define(['models/utils'],function(utils)
{
     function _StartAJAX(box_data)
     {
          // Purpose: use an ajax call to find if the email address is already in the database as a use.  Return an ajax object.
          return $.ajax({
               cache:    false,
               url:      'utils/getBoxes.php',
               dataType: 'text',
               data: box_data
          });
     }

     function _CheckResponse(response, updating, box_name)
     {
          var  _err_msg;

          // box name already present
          if (response === 'duplicate box name' && ! updating)
          {
               // error message
               _err_msg = 'A box with the name <b>'
                              + box_name +
                              '  </b>already exists.';

               utils.dialog_window(_err_msg, '#add_new_box_fieldset');
               return false;
          }

          // box loc already full
          else if (response === 'duplicate box loc')
          {
               // error message
               _err_msg = 'The location where you would like to add this box already has a box in it.';

               utils.dialog_window(_err_msg, '#location_fieldset');
               return false;
          }

          // if everything is filled out change form_checked to 1 so php can run
          else if (response === 'Passed Tests')
          {
               // allows php form to submit
               $('input#form_checked').val('1');

               // submit form
               $('form#create_box').submit();

               // stop php form from working again
               $('input#form_checked').val('0');
          }
          else
          {
               utils.dialog_window('returned response from ajax ' + response, '#add_new_box_fieldset');
               return false;
          }
          return false;
     }

     return {
          StartAJAX:_StartAJAX,
          CheckResponse:_CheckResponse
     }

});
