define(['models/utils'],function(utils)
{
     function _StartAJAX(freezer_name)
     {
          // Purpose: use an ajax call to find if the email address is already in the database as a use.  Return an ajax object.

          return $.ajax({
               cache:    false,
               url:      'utils/getFreezers.php',
               dataType: 'text',
               data: {'freezer_name': freezer_name}
          });
     }

     function _CheckResponse(response, updating, freezer_name)
     {
          // Purpose: Check if response from the above ajax call and see if the email address was already in the database.  If so tell the user.  If not submit form.

          // box name already present
          if (response === 'duplicate freezer name' && ! updating)
          {
               // error message
               var	err_msg = 'A freezer with the name <b>'
                              + freezer_name +
                              '  </b>already exists.';

               utils.dialog_window(err_msg, '#freezerName');
               return false;
          }

          // if everything is filled out change form_checked to 1 so php can run
          else if ((response === 'Passed Tests' )|| (response === 'duplicate freezer name' && updating))
          {
               // allows php form to submit
               $('input#form_checked').val('1');

               // submit form
               $('form#create_freezer').submit();

               return true;
          }
          else
          {
               utils.dialog_window('returned response from ajax ' + response, '#freezerName');
               return false;
          }
     }

     return {
          StartAJAX:_StartAJAX,
          CheckResponse:_CheckResponse
     }

});
