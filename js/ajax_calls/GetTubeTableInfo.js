define(function()
{

     function _StartAJAX(tube_id)
     {
          // Purpose: use an ajax call to find if the email address is already in the database as a use.  Return an ajax object.
          return $.ajax({
               cache:    false,
               url:      'utils/getTubeTableInfo.php',
               dataType: 'json',
               data: {'tube_id':tube_id}
          });
     }

     return {
          StartAJAX:_StartAJAX,

     }

});
