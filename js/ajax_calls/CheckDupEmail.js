define(['models/utils'],function(utils)
{
     function _StartAJAX(email_address)
     {
          // Purpose: use an ajax call to find if the email address is already in the database as a use.  Return an ajax object.
          return $.ajax({
               cache:    false,
               url:      'utils/getEmails.php',
               dataType: 'text',
               data: {'new_email': email_address}
          });
     }

     function _CheckResponse(response)
     {
          // Purpose: Check if response from the above ajax call and see if the email address was already in the database.  If so tell the user.  If not submit form.

          // check if email address is already in user table
          if (response === 'duplicate email address')
          {
               utils.dialog_window('This email address is already added.', '#emailAddress');
               return false;
          }
          // if not submit form
          else if (response === 'Passed Tests')
          {
               // allows php form to submit
               $('input#form_checked_create').val('1');

               // submit form
               $('form#create_user').submit();

               return true;
          }
          else
          {
               utils.dialog_window('Something went wrong with the ajax call. ' + response, '#emailAddress');
               return false;
          }
          return false;
     }

     return {
          StartAJAX:_StartAJAX,
          CheckResponse:_CheckResponse
     }

});
