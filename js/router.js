define(['models/utils'],function(utils)
{
     // Set up a single page application router.

     var  _routes =   [
                         // Login user pages
                         {page: 'change_password', controller:'change_password'},
                         {page: 'login', controller:'login'},

                         {page: 'home', controller: 'home'},

                         // add pages
                         {page: 'freezer_form', controller:'freezer_form'},
                         {page: 'box_form', controller: 'box_form'},
                         {page: 'tube_form', controller: 'tube_form'},
                         {page: 'req_form', controller: 'req_form'},
                         {page: 'group_form', controller: 'group_form'},
                         {page: 'unique_home', controller: 'unique_home'},
                         {page: 'irb_form', controller: 'irb_form'},

                         // update choose pages
                         {page: 'freezer_change', controller:'update'},
                         {page: 'box_change', controller: 'update'},
                         {page: 'tube_change', controller: 'tube_change'},
                         {page: 'req_change', controller: 'update'},
                         {page: 'group_change', controller: 'update'},

                         // reports
                         {page: 'box_info', controller: 'box_info'},
                         {page: 'report_request_filled', controller: 'report_request_filled'},
                         {page: 'using_freezer_report', controller: 'using_freezer_report'},

                         // remove
                         {page: 'remove_tube_form', controller: 'remove_tube_form'},

                         // special
                         {page: 'admin', controller: 'admin'}

                    ],
          _default_page = 'login',
          _php_vars = utils.PhpVars(),
          _page = '',
          _browser_ok,
          _test_status;

     function _startRouting(test_status)
     {
          // Purpose: Set route to default and start the hash check which starts the correct controller
          _hashCheck();
          // _SetTestStatus(test_status);
          // _SetLocalStorageVars();
     }

     function _SetTestStatus(test_status)
     {
          _test_status = test_status;
     }

     function _hashCheck()
     {
          // Purpose: Check to see the anchor part of the url

          var  _i,
               _current_route,
               _curr_page = _GetCurrPage();

          // reset php_vars to current
          _php_vars = utils.PhpVars();

          // check if hash has changed
          if (_curr_page != _page)
          {
               // if it changed find the controller of the changed hash and load that controller
               for (_i = 0; _current_route = _routes[_i++];)
               {
                    if (_curr_page === _current_route.page)
                    {
                         _loadController(_current_route.controller);
                    }
               }

               // set page to current page
               _page = _curr_page;
          }
     }

     function _loadController(controller_name)
     {
          // Purpose: Change the pages which are loaded if the hash was changed via require

          require(['controllers/' + controller_name], function(controller)
          {
               controller.start(_test_status, _php_vars);
          });
     }

     function _SetLocalStorageVars()
     {
          //Purpose: Set Local Storage variables

          // make sure local storage is supported
          if (typeof(Storage) !== 'undefined')
          {

          }
          else
          {
               $('#message_center')
                    .attr('class', 'alert alert-danger show')
                    .append('Sorry, your browser does not support web storage...');
          }
     }

     function _GetCurrPage()
     {
          // Purpose: Get the current page in phpVars if page is not in php_vars make page the default_page.  If the home page or login is loaded make sure page is set to home or login.


          // load pages where page is defined and not equal to login or home
          if ( ('page' in _php_vars) && ( _php_vars['page'] != 'home' || _php_vars['page'] != 'login' ) )
          {
               return _php_vars['page'];
          }

          // load home page when home-check-page-here is found and page == home
          else if ( ('page' in _php_vars) &&
          ( $('#home-check-page-here').length > 0 )
          ( _php_vars['page'] == 'home'))
          {
               return _php_vars['page'];
          }

          // load home page when login-check-page-here is found and page == login
          else if ( ('page' in _php_vars) &&
          ( $('#login-check-page-here').length > 0 )
          ( _php_vars['page'] == 'login'))
          {
               return _php_vars['page'];
          }

          // if page is not defined redirect to login
          else
          {
               window.location.href = "?page=login";
          }

     }

     return {
          startRouting: _startRouting,
          hashCheck:_hashCheck,
          SetLocalStorageVars:_SetLocalStorageVars
     };

});
