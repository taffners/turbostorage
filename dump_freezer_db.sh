#!/bin/sh

# Purpose:  Backup all turbostorage and mysql db to a remote git 
# repository.  Place this file under a cron job. Your repositiory
# will need to be set up as ssh.


# set up all the mysqldump variables
DIRECTORY=/var/www/html/turbostorage;
FILE=freezer_backup.sql;
FILE_LOC=${DIRECTORY}/${FILE};
DATABASE=freezer;
USER=login;
PASS=password;

# Remove the previous version of the file
unalias rm     2> /dev/null;
rm ${FILE_LOC}     2> /dev/null;

# Do the mysql database backup (dump) on database on localhost server
mysqldump --opt --user=${USER} --password=${PASS} ${DATABASE} > ${FILE_LOC};

# file is kept unziped because git does not perform differential storage on GZip files

# change directory 
cd ${DIRECTORY};

# Back up all of turbostorage
git add *

# git status

# Commit changes to the local repository
current_date_time="`date +%Y_%m_%d_%H_%M_%S`";
# echo $current_date_time;

git commit -m "Automatic Backup @ ${current_date_time}";

# git status

git push origin master
