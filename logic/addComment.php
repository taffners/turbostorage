<?php

	//make a comment array 
	//Array ( [comment] => [userID] => [botCheck] => [creationTime] => [commentID] => [CreateFreezerSubmit] => Submit )
	$commentArray = array();
	$commentArray['comment'] = $_POST['comment'];
	$commentArray['userID'] = $_POST['userID'];
	$commentArray['botCheck'] = $_POST['botCheck'];
	$commentArray['creationTime'] = $_POST['creationTime'];
	$commentArray['commentID'] = $_POST['commentID'];

	//add comment to DB first and get commentID
	$tableName = 'commentTable'; //in db
	$submitButtonName = 'CreateFreezerSubmit'; //id of submit button on form
	$nonDuplicateCol = ''; 

	$comment_db = new DataBase($conn, $commentArray, $submitButtonName, $tableName, $nonDuplicateCol, $_SESSION['loggedInUser']);

	//clean up $_POST and find if a bot is present
	//remove submit button, add a creation time
	$botFound = $comment_db->formatPost();
	
	//If no bot was found add comment and get comment ID
	if($botFound === 'no')
	{
		$result = $comment_db->addOrModifyRecord();

		if (isset($freezerArray))
		{
			$freezerArray['commentID'] = $result[1];
		}
		else if (isset($groupArray))
		{

			$groupArray['commentID'] = $result[1];
		}
		
	}
	else//bot found
	{
		mysqli_close($conn);
		echo '<div class="error">I think you are a Robot</div>';
	}			


?>