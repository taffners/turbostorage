<?php

	// get what action needs to be completed (update or delete)
	if(isset($_REQUEST['action']))
	{
		$action = $_REQUEST['action'];
	}

	// make default action to update
	else
	{
		$action = 'update';
	}


	//
	if($action == 'delete')
	{

		if(isset($_REQUEST['freezerID']))
		{

			$inputs['freezerID'] = $_REQUEST['freezerID'];

			$result = $db->deleteRecord('freezerTable',$inputs);
		}

		$link = 'delete';
		// get freezer which do not have anything in them
		$freezerList = $db->listAll('freezer-nobox');
	}

	// for updating go to freezer form
	else
	{
		$link = 'freezer_form';

		// get all freezer information
		$freezerList = $db->listAll('freezer');
	}



?>
