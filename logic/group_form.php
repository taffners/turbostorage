<?php

	if(isset($_POST) && $_POST['form_checked'] == 1)
	{
		//make Group array
		$groupArray = array();
		$groupArray['groupID'] = $_POST['groupID'];
		$groupArray['userID'] = USER_ID;
		$groupArray['groupName'] = $_POST['groupName'];
		$groupArray['reqRequired'] = $_POST['reqRequired'];

		if ($groupArray['reqRequired'] == '')
		{
			$groupArray['reqRequired'] = 0;
		}

		// add to db
		$group_result = $db->addOrModifyRecord('groupTable',$groupArray);

		// get group id
		$groupID = $group_result[1];

		//this gets set to false if any of the inserts or updates do not work
		$insert_ok = true;

		if($group_result[0])
		{
			// Make memeber array
			$memberArray = array();

			// add each member to the groupMemberTable use in the above groupID
			foreach ($_POST['group'] as $key => $value)
			{
				$id_pairs = explode('_', $value);
				$memberArray['memberID'] = $id_pairs[1];
				$memberArray['groupID'] = $groupID;
				$memberArray['userID'] = $id_pairs[0];

				// add member to db
				$member_result =  $db->addOrModifyRecord('groupMemberTable',$memberArray);

				if(!$member_result[0])
				{
					$insert_ok = false;
					break;
				}
			}

			// make comment array

			// Make sure a comment was added before adding
			if ($_POST['comment'] != '')
			{
				// make comment array
				$commentArray = array();
				$commentArray['userID'] = USER_ID;
				$commentArray['comment'] = $_POST['comment'];
				$commentArray['commentType'] = 'group';
				$commentArray['commentRef'] = $groupID;

				$comment_result = $db->addOrModifyRecord('commentTable',$commentArray);

				if(!$comment_result[0])
				{
					$insert_ok = false;
				}
			}
		}

		else
		{
			$insert_ok = false;
		}


		//if $insert_ok is still true, send to success page
		if($insert_ok)
		{
			header('Location:'.REDIRECT_URL.'?page=home&success=true');
		}

		//if it isn't re-load the form with an error message
		else
		{
			$message = 'Your group could not be added at this time';
		}

	}

	else if(isset($_GET['groupID']))
	{

		$groupArray = $db->get('group',$_GET['groupID']);

		$memberArray = $db->get('member',$_GET['groupID']);

	}
	// get all of the groups the user belongs to
	$user_groups = $db->listAll('groups-user-in', USER_ID);

	$member_groups = $db->listAll('members-in-group', USER_ID);

	$all_users = $db->listAll('users');

	$groups_comments =  $db->listAll('groups-comments', USER_ID);
?>
