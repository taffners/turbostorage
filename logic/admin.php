<?php
     $AllRoles = $db->listAll('roles');

     $AllUserRoleList = $db->listAll('all-user-roles');

     $userNameList = $db->listAll('user-names');

     // get all of the groups the user belongs to
     $user_groups = $db->listAll('admin-groups');

     $member_groups = $db->listAll('admin-members-in-group');

     // list boxes which this user can use
     $boxList = $db->listAll('admin-all-boxes');

     // requests outstanding
     $requestList = $db->listAll('outstanding-requests');


?>
