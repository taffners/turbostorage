<div class="form-group row">
	<div class="col-sm-6">

<?php
	//find all freezers
	$searchQuery = 'SELECT * FROM freezerTable
		LEFT JOIN commentTable 
		ON freezerTable.commentID=commentTable.commentID;';
	
	$searchResult = mysqli_query($conn, $searchQuery);

?>
		<table class="QueryInfo" id="FreezerInfo">
		

			<tr>
				<th style="display:none;">freezer ID</th>
				<th>Freezer Name</th>
				<th>Room Number</th>
				<th>Temp</th>
				<th>Total # of Shelfs</th>
				<th>Comments</th>

			</tr>
			

<?php while ($row = mysqli_fetch_array($searchResult)): ?>	

			<tr>
				<td style="display:none;"><?php echo $row['freezerID']; ?></td>
				<td><?php echo $row['freezerName']; ?></td>
				<td><?php echo $row['roomNum']; ?></td>
				<td><?php echo $row['temp']; ?>&deg;C</td>
				<td><?php echo $row['numShelf']; ?></td>
				<td><?php echo $row['comment']; ?></td>
			</tr>




<?php endwhile; ?>


		</table>
	</div>
</div>