<?php

     // get all of the groups the user belongs to
     $user_groups = $db->listAll('groups-user-in', USER_ID);

     $member_groups = $db->listAll('members-in-group', USER_ID);

     $freezerList = $db->listAll('freezer');

     // list boxes which this user can use
     $boxList = $db->listAll('box-user', USER_ID);

     // User Info
     $user_info = $db->listAll('user-info', USER_ID);

     // requests outstanding
     $requestList = $db->listAll('outstanding-requests', USER_ID);

?>
