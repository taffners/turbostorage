<?php

     if(isset($_POST) && $_POST['form_checked'] == 1)
     {
          // Get all information pertaining to the userID in the userTable
          $AnswerArray = $db->listAll('user-answers',$_POST['userID']);

          $ans1 = md5(strtoupper($_POST['securityAnswer']));
          $ans2 = md5(strtoupper($_POST['securityAnswer2']));

          $passwordArray['userID'] = $AnswerArray[0]['userID'];
          $passwordArray['firstName'] = $AnswerArray[0]['firstName'];
          $passwordArray['lastName'] = $AnswerArray[0]['lastName'];
          $passwordArray['emailAddress'] = $AnswerArray[0]['emailAddress'];
          $passwordArray['password'] = md5($_POST['password']);
          $passwordArray['role'] = $AnswerArray[0]['role'];
          $passwordArray['status'] = $AnswerArray[0]['status'];
          $passwordArray['phoneNumber'] = $AnswerArray[0]['phoneNumber'];
          $passwordArray['labName'] = $AnswerArray[0]['labName'];
          $passwordArray['securityQuestion'] = $AnswerArray[0]['securityQuestion'];
          $passwordArray['securityAnswer'] = $AnswerArray[0]['securityAnswer'];
          $passwordArray['securityQuestion2'] = $AnswerArray[0]['securityQuestion2'];
          $passwordArray['securityAnswer2'] = $AnswerArray[0]['securityAnswer2'];

          // Check if security answer 1 matches saved
          if($ans1 == $AnswerArray[0]['securityAnswer'] && $ans2 == $AnswerArray[0]['securityAnswer2'])
          {

               $user_result = $db->addOrModifyRecord('userTable',$passwordArray);

               // if $result == 1 then replace worked so redirect to login page and
               if($user_result[0] == 1)
               {
                    header('Location: ?page=login&SuccessfulChangePassword=true');
               }
               else
               {
                    header('Location: ?page=login&something_wrong=true');
               }
          }

          // if failed set failed_question_check to true so an error message appears
          else
          {
               header('Location: ?page=change_password&failed_question_check=true');
          }

     }
?>
