<?php

	if(isset($_POST) && $_POST['form_checked'] == 1)
	{
		// make request
		$request = array();

		$request['piName'] = $_POST['piName'];
		$request['piEmail'] = $_POST['piEmail'];
		$request['labContactName'] = $_POST['labContactName'];
		$request['labContactEmail'] = $_POST['labContactEmail'];
		$request['labContactPhone'] = $_POST['labContactPhone'];
		$request['requestDate'] = $_POST['requestDate'];
		$request['sampleType'] = $_POST['sampleType'];
		$request['quantiy'] = $_POST['quantiy'];
		$request['quantiyUnits'] = $_POST['quantiyUnits'];
		$request['sampleIDsRequested'] = $_POST['sampleIDsRequested'];
		$request['researchAbstract'] = $_POST['researchAbstract'];
		$request['refs'] = $_POST['refs'];
		$request['controls'] = $_POST['controls'];
		$request['assayWorking'] = $_POST['assayWorking'];
		$request['refillReason'] = $_POST['refillReason'];
		$request['data_contribution'] = $_POST['data_contribution'];
		$request['approvedBy'] = $_POST['approvedBy'];
		$request['approved'] = $_POST['approved'];
		$request['ApproveDate'] = $_POST['ApproveDate'];
		$request['requestFilled'] = $_POST['requestFilled'];
		$request['requestors_irb_num'] = $_POST['requestors_irb_num'];

		$request['reqID'] = $_POST['reqID'];
		$request['userID'] = USER_ID;

		$request_result = $db->addOrModifyRecord('requestTable',$request);

		// get request id
		$requestID = $request_result[1];

		//this gets set to false if any of the inserts or updates do not work
		$insert_ok = true;

		// make sure inserted correctly
		if ($request_result[0])
		{
			// make comment array

			// Make sure a comment was added before adding
			if ($_POST['comment'] != '')
			{
				// make comment array
				$commentArray = array();
				$commentArray['userID'] = USER_ID;
				$commentArray['comment'] = $_POST['comment'];
				$commentArray['commentType'] = 'request';
				$commentArray['commentRef'] = $requestID;

				$comment_result = $db->addOrModifyRecord('commentTable',$commentArray);

				if(!$comment_result[0])
				{
					$insert_ok = false;
				}
			}

			// add everything that was updated to updateTable
			if ($insert_ok)
			{
				$update_array = array();

				$update_array['userID'] = USER_ID;

				$update_array['updateRef'] = $requestID;

				$update_array['updateType'] = 'request';

				$update_changes = json_encode($_POST);
				$update_array['updateChanges'] = $update_changes;

				$update_result = $db->addOrModifyRecord('updateTable',$update_array);

				if(!$update_result[0])
				{
					$insert_ok = false;
				}

			}

		}
		else
		{
			$insert_ok = false;
		}

		//if $insert_ok is still true, send to success page
		if($insert_ok)
		{
			header('Location:'.REDIRECT_URL.'?page=home&success=true');
		}

		//if it isn't re-load the form with an error message
		else
		{
			$message = 'Your request could not be added at this time';
		}
	}
	else if(isset($_GET['reqID']))
	{
		$requestArray = $db->get('request',$_GET['reqID'] );
	}



?>
