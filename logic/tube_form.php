<?php
	if(isset($_POST) && $_POST['form_checked'] == 1)
	{
		//this gets set to false if any of the inserts or updates do not work
		$insert_ok = true;
		$message = '';

		// get all of the the values for the tube table
			$tubeArray['tubeID'] = $_POST['tubeID'];
			$tubeArray['sampleName'] = $_POST['sampleName'];
			$tubeArray['freezeDate'] = $_POST['freezeDate'];
			$tubeArray['color'] = $_POST['color'];
			$tubeArray['labBook'] = $_POST['labBook'];
			$tubeArray['pageNum'] = $_POST['pageNum'];
			$tubeArray['sampleType'] = $_POST['sampleType'];
			$tubeArray['experimentName'] = $_POST['experimentName'];
			$tubeArray['consentYesNo'] = $_POST['consentYesNo'];
			$tubeArray['locConsent'] = $_POST['locConsent'];
			$tubeArray['consentNotes'] = $_POST['consentNotes'];
			$tubeArray['consentApprovalDate'] = $_POST['consentApprovalDate'];
			$tubeArray['openAccess'] = $_POST['openAccess'];

			$tubeArray['irbID'] = $_POST['irbID'];

			if ($_POST['userID'] != "")
			{
				$tubeArray['userID'] = $_POST['userID'];
			}
			else
			{
				$tubeArray['userID'] = USER_ID;
			}

			$tubeArray['published'] = $_POST['published'];
			$tubeArray['mta'] = $_POST['mta'];
			$tubeArray['frozeBy'] = $_POST['frozeBy'];

			if ($_POST['intialNumTubes'] != "")
			{
				$tubeArray['intialNumTubes'] = $_POST['intialNumTubes'];
			}
			else
			{
				$tubeArray['intialNumTubes'] = count($_POST['space']);
			}


			$tube_result = $db->addOrModifyRecord('tubeTable',$tubeArray);

		// Get all the values for the sample type vars table

		if($tube_result[0])
		{
			// Get the tube id assigned to the tube just added to db
			$tubeID = $tube_result[1];

			// Find out which group if any were filled in
			// There are currently 11 groups in the tube_form html
			$not_empty = False;
			for ($i = 1; $i <= 11; $i++)
			{
				// Get current group name.
				$curr_Group = 'Group'.$i;

				// Find out which group is not Empty

				foreach ($_POST[$curr_Group] as $key => $value)
				{

					if (!empty($value))
					{

						// Put together the the sample_variables array with everything in the group
						$sample_variables[$key] = $value;

						$not_empty = True;
					}
				}
			}


			// Add sample variable info to sampleTypeVariables table if sample info was added
			if ($not_empty)
			{
				$sample_result = $db->addOrModifySampleVariables('sampleTypeVariables', $sample_variables, $tubeID);

				if(!$sample_result[0])
				{
					$insert_ok = false;
					$message .= 'problem with the sample ';
				}
			}

			// Add the location of each sample and get space ID if action != updateTube
			if ($_REQUEST['action'] != 'updateTube')
			{
				foreach ($_POST['space'] as $key => $value)
				{
					// make and empty space array
					$space_array = [];

					// Add yLoc to space_array
					$space_array['yLoc'] = $value['y'];

					// Add xLoc to space_array
					$space_array['xLoc'] = $value['x'];

					// add boxID to space_array
					$space_array['boxID'] = $_POST['BoxID'];

					// add tubeID to space_array
					$space_array['tubeID'] = $tubeID;

					// add space_array to db
					$tube_result = $db->addOrModifyRecord('spaceTable', $space_array);

					if(!$tube_result[0])
					{
						$insert_ok = false;
						$message .= 'problem with the space ';
						break;
					}
				}
			}

			// add all updated information to the update table for updated tubes
			if ($_REQUEST['action'] == 'updateTube')
			{
				$update_array = array();

				$update_array['userID'] = USER_ID;

				$update_array['updateRef'] = $tubeID;

				$update_array['updateType'] = 'update_tube';

				$update_changes = json_encode($_POST);
				$update_array['updateChanges'] = $update_changes;

				$update_result = $db->addOrModifyRecord('updateTable',$update_array);

				if(!$update_result[0])
				{
					$insert_ok = false;
				}
			}

			// Make sure a comment was added before adding
			if ($_POST['comment'] != '')
			{
				// make comment array
				$commentArray = array();
				$commentArray['userID'] = USER_ID;
				$commentArray['comment'] = $_POST['comment'];
				$commentArray['commentType'] = 'tube';
				$commentArray['commentRef'] = $tubeID;

				// add comment to db
				$comment_result = $db->addOrModifyRecord('commentTable',$commentArray);

				if(!$comment_result[0])
				{
					$insert_ok = false;
					$message .= 'problem with the comment ';
				}
			}
		}

		else
		{
			$insert_ok = false;
			$message .= 'problem with the tube ';
		}


		//if $insert_ok is still true, send to success page
		if($insert_ok)
		{
			header('Location:'.REDIRECT_URL.'?page=home&success=true&boxID='.$_POST['BoxID']);
		}

		//if it isn't re-load the form with an error message
		else
		{
			$message = 'Something went wrong: '. $message;
		}
		/*
			When you upload a file, you get an associative array that contains the following elements

			[name] => the file name (ex. lighthouse.svg)
			[type] => the file type (ex. image/svg+xml)
			[tmp_name] => a temporary file name and location where php stores the file while you work with it (ex. /Applications/MAMP/tmp/php/phpJEsyEJ)
			[error] => whether or not there was an error with the upload (0 if no errors, 1 if there are errors)
			[size] => file size in bytes (74174)
		*/
/*
		if(isset($_FILES['image_file']))
		{
			//file types that you will allow to be uploaded
			//if you need to add more, go to https://www.sitepoint.com/web-foundations/mime-types-complete-list/
			$allowed_mimes = array('image/svg+xml','image/gif','image/jpeg','image/png','image/tiff','text/html','text/plain','application/msword','application/excel','application/mspowerpoint','application/pdf');

			//pro tip - always assume it's a bad file type until you know that it isn't :)
			$file_ok = false;

			for($i=0;$i<sizeof($allowed_mimes);$i++)
				{
					$file_ok = true;
					break;
				}
				{
					$file_ok = true;
					break;
				}
			}

			//if the file is ok
			if($file_ok)
			{
				//read the contents of the file into a string;
				$file_contents = file_get_contents($_FILES['image_file']['tmp_name']);
				//get an md5 hash of the file contents. this will be used as a unique identifier to see if the file has already been uploaded
				$md5 = md5($file_contents);

				$copy_ok = copy($_FILES['image_file']['tmp_name'], 'uploads/'.$_FILES['image_file']['name']);

				if(!$copy_ok)
				{
					echo 'File not copied';
				}

				else
				{
					$file_info['name'] = $_FILES['image_file']['name'];
					$file_info['size'] = $_FILES['image_file']['size'];
					$file_info['mime'] = $_FILES['image_file']['type'];
					$file_info['md5'] = $md5;
					$file_info['upload_time'] = time();
				}
			}

			else
			{
				echo 'Danger! Danger!';
			}
		}
//print_r($_FILES['image_file']);
/*

		$boxArray = $_POST;
		unset($boxArray['CreateBoxSubmit']);
		unset($boxArray['botCheck']);
		unset($boxArray['boxLocation']);

		if(!$boxArray['creationTime'])
		{
			$boxArray['creationTime'] = time();
		}

		$tableName = 'boxTable';


		$box_result = $db->addOrModifyRecord('boxTable',$boxArray);
*/

	}

	elseif (isset($_GET['tubeID']))
	{
		$tubeArray = $db->get('tube-info',$_GET['tubeID']);

		$spaceArray = $db->listAll('space-info',$_GET['tubeID']);
	}


	$irb_array = $db->listAll('all-irbs');

	$boxList = $db->listAll('box-user', USER_ID);

?>
