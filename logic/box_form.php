<?php
	// add box
	if(isset($_POST) && $_POST['form_checked'] == 1)
	{

		// make box array
			$boxArray = array();
			$boxArray['freezerID'] = $_POST['freezerID'];
			$boxArray['boxID'] = $_POST['boxID'];
			$boxArray['boxName'] = $_POST['boxName'];
			$boxArray['xSize'] = $_POST['xSize'];
			$boxArray['ySize'] = $_POST['ySize'];
			$boxArray['rackNum'] = $_POST['rackNum'];
			$boxArray['shelfNum'] = $_POST['shelfNum'];
			$boxArray['privacyLevel'] = $_POST['privacyLevel'];
			$boxArray['userID'] = USER_ID;
			$boxArray['rackSpaceNum'] = $_POST['rackSpaceNum'];

		// add a group if privacy level is Group
			if ($boxArray['privacyLevel'] == 'Group')
			{
				$boxArray['groupID'] = $_POST['groupID'];
			}

		// add box to db
			$box_result = $db->addOrModifyRecord('boxTable',$boxArray);

			$boxID = $box_result[1];

		//this gets set to false if any of the inserts or updates do not work
		$insert_ok = true;
		$message = '';

		if($box_result[0])
		{
			// Make sure a comment was added before adding
			if ($_POST['comment'] != '')
			{
				// make comment array
				$commentArray = array();
				$commentArray['commentID'] = $_POST['commentID'];
				$commentArray['userID'] = USER_ID;
				$commentArray['comment'] = $_POST['comment'];
				$commentArray['commentType'] = 'box';
				$commentArray['commentRef'] = $boxID;

				$comment_result = $db->addOrModifyRecord('commentTable',$commentArray);

				if(!$comment_result[0])
				{
					$insert_ok = false;
					$message .= 'problem with comment';
				}
			}
		}

		else
		{
			$insert_ok = false;
			$message .= 'problem with box';
		}

		//if $insert_ok is still true, send to success page
		if($insert_ok)
		{
			header('Location:'.REDIRECT_URL.'/?page=home&success=true&boxID='.$boxID);
		}

		//if it isn't re-load the form with an error message
		else
		{
			$message = 'Something went wrong: '. $message;
		}

	}

	else if(isset($_GET['boxID']))
	{

		$boxArray = $db->get('box',$_GET['boxID']);
	}

	// list of freezers to add to
	$freezerList = $db->listAll('freezer');

	// list of groups the user is in
	$user_groups = $db->listAll('groups-user-in', USER_ID);

	// list boxes which this user can use
	$boxList = $db->listAll('box-user', USER_ID);
	

?>
