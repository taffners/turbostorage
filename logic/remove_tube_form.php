<?php


     if(isset($_POST) && $_POST['form_checked'] == 1)
     {

          // remove spaceID row
          $spaceArray = array();

          $spaceArray['spaceID'] = $_POST['spaceID'];

          $delete_ok = true;

          $space_result = $db->deleteRecord('spaceTable',$spaceArray);

          if ($space_result[0])
          {
               // update updateTable with
                    // userID, updateRef == tubeid, updateType == boxID, xLoc, yLoc
                    // update type == delete_tube

                    $updateArray = array();

                    $update_array['userID'] = USER_ID;

                    $update_array['updateRef'] = $_POST['tubeID'];

                    $update_array['updateType'] = 'delete_tube';

                    $update_changes =  json_encode($_POST);
                    $update_array['updateChanges'] = $update_changes;

                    $update_result = $db->addOrModifyRecord('updateTable',$update_array);

                    if(!$update_result[0])
				{
					$delete_ok = false;
				}

                    if($delete_ok && $_POST['reqID'] != '')
                    {
                         // add to request remove log to link request with the tube id
                              // add tubeID, reqID,

                              $remove_request_log = array();

                              $remove_request_log['tubeID'] = $_POST['tubeID'];

                              $remove_request_log['tubeName'] = $_POST['tubeName'];

                              $remove_request_log['reqID'] = $_POST['reqID'];

                              $remove_request_log['freezerName'] = $_POST['freezerName'];

                              $remove_request_log['boxName'] = $_POST['boxName'];

                              $remove_request_log['shelfNum'] = $_POST['shelfNum'];

                              $remove_request_log['rackNum'] = $_POST['rackNum'];

                              $remove_request_log['userID'] = USER_ID;

                              $remove_request_log['rackSpaceNum'] = $_POST['rackSpaceNum'];

                              // make space loc
                                   // xLoc == row # change this to a letter
                                   // yLoc == column keep as number
                                   $alpha = range('A', 'Z');
                                   $xLoc = $_POST['xLoc'];
                                   $space = $alpha[$xLoc].$_POST['yLoc'];

                              $remove_request_log['spaceLoc'] = $space;

                              $remove_result = $db->addOrModifyRecord('requestRemoveLog',$remove_request_log);

                         if(!$update_result[0])
     				{
     					$delete_ok = false;
     				}

                    }
          }

          //if $insert_ok is still true, send to success page
          if($delete_ok)
          {
               header('Location:'.REDIRECT_URL.'?page=home&success=true&boxID='.$_POST['boxID']);
          }

          //if it isn't re-load the form with an error message
          else
          {
               $message = 'Your tube could not be removed at this time';
          }

     }

     // get all of the groups the user belongs to
     $user_groups = $db->listAll('groups-user-in', USER_ID);

     $member_groups = $db->listAll('members-in-group', USER_ID);

     $freezerList = $db->listAll('freezer');

     // list boxes which this user can use
     $boxList = $db->listAll('box-user', USER_ID);

     // User Info
     $user_info = $db->listAll('user-info', USER_ID);

     // outstanding requests
     $requestList = $db->listAll('outstanding-approved-requests', USER_ID);

     // is a request form required to remove this sample??



?>
