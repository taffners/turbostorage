<?php

	if(isset($_REQUEST['action']))
	{
		$action = $_REQUEST['action'];
	}

	else
	{
		$action = 'update';
	}


	if($action == 'delete')
	{
		if(isset($_REQUEST['groupID']))
		{
			$inputs['groupID'] = $_REQUEST['groupID'];

			$result = $db->deleteRecord('groupTable',$inputs);
		}

		$link = 'delete_group';
	}

	else
	{
		$link = 'group_form';

		// get all of the groups the user belongs to
		$user_groups = $db->listAll('groups-user-in', USER_ID);

		$member_groups = $db->listAll('members-in-group', USER_ID);

		$groups_comments =  $db->listAll('groups-comments', USER_ID);
	}


?>
