<?php

	if(isset($_POST) && $_POST['form_checked'] == 1)
	{
		// make freezer array
		$freezerArray = array();

		$freezerArray['userID'] = USER_ID;
		$freezerArray['freezerName'] =  $_POST['freezerName'];
		$freezerArray['roomNum'] =  $_POST['roomNum'];
		$freezerArray['numShelf'] =  $_POST['numShelf'];
		$freezerArray['temp'] =  $_POST['temp'];
		$freezerArray['freezerID'] = $_POST['freezerID'];


		$freezer_result = $db->addOrModifyRecord('freezerTable',$freezerArray);

		$freezerID = $freezer_result[1];

		//this gets set to false if any of the inserts or updates do not work
		$insert_ok = true;

		if($freezer_result[0])
		{

			// Make sure a comment was added before adding
			if ($_POST['comment'] != '')
			{
				// make comment array
				$commentArray = array();
				$commentArray['commentID'] = $_POST['commentID'];
				$commentArray['userID'] = USER_ID;
				$commentArray['comment'] = $_POST['comment'];
				$commentArray['commentType'] = 'freezer';
				$commentArray['commentRef'] = $freezerID;

				// add comment to db
				$comment_result = $db->addOrModifyRecord('commentTable',$commentArray);

				if(!$comment_result[0])
				{
					$insert_ok = false;
				}
			}

		}

		else
		{
			$insert_ok = false;
		}

		//if $insert_ok is still true, send to success page
		if($insert_ok)
		{
			header('Location:'.REDIRECT_URL.'?page=home&success=true');
		}

		//if it isn't re-load the form with an error message
		else
		{
			$message = 'Your freezer could not be added at this time';
		}
	}

	// if userID is set make freezer array to fill in form
	else if(isset($_GET['freezerID']))
	{
		$freezerArray = $db->get('freezer',$_GET['freezerID']);

	}

	$freezerList = $db->listAll('freezer');


?>
