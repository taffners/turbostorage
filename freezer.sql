-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 28, 2017 at 01:28 PM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `freezer`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `all_group_members_vw`
--
CREATE TABLE IF NOT EXISTS `all_group_members_vw` (
`groupID` int(10) unsigned
,`allMembers` text
,`groupName` varchar(32)
);
-- --------------------------------------------------------

--
-- Table structure for table `boxTable`
--

CREATE TABLE IF NOT EXISTS `boxTable` (
  `boxID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `freezerID` int(10) unsigned NOT NULL,
  `boxName` varchar(32) DEFAULT NULL,
  `xSize` tinyint(3) unsigned NOT NULL,
  `ySize` tinyint(3) unsigned NOT NULL,
  `rackNum` tinyint(3) unsigned NOT NULL,
  `shelfNum` tinyint(3) unsigned NOT NULL,
  `privacyLevel` enum('Private','Group','Public') NOT NULL,
  `userID` int(11) NOT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `groupID` int(11) DEFAULT NULL,
  `rackSpaceNum` int(11) DEFAULT NULL,
  PRIMARY KEY (`boxID`),
  UNIQUE KEY `boxID` (`boxID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

-- --------------------------------------------------------

--
-- Table structure for table `commentTable`
--

CREATE TABLE IF NOT EXISTS `commentTable` (
  `commentID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `commentType` enum('freezer','group','box','tube','request') DEFAULT NULL,
  `commentRef` int(10) unsigned NOT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`commentID`),
  UNIQUE KEY `commentID` (`commentID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=231 ;

-- --------------------------------------------------------

--
-- Table structure for table `consentTable`
--

CREATE TABLE IF NOT EXISTS `consentTable` (
  `consentID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `consentDate` date NOT NULL,
  `IRBQuestion` text NOT NULL,
  `answer` text NOT NULL,
  `irbID` int(11) DEFAULT NULL,
  PRIMARY KEY (`consentID`),
  UNIQUE KEY `consentID` (`consentID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `freezerTable`
--

CREATE TABLE IF NOT EXISTS `freezerTable` (
  `freezerID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `freezerName` varchar(32) NOT NULL,
  `roomNum` int(10) unsigned NOT NULL,
  `numShelf` int(11) NOT NULL,
  `temp` int(11) NOT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`freezerID`),
  UNIQUE KEY `freezerID` (`freezerID`),
  UNIQUE KEY `freezerName` (`freezerName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

--
-- Table structure for table `groupMemberTable`
--

CREATE TABLE IF NOT EXISTS `groupMemberTable` (
  `memberID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`memberID`),
  UNIQUE KEY `memberID` (`memberID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `groupTable`
--

CREATE TABLE IF NOT EXISTS `groupTable` (
  `groupID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupName` varchar(32) DEFAULT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userID` int(11) DEFAULT NULL,
  `reqRequired` int(1) DEFAULT NULL,
  PRIMARY KEY (`groupID`),
  UNIQUE KEY `groupID` (`groupID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `group_boxes_vw`
--
CREATE TABLE IF NOT EXISTS `group_boxes_vw` (
`boxID` int(10) unsigned
,`boxName` varchar(32)
,`privacyLevel` enum('Private','Group','Public')
,`groupID` int(11)
,`xSize` tinyint(3) unsigned
,`ySize` tinyint(3) unsigned
,`shelfNum` tinyint(3) unsigned
,`rackNum` tinyint(3) unsigned
,`rackSpaceNum` int(11)
,`groupName` varchar(32)
,`freezerName` varchar(32)
,`roomNum` int(10) unsigned
,`reqRequired` int(1)
,`emptySpaces` bigint(21) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `group_member_vw`
--
CREATE TABLE IF NOT EXISTS `group_member_vw` (
`groupID` int(10) unsigned
,`userName` varchar(101)
,`groupName` varchar(32)
);
-- --------------------------------------------------------

--
-- Table structure for table `irbTable`
--

CREATE TABLE IF NOT EXISTS `irbTable` (
  `irbID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `irbProjectTitle` varchar(255) NOT NULL,
  `irbPI` varchar(100) NOT NULL,
  `irbNum` varchar(20) NOT NULL,
  PRIMARY KEY (`irbID`),
  UNIQUE KEY `irbID` (`irbID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `loginAttemps`
--

CREATE TABLE IF NOT EXISTS `loginAttemps` (
  `loginID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ipAddress` varchar(15) DEFAULT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `emailAddress` varchar(50) NOT NULL,
  PRIMARY KEY (`loginID`),
  UNIQUE KEY `loginID` (`loginID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `private_boxes_vw`
--
CREATE TABLE IF NOT EXISTS `private_boxes_vw` (
`boxID` int(10) unsigned
,`boxName` varchar(32)
,`privacyLevel` enum('Private','Group','Public')
,`userID` int(11)
,`xSize` tinyint(3) unsigned
,`ySize` tinyint(3) unsigned
,`shelfNum` tinyint(3) unsigned
,`rackNum` tinyint(3) unsigned
,`rackSpaceNum` int(11)
,`freezerName` varchar(32)
,`roomNum` int(10) unsigned
,`emptySpaces` bigint(21) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `public_boxes_vw`
--
CREATE TABLE IF NOT EXISTS `public_boxes_vw` (
`boxID` int(10) unsigned
,`boxName` varchar(32)
,`privacyLevel` enum('Private','Group','Public')
,`xSize` tinyint(3) unsigned
,`ySize` tinyint(3) unsigned
,`shelfNum` tinyint(3) unsigned
,`rackNum` tinyint(3) unsigned
,`rackSpaceNum` int(11)
,`freezerName` varchar(32)
,`roomNum` int(10) unsigned
,`emptySpaces` bigint(21) unsigned
);
-- --------------------------------------------------------

--
-- Table structure for table `requestRemoveLog`
--

CREATE TABLE IF NOT EXISTS `requestRemoveLog` (
  `removeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reqID` int(10) unsigned NOT NULL,
  `tubeID` int(10) unsigned NOT NULL,
  `freezerName` varchar(32) DEFAULT NULL,
  `boxName` varchar(32) DEFAULT NULL,
  `shelfNum` tinyint(3) DEFAULT NULL,
  `rackNum` tinyint(3) DEFAULT NULL,
  `rackSpaceNum` tinyint(3) DEFAULT NULL,
  `tubeName` varchar(15) DEFAULT NULL,
  `spaceLoc` varchar(15) DEFAULT NULL,
  `userID` int(10) DEFAULT NULL,
  PRIMARY KEY (`removeID`),
  UNIQUE KEY `removeID` (`removeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `requestTable`
--

CREATE TABLE IF NOT EXISTS `requestTable` (
  `reqID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userID` int(10) unsigned NOT NULL,
  `piName` varchar(50) NOT NULL,
  `piEmail` varchar(50) NOT NULL,
  `labContactName` varchar(50) NOT NULL,
  `labContactEmail` varchar(50) NOT NULL,
  `labContactPhone` varchar(15) DEFAULT NULL,
  `requestDate` date NOT NULL,
  `sampleType` enum('Antibody','Bacterial_Cultures','Blood','Body_fluids','Bone','Cell_Lines','Cell_Pellet','Chemical','Cytokine','DNA','Plasma','PBMC','RNA','Serum','Urine','Virus','Whole_Tissue') DEFAULT NULL,
  `quantiy` int(10) unsigned NOT NULL,
  `quantiyUnits` varchar(15) DEFAULT NULL,
  `sampleIDsRequested` varchar(255) NOT NULL,
  `researchAbstract` varchar(255) NOT NULL,
  `refs` varchar(255) NOT NULL,
  `controls` varchar(255) NOT NULL,
  `assayWorking` varchar(255) NOT NULL,
  `refillReason` varchar(255) NOT NULL,
  `data_contribution` varchar(255) NOT NULL,
  `approvedBy` varchar(50) NOT NULL,
  `approved` tinyint(3) unsigned NOT NULL,
  `ApproveDate` date NOT NULL,
  `requestFilled` tinyint(3) unsigned NOT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `requestors_irb_num` int(10) unsigned NOT NULL,
  PRIMARY KEY (`reqID`),
  UNIQUE KEY `reqID` (`reqID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roleTable`
--

CREATE TABLE IF NOT EXISTS `roleTable` (
  `roleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `roleName` varchar(100) NOT NULL,
  PRIMARY KEY (`roleID`),
  UNIQUE KEY `roleID` (`roleID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------
--
-- Dumping data for table `roleTable`
--

INSERT INTO `roleTable` (`roleID`, `roleName`) VALUES
(1, 'Check Out Unique Numbers'),
(2, 'Sample Request'),
(3, 'Add IRBs'),
(4, 'IRB Questions and Answers'),
(5, 'Admin');
--
-- Table structure for table `sampleTypeVariables`
--

CREATE TABLE IF NOT EXISTS `sampleTypeVariables` (
  `sampleTypeVariableID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tubeID` int(10) DEFAULT NULL,
  `variableName` varchar(32) NOT NULL,
  `variableValue` varchar(255) NOT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sampleTypeVariableID`),
  UNIQUE KEY `sampleTypeVariableID` (`sampleTypeVariableID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=713 ;

-- --------------------------------------------------------

--
-- Table structure for table `spaceTable`
--

CREATE TABLE IF NOT EXISTS `spaceTable` (
  `spaceID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `xLoc` tinyint(3) unsigned NOT NULL,
  `yLoc` tinyint(3) unsigned NOT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `boxID` int(11) DEFAULT NULL,
  `tubeID` int(11) DEFAULT NULL,
  PRIMARY KEY (`spaceID`),
  UNIQUE KEY `spaceID` (`spaceID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3185 ;

-- --------------------------------------------------------

--
-- Table structure for table `tubeTable`
--

CREATE TABLE IF NOT EXISTS `tubeTable` (
  `tubeID` int(11) NOT NULL AUTO_INCREMENT,
  `sampleName` varchar(15) NOT NULL,
  `sampleType` enum('Antibody','Bacterial_Cultures','Blood','Body_fluids','Bone','Cell_Lines','Cell_Pellet','Chemical','Cytokine','DNA','Plasma','PBMC','RNA','Serum','Urine','Virus','Whole_Tissue','Primary_Cells') DEFAULT NULL,
  `freezeDate` date NOT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `experimentName` varchar(30) DEFAULT NULL,
  `labBook` varchar(30) DEFAULT NULL,
  `pageNum` int(11) DEFAULT NULL,
  `published` int(1) DEFAULT NULL,
  `mta` int(1) DEFAULT NULL,
  `public` enum('y','n') NOT NULL,
  `color` varchar(7) NOT NULL,
  `userID` int(11) DEFAULT NULL,
  `frozeBy` varchar(50) DEFAULT NULL,
  `intialNumTubes` tinyint(3) unsigned DEFAULT NULL,
  `consentYesNo` tinyint(3) DEFAULT NULL,
  `consentApprovalDate` date NOT NULL,
  `locConsent` varchar(32) DEFAULT NULL,
  `consentNotes` varchar(255) DEFAULT NULL,
  `irbID` int(11) DEFAULT NULL,
  `openAccess` int(11) DEFAULT NULL,
  PRIMARY KEY (`tubeID`),
  UNIQUE KEY `tubeID` (`tubeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=393 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `tube_info_vw`
--
CREATE TABLE IF NOT EXISTS `tube_info_vw` (
`tubeID` int(11)
,`spaceID` int(10) unsigned
,`boxID` int(11)
,`sampleName` varchar(15)
,`sampleType` enum('Antibody','Bacterial_Cultures','Blood','Body_fluids','Bone','Cell_Lines','Cell_Pellet','Chemical','Cytokine','DNA','Plasma','PBMC','RNA','Serum','Urine','Virus','Whole_Tissue','Primary_Cells')
,`freezeDate` date
,`experimentName` varchar(30)
,`labBook` varchar(30)
,`pageNum` int(11)
,`published` int(1)
,`mta` int(1)
,`color` varchar(7)
,`frozeBy` varchar(50)
,`userID` int(11)
,`intialNumTubes` tinyint(3) unsigned
,`consentYesNo` tinyint(3)
,`locConsent` varchar(32)
,`consentApprovalDate` date
,`consentNotes` varchar(255)
,`openAccess` int(11)
,`irbID` int(11)
,`groupID` int(11)
,`boxName` varchar(32)
,`privacyLevel` enum('Private','Group','Public')
,`reqRequired` int(1)
,`xLoc` tinyint(3) unsigned
,`yLoc` tinyint(3) unsigned
,`irbProjectTitle` varchar(255)
,`irbPI` varchar(100)
,`irbNum` varchar(20)
);
-- --------------------------------------------------------

--
-- Table structure for table `uniqueTable`
--

CREATE TABLE IF NOT EXISTS `uniqueTable` (
  `uniqueID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userID` int(10) unsigned NOT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `used` int(1) DEFAULT '0',
  PRIMARY KEY (`uniqueID`),
  UNIQUE KEY `uniqueID` (`uniqueID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12108 ;

-- --------------------------------------------------------

--
-- Table structure for table `updateTable`
--

CREATE TABLE IF NOT EXISTS `updateTable` (
  `updateID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userID` int(10) unsigned NOT NULL,
  `updateRef` int(10) unsigned NOT NULL,
  `updateType` enum('freezer','group','box','tube','request','delete_tube','update_tube') DEFAULT NULL,
  `updateChanges` blob NOT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`updateID`),
  UNIQUE KEY `updateID` (`updateID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=301 ;

-- --------------------------------------------------------

--
-- Table structure for table `userRoleTable`
--

CREATE TABLE IF NOT EXISTS `userRoleTable` (
  `userRoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `roleID` int(10) NOT NULL,
  `userID` int(10) NOT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createrUserID` int(10) NOT NULL,
  PRIMARY KEY (`userRoleID`),
  UNIQUE KEY `userRoleID` (`userRoleID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `userTable`
--

CREATE TABLE IF NOT EXISTS `userTable` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `emailAddress` varchar(40) NOT NULL,
  `password` varchar(60) NOT NULL,
  `role` varchar(32) NOT NULL,
  `status` varchar(8) NOT NULL,
  `phoneNumber` varchar(11) DEFAULT NULL,
  `labName` varchar(20) DEFAULT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `securityQuestion` tinyint(3) unsigned NOT NULL,
  `securityAnswer` varchar(32) DEFAULT NULL,
  `securityQuestion2` tinyint(3) DEFAULT NULL,
  `securityAnswer2` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `userID` (`userID`),
  UNIQUE KEY `emailAddress` (`emailAddress`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `user_name_groupID_vw`
--
CREATE TABLE IF NOT EXISTS `user_name_groupID_vw` (
`groupID` int(10) unsigned
,`memberID` int(10) unsigned
,`userID` int(11)
,`firstName` varchar(50)
,`lastName` varchar(50)
);
-- --------------------------------------------------------

--
-- Structure for view `all_group_members_vw`
--
DROP TABLE IF EXISTS `all_group_members_vw`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `all_group_members_vw` AS select `group_member_vw`.`groupID` AS `groupID`,group_concat(`group_member_vw`.`userName` separator ',') AS `allMembers`,`group_member_vw`.`groupName` AS `groupName` from `group_member_vw` group by `group_member_vw`.`groupID`;

-- --------------------------------------------------------

--
-- Structure for view `group_boxes_vw`
--
DROP TABLE IF EXISTS `group_boxes_vw`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `group_boxes_vw` AS select `b`.`boxID` AS `boxID`,`b`.`boxName` AS `boxName`,`b`.`privacyLevel` AS `privacyLevel`,`b`.`groupID` AS `groupID`,`b`.`xSize` AS `xSize`,`b`.`ySize` AS `ySize`,`b`.`shelfNum` AS `shelfNum`,`b`.`rackNum` AS `rackNum`,`b`.`rackSpaceNum` AS `rackSpaceNum`,`g`.`groupName` AS `groupName`,`f`.`freezerName` AS `freezerName`,`f`.`roomNum` AS `roomNum`,`g`.`reqRequired` AS `reqRequired`,((`b`.`xSize` * `b`.`ySize`) - count(`st`.`boxID`)) AS `emptySpaces` from (((`boxTable` `b` join `groupTable` `g` on((`b`.`groupID` = `g`.`groupID`))) join `freezerTable` `f` on((`b`.`freezerID` = `f`.`freezerID`))) left join `spaceTable` `st` on((`b`.`boxID` = `st`.`boxID`))) where (`b`.`privacyLevel` = 'Group') group by `b`.`boxID`;

-- --------------------------------------------------------

--
-- Structure for view `group_member_vw`
--
DROP TABLE IF EXISTS `group_member_vw`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `group_member_vw` AS select `gmt`.`groupID` AS `groupID`,concat(`ut`.`firstName`,' ',`ut`.`lastName`) AS `userName`,`gt`.`groupName` AS `groupName` from ((`groupMemberTable` `gmt` join `userTable` `ut` on((`ut`.`userID` = `gmt`.`userID`))) join `groupTable` `gt` on((`gmt`.`groupID` = `gt`.`groupID`)));

-- --------------------------------------------------------

--
-- Structure for view `private_boxes_vw`
--
DROP TABLE IF EXISTS `private_boxes_vw`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `private_boxes_vw` AS select `b`.`boxID` AS `boxID`,`b`.`boxName` AS `boxName`,`b`.`privacyLevel` AS `privacyLevel`,`b`.`userID` AS `userID`,`b`.`xSize` AS `xSize`,`b`.`ySize` AS `ySize`,`b`.`shelfNum` AS `shelfNum`,`b`.`rackNum` AS `rackNum`,`b`.`rackSpaceNum` AS `rackSpaceNum`,`f`.`freezerName` AS `freezerName`,`f`.`roomNum` AS `roomNum`,((`b`.`xSize` * `b`.`ySize`) - count(`st`.`boxID`)) AS `emptySpaces` from ((`boxTable` `b` join `freezerTable` `f` on((`b`.`freezerID` = `f`.`freezerID`))) left join `spaceTable` `st` on((`b`.`boxID` = `st`.`boxID`))) where (`b`.`privacyLevel` = 'Private') group by `b`.`boxID`;

-- --------------------------------------------------------

--
-- Structure for view `public_boxes_vw`
--
DROP TABLE IF EXISTS `public_boxes_vw`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `public_boxes_vw` AS select `b`.`boxID` AS `boxID`,`b`.`boxName` AS `boxName`,`b`.`privacyLevel` AS `privacyLevel`,`b`.`xSize` AS `xSize`,`b`.`ySize` AS `ySize`,`b`.`shelfNum` AS `shelfNum`,`b`.`rackNum` AS `rackNum`,`b`.`rackSpaceNum` AS `rackSpaceNum`,`f`.`freezerName` AS `freezerName`,`f`.`roomNum` AS `roomNum`,((`b`.`xSize` * `b`.`ySize`) - count(`st`.`boxID`)) AS `emptySpaces` from ((`boxTable` `b` join `freezerTable` `f` on((`b`.`freezerID` = `f`.`freezerID`))) left join `spaceTable` `st` on((`b`.`boxID` = `st`.`boxID`))) where (`b`.`privacyLevel` = 'Public') group by `b`.`boxID`;

-- --------------------------------------------------------

--
-- Structure for view `tube_info_vw`
--
DROP TABLE IF EXISTS `tube_info_vw`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tube_info_vw` AS select `tt`.`tubeID` AS `tubeID`,`st`.`spaceID` AS `spaceID`,`st`.`boxID` AS `boxID`,`tt`.`sampleName` AS `sampleName`,`tt`.`sampleType` AS `sampleType`,`tt`.`freezeDate` AS `freezeDate`,`tt`.`experimentName` AS `experimentName`,`tt`.`labBook` AS `labBook`,`tt`.`pageNum` AS `pageNum`,`tt`.`published` AS `published`,`tt`.`mta` AS `mta`,`tt`.`color` AS `color`,`tt`.`frozeBy` AS `frozeBy`,`tt`.`userID` AS `userID`,`tt`.`intialNumTubes` AS `intialNumTubes`,`tt`.`consentYesNo` AS `consentYesNo`,`tt`.`locConsent` AS `locConsent`,`tt`.`consentApprovalDate` AS `consentApprovalDate`,`tt`.`consentNotes` AS `consentNotes`,`tt`.`openAccess` AS `openAccess`,`tt`.`irbID` AS `irbID`,`bt`.`groupID` AS `groupID`,`bt`.`boxName` AS `boxName`,`bt`.`privacyLevel` AS `privacyLevel`,`gt`.`reqRequired` AS `reqRequired`,`st`.`xLoc` AS `xLoc`,`st`.`yLoc` AS `yLoc`,`it`.`irbProjectTitle` AS `irbProjectTitle`,`it`.`irbPI` AS `irbPI`,`it`.`irbNum` AS `irbNum` from ((((`spaceTable` `st` left join `tubeTable` `tt` on((`st`.`tubeID` = `tt`.`tubeID`))) left join `boxTable` `bt` on((`st`.`boxID` = `bt`.`boxID`))) left join `groupTable` `gt` on((`bt`.`groupID` = `gt`.`groupID`))) left join `irbTable` `it` on((`tt`.`irbID` = `it`.`irbID`)));

-- --------------------------------------------------------

--
-- Structure for view `user_name_groupID_vw`
--
DROP TABLE IF EXISTS `user_name_groupID_vw`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `user_name_groupID_vw` AS select `gm`.`groupID` AS `groupID`,`gm`.`memberID` AS `memberID`,`u`.`userID` AS `userID`,`u`.`firstName` AS `firstName`,`u`.`lastName` AS `lastName` from (`groupMemberTable` `gm` left join `userTable` `u` on((`gm`.`userID` = `u`.`userID`)));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
