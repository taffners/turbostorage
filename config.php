<?php
	define('SITE_TITLE', 'Turbo Storage');
	define('TURBO_VERISION', 'v1.6');
	/*  Database Information - Required!!  */
	/* -- Configure the Variables Below --*/
	define('DB_HOST', 'localhost');
	define('DB_USER', 'root');

	define('ROOT_URL',explode('/', $_SERVER['REQUEST_URI'])[1]);

	// set location of class dir & redirect address for dev vs live code
	if (ROOT_URL === 'devs')
	{

		define('CLASS_DIR', ($_SERVER['DOCUMENT_ROOT'].'/devs/turbostorage/classes/'));

		define('REDIRECT_URL', '/devs/turbostorage');

	}
	elseif (ROOT_URL === 'turbostorage')
	{
		define('CLASS_DIR', ($_SERVER['DOCUMENT_ROOT'].'/turbostorage/classes/'));

		define('REDIRECT_URL', '/turbostorage');
	}

	// get database password depending on if at work or not
	if (gethostname() == 'samantha-All-Series')
	{
		define('DB_PASSWORD', 'Smt@wylab');
	}
	else
	{
		define('DB_PASSWORD', 'root');
	}

	// set location of databse for dev vs live code
	if (ROOT_URL === 'devs')
	{
		define('DB_NAME', 'freezer_dev');
	}
	elseif (ROOT_URL === 'turbostorage')
	{
		define('DB_NAME', 'freezer');
	}

	/* Database Stuff, do not modify below this line */
	$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD) or die ("Couldn't connect to server.");
	$db = mysqli_select_db($conn, DB_NAME) or die("Couldn't select database.");

	// Add every class in the class dir
	if($handle = opendir(CLASS_DIR))
	{
		while(false !== ($file = readdir($handle)))
		{
    		if($file === '.' || $file === '..')
    		{
        		continue;
        	}

			require_once(CLASS_DIR.$file);
	    }

    		closedir($handle);
	}

	$db = new DataBase($conn);

	// make a date functions object
	$dfs = new DateFuncs();

	// Make a utils object
	$utils = new Utils();

	session_start();

	// see if user is logged in.  If the user is not logged in set to login page or change_password
	if(!isset($_SESSION['user'])  || !isset($_REQUEST['page']))
	{
		if (isset($_REQUEST['page']) && $_REQUEST['page'] === 'change_password')
		{
			$page = 'change_password';
		}
		else
		{
			$page = 'login';
		}
	}
	//
    	elseif(isset($_SESSION['user']))
	{
		define('USER_ID', $_SESSION['user']['userID']);

		//Make the page Name
		if(isset($_REQUEST['page']))
		{
			$page = $_REQUEST['page'];
		}

		else
		{
			$page = 'home';

		}

		// set up home Variables
		if ($page === 'home')
		{

			// set a greeting
			define('GREETING', $utils->TimeDayGreeting());

			// pic a funny picture to display
			define('FUNNY_PIC', $utils->PickRandomFileFromFolder('img/penguins/funny_pics'));

			// pic a funny picture to display
			define('HAPPY_PIC', $utils->PickRandomFileFromFolder('img/penguins/happy_pics'));
		}

		// Find all of the users roles
		$roleList = $db->listAll('user-roles', USER_ID);
	}

	session_write_close();
?>
