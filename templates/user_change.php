<div class="FormBoxCustom">
	<div class='container-fluid'>
		<table class="QueryInfo" id="BoxInfo">
			<thead>
				<tr>
					<th>user Name</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					for($i=0;$i<sizeof($userList);$i++)
					{
				?>
						<tr id="<?= $userList[$i]['userID']; ?>">
							<td>
								<?php 
									if($link == 'delete')
									{
								?>
										<a href="?page=user_list&action=<?= $link; ?>&userID=<?= $userList[$i]['userID']; ?>">
											<?= $userList[$i]['userName']; ?>
										</a>
								<?php
									}

									else
									{
								?>
										<a href="?page=user_form&freezerID=<?= $userList[$i]['userID']; ?>">
											<?= $userList[$i]['userName']; ?>
										</a>
								<?php
									}
								?>
							</td>
						</tr>
				<?php 
					}
				?>
			</tbody>
		</table>
	</div>
</div>
