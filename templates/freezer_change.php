<fieldset id='make-group-fieldset'></p>
	<legend>
		<?php
			if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update')
			{
		?>
				Update a freezer
		<?php
			}

			elseif (isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete')
			{
		?>
				Delete a Freezer Without Any Boxes In Them

		<?php
			}
		?>
	</legend>
	<div class="row">
		<?php
			if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update')
			{
		?>
				<div class="alert alert-success" role="alert">
					Click on the freezer row that you would like to update
				</div>
		<?php
			}

			elseif (isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete')
			{
		?>
				<div class="alert alert-Danger" role="alert">
					Click on the freezer row that you would like to delete.<br>
					<h1>Deletion is Permanent</h1>
				</div>

		<?php
			}
		?>




	</div>
	<div class="FormBoxCustom">
		<div class='container-fluid'>
			<?php
				require_once('templates/lists/freezer_list.php');
			?>
		</div>
	</div>
</fieldset>
