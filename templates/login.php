<div id="login-check-page-here"></div>
<?php
	if(isset($_REQUEST['userAdded']))
	{
		if(!$_REQUEST['userAdded'])
		{
?>
			<div class="col-xs-12 col-sm-12 col-md-12 pull-left alert alert-danger">
				User Could Not Be Added.  Because you are dumb.
			</div>
<?php
		}
		else
		{
?>
			<div class="col-xs-12 col-sm-12 col-md-12 pull-left alert alert-success">
				User Added.  Go ahead and login below.
			</div>
<?php
		}
	}
	elseif (isset($_REQUEST['failedLogin']) && $_REQUEST['attemptNum'] == 1)
	{
?>
		<div class="col-xs-12 col-sm-12 col-md-12 pull-left alert alert-danger">
			<div class="col-xs-6 col-sm-6 col-md-9">
				The email and/or Password you entered does not match our records. Try again.
			</div>
			<div class="col-xs-6 col-sm-6 col-md-3">
				<img src="img/failure.png" alt="firefox" height="240" width="240">
			</div>
		</div>
<?php
	}
 	elseif (isset($_REQUEST['failedLogin']) && $_REQUEST['attemptNum'] > 1  && $_REQUEST['attemptNum'] < 5)
	{
?>
		<div class="col-xs-12 col-sm-12 col-md-12 pull-left alert alert-warning">
			You have entered incorrect information <?= $_REQUEST['attemptNum']; ?> times.  5 incorrect attempts will lock your account.
		</div>
<?php
	}
	elseif (isset($_REQUEST['SuccessfulChangePassword']))
	{
?>
		<div class="col-xs-12 col-sm-12 col-md-12 pull-left alert alert-success">
			Password Changed Successfully!  Now you can login.
		</div>

<?php
	}
?>

<div class='container-fluid' id="loginContainer" >
	<div class='row' id="loginOutsideRow">;

		<div class='col-xs-offset-1 col-xs-10 col-sm-offset-1 col-sm-10 col-md-offset-3 col-md-6' id="loginCol" >
		<!-- row for description -->
			<div class="row" id="siteDescription">
				<div class='container-fluid TopName'>
					<?= SITE_TITLE; ?>
				</div>
				<div class='container-fluid FormHeaderCustom' >
					A Biological Sample Inventory Management System
				</div>

				Welcome to <?= SITE_TITLE; ?>!  <?= SITE_TITLE; ?> is an Inventory Management Software for tracking your frozen biological samples.  It is designed to be highly flexibile, cutomizable, efficient, visual, and easy to use.
				It allows you to use the same program for all your cold storage needs in a multi-user environment. The program includes the most commonly used storage formats in one program so that you do not have to switch between applications.
			</div>

			<!-- Hide all login information if they are using a browser which is not supported -->
			<div id="all_login_fields" class="show">
				<div class="FormBoxCustom">
					<form method='post' action="utils/process_login.php" class="form-horizontal" id="user_login_form" style='padding:10px;'>
						<p class='FormHeaderCustom'>User Login</p>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-2 col-md-2">
								<label for="inputEmailAddress" class="form-control-label">Email Address</label>
							</div>
							<div class="col-xs-12 col-sm-10 col-md-10 pull-left">
								<input type='email' id="inputEmailAddress" name='emailAddress' class="form-control" maxlength="40" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" placeholder='Full Email Address'>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-2 col-md-2">
								<label for="inputPassword" class="form-control-label">Password</label>
							</div>

							<div class="col-xs-10 col-sm-7 col-md-7 pull-left">
								<input type='password' class="form-control" id="inputPassword" name='password' maxlength="60" placeholder='Password'>
							</div>

							<div class="col-xs-2 col-sm-2 col-md-2 pull-left">
								<button type="button" id="show_password" class="btn btn-primary btn-lg">
									Show
								</button>
							</div>

						</div>

						<input type="hidden" id="form_checked_login" name="form_checked_login" value="0"/>

						<div class="form-group row">
							<div class="col-xs-8 col-sm-8 col-md-8">
								<button type="submit" id="loginForm" name="loginForm" value="Submit" class='btn btn-primary btn-lg'>Sign In</button>
							</div>

						</div>
						<div class="form-group row">

							<div class="col-xs-offset-9 col-xs-3 col-sm-offset-9 col-sm-3 col-md-offset-9 col-md-3 " style="margin-bottom:20px;">
								<button type="button" id="create_user_button" class="btn btn-success btn-md">
									Create New User
								</button>

							</div>
							<div class="col-xs-offset-9 col-xs-3 col-sm-offset-9 col-sm-3 col-md-offset-9 col-md-3 ">
								<button type="button" id="forgot_password" class="btn btn-warning btn-md">
									Forgot or Change <br>Password
								</button>
							</div>
						</div>
					</form>
				</div>

				<form id="create_user" class="form-horizontal hide" method="post" action="utils/create_user.php" style='padding:10px;'>
					<p class='FormHeaderCustom'>Create a New User</p>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-3 col-md-3">
							<label for="firstName" class="form-control-label">First Name</label>
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9 pull-left">
							<input type='text' id="firstName" maxlength="50" name='firstName' class="form-control"  value="">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-3 col-md-3">
							<label for="lastName" class="form-control-label">Last Name</label>
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9 pull-left">
							<input type='text' id="lastName" name='lastName' maxlength="50" class="form-control"  value="">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-3 col-md-3">
							<label for="emailAddress" class="form-control-label">Email Address</label>
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9 pull-left">
							<input type='email' id="emailAddress" maxlength="40" name='emailAddress' class="form-control"   pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" value="">
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 pull-left alert alert-success">
						Format for phone must include the area code.  Only numbers are allowed (no dashes or parentheses).  XXXXXXXXX
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-3 col-md-3">
							<label for="phoneNumber" class="form-control-label">Cell Phone #</label>
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9 pull-left">
							<input type='tel' id="phoneNumber" maxlength="11" name='phoneNumber' class="form-control"   pattern=".{10,11}" value="">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-3 col-md-3">
							<label for="labName" class="form-control-label">Lab Name</label>
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9 pull-left">
							<input type='text' id="labName" name='labName' maxlength="20"  class="form-control" value="">
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 pull-left alert alert-success">
						Password must be at least 8 characters long.
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-3 col-md-3">
							<label for="password" class="form-control-label">Password</label>
						</div>
						<div class="col-xs-10 col-sm-7 col-md-7 pull-left">
							<input type='password' id="password_create" name='password' class="form-control" maxlength="60"  pattern=".{8,}" value="">
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 pull-left">
							<button type="button" id="show_password_create" class="btn btn-primary btn-lg">
								Show
							</button>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-3 col-md-3">
							<label for="password2" class="form-control-label">Confirm Password</label>
						</div>
						<div class="col-xs-10 col-sm-7 col-md-7 pull-left">
							<input type='password' id="password_create2" name='password2' class="form-control" maxlength="60"  pattern=".{8,}" value="">
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 pull-left">
							<button type="button" id="show_password_create2" class="btn btn-primary btn-lg">
								Show
							</button>
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-12 pull-left alert alert-success">
						Below select and answer two security questions. This question will be used in resetting your passoword.
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-3 col-md-3">
							<label for="securityQuestion" class="form-control-label">Security Question 1</label>
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9 pull-left">
							<select class="form-control" name="securityQuestion" id="securityQuestion" >
								<option disabled selected value> -- select an option -- </option>
								<option value="1">What was the name of your first pet?</option>
								<option value="2">What High School did you attend?</option>
								<option value="3">In which City where you born?</option>
								<option value="4">What is your mother's maddien name?</option>
								<option value="5">What was your childhood nickname?</option>
								<option value="6">What is the name of your favorite childhood friend?</option>
								<option value="7">What school did you attend for sixth grade?</option>
								<option value="8">What street did you live on in third grade?</option>
								<option value="9">In what city or town did your mother and father meet?</option>
								<option value="10">In what city or town was your first job?</option>
								<option value="11">What is your maternal grandmother's maiden name?</option>
								<option value="12">In what city does your nearest sibling live?</option>
							</select>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-3 col-md-3">
							<label for="securityAnswer" class="form-control-label">Security Answer 1</label>
						</div>
						<div class="col-xs-10 col-sm-7 col-md-7 pull-left">
							<input type='password' id="securityAnswerCreate" maxlength="32" name='securityAnswer'  class="form-control" value="">
						</div>

						<div class="col-xs-2 col-sm-2 col-md-2 pull-left">
							<button type="button" id="show_security_answer_create" class="btn btn-primary btn-lg">
								Show
							</button>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-3 col-md-3">
							<label for="securityQuestion2" class="form-control-label">Security Question 2</label>
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9 pull-left">
							<select class="form-control" name="securityQuestion2" id="securityQuestion2" >
								<option disabled selected value> -- select an option -- </option>
								<option value="1">What was the name of your first pet?</option>
								<option value="2">What High School did you attend?</option>
								<option value="3">In which City where you born?</option>
								<option value="4">What is your mother's maddien name?</option>
								<option value="5">What was your childhood nickname?</option>
								<option value="6">What is the name of your favorite childhood friend?</option>
								<option value="7">What school did you attend for sixth grade?</option>
								<option value="8">What street did you live on in third grade?</option>
								<option value="9">In what city or town did your mother and father meet?</option>
								<option value="10">In what city or town was your first job?</option>
								<option value="11">What is your maternal grandmother's maiden name?</option>
								<option value="12">In what city does your nearest sibling live?</option>
							</select>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-3 col-md-3">
							<label for="securityAnswer2" class="form-control-label">Security Answer 2</label>
						</div>
						<div class="col-xs-10 col-sm-7 col-md-7 pull-left">
							<input type='password' id="securityAnswerCreate2" maxlength="32" name='securityAnswer2'  class="form-control" value="">
						</div>

						<div class="col-xs-2 col-sm-2 col-md-2 pull-left">
							<button type="button" id="show_security_answer_create2" class="btn btn-primary btn-lg">
								Show
							</button>
						</div>
					</div>



					<input type="hidden" id="form_checked_create" name="form_checked_create" value="0"/>

					<div class="form-group row">
						<div class="col-xs-10 col-sm-10 col-md-10">
							<input type="submit" id="create_user_submit" name="create_user_submit" value="Create New User" class='btn btn-primary btn-lg'>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2">
							<button type="reset" id="cancel_creating_user" class='btn btn-primary btn-lg'>
								<a href="?page=login">Cancel</a>
							</button>
						</div>
					</div>

				</form>
			</div>

			<div id="browser_warning" class="hide">
				<div class="col-xs-12 col-sm-12 col-md-12 pull-left alert alert-danger">
					Currently only Chrome &amp; Firefox web browsers are supported.  Please switch your browser.
					<div class="row">
						<div class="col-xs-offset-2 col-xs-6 col-sm-offset-2 col-sm-6 col-md-offset-2 col-md-6">
							<a href="https://www.mozilla.org/en-US/firefox/new/">
								<img src="img/firefox.jpg" alt="firefox" height="50" width="50">
							</a>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2">
							<a href="https://www.google.com/chrome/browser/desktop/">
								<img src="img/Chrome.png" alt="chrome" height="50" width="50">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
