<div class="col-xs-12 col-sm-12 col-md-12">
     <fieldset id='make-group-fieldset'></p>
          <legend id='permissions-legend' class='show'>
               Set Turbo Storage Page Permissions for All Users
          </legend>
          <div style="overflow-x:auto;">

               <table class="responsive-table-input-matrix QueryInfo dataTable">
          		<thead>
               		<tr>
               			<th></th>
                              <?php
                                   for($i=0; $i<sizeof($AllRoles); $i++)
                                   {
                              ?>
                                        <th id="header-id-<?= $AllRoles[$i]['roleID'];?>"><?= $AllRoles[$i]['roleName'];?></th>
                              <?php
                                   }

                              ?>
               		</tr>
          		</thead>
          		<tbody>
                         <?php
                         // iterate over all users names and then find all their roles
                              for($i=0; $i<sizeof($userNameList); $i++)
                              {
                         ?>
                              <tr id="row-id-<?= $userNameList[$i]['userID'];?>">
                                   <td id="cell-id-<?= $userNameList[$i]['userID'];?>"><?= $userNameList[$i]['userName'];?></td>

                              <?php
                              // iterate over all the roles and find if the user has permission or not
                                   for($k=0; $k<sizeof($AllRoles); $k++)
                                   {
                                        // add id for each td is td_userRoleID_userID_role_ID

                                        // find if user has permission
                                        $found_permission = false;
                                        for($j=0; $j<sizeof($AllUserRoleList); $j++)
                                        {
                                             if($AllUserRoleList[$j]['roleID'] == $AllRoles[$k]['roleID'] && $AllUserRoleList[$j]['userID'] == $userNameList[$i]['userID'])
                                             {
                                                  $found_permission = true;
                                             ?>

                                                  <td>
                                                       <input id="input_<?= $AllUserRoleList[$j]['userRoleID'];?>_<?= $AllUserRoleList[$j]['userID']; ?>_<?= $AllUserRoleList[$j]['roleID'];?>" type="checkbox" checked data-toggle="tooltip" data-placement="bottom" title="Click to Change <?= $AllRoles[$k]['roleName']; ?> Permission For <?= $AllUserRoleList[$j]['userName'];?>">
                                                  </td>
                                             <?php
                                                  break;
                                             }
                                        }

                                        if(!$found_permission)
                                        {
                                        ?>
                                             <td><input id="input_e_<?= $userNameList[$i]['userID']; ?>_<?= $AllRoles[$k]['roleID'];?>" type="checkbox" data-toggle="tooltip" data-placement="bottom" title="Click to Change <?= $AllRoles[$k]['roleName']; ?> Permission For <?= $userNameList[$i]['userName']; ?>"></td>
                                        <?php
                                        }
                              ?>

                              <?php
                                   }
                              ?>
                              </tr>
                         <?php
                              }

                         ?>

          		</tbody>
          	</table>
          </div>
     </fieldset>
     <!-- tubes already added -->
          <!-- tube search results -->
          <?php
               require_once('templates/lists/tube_search_list.php');
          ?>

     <!-- Boxes already added -->
     <div class="col-xs-12 col-sm-12 col-md-12">
          <fieldset id="make-group-fieldset">
               <legend>All Turbo Storage Boxes</legend>
               <?php
                    require_once('templates/lists/box_list.php');
               ?>
          </fieldset>
     </div>

     <!-- requests outstanding -->
     <div class="col-xs-12 col-sm-12 col-md-12">
          <?php
               require_once('templates/lists/req_list.php');
          ?>
     </div>

     <!-- groups you belong to box -->
     <div class="col-xs-12 col-sm-12 col-md-6">
          <?php
               require_once('templates/lists/group_list.php');
          ?>
     </div>

</div>
