<div class='container-fluid'>
	<div class="col-xs-12 col-sm-12 col-md-12 alert alert-danger">
		This form is to delete a tube from a box space.  This will free up the box space so another tube can be added to that space. All data related to the previous tube is retained and can be viewed in the reports section of # of tubes intial vs current.  The current # of tubes will be reduced.
	</div>
	<form name='remove_tube' id="remove_tube" class='form-horizontal' method='post'>

		<!-- tube search results -->
		<?php
			require_once('templates/lists/tube_search_list.php');
		?>


		<!-- Boxes already added -->
		<div class="col-xs-12 col-sm-12 col-md-12">
			<fieldset id="make-group-fieldset">
				<legend>Your Boxes already added</legend>
				<?php
					require_once('templates/lists/box_list.php');
				?>
			</fieldset>
		</div>

		<!-- Save clicked tube here -->
		<div id="clicked-tubes" class="hide"></div>

		<!-- save request form info here -->
		<div id="clicked-request" class="hide">

			<input id="clicked-request-id" name="reqID" value=""/>
			<input id="reqRequired-id" name="reqRequired" value=""/>
			<input id="freezerName-id" name="freezerName" value=""/>
			<input id="boxName-id" name="boxName" value=""/>
			<input id="shelfNum-id" name="shelfNum" value=""/>
			<input id="rackNum-id" name="rackNum" value=""/>
			<input id="rackSpaceNum-id" name="rackSpaceNum" value=""/>

		</div>

		<!-- Request form  -->
		<div id="request_form_table_div" class="col-xs-12 col-sm-12 col-md-12 hide">
			<div class="col-xs-12 col-sm-12 col-md-12 alert alert-success">
				This Sample requires a Sample Request Form.  Click on the <b>approved</b> sample request form that this sample is linked to.<span class="required-field">*</span>
				<br><br>
				If you do not see your sample request make sure that the request has a status of approved but not filled.  Only change request to filled if all samples have been removed.
			</div>


			<?php
				require_once('templates/lists/req_list.php');
			?>
		</div>

		<input type="hidden" id="form_checked" name="form_checked" value="0"/>

		<button type="submit" id="removeTubeSubmit" name="removeTubeSubmit" value="Submit"  class="btn btn-primary btn-lg">Remove this Tube</button>
	</form>

</div>
