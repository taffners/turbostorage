

<fieldset id-"outstanding-requests-fieldset">
     <?php
          if(isset($_REQUEST['page']) && $_REQUEST['page'] == 'remove_tube_form')
          {
     ?>
               <legend>Approved Outstanding Sample Requests</legend>
     <?php
          }
          elseif (isset($_REQUEST['page']) && $_REQUEST['page'] == 'report_request_filled')
          {
     ?>
               <legend>Filled Sample Requests</legend>
     <?php
          }
          else
          {
     ?>
               <legend>Outstanding Sample Requests</legend>
     <?php
          }
     ?>



     <div class="row" style="overflow-x:auto;">
          <table class="QueryInfo dataTable"
               <?php
                    if ((isset($_REQUEST['page']) && $_REQUEST['page'] == 'req_change'))
                    {
               ?>
                         id="addedRequestsList_hover"
               <?php
                    }
                    else
                    {
               ?>
                         id="addedRequestsList"
               <?php
                    }

               ?>
          >
               <thead>
                    <th>Request #</th>
                    <th class="hide">tubeID</th>
                    <th class="hide">userID</th>
                    <th>piName</th>
                    <th>labContactName</th>
                    <th>requestDate</th>
                    <th>sampleType</th>
                    <th>approved</th>
                    <th>requestFilled</th>
                    <?php
                         if (isset($_REQUEST['page']) && ($_REQUEST['page'] == 'remove_tube_form' || $_REQUEST['page'] == 'home') )
                         {
                    ?>
                              <th>comments</th>
                    <?php
                         }
                    ?>



               </thead>
               <tbody>
                    <?php

                         for($i=0;$i<sizeof($requestList);$i++)
                         {
                    ?>
                              <tr id="reqID_<?= $requestList[$i]['reqID']; ?>"
                                   <?php
                                        if((isset($_REQUEST['action']) && $_REQUEST['action'] == 'update'))
                                        {
                                   ?>
                                             class="update-redirect hover_row" data-href="?page=req_form&action=update&reqID=<?= $requestList[$i]['reqID']; ?>"
                                   <?php
                                        }
                                        elseif(isset($_REQUEST['page']) && $_REQUEST['page'] == 'report_request_filled')
                                        {
                                   ?>
                                             class="view-redirect hover_row" data-href="?page=req_form&action=view&reqID=<?= $requestList[$i]['reqID']; ?>"

                                   <?php
                                        }
                                        elseif(isset($_REQUEST['action']) && $_REQUEST['action'] === 'deleteTube')
                                        {
                                   ?>
                                             class='hover_row'
                                   <?php
                                        }
                                   ?>

                              >
                                   <td><?= $requestList[$i]['reqID']; ?></td>
                                   <td class="hide"><?= $requestList[$i]['tubeID']; ?></td>
                                   <td class="hide"><?= $requestList[$i]['userID']; ?></td>
                                   <td><?=  $requestList[$i]['piName']; ?></td>
                                   <td><?= $requestList[$i]['labContactName']; ?></td>
                                   <td><?=  $dfs->ChangeDateFormatUS($requestList[$i]['requestDate'], TRUE); ?></td>
                                   <td><?= $requestList[$i]['sampleType']; ?></td>
                                   <td>
                                        <?php
                                             if($requestList[$i]['approved'] == 1)
                                             {
                                        ?>
                                                  Approved
                                        <?php
                                             }
                                             elseif($requestList[$i]['approved'] == 0)
                                             {
                                        ?>
                                                  Denied
                                        <?php
                                             }
                                             elseif($requestList[$i]['approved'] == 2)
                                             {
                                        ?>
                                                  Waiting
                                        <?php
                                             }
                                        ?>

                                   </td>
                                   <td>
                                        <?php
                                             if($requestList[$i]['requestFilled'] == 0)
                                             {
                                        ?>
                                                  No
                                        <?php
                                             }
                                             elseif($requestList[$i]['requestFilled'] == 1)
                                             {
                                        ?>
                                                  Yes
                                        <?php
                                             }

                                        ?>

                                   </td>

                                   <?php
                                        if (isset($_REQUEST['page']) && ($_REQUEST['page'] == 'remove_tube_form' || $_REQUEST['page'] == 'home') )
                                        {
                                   ?>
                                             <td>
                                                  <button type="button" id="comment<?= $freezerList[$i]['freezerID']; ?>" class="btn btn-primary btn-lg add_comment_button"
                                                  data-comment-type="request" data-comment-id="<?= $requestList[$i]['reqID'];?>" onclick="addComments('request',);">
                                                       <span class="glyphicon glyphicon-comment"></span>
                                                  </button>
                                             </td>
                                   <?php
                                        }
                                   ?>


                              </tr>
                    <?php
                         }
                    ?>


               </tbody>

          </table>

     </div>
</fieldset>
