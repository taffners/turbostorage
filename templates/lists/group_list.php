<fieldset id="Groups-you-Belong-To-fieldset">
     <?php

          if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete')
          {
     ?>
               <legend>Delete a Group which is Not Being Used</legend>

     <?php
          }
          else
          {
     ?>
          <legend>Groups You Belong To</legend>
     <?php
          }
     ?>


     <div class="panel-group" id="accordion">
          <!-- iterate over all groups that the user is in -->
          <?php
               for ($i=0;$i<sizeof($user_groups); $i++)
               {
                    $members_userIDs = '';

                    // Get members in group
                    for ($j=0; $j<sizeof($member_groups); $j++)
                    {

                         if ($member_groups[$j]['groupID'] == $user_groups[$i]['groupID'])
                         {

                              $members_userIDs .= $member_groups[$j]['userID'].'_'.
                                   $member_groups[$j]['memberID'].',';


                         }
                    }
          ?>


                    <div class="panel panel-primary">
                         <div

                              <?php
                                   if(isset($_REQUEST['page']) && $_REQUEST['page'] == 'group_change')
                                   {
                              ?>
                                        class="panel-heading update-redirect hover_accordion" data-href="?page=group_form&action=update&userID=<?= USER_ID; ?>&groupID=<?= $user_groups[$i]['groupID'] ; ?>&members_userIDs=<?= $members_userIDs; ?>" data-toggle="tooltip" data-placement="top" title="Click to Edit Group"
                              <?php
                                   }
                                   else
                                   {
                              ?>
                                        class="panel-heading"
                              <?php
                                   }
                              ?>
                         >
                              <h4 class="panel-title">
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i;?>">
                                        <?php echo $user_groups[$i]['groupName']; ?>
                                   </a>
                              </h4>


                         </div>

                         <div id="collapse<?php echo $i;?>" class="panel-collapse collapse in">
                              <div class="panel-body">
                                   <div class="col-xs-10 col-sm-10 col-md-10">
                                        <?php
                                             $to_echo = '';
                                             $num_members = 0;

                                             // iterate over all the users in groups that the current userID is in
                                             for ($j=0;$j<sizeof($member_groups); $j++)
                                             {
                                                  if ($member_groups[$j]['groupName'] == $user_groups[$i]['groupName'])
                                                  {

                                                       // Add comma's between names
                                                       if ($num_members > 0)
                                                       {
                                                            $to_echo .= ', ';
                                                       }

                                                       $to_echo .= $member_groups[$j]['firstName'].' ';
                                                       $to_echo .= $member_groups[$j]['lastName'];
                                                       $num_members++;
                                                  }
                                             }
                                             echo $to_echo;

                                        ?>
                                   </div>

                                   <div class="col-xs-2 col-sm-2 col-md-2">
                                        <button type="button" id="comment<?= $user_groups[$i]['groupID']; ?>" class="btn btn-primary btn-lg add_comment_button" data-comment-type="group" data-comment-id="<?= $user_groups[$i]['groupID'] ?>">
                                             <span class="glyphicon glyphicon-comment"></span>
                                        </button>
                                   </div>

                              </div>
                         </div>
                    </div>


          <?php
               }


          ?>

     </div>
</fieldset>
