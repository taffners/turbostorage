<!-- freezers already added -->

<div style="overflow-x:auto;">
     <table class="QueryInfo dataTable" id="FreezerInfo">
          <thead>
               <tr>
                    <th style="display:none;">freezer ID</th>
                    <th>Name</th>
                    <th>Room #</th>
                    <th>Temp</th>
                    <th># of Shelves</th>
                    <th>Comments</th>
               </tr>
          </thead>
          <tbody>
               <?php
                    for($i=0;$i<sizeof($freezerList);$i++)
                    {
               ?>
                         <tr id="freezer_<?= $freezerList[$i]['freezerID']; ?>"
                              <?php
                                   if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update'  && $_REQUEST['page'] != 'box_form')
                                   {
                              ?>
                                        class="hover_row update-redirect freezer_info_row" data-href="?page=freezer_form&action=update&freezerID=<?= $freezerList[$i]['freezerID']; ?>"
                                        data-numShelves="<?= $freezerList[$i]['numShelf']; ?>"
                              <?php
                                   }
                                   elseif (isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete')
                                   {
                              ?>
                                        class="hover_row delete-redirect freezer_info_row" data-href="?page=freezer_change&action=<?= $link; ?>&freezerID=<?= $freezerList[$i]['freezerID']; ?>"
                                        data-itemName="<?= $freezerList[$i]['freezerName'];?>"
                                        data-numShelves="<?= $freezerList[$i]['numShelf']; ?>"

                              <?php
                                   }
                                   elseif (isset($_REQUEST['page']) && $_REQUEST['page'] == 'home')
                                   {
                              ?>
                                        class="no_hover freezer_info_row"
                                        data-numShelves="<?= $freezerList[$i]['numShelf']; ?>"
                              <?php
                                   }
                                   elseif (isset($_REQUEST['page']) && $_REQUEST['page'] === 'box_form')
                                   {
                              ?>
                                        class="hover_row freezer_info_row"
                              <?php
                                   }
                                   else
                                   {
                              ?>
                                        class="freezer_info_row"
                                        data-numShelves="<?= $freezerList[$i]['numShelf']; ?>"
                              <?php
                                   }

                              ?>


                         >
                              <td style="display:none;">freezer_<?= $freezerList[$i]['freezerID']; ?></td>
                              <td><?php echo $freezerList[$i]['freezerName']; ?></td>
                              <td><?php echo $freezerList[$i]['roomNum']; ?></td>
                              <td><?php echo $freezerList[$i]['temp']; ?>&deg;C</td>
                              <td><?php echo $freezerList[$i]['numShelf']; ?></td>
                              <td>
                                   <button type="button" id="comment<?= $freezerList[$i]['freezerID']; ?>" class="btn btn-primary btn-lg add_comment_button" data-comment-type="freezer" data-comment-id="<?= $freezerList[$i]['freezerID']; ?>">
                                        <span class="glyphicon glyphicon-comment"></span>
                                   </button>
                              </td>
                         </tr>
               <?php
                    }


               ?>
          </tbody>
     </table>
</div>
