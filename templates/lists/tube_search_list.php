<!-- tubes search added -->
<div class="col-xs-12 col-sm-12 col-md-12">
	<fieldset id="search-tubes-fieldset">
		<?php
			if(isset($_REQUEST['page']) && $_REQUEST['page'] === 'tube_change')
			{
		?>
				<legend>Search for a Tube to Update Then Click on the Sample Row</legend>
		<?php
			}
			else
			{
		?>
				<legend>Search Your Tubes</legend>
		<?php
			}
		?>


		<div class="row">
			<div class="col-xs-12 col-sm-5 col-md-3 pull-left">
				<input type="text" class="form-control input-sm searchPressEnter" placeholder="" aria-controls="addedBoxesList" id="TubeSearchTerm">
			</div>

			<div class="col-xs-12 col-sm-1 col-md-1">
				<button type="button" id="searchTubes" name="searchTubes" class="btn btn-primary btn-lg">Search
				</button>
			</div>
		</div>

		<!-- Add search results of table here -->
		<div class="row">
			<div style="overflow-x:auto;margin-top:20px;">
				<table class="QueryInfo dataTableNoSort" id="tubeSearchResults">
					<thead>
						<tr>
							<th style="display:none;">tubeID</th>
							<th style="display:none;">spaceID</th>
							<th style="display:none;">boxID</th>
							<th>Sample Name</th>
							<th>Type</th>
							<th>Freeze Date</th>
							<th>Box Name</th>
							<th>Froze By</th>
							<th>Initial # of Tubes</th>
							<th>Current # of Tubes</th>
						</tr>
					</thead>
					<tbody class="hide" id="clone_body">
						<tr class="notselected" id="clone_row">
							<td style="display:none;" data-row-name="tubeID">tubeID</td>
							<td style="display:none;" data-row-name="spaceID">spaceID</td>
							<td style="display:none;" data-row-name="boxID">boxID</td>
							<td data-row-name="sampleName">sampleName</td>
							<td data-row-name="sampleType">sampleType</td>
							<td data-row-name="freezeDate">freezeDate</td>
							<td data-row-name="boxName">boxName</td>
							<td data-row-name="frozeBy">Froze By</td>
							<td data-row-name="intialNumTubes">Initial # of Tubes</td>
							<td data-row-name="tubeCount">Current # of Tubes</td>
						</tr>
					</tbody>
					<tbody id="insert_search_results_here">

					</tbody>
				</table>
			</div>
		</div>
	</fieldset>
</div>
