<!-- Display all users -->
<div class="row"  style="overflow-x:auto;">
     <table class="QueryInfo dataTable" id="Users_List" >
          <thead>
               <tr>
                    <th style="display:none;">userID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
				<th>Email</th>
				<th>Phone #</th>
				<th>Lab</th>
               </tr>
          </thead>

		<tbody>
			<?php

				for($i=0;$i<sizeof($all_users);$i++)
				{
			?>
					<tr

                         <?php
                              if(isset($_REQUEST['page']) && $_REQUEST['page'] == 'group_form')
                              {
                         ?>
                                   class="hover_row add-user-event add-delete-toggle-user"
                         <?php
                              }
                              else
                              {
                         ?>
                                   class="hover_row notselected"
                         <?php
                              }
                         ?>

                         id="userID_<?= $all_users[$i]['userID']; ?>">
						<td class="user_id" style="display:none;" id="userID_<?= $all_users[$i]['userID']; ?>"><?= $all_users[$i]['userID']; ?></td>
						<td><?= $all_users[$i]['firstName']; ?></td>
						<td><?= $all_users[$i]['lastName']; ?></td>
						<td><?= $all_users[$i]['emailAddress']; ?></td>
						<td><?= $all_users[$i]['phoneNumber']; ?></td>
						<td><?= $all_users[$i]['labName']; ?></td>
					</tr>

			<?php
				}
			?>

		</tbody>
	</table>
</div>
