<!-- Boxes already added -->
<!--Box Table is filled in here  -->
<?php
     if (isset($_REQUEST['page']) && ($_REQUEST['page'] === 'home' || $_REQUEST['page'] === 'tube_form' ) || $_REQUEST['page'] == 'box_info')
     {
?>
          <div class="row" style="margin-bottom:10px;">
               <div class="col-xs-offset-5 col-sm-offset-6 col-md-offset-8 col-xs-1 col-sm-1 col-md-1" style="padding-top:2px;">
                    <u>Filters:</u>
               </div>
               <div class="col-xs-6 col-sm-5 col-md-3">
                    <button type="button" class="btn btn-info btn-xs" id="private-box-filter-button">private</button>
                    <button type="button" class="btn btn-info btn-xs" id="public-box-filter-button">public</button>
                    <button type="button" class="btn btn-info btn-xs" id="group-box-filter-button">group</button>
                    <button type="button" class="btn btn-default btn-xs" id="full-box-filter-button">full</button>
                    <button type="button" class="btn btn-default btn-xs" id="unfull-box-filter-button">not full</button>
                    <button type="button" class="btn btn-success btn-xs" id="all-box-filter-button">all</button>
               </div>
          </div>
<?php
     }
?>

<div class="row"  style="overflow-x:auto;">
     <table class="QueryInfo dataTable" id="addedBoxesList" >
          <thead>
               <tr>
                    <th class="hide">ID</th>
                    <th
                    <?php
                         if ($_REQUEST['page'] != 'box_info')
                         {
                    ?>
                              class="hide"
                    <?php
                         }
                    ?> >Save Box Info</th>
                    <th
                    <?php
                         if ($_REQUEST['page'] != 'box_info')
                         {
                    ?>
                              class="hide"
                    <?php
                         }
                    ?> >Save Box Layout</th>
                    <th>Box Name</th>
                    <th>Access</th>
                    <th>Group</th>
                    <th>X Size</th>
                    <th>Y Size</th>
                    <th>Freezer Name</th>
                    <th>Freezer Room Num</th>
                    <th>Shelf Number</th>
                    <th>Rack Number</th>
                    <th>Rack Space Number</th>
                    <th>Request Form Required</th>
                    <th># Spaces Empty</th>
                    <th>Comments</th>
               </tr>
          </thead>

          <tbody>

          <!-- fill in the data for all the boxes -->
          <?php

               for($i=0;$i<sizeof($boxList);$i++)
               {

          ?>

                    <tr id="box_<?= $boxList[$i]['boxID']; ?>"
                         <?php
                              // list for all the boxes present for updating
                              if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update' && isset($_REQUEST['page']) && $_REQUEST['page'] === 'box_change')
                              {
                         ?>
                                   class="hover_row update-redirect" data-href="?page=box_form&action=update&boxID=<?= $boxList[$i]['boxID']; ?>"
                         <?php
                              }
                              // for deleting box
                              elseif (isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete')
                              {
                         ?>
                                   class="hover_row delete-redirect" data-href="?page=box_change&action=<?= $link; ?>&boxID=<?= $boxList[$i]['boxID']; ?>"

                         <?php
                              }
                              elseif (isset($_REQUEST['page']) && ($_REQUEST['page'] === 'home' || $_REQUEST['page'] === 'tube_form' ) || $_REQUEST['page'] == 'box_info')
                              {
                                   switch($boxList[$i]['privacyLevel'])
                                   {
                                        case 'Private':
                                             $privacy_level = 'private-filter';
                                             break;
                                        case 'Public':
                                             $privacy_level = 'public-filter';
                                             break;
                                        case 'Group':
                                             $privacy_level = 'group-filter';
                                             break;
                                   }
                                   switch($boxList[$i]['emptySpaces'])
                                   {
                                        case 0:
                                             $full_level = 'full-filter';
                                             break;
                                        default:
                                             $full_level = 'unfull-filter';
                                             break;
                                   }
                                   $filter_class = $privacy_level.' '.$full_level.' all-filter'

                         ?>
                                   class="hover_row pop-up-box pop-up-print-table <?= $filter_class;?>"
                         <?php
                              }
                              elseif (isset($_REQUEST['page']) && $_REQUEST['page'] === 'box_form')
                              {
                         ?>
                                   class="no_hover"
                         <?php
                              }
                              elseif (isset($_REQUEST['page']) && $_REQUEST['page'] === 'remove_tube_form' && $_REQUEST['action'] === 'deleteTube')
                              {
                         ?>
                                   class="hover_row"
                         <?php
                              }
                         ?>
                         data-reqRequired=
                         <?php
                              if($boxList[$i]['reqRequired'] == '')
                              {
                         ?>
                                   "0"
                         <?php
                              }
                              else
                              {
                         ?>
                                   "<?= $boxList[$i]['reqRequired']; ?>"
                         <?php
                              }

                              if ($_REQUEST['page'] === 'box_info')
                              {
                         ?>

                         <?php
                              }
                         ?>

                         data-freezer-name="<?= $boxList[$i]['freezerName']; ?>"
                         data-box-name="<?= $boxList[$i]['boxName']; ?>"
                         data-shelf-num="<?= $boxList[$i]['shelfNum']; ?>"
                         data-rack-num="<?= $boxList[$i]['rackNum']; ?>"
                         data-rack-space-num="<?= $boxList[$i]['rackSpaceNum']; ?>"
                         data-x-size="<?= $boxList[$i]['xSize']; ?>"
                         data-y-size="<?= $boxList[$i]['ySize']; ?>"
                         data-room-num="<?= $boxList[$i]['roomNum']; ?>"
                         data-rack-num="<?= $boxList[$i]['rackNum']; ?>"
                         data-box-id="<?= $boxList[$i]['boxID']; ?>"
                         data-itemName="<?= $boxList[$i]['boxName']; ?>"
                    ><!--links-->
                         <td class="hide">

                         </td><!--links-->

                         <?php
                              if ($_REQUEST['page'] === 'box_info')
                              {
                         ?>
                         <td>
                              <button type="button" id="box_info_save_csv<?= $boxList[$i]['boxID'] ?>" class="btn btn-primary btn-lg" data-toggle="tooltip" data-placement="right" title="Click to download csv of box info">
                                   <a href="utils/GetBoxInfoCSV.php?box_id=<?= $boxList[$i]['boxID'] ?>">
                                        <img src="img/save.png" style="height:23px;width:23px;">
                                        <img src="img/box-open.png" style="height:23px;width:23px;">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                   </a>
                              </button>
                         </td>
                         <td>
                              <button type="button" id="box_info_save_csv<?= $boxList[$i]['boxID'] ?>" class="btn btn-primary btn-lg" data-toggle="tooltip" data-placement="right" title="Click to download csv of Box Layout">
                                   <a href="utils/GetGridCSV.php?box_id=<?= $boxList[$i]['boxID'] ?>">
                                        <img src="img/save.png" style="height:23px;width:23px;">
                                        <img src="img/grid.svg" style="height:23px;width:23px;">
                                   </a>
                              </button>
                         </td>
                         <?php
                              }
                              else
                              {
                         ?>
                                   <td class="hide"></td>
                                   <td class="hide"></td>
                         <?php
                              }
                         ?>

                         <td><?= $boxList[$i]['boxName']; ?></td><!--box Name-->
                         <td><?= $boxList[$i]['privacyLevel']; ?></td>
                         <td><?= $boxList[$i]['groupName']; ?></td>
                         <td><?= $boxList[$i]['xSize']; ?></td><!--X Size-->
                         <td><?= $boxList[$i]['ySize']; ?></td><!--Y Size-->
                         <td><?= $boxList[$i]['freezerName']; ?></td><!--Freezer Name-->
                         <td><?= $boxList[$i]['roomNum']; ?></td><!--Freezer Room Num-->
                         <td><?= $boxList[$i]['shelfNum']; ?></td><!--Shelf Number-->
                         <td><?= $boxList[$i]['rackNum']; ?></td><!---->
                         <td><?= $boxList[$i]['rackSpaceNum']; ?></td><!--Rack Number-->

                         <td>
                              <?php
                                   if($boxList[$i]['reqRequired'] == 0)
                                   {
                              ?>
                                        No

                              <?php
                                   }
                                   else
                                   {
                              ?>
                                        Yes
                              <?php
                                   }
                              ?>


                         </td>
                         <td><?= $boxList[$i]['emptySpaces']; ?></td>

                         <td>
                              <button type="button" id="comment<?php if (isset($freezerList[$i]['freezerID'])) echo $freezerList[$i]['freezerID']; ?>" class="btn btn-primary btn-lg add_comment_button" data-comment-type="box" data-comment-id="<?= $boxList[$i]['boxID']; ?>">
                                   <span class="glyphicon glyphicon-comment"></span>
                              </button>
                         </td>
                    </tr>
          <?php
               }
          ?>
          </tbody>
     </table>
</div>
<!-- A hidden div with all of the information of the choosen box -->

<!-- add boxid here -->
     <input type="hidden" name="BoxID" id="BoxID" value="<? if (isset($_REQUEST['boxID'])) echo $_REQUEST['boxID']; ?>">


<div id="choosen_print_sheet">
     <div id="choosen_box_Info" class="show">

          <?php
               if($_REQUEST['page'] === 'tube_form')
               {
          ?>
               <div class="row">
                    <div class="alert alert-success hide" role="alert" id="select_spots">
                         <h4>Select an empty Spot or Spots to add Samples to <span class="required-field">*</span></h4>
                    </div>
               </div>
          <?php
               }
               else if ($_REQUEST['page'] === 'remove_tube_form')
               {
          ?>
                    <div class="row">
                         <div class="alert alert-success hide" role="alert" id="select_spots">
                              <h4>Select the tube you would like to remove.<span class="required-field">*</span></h4>
                         </div>
                    </div>
          <?php
               }
          ?>
          <div class="row hide">
               <!-- clone for making box grid -->
               <div id="clone_grid_div">
                    <table class="grid" id="clone_grid_table">
                         <thead id="clone_grid_thead">
                              <tr id="clone_grid_thead_row">
                                   <td class="headerCell" id="clone_grid_thead_column" data-r="r" data-c="c"></td>
                                   <!-- etc... -->
                              </tr>
                         </thead>
                         <tbody id="clone_grid_tbody">
                              <tr id="clone_grid_tbody_row">
                                   <td class="headerCell" id="clone_grid_tbody_headerCell" data-r="r" data-c="c">A</td>
                                   <td class="emptyCell" id="clone_grid_tbody_emptyCell" data-r="r" data-c="c"></td>
                                   <td class="fullCell pop-up-accordion" id="clone_grid_tbody_fullCell"
                                   data-r="r" data-c="c"
                                   data-tube-i-d="tubeID" data-space-i-d="spaceID" data-box-i-d="boxID"
                                   data-color="color"
                                   >LongSampleName</td>
                              </tr>
                         </tbody>
                    </table>
               </div>
          </div>
          <?php
               if($_REQUEST['page'] === 'home')
               {
          ?>
                    <div class="row">
                         <div class="col-xs-1 col col-xs-offset-10 col-sm-1 col-sm-offset-10 col-md-offset-10 col-md-1 hide" style="margin-top:10px;margin-bottom:10px;" id="box-location-modal-button">
                              <button class="btn btn-primary btn-lg" role="button" data-toggle="tooltip" data-placement="top" title="Location of Box Tips"><span class="glyphicon glyphicon-info-sign"></span><img src="img/box-open.png" style='height:23px;width:23px;'></button>
                         </div>

                         <div class="col-xs-1 col col-sm-1 col-md-1 hide" style="margin-top:10px;margin-bottom:10px;" id="save-button-home">
                              <button  id="print-box-info-tube-report-button"class="btn btn-primary btn-lg" role="button" data-toggle="tooltip" data-placement="top" title="Save Box"><img src="img/save.png" style='height:23px;width:23px;'></button>
                         </div>
                    </div>
          <?php
               }
          ?>
          <div class="row">
               <div class="col-xs-12 col-sm-12 col-md-6" id="insertBoxHere" >
                    <div class="container-fluid TopName hide" id="turbo-storage-icon-printing"><img src="img/Snowflake.ico" style='height:30px;width:30px;'><?= SITE_TITLE; ?></div>
                    <table class="grid" id="grid_table">
                         <thead id="grid_thead">
                              <tr id="grid_thead_row">

                              </tr>
                         </thead>
                         <tbody id="grid_tbody">

                         </tbody>
                    </table>
                    <?php
                         if($_REQUEST['page'] === 'tube_form' || $_REQUEST['page'] === 'home' || $_REQUEST['page'] === 'remove_tube_form')
                         {
                    ?>
                         <div class="row hide" id="print_table_div" style="margin-top:10px;">
                              <div class="col-xs-10 col-md-10">
                                   <table class="print_info">
                                        <thead>
                                             <tr>
                                                  <th>Box Name</th>
                                                  <th>Freezer Name</th>
                                                  <th>Freezer Room #</th>
                                                  <th>Shelf #</th>
                                                  <th>Rack #</th>
                                                  <th>Rack Space #</th>
                                             </tr>
                                        </thead>

                                        <tbody class="hide" id="clone_body_print_info">
                                             <tr id="clone_row_print_info">
                                                  <td data-row-name="boxName" data-attr-name="box-name"></td>
                                                  <td data-row-name="freezerName" data-attr-name="freezer-name"></td>
                                                  <td data-row-name="roomNum" data-attr-name="room-num"></td>
                                                  <td data-row-name="shelfNum" data-attr-name="shelf-num"></td>
                                                  <td data-row-name="rackNum" data-attr-name="rack-num"></td>
                                                  <td data-row-name="rackSpaceNum" data-attr-name="rack-space-num"></td>
                                             </tr>
                                        </tbody>
                                        <tbody id="insert_print_info_here">

                                        </tbody>
                                   </table>
                              </div>
                         </div>
                    <?php
                         }
                    ?>
               </div>

               <!-- tube info tables insert here -->
               <div id="insert-tube-info-tables-here">
               </div>

          </div>

          <div id="cloning-tables">
               <div class="col-xs-12 col-sm-12 col-md-6 show" id="tube-info-clone-div">
                    <div class="row show" sytle="padding-bottom:5px;">
                         <div class="dup-tube-button hide">
                              <button  class="btn btn-primary btn-lg" role="button" ><img src="img/duplicate_tubes.png" style='height:23px;width:50px;'></button>
                         </div>
                         <div class="update-tube-button hide">
                              <button  class="btn btn-primary btn-md" role="button"><img src="img/update_tube.png" style='height:23px;width:31px;'></button>
                         </div>
                         <div class="delete-tube-info-table hide" id="delete-tube-info-table-clone">
                              <button  class="btn btn-primary btn-md" role="button"><img src="img/button-delete-icon.png" style='height:23px;width:31px;'></button>
                         </div>
                    </div>
               </div>

               <div class="col-xs-12 col-sm-12 col-md-6 hide" >

                    <table class="QueryInfo" id="tube-info-contents-clone">
                         <thead>
                              <tr id="table-info-clone-sampleName">
                                   <th colspan="2" data-row-name="sampleName">Sample Name</th>
                              </tr>
                         </thead>
                         <tbody class="smaller-type">
                              <tr id="table-info-clone-comments">
                                   <td class="description-col">Comments</td>
                                   <td data-row-name="comments">None</td>
                              </tr>
                              <tr id="table-info-clone-sampleTypeVars">
                                   <td class="description-col" data-row-name="variableName">Comments</td>
                                   <td data-row-name="variableValue">None</td>
                              </tr>
                              <tr id="table-info-clone-boxName">
                                   <td class="description-col">Box Name</td>
                                   <td data-row-name="boxName"></td>
                              </tr>
                              <tr id="table-info-clone-reqRequired">
                                   <td class="description-col">Removal Request</td>
                                   <td data-row-name="reqRequired"></td>
                              </tr>
                              <tr id="table-info-clone-intialNumTubes">
                                   <td class="description-col">Initial # of Tubes</td>
                                   <td data-row-name="intialNumTubes"></td>
                              </tr> comments and make them into a numbered list
                              <tr id="table-info-clone-freezeDate">
                                   <td class="description-col">Freeze Date</td>
                                   <td data-row-name="freezeDate"></td>
                              </tr>
                              <tr id="table-info-clone-frozeBy">
                                   <td class="description-col">Froze By</td>
                                   <td data-row-name="frozeBy"></td>
                              </tr>
                              <tr id="table-info-clone-labBook">
                                   <td class="description-col">Lab Book</td>
                                   <td data-row-name="labBook"></td>
                              </tr>
                              <tr id="table-info-clone-experimentName">
                                   <td class="description-col">Experiment Name</td>
                                   <td data-row-name="experimentName"></td>
                              </tr>
                              <tr id="table-info-clone-pageNum">
                                   <td class="description-col">Page Number</td>
                                   <td data-row-name="pageNum"></td>
                              </tr>
                              <tr id="table-info-clone-published">
                                   <td class="description-col">Published</td>
                                   <td data-row-name="published"></td>
                              </tr>
                              <tr id="table-info-clone-mta">
                                   <td class="description-col">MTA</td>
                                   <td data-row-name="mta"></td>
                              </tr>
                              <tr id="table-info-clone-consentYesNo">
                                   <td class="description-col">Have a Consent?</td>
                                   <td data-row-name="consentYesNo"></td>
                              </tr>
                              <tr id="table-info-clone-consentApprovalDate">
                                   <td class="description-col">Consent Approval Date</td>
                                   <td data-row-name="consentApprovalDate"></td>
                              </tr>
                              <tr id="table-info-clone-IRBQuestion">
                                   <td class="description-col">IRB Question</td>
                                   <td data-row-name="IRBQuestion"></td>
                              </tr>
                              <tr id="table-info-clone-answer">
                                   <td colspan="2" data-row-name="answer"></td>
                              </tr>
                              <tr id="table-info-clone-locConsent">
                                   <td class="description-col">Consent Location</td>
                                   <td data-row-name="locConsent"</td>
                              </tr>
                              <tr id="table-info-clone-consentNotes">
                                   <td class="description-col">Consent Notes</td>
                                   <td data-row-name="consentNotes"></td>
                              </tr>
                              <tr id="table-info-clone-openAccess">
                                   <td class="description-col">Open Access Sharing</td>
                                   <td data-row-name="openAccess"></td>
                              </tr>
                              <tr id="table-info-clone-sampleType">
                                   <td class="description-col">Sample Type</td>
                                   <td data-row-name="sampleType"></td>
                              </tr>
                              <tr id="table-info-clone-irbProjectTitle">
                                   <td class="description-col">IRB Title</td>
                                   <td data-row-name="irbProjectTitle"></td>
                              </tr>
                              <tr id="table-info-clone-irbPI">
                                   <td class="description-col">IRB PI</td>
                                   <td data-row-name="irbPI"></td>
                              </tr>
                              <tr id="table-info-clone-irbNum">
                                   <td class="description-col">IRB Number</td>
                                   <td data-row-name="irbNum"></td>
                              </tr>
                         </tbody>
                    </table>
               </div>
          </div>
     </div>

</div>

<!-- Insert all locations of choosen cells here as hidden fields-->
<div class="hide" id="Clicked_Locs">

</div>
