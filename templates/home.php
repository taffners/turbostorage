<div class="hide" id="home-check-page-here"></div>
<div class='container-fluid'>
	<!-- welcome -->
	<div class="row" id="welcome">

			<?php
				if(isset($_REQUEST['success']))
				{
			?>
					<div class="success">
						<div class="row">
							<div class="col-xs-9 col-sm-9 col-md-9">
								SUCCESSS!
							</div>
							<div class="col-xs-3 col-sm-3 col-md-3">
								<img src="<?= HAPPY_PIC; ?>" style='height:150px;' alt="happy picture of a penguin">
							</div>
						</div>
					</div>
			<?php
				}
				else
				{
			?>
					<div class="row">
						<div class="col-xs-9 col-sm-9 col-md-9">
							<?php
								echo GREETING.' '.$user_info[0]['firstName'];
							?>
						</div>
						<div class="col-xs-3 col-sm-3 col-md-3">
							<img src="<?= FUNNY_PIC; ?>" style="height:150px;" alt="funny picture of a penguin">
						</div>
					</div>

			<?php
				}
			?>

			<?php
				if(isset($_REQUEST['boxID']))
				{
			?>
					<input type="hidden" id="boxID" name="boxID" value="<?= $boxID; ?>"/>
			<?php
				}
				else
				{
			?>
					<input type="hidden" id="boxID" name="boxID" value=""/>
			<?php
				}

			?>


	</div>
	<div class="row" style="margin-bottom:20px;">
		<div class="col-xs-3 col-sm-3 col-md-3"></div>
		<div class="col-xs-3 col-sm-3 col-md-3">
			<a href="?page=tube_form&action=add" class="btn btn-success btn-lg" role="button">Add a Tube</a>
		</div>

		<div class="col-xs-3 col-sm-3 col-md-3">
			<a href="?page=remove_tube_form&action=deleteTube" class="btn btn-danger btn-lg" role="button">Remove a Tube</a>
		</div>
		<div class="col-xs-3 col-sm-3 col-md-3"></div>
	</div>


	<!-- tubes already added -->


		<!-- tube search results -->
		<?php
			require_once('templates/lists/tube_search_list.php');
		?>

	<!-- Boxes already added -->
	<div class="col-xs-12 col-sm-12 col-md-12">
		<fieldset id="make-group-fieldset">
			<legend>Your Boxes already added</legend>
			<?php
				require_once('templates/lists/box_list.php');
			?>
		</fieldset>
	</div>


	<?php
		for($i=0; $i < sizeof($roleList); $i++)
		{
			if($roleList[$i]['roleName'] === 'Sample Request')
			{
	?>
	<!-- requests outstanding -->
	<div class="col-xs-12 col-sm-12 col-md-12">
		<?php
			require_once('templates/lists/req_list.php');
		?>
	</div>
	<?php
			}
		}
	?>


	<!-- groups you belong to box -->
	<div class="col-xs-12 col-sm-12 col-md-6">
		<?php
			require_once('templates/lists/group_list.php');
		?>
	</div>

	<!-- freezers already added -->
	<div class="col-xs-12 col-sm-12 col-md-6">
		<fieldset id='make-group-fieldset'></p>
			<legend>Freezers already added</legend>
			<?php
				require_once('templates/lists/freezer_list.php');
			?>
		</fieldset>
	</div>
</div>
