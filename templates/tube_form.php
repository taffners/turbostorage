<div class="container-fluid" id="form_container">
     <h1 id="response"></h1>
     <form id="create_tube" class="form-horizontal" method="post">
          <fieldset id="Tube-Storage-Info-fieldset">
               <legend>Tube Storage Info</legend>
               <!-- general tube description info -->

                    <div class="form-group">
                         <input type="hidden" id="tubeID" name="tubeID" value="<?= $tubeArray['tubeID']; ?>"/>

                         <input type="hidden" id="intialNumTubes" name="intialNumTubes" value="<?= $tubeArray['intialNumTubes']; ?>"/>

                         <input type="hidden" id="userID" name="userID" value="<?= $tubeArray['userID']; ?>"/>

                         <div class="col-xs-12 col-sm-12 col-md-12 pull-left alert alert-success">
                              Max length of name is 15.  Put more information in the comments below.
                         </div>
                         <div class="col-xs-12 col-sm-4 col-md-2">
                              <label for="sampleName">Name On Tube: <span class="required-field">*</span></label>
                         </div>
                         <div class="col-xs-12 col-sm-4 col-md-2 pull-left">

                              <input class="form-control" type="text" maxlength="15" id="sampleName" name="sampleName" value="<?= $tubeArray['sampleName']; ?>" required/>
                         </div>
                    </div>
                    <div class="form-group">
                         <div class="col-xs-12 col-sm-4 col-md-2">
                              <label for="freezeDate">Freeze Date:</label>
                         </div>

                         <div class="col-xs-6 col-sm-4 col-md-3">
                              <input class="form-control todayDate date-picker" type="text" id="freezeDate" name="freezeDate" value="<?= $dfs->ChangeDateFormatUS($tubeArray['freezeDate'], TRUE); ?>" placeholder="MM/DD/YYYY"/>
                         </div>
                         <div class="col-xs-6 col-sm-4 col-md-5 pull-left">
                              <button type="button" class="btn btn-primary todays-date" data-input-id="freezeDate">Add Today's Date</button>
                         </div>

                    </div>
                    <div class="form-group">
                         <div class="col-xs-12 col-sm-4 col-md-2">
                              <label for="frozeBy">Froze By:</label>
                         </div>
                         <div class="col-xs-12 col-sm-4 col-md-3">
                              <input class="form-control" type="text" id="frozeBy" name="frozeBy" value="<?= $tubeArray['frozeBy']; ?>"/>
                         </div>


                    </div>
                    <div class="form-group">
                         <div class="col-xs-12 col-sm-4 col-md-2">
                              <label for="color">What color is your tube label?</label>
                         </div>
                         <div class="col-xs-12 col-sm-4 col-md-2 pull-left">

                              <select class="form-control" name="color" id="tube_color">
                                   <option value="white" style="background:white" <?php if($tubeArray['color'] === 'white') echo 'selected'; ?>>white</option>
                                   <option value="red" style="background:red" <?php if($tubeArray['color'] === 'red') echo 'selected'; ?>>red</option>
                                   <option value="#FF9933" style="background:#FF9933" <?php if($tubeArray['color'] === '#FF9933') echo 'selected'; ?>>orange  </option>
                                   <option value="#B0E0E6" style="background:#B0E0E6" <?php if($tubeArray['color'] === '#B0E0E6') echo 'selected'; ?>> blue</option>
                                   <option value="#BCE5AF" style="background:#BCE5AF" <?php if($tubeArray['color'] === '#BCE5AF') echo 'selected'; ?>>green </option>
                                   <option value="#FFFF4C" style="background:#FFFF4C" <?php if($tubeArray['color'] === '#FFFF4C') echo 'selected'; ?>>yellow </option>
                                   <option value="#B87FED" style="background:#B87FED" <?php if($tubeArray['color'] === '#B87FED') echo 'selected'; ?>>purple</option>
                                   <option value="#F493E2" style="background:#F493E2" <?php if($tubeArray['color'] === '#F493E2') echo 'selected'; ?>>pink </option>
                                   <option value="#f5d59a" style="background:#f5d59a" <?php if($tubeArray['color'] === '#f5d59a') echo 'selected'; ?>>tan </option>
                                   <option value="#c7c7c7" style="background:#c7c7c7" <?php if($tubeArray['color'] === '#c7c7c7') echo 'selected'; ?>>grey</option>
                              </select>
                         </div>
                    </div>

               <!-- add all boxes in db here-->
               <?php
                    if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'add')
                    {
               ?>
                    <div id="boxTable_div">
                         <!-- green alert box notifying to choose a box -->
                         <div class="row">
                              <div class="alert alert-success" role="alert">
                                   <h4>Choose a box to add a your samples to below <span class="required-field">*</span> </h4>
                              </div>
                         </div>

                         <!-- add all of the box data issues here -->
                         <div class="row">
                              <div class="alert alert-danger hide" id="Box_Errors"></div>

                         </div>

                         <!--Box Table is filled in here  -->
                         <?php
          				require_once('templates/lists/box_list.php');
          			?>
                    </div>
               <?php
                    }
               ?>
               <!-- add location information if action == updateTube -->
               <?php
                    if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'updateTube')
                    {
               ?>
                         <div class="panel-group">
                              <div class="panel panel-primary">
                                   <div class="panel-heading">
                                        <h4 class="panel-title">
                                             <a data-toggle="collapse" href="#sample_loc_info" aria-expanded="true">Tubes Location Information</a>
                                        </h4>
                                   </div>
                                   <div id="sample_loc_info" class="panel-collapse collapse in" aria-expanded="true">
                                        <ul class="list-group">
                                             <li class="list-group-item" id="box_loc_accordian">
                                                  <u>Box Name</u>: <?= $_REQUEST['boxName']; ?>
                                             </li>
                                             <li class="list-group-item"   id="spaces_loc_accordian">
                                                  <u>Spaces</u>:
                                             <?php

                                                  $spaceArray = $utils->DoubleAscSort('xLoc', 'yLoc', $spaceArray);

                                                  for ($i=0; $i<count($spaceArray); $i++)
                                                  {
                                                       // make space loc
                                                            // xLoc == row # change this to a letter
                                                            // yLoc == column keep as number
                                                            if($i!=0)
                                                            {
                                                                 print_r(', ');
                                                            }
                                                            $alpha = range('A', 'Z');
                                                            $xLoc = $spaceArray[$i]['xLoc'];
                                                            $space = $alpha[$xLoc -1].$spaceArray[$i]['yLoc'];
                                                            print_r($space);
                                                  }
                                             ?>

                                             </li>
                                        </ul>

                                   </div>
                              </div>
                         </div>
               <?php
                    }

               ?>

          </fieldset>

          <fieldset id="Experiment-Info-fieldset">
               <legend>Experiment Info</legend>

               <div class="row form-group">
                    <div class="col-xs-12 col-sm-12 col-md-4">
                         <label for="experimentName">Experiment Name:</label>
                         <input class="form-control" type="text" id="experimentName" name="experimentName" maxlength="30"  value="<?= $tubeArray['experimentName']; ?>"/>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                         <label for="labBook">Lab Book Name:</label>
                         <input class="form-control" type="text" id="labBook" maxlength="30" name="labBook" value="<?= $tubeArray['labBook']; ?>"/>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                         <label for="pageNum">Page Number:</label>
                         <input class="form-control" type="number" id="pageNum" name="pageNum" value="<?= $tubeArray['pageNum']; ?>"/>
                    </div>
               </div>
               <div class="row form-group">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                         <label for="published">Sample Published:</label>
                         <input type="checkbox" id="published" name="published" value="1" <?php if($tubeArray['published'] == 1) echo 'checked'; ?>/>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                         <label for="mta">Sample has a MTA:</label>
                         <input type="checkbox" id="mta" name="mta" value="1" <?php if($tubeArray['mta'] == 1) echo 'checked'; ?> />
                    </div>
               </div>
          </fieldset>
          <fieldset id="Experiment-Info-fieldset">
               <legend>IRB / Consent Info</legend>
               <div class="form-group row">
                    <div class="col-xs-4 col-sm-4 col-md-4">
                         <label for="consentYesNo" class="form-control-label">
                              Does this sample have a consent? <span class="required-field">*</span>
                         </label>
                         <div class="radio">
                              <label class="radio-inline">
                                   <input type="radio" id="consent_yes" name="consentYesNo" required value="1"<?php if($tubeArray['consentYesNo'] == '1') echo 'checked'; ?>  >Yes</input>
                              </label>
                              <label class="radio-inline">
                                   <input type="radio" id="consent_no" name="consentYesNo" required value="0" <?php if($tubeArray['consentYesNo'] == '0') echo 'checked'; ?> >No</input>
                              </label>
                         </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                         <label for="consentApprovalDate" class="form-control-label">
                              Date Consent was Approved by IRB
                         </label>
                         <input type="text" class="form-control date-picker" id="consentApprovalDate" name="consentApprovalDate" value="<?= $dfs->ChangeDateFormatUS($tubeArray['consentApprovalDate'], TRUE); ?>" placeholder="MM/DD/YYYY">
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                         <label for="locConsent" class="form-control-label">
                              Location of Paper Consent
                         </label>
                         <input type="text" class="form-control" id="locConsent" name="locConsent" maxlength="32" value="<?= $tubeArray['locConsent']; ?>">
                    </div>
               </div>

               <div  class="row form-group">
                    <div class="col-xs-12 col-sm-4 col-md-4">
                         <label for="openAccess">Open Access Sharing:</label>
                         <div class="radio">
                              <label class="radio-inline">
                                   <input type="radio" id="openAccess_opt_out" name="openAccess"  value="0"<?php if($tubeArray['openAccess'] == '0') echo 'checked'; ?>  >Out Out</input>
                              </label>
                              <label class="radio-inline">
                                   <input type="radio" id="openAccess_opt_in" name="openAccess" value="1" <?php if($tubeArray['openAccess'] == '1') echo 'checked'; ?> >Opt In</input>
                              </label>
                              <label class="radio-inline">
                                   <input type="radio" id="openAccess_not_asked" name="openAccess" value="2" <?php if($tubeArray['openAccess'] == '2') echo 'checked'; ?> >Question Not Asked</input>
                              </label>
                         </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                         <label for="irbID">IRB Project Title:</label>
                         <select id="irbID" name="irbID" class="form-control">

                              <!-- if this is an update use the tube array to build selection list otherwise use the irb_array -->
                              <option value="" selected="selected"></option>
                              <?php
                                   if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'updateTube')
                                   {
                                        for ($i=0; $i<sizeof($irb_array); $i++)
                                        {
                              ?>
                                             <option value="<?php echo $irb_array[$i]['irbID']; ?>" <?php if($tubeArray['irbID'] == $irb_array[$i]['irbID']) echo 'selected="selected"'; ?> >
                                                  <?php echo $irb_array[$i]['irbProjectTitle']; ?>
                                             </option>
                              <?php
                                        }
                                   }
                                   else
                                   {
                                        for ($i=0; $i<sizeof($irb_array); $i++)
                                        {
                              ?>
                                             <option value="<?php echo $irb_array[$i]['irbID']; ?>">
                                                  <?php echo $irb_array[$i]['irbProjectTitle']; ?>
                                             </option>
                              <?php
                                        }
                                   }
                              ?>
                         </select>
                    </div>
               </div>
               <div class="row form-group">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                         <label for="consentNotes">Notes on Consent:</label>
                         <textarea class="form-control" name="consentNotes" rows="2" id="consentNotes" maxlength="255" ><?= $tubeArray['consentNotes']; ?></textarea>
                    </div>
               </div>
               <div id="insert-consent-irb-info-here" class="hide">
               </div>


               <div id="cloning-tables-irb">
                    <div class="col-xs-12 col-sm-12 col-md-12 show" id="consent-irb-info-clone-div">

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 hide" >
                         <table class="QueryInfo" id="consent-irb-info-clone">
                              <thead>
                                   <tr id="consent-irb-info-clone-header">
                                        <th colspan="2">IRB Approval Information</th>
                                   </tr>
                              </thead>
                              <tbody class="smaller-type">
                                   <tr id="consent-irb-info-clone-IRBQuestion">
                                        <td class="description-col">IRB Question</td>
                                        <td data-row-name="IRBQuestion"></td>
                                   </tr>
                                   <tr id="consent-irb-info-clone-answer">
                                        <td colspan="2" data-row-name="answer"></td>
                                   </tr>
                              </tbody>
                         </table>
                    </div>
               </div>
          </fieldset>

          <fieldSet id="Sample-Type-Specfic-Questions-fieldset">
               <legend>Sample Type Specfic Questions</legend>

               <div class="row form-group">
                    <div class="col-xs-12 col-sm-4 col-md-2">
                         <label for="sampleType">Sample Type: <span class="required-field">*</span></label>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-2 pull-left">

                         <select class="form-control" id="sampleType" name="sampleType">
                              <option value="" selected="selected"></option>
                              <option value="" <?php if($tubeArray['sampleType'] == '') echo 'selected'; ?>></option>
                              <option value="Antibody" <?php if($tubeArray['sampleType'] == 'Antibody') echo 'selected'; ?>>Antibody</option>
                              <option value="Bacterial_Cultures" <?php if($tubeArray['sampleType'] == 'Bacterial_Cultures') echo 'selected'; ?>>Bacterial Cultures</option>
                              <option value="Blood" <?php if($tubeArray['sampleType'] == 'Blood') echo 'selected'; ?>>Blood</option>
                              <option value="Body_fluids" <?php if($tubeArray['sampleType'] == 'Body_fluids') echo 'selected'; ?>>Body fluids</option>
                              <option value="Bone" <?php if($tubeArray['sampleType'] == 'Bone') echo 'selected'; ?>>Bone</option>
                              <option value="Cell_Lines" <?php if($tubeArray['sampleType'] == 'Cell_Lines') echo 'selected'; ?>>Cell lines</option>
                              <option value="Cell_Pellet" <?php if($tubeArray['sampleType'] == 'Cell_Pellet') echo 'selected'; ?>>Cell Pellet</option>
                              <option value="Chemical" <?php if($tubeArray['sampleType'] == 'Chemical') echo 'selected'; ?>>Chemical</option>
                              <option value="Cytokine" <?php if($tubeArray['sampleType'] == 'Cytokine') echo 'selected'; ?>>Cytokine</option>
                              <option value="DNA" <?php if($tubeArray['sampleType'] == 'DNA') echo 'selected'; ?>>DNA</option>
                              <option value="PBMC" <?php if($tubeArray['sampleType'] == 'PBMC') echo 'selected'; ?>>PBMC</option>
                              <option value="Plasma" <?php if($tubeArray['sampleType'] == 'Plasma') echo 'selected'; ?>>Plasma</option>
                              <option value="Primary_Cells" <?php if($tubeArray['sampleType'] == 'Primary_Cells') echo 'selected'; ?>>Primary Cells</option>
                              <option value="RNA" <?php if($tubeArray['sampleType'] == 'RNA') echo 'selected'; ?>>RNA</option>
                              <option value="Serum" <?php if($tubeArray['sampleType'] == 'Serum') echo 'selected'; ?>>Serum</option>
                              <option value="Urine" <?php if($tubeArray['sampleType'] == 'Urine') echo 'selected'; ?>>Urine</option>
                              <option value="Virus" <?php if($tubeArray['sampleType'] == 'Virus') echo 'selected'; ?>>Virus</option>
                              <option value="Whole_Tissue" <?php if($tubeArray['sampleType'] == 'Whole_Tissue') echo 'selected'; ?>>Whole Tissue</option>

                         </select>
                    </div>
               </div>

               <!-- Where all the Questions are unhidden depending on  the Sample type choosen-->
               <div id="SampleTypeGroups">

                    <div id="SampleTypeGroups">

                         <!-- sample types Antibody -->
                         <div id='Group1' class="hide">
                              <!-- protein -->
                              <div class="row form-group" id="protein_Div_Group1">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Protein">Name of protein:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Protein" id="Group1_Protein" name="Group1[Protein]" value="">
                                   </div>
                              </div>

                              <!-- Isotype -->
                              <div  class="row form-group" id="Isotype_Div_Group1">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Isotype">Isotype:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Isotype" id="Group1_Isotype" name="Group1[Isotype]" value="">
                                   </div>
                              </div>

                              <!-- Clone -->
                              <div  class="row form-group" id="Clone_Div_Group1">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Clone">Clone:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Clone" name="Group1[Clone]" id="Group1_Clone" value="">
                                   </div>
                              </div>

                              <!-- Color -->
                              <div  class="row form-group" id="Color_Div_Group1">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Tube_Color">Color:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Tube_Color" name="Group1[Tube_Color]" id="Group1_Tube_Color" value="">
                                   </div>
                              </div>

                              <!-- expiryDate -->
                              <div  class="row form-group" id="Expiration_Date_Div_Group1">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Expiration_Date">Expiration Date:</label>
                                   </div>

                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Expiration_Date date-picker" id="Group1_Expiration_Date" name="Group1[Expiration_Date]" value="">
                                   </div>
                              </div>
                         </div>

                         <!-- sample types bacterial Cultures -->
                         <div id="Group2" class="hide">

                              <!-- species -->
                              <div  class="row form-group" id="Species_Div_Group2">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Species">Species of Origin:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Species" name="Group2[Species]" id="Group2_Species" value="">
                                   </div>
                              </div>

                              <!-- GeneticManipulation -->
                              <div  class="row form-group" id="Genetic_Manipulation_Div_Group2">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Genetic_Manipulation">Genetic Manipulation:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Genetic_Manipulation" name="Group2[Genetic_Manipulation]" id="Group2_Genetic_Manipulation" value="">
                                   </div>
                              </div>

                              <!-- vector -->
                              <div  class="row form-group" id="Vector_Div_Group2">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Vector">Vector:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Vector" name="Group2[Vector]" id="Group2_Vector" value="">
                                   </div>
                              </div>
                              <!-- growingConditions -->
                              <div  class="row form-group" id="Growing_Conditions_Div_Group2">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Growing_Conditions">Growing Conditions:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-8 col-md-10 pull-left">
                                        <textarea class="form-control Growing_Conditions" name="Group2[Growing_Conditions]" rows="3" id="Group2_Growing_Conditions" maxlength="255" value=""></textarea>
                                   </div>
                              </div>
                         </div>

                         <!-- sample types cell Lines -->
                         <div id="Group3" class="hide">

                              <!-- species -->
                              <div  class="row form-group" id="Species_Div_Group3">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Species">Species of Origin:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Species" name="Group3[Species]" id="Group3_Species" value="">
                                   </div>
                              </div>

                              <!-- Genetic_Manipulation -->
                              <div  class="row form-group" id="Genetic_Manipulation_Div_Group3">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Genetic_Manipulation">Genetic Manipulation:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Genetic_Manipulation" id="Group3_Genetic_Manipulation"  name="Group3[Genetic_Manipulation]" maxlength="255" value="">
                                   </div>
                              </div>

                              <!-- TotalNum -->
                              <div  class="row form-group" id="Total_Num_Div_Group3">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Total_Num">Total Number Cells (million cells):</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="number" class="form-control Total_Num" id="Group3_Total_Number_per_million_cells" name="Group3[Total_Number_per_million_cells]" min="0" step="0.01" value="">
                                   </div>
                              </div>

                              <!-- vector -->
                              <div  class="row form-group" id="Vector_Div_Group3">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Vector">Vector:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Vector" id="Group3_Vector" name="Group3[Vector]" value="">
                                   </div>
                              </div>

                              <!-- growingConditions -->
                              <div  class="row form-group" id="Growing_Conditions_Div_Group3">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Growing_Conditions">Growing Conditions:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-8 col-md-10 pull-left">
                                        <textarea class="form-control Growing_Conditions" name="Group3[Growing_Conditions]" rows="3" id="Group3_Growing_Conditions" maxlength="255" value=""></textarea>
                                   </div>
                              </div>
                         </div>

                         <!-- sample types blood, Body Fluids, Bone, Plasma, Serum, Urine, Whole Tissue-->
                         <div id="Group4" class="hide">

                              <!-- species -->
                              <div  class="row form-group" id="Species_Div_Group4">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Species">Species of Origin:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Species" name="Group4[Species]" id="Group4_Species" value="">
                                   </div>
                              </div>

                              <!-- TotalVol -->
                              <div  class="row form-group" id="Total_Vol_Div_Group4">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Total_Vol">Total Volume per Tube:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="number" class="form-control Total_Vol" id="Group4_Total_Vol" name="Group4[Total_Vol]" value="" min="0" step="0.01">
                                   </div>
                              </div>
                                   <!-- Unit_Of_Measure_Total -->
                              <div  class="row form-group" id="Unit_Of_Measure_Total_Div_Group4">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Unit_Of_Measure_Total">Unit:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <select class="form-control Unit_Of_Measure_Total" name="Group4[Unit_Of_Measure_Total]" id="Group4_Unit_Of_Measure_Total" >
                                             <option selected="selected" value=""></option>
                                             <option value="ul" >ul</option>
                                             <option value="ug" >ug</option>
                                             <option value="ml" >ml</option>

                                        </select>
                                   </div>
                              </div>
                         </div>

                         <!-- sample types Chemical-->
                         <div id="Group5" class="hide">

                              <!-- QuantiyPerTubeDiv -->
                              <div  class="row form-group" id="Quantiy_Per_Tube_Div_Group5">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Quantiy_Per_Tube">Quantiy Per tube:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="number" class="form-control Quantiy_Per_Tube" id="Group5_Quantiy_Per_Tube" name="Group5[Quantiy_Per_Tube]" value="" min="0" step="0.01">
                                   </div>
                              </div>
                                   <!-- Unit_Of_Measure_Total -->
                              <div  class="row form-group" id="Unit_Of_Measure_Total_Div_Group5">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Unit_Of_Measure_Total">Unit:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <select class="form-control Unit_Of_Measure_Total" id="Group5_Unit_Of_Measure_Total" name="Group5[Unit_Of_Measure_Total]">
                                             <option selected="selected" value=""></option>
                                             <option value="ul">ul</option>
                                             <option value="ug">ug</option>
                                             <option value="ml">ml</option>
                                        </select>
                                   </div>
                              </div>

                              <!-- expiryDate -->
                              <div  class="row form-group" id="Expiration_Date_Div_Group5">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Expiration_Date">Expiration Date:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Expiration_Date date-picker" name="Group5[Expiration_Date]" id="Group5_Expiration_Date" value="">
                                   </div>
                              </div>

                         </div>

                         <!-- sample types Cytokine-->
                         <div id="Group6" class="hide">

                              <!-- concentration -->
                              <div  class="row form-group" id="concentration_Div_Group6">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="concentration">Concentration tube:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="number" class="form-control concentration" id="Group6_concentration" name="Group6[concentration]" value="" min="0" step="0.01">
                                   </div>
                              </div>
                                   <!-- unitOfMeasureConcentration -->
                              <div  class="row form-group" id="Unit_Of_Measure_Concentration_Div_Group6">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Unit_Of_Measure_Concentration">Unit:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <select class="form-control Unit_Of_Measure_Concentration" id="Group6_Unit_Of_Measure_Concentration"  name="Group6[Unit_Of_Measure_Concentration]">
                                             <option selected="selected" value=""></option>
                                             <option value="ng/ul">ng/ul</option>
                                             <option value="ug">ug</option>
                                             <option value="ml">ml</option>
                                        </select>
                                   </div>

                              </div>

                              <!-- QuantiyPerTubeDiv -->
                              <div class="row form-group" id="Quantiy_Per_Tube_Div_Group6">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Quantiy_Per_Tube">Quantiy Per tube:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="number" class="form-control Quantiy_Per_Tube" id="Group6_Quantiy_Per_Tube" name="Group6[Quantiy_Per_Tube]" value="" min="0" step="0.01">
                                   </div>
                              </div>
                                   <!-- Unit_Of_Measure_Total -->
                              <div  class="row form-group" id="Unit_Of_Measure_Total_Div_Group6">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Unit_Of_Measure_Total">Unit:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <select class="form-control Unit_Of_Measure_Total" id="Group6_Unit_Of_Measure_Total" name="Group6[Unit_Of_Measure_Total]">
                                             <option selected="selected" value=""></option>
                                             <option value="ul">ul</option>
                                             <option value="ug">ug</option>
                                             <option value="ml">ml</option>

                                        </select>
                                   </div>
                              </div>

                              <!-- expiryDate -->
                              <div class="row form-group" id="Expiration_Date_Div_Group6">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Expiration_Date">Expiration Date:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Expiration_Date date-picker" id="Group6_Expiration_Date" name="Group6[Expiration_Date]" value="">
                                   </div>
                              </div>

                         </div>

                         <!-- sample types DNA, RNA-->
                         <div id="Group7" class="hide">

                              <!-- species -->
                              <div class="row form-group" id="Species_Div_Group7">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Species">Species of Origin:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Species" name="Group7[Species]" id="Group7_Species" value="">
                                   </div>
                              </div>

                              <!-- GeneticManipulation -->
                              <div class="row form-group" id="Genetic_Manipulation_Div_Group7">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Genetic_Manipulation">Genetic Manipulation:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Genetic_Manipulation" name="Group7[Genetic_Manipulation]" id="Group7_Genetic_Manipulation" value="">
                                   </div>
                              </div>

                              <!-- vector -->
                              <div class="row form-group" id="Vector_Div_Group7">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Vector">Vector:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Vector" name="Group7[Vector]" id="Group7_Vector" value="">
                                   </div>
                              </div>

                              <!-- concentration -->
                              <div class="row form-group" id="Concentration_Div_Group7">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Concentration">Concentration tube:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="number" class="form-control Concentration" name="Group7[Concentration]" id="Group7_Concentration"value="" min="0" step="0.01">
                                   </div>
                              </div>
                                   <!-- unitOfMeasureConcentration -->
                              <div class="row form-group" id="Unit_Of_Measure_Concentration_Div_Group7">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Unit_Of_Measure_Concentration">Unit:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <select class="form-control Unit_Of_Measure_Concentration" id="Group7_Unit_Of_Measure_Concentration" name="Group7[Unit_Of_Measure_Concentration]">
                                             <option selected="selected" value=""></option>
                                             <option value="ml">ml</option>
                                             <option value="ug" >ug</option>
                                             <option value="ul" >ul</option>
                                        </select>
                                   </div>

                              </div>

                         </div>

                         <!-- sample types Virus-->
                         <div id="Group8" class="hide">
                              <!-- species -->
                              <div class="row form-group" id="Species_Div_Group8">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Species">Species of Origin:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Species" name="Group8[Species]" id="Group8_Species" value="">
                                   </div>
                              </div>

                              <!-- GeneticManipulation -->
                              <div class="row form-group" id="Genetic_Manipulation_Div_Group8">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Genetic_Manipulation">Genetic Manipulation:</label>
                                   </div>

                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Genetic_Manipulation" name="Group8[Genetic_Manipulation]" id="Group8_Genetic_Manipulation" value="">
                                   </div>
                              </div>

                              <!-- concentration -->
                              <div class="row form-group" id="Concentration_Div_Group8">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Concentration">Concentration tube:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="number" class="form-control Concentration" name="Group8[Concentration]" id="Group8_Concentration" value="" min="0" step="0.01">
                                   </div>
                              </div>
                                   <!-- unitOfMeasureConcentration -->
                              <div class="row form-group" id="Unit_Of_Measure_Concentration_Div_Group8">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Unit_Of_Measure_Concentration">Unit:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <select class="form-control Unit_Of_Measure_Concentration" id="Group8_Unit_Of_Measure_Concentration" name="Group8[Unit_Of_Measure_Concentration]">
                                             <option selected="selected" value=""></option>
                                             <option value="ml" >ml</option>
                                             <option value="ug" >ug</option>
                                             <option value="ul" >ul</option>
                                        </select>
                                   </div>

                              </div>

                              <!-- QuantiyPerTubeDiv -->
                              <div class="row form-group" id="Quantiy_Per_Tube_Div_Group8">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Quantiy_Per_Tube">Quantiy Per tube:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="number" class="form-control Quantiy_Per_Tube" id="Group8_Quantiy_Per_Tube" name="Group8[Quantiy_Per_Tube]" value="" min="0" step="0.01">
                                   </div>
                              </div>

                              <!-- Unit_Of_Measure_Total -->
                              <div class="row form-group" id="Unit_Of_Measure_Total_Div_Group8">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Unit_Of_Measure_Total">Unit:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <select class="form-control Unit_Of_Measure_Total" id="Group8_Unit_Of_Measure_Total" name="Group8[Unit_Of_Measure_Total]">
                                             <option selected="selected" value=""></option>
                                             <option value="ul" >ul</option>
                                             <option value="ug" >ug</option>
                                             <option value="ml" >ml</option>
                                        </select>
                                   </div>
                              </div>

                         </div>

                         <!-- sample types PBMC -->
                         <div id="Group9" class="hide">

                              <!-- species -->
                              <div  class="row form-group" id="Species_Div_Group9">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Species">Species of Origin:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Species" name="Group9[Species]" id="Group9_Species"value="">
                                   </div>
                              </div>

                              <!-- Genetic_Manipulation -->
                              <div  class="row form-group" id="Genetic_Manipulation_Div_Group9">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Genetic_Manipulation">Genetic Manipulation:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Genetic_Manipulation" id="Group9_Genetic_Manipulation" name="Group9[Genetic_Manipulation]" value="" >
                                   </div>
                              </div>

                              <!-- TotalNum -->
                              <div  class="row form-group" id="Total_Num_Div_Group9">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Total_Num">Total Number Cells (million cells):</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="number" class="form-control Total_Num" name="Group9[Total_Number_per_million_cells]" id="Group9_Total_Number_per_million_cells" min="0" step="0.01" value="">

                                   </div>
                              </div>

                              <!--   -->
                              <!-- <div  class="row form-group" id="collection_time_Div_Group9">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="collection_time">Collection Time</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <div class='input-group date' id='collection_time_Group9'>
                                             <input type="text" class="form-control Total_Num date_display" id="Group9_collection_time" name="Group9[collection_time]" value="">

                                        </div>
                                   </div>
                              </div> -->

                              <!-- freeze time -->
                              <!-- <div  class="row form-group" id="freeze_time_Div_Group9">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="freeze_time">Freeze Time</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <div class='input-group date' id='collection_time_Group9'>
                                             <input type="text" class="form-control Total_Num date_display date_display" name="Group9[freeze_time]" id="Group9_freeze_time" value="">

                                        </div>
                                   </div>
                              </div> -->
                         </div>

                         <!-- sample types Cell_Pellet -->
                         <div id="Group10" class="hide">

                              <!-- species -->
                              <div  class="row form-group" id="Species_Div_Group10">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Species">Species of Origin:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Species" name="Group10[Species]" id="Group10_Species" value="">
                                   </div>
                              </div>
                         </div>

                         <!-- sample types Primary_Cells -->
                         <div id="Group11" class="hide">

                              <!-- species -->
                              <div  class="row form-group" id="Species_Div_Group11">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Species">Species of Origin:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Species" name="Group11[Species]" id="Group11_Species" value="">
                                   </div>
                              </div>

                              <!-- Tissue Type -->
                              <div  class="row form-group" id="Tissue_Div_Group11">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Tissue">Tissue of Origin:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="text" class="form-control Tissue" name="Group11[Tissue]" id="Group11_Tissue" value="">
                                   </div>
                              </div>
                              <!-- TotalNum -->
                              <div  class="row form-group" id="Total_Num_Div_Group11">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Total_Num">Total Number Cells (million cells):</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-4 col-md-2 pull-left">
                                        <input type="number" class="form-control Total_Num" name="Group11[Total_Number_per_million_cells]" id="Group11_Total_Number_per_million_cells" min="0" step="0.01" value="">

                                   </div>
                              </div>

                              <!-- growingConditions -->
                              <div  class="row form-group" id="Growing_Conditions_Div_Group11">
                                   <div class="col-xs-12 col-sm-4 col-md-2">
                                        <label for="Growing_Conditions">Growing Conditions:</label>
                                   </div>
                                   <div class="col-xs-12 col-sm-8 col-md-10 pull-left">
                                        <textarea class="form-control Growing_Conditions" name="Group11[Growing_Conditions]" rows="3" id="Group11_Growing_Conditions" maxlength="255" value=""></textarea>
                                   </div>
                              </div>
                         </div>


          </fieldSet>

          <fieldSet id="Comments-fieldset">
               <legend>Comments</legend>

               <input type="hidden" id="commentID" name="commentID" value="<?= $tubeArray['commentID']; ?>"/>



               <div class="row form-group">
                    <label for="comment" class="col-sm-4 form-control-label" id="commentLabel">Comments:</label>
     			<div class="col-xs-10 col-sm-10 col-md-10">
     				<textarea class="form-control" rows="3" id="comment" name="comment" maxlength="255"value=""/></textarea>
     			</div>

                    <?php

                         if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'updateTube' )
                         {
                    ?>
                              <div class="col-xs-2 col-sm-2 col-md-2">
                                   <button type="button" id="comment<?= $tubeArray['tubeID']; ?>" class="btn btn-primary btn-lg add_comment_button" data-comment-type="sample" data-comment-id="<?= $tubeArray['tubeID']; ?>">
                                        <span class="glyphicon glyphicon-comment"></span>
                                   </button>
                              </div>

                    <?php
                         }
                    ?>
               </div>

          </fieldSet>
          <input type="hidden" id="intialNumTubes" name="intialNumTubes" value="<?= $tubeArray['intialNumTubes']; ?>"/>
          <!-- <fieldset id="Attached-File-fieldset"> -->
            <!-- https://www.script-tutorials.com/pure-html5-file-upload/ -->
            	<!-- <legend>Attached File</legend>
            	Add here vector files, protocols, Gels pictures etc..



                      <div class="upload_form_cont">

                          	<input type="file" name="image_file" id="image_file" onchange="fileSelected();" style="padding-left:10px;" />

       				<input type="button" value="Upload" onclick="startUploading()" />

                           <div id="fileinfo">

                               <div id="filename"></div>

                               <div id="filesize"></div>

                               <div id="filetype"></div>

                               <div id="filedim"></div>

                           </div>

                           <div id="error"></div>

                           <div id="error2"></div>

                           <div id="abort"></div>

                           <div id="warnsize"></div>

                           <div id="progress_info">

                               <div id="progress"></div>

                               <div id="progress_percent">&nbsp;</div>

                               <div class="clear_both"></div>

                               <div>

                                   <div id="speed">&nbsp;</div>

                                   <div id="remaining">&nbsp;</div>

                                   <div id="b_transfered">&nbsp;</div>

                                   <div class="c{lear_both"></div>

                               </div>

                               <div id="upload_response"></div>

                           </div>





                       <img id="preview" />

                   </div> -->



            <!-- </fieldset> -->

          <input type="hidden" id="form_checked" name="form_checked" value="0"/>

          <div class="col-xs-10 col-sm-10 col-md-11">
               <button type="submit" id="CreateTubeSubmit" name="CreateTubeSubmit" value="Submit"  class="btn btn-primary btn-lg"><span id="new_item" class="show">Create Tubes</span><span id="update_item" class="hide">Update Tubes</span></button>
          </div>

          <div class="col-xs-2 col-sm-2 col-md-1">
               <button id="cancel" class="btn btn-primary btn-lg">
                    <a href="?page=home">Cancel</a>
               </button>
          </div>


     </form>

</div>
