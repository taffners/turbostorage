<div id="ajax_response">
</div>
<div class='container-fluid' id="box_container" >
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6">
			<form id="create_box" class="form" method="post" class="form-horizontal">
				<fieldset id="add_new_box_fieldset">
					<legend id='new_legend' class='show'>
						Add a New Box
					</legend>
					<legend id='update_legend' class='hide'>
						Update Box
					</legend>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="boxName" class="form-control-label">Box Name: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="boxName" name="boxName" maxlength="32" value="<?= $boxArray['boxName'];?>" />
						</div>
					</div>
				</fieldset>

				<fieldset id="box_size_fieldset">
					<legend>Size of Box</legend>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<label for"xSize">X <span class="required-field">*</span></label>
							<input type="number" id="xSize" name="xSize" class="form-control" value="<?= $boxArray['xSize'];?>" />
						</div>

						<div class="col-xs-12 col-sm-6 col-md-6">
							<label for"ySize">Y <span class="required-field">*</span></label>
							<input type="number" id="ySize" name="ySize" class="form-control" value="<?= $boxArray['ySize'];?>" />
						</div>
					</div>
				</fieldset>

				<fieldset id="location_fieldset">
					<legend>Location Info</legend>
					<!-- add all freezers -->
					<div class="row">
						<div class="alert alert-success" role="alert">
							<h4>Choose a Freezer to Add the Box to <span class="required-field">*</span></h4>
						</div>
					</div>
					<div class="form-group row">
						<?php
							require_once('templates/lists/freezer_list.php');
						?>
					</div>
					<?php
						require_once('templates/location_box_tips.php');
					?>

				</fieldset>

				<fieldset>
					<legend>Box Access Info</legend>
					<div class="row">
						<div class="alert alert-success" role="alert">
							<?= SITE_TITLE; ?> sets access levels of samples based on box privacy levels.
							<br><br>
							<ul>
								<li>Private</li>
									<ul>
										<li>Only you will be able to see this box and its contents</li>
									</ul>
								<li>Group</li>
									<ul>
										<li>Everyone in that group can see this box and its contents</li>
									</ul>
								<li>Public</li>
									<ul>
										<li>Everyone that has a <?= SITE_TITLE; ?> user account cans see these samples and its contents</li>
									</ul>
							</ul>


						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="privacyLevel" class="form-control-label">Privacy Level: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<select id="privacyLevel" name="privacyLevel" class="form-control">

								<option value="Private" <?php if($boxArray['privacyLevel'] == 'Private') echo 'selected="true"'; ?> > Private </option>

								<option value="Group" <?php if($boxArray['privacyLevel'] == 'Group') echo 'selected="true"'; ?> > Group</option>

								<option value="Public" <?php if($boxArray['privacyLevel'] == 'Public') echo 'selected="true"'; ?> >Public </option>

							</select>
						</div>

					</div>
					<div class="form-group row hide" id="group_div">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="groupID" class="form-control-label">
								Groups: <span class="required-field">*</span>
							</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<select id="groups" name="groupID" class="form-control">
								<?php
									for ($i=0; $i<sizeof($user_groups); $i++)
									{
								?>
									<option value="<?php echo $user_groups[$i]['groupID']; ?>" <?php if($boxArray['groupID'] == $user_groups[$i]['groupID']) echo 'selected="true"';?> >
										<?php echo $user_groups[$i]['groupName']; ?>
									</option>

								<?php
									}


								?>

							</select>
						</div>

					</div>
				</fieldset>

				<fieldset>
					<legend>Box Comments</legend>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="comment" class="form-control-label"id="commentLabel">Comments:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" rows="3" maxlength="255" id="comment" name="comment"/></textarea>
						</div>
					</div>
				</fieldset>

				<input type="hidden" id="freezerID" class="boxLocField" name="freezerID" value="<?= $boxArray['freezerID']; ?>"/>

				<input type="hidden" id="boxID" name="boxID" value="<?= $boxArray['boxID']; ?>"/>

				<input type="hidden" id="form_checked" name="form_checked" value="0"/>
				<div class="row"style="display:inline;">

					<div class="col-xs-9 col-sm-10 col-md-10">
						<button type="submit" id="CreateBoxSubmit" name="CreateBoxSubmit" value="Submit" class="btn btn-primary btn-lg"><span id="new_item" class="show">Add a Box</span><span id="update_item" class="hide">Update Box</span>
						</button>
					</div>
					<div class="col-xs-3 col-sm-2 col-md-2">
						<button id="cancel" class="btn btn-primary btn-lg">
							<a href="?page=home">Cancel</a>
						</button>
					</div>

				</div>
			</form>

		</div>
		<!-- Boxes already added -->
		<div class="col-xs-12 col-sm-12 col-md-6">
			<div class='container-fluid'>
			     <div class="row">
			          <div class="col-xs-12 col-sm-12 col-md-12">

			               <fieldset id='make-group-fieldset'></p>
			                    <legend>Your Boxes already added</legend>
							<?php
								require_once('templates/lists/box_list.php');
							?>

			               </fieldset>
			          </div>
			     </div>
			</div>
		</div>
	</div>
</div>
