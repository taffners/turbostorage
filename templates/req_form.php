
<div class='container-fluid'>
	<div class="row">
		<!-- add a new freezer -->
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="col-xs-12 col-sm-12 col-md-12 alert alert-success">
				<b><u>Protocol on Request Forms</u></b>
				<ul>
					<li>General Requirements of Request Forms</li>
					<ul>
						<li>Necessary to use with Biobank Samples and not necessary for all other samples</li>
						<li>Can only be linked to Sample withdraws from a box with a privacy level of Group (add a box or update a box)</li>
						<li>The group needs to answer yes to sample request form required (add a group or update a group)</li>
					</ul>
					<li>Step by Step use After Above Requirements are Met</li>
					<ul>
						<li>Place request (add a request)</li>
						<li>All outstanding requests can be seen by anyone on the home page</li>
						<li>Request needs to be approved (update a Request)</li>
						<li>Remove tubes requested and click on the request the removal is linked to for each tube removed (remove a tube)</li>
						<li>Update Request Form to Yes the request is filled (update a tube)</li>
						<li>The request will no longer be visable on the homepage or update a request since it has be completed</li>
						<li>All completed requests can be found under reports (Requests Filled)</li>
					</ul>
				</ul>
			</div>
			<form name='create_request' id="create_request" class='form-horizontal' method='post'>

				<fieldset id="office-use-fieldset"
					<?php
						if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update')
						{
					?>
							class="show"
					<?php

						}
						else
						{
					?>
							class="hide"
					<?php
						}
					?>
				>
					<legend>Status of Request Info</legend>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-2 col-md-2">
							<label for="approvedBy" class="form-control-label">Approved By:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="approvedBy" name="approvedBy" maxlength="50" value="<?= $requestArray['approvedBy'];?>"/>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-2 col-sm-2 col-md-2">
							<label for="approved" class="form-control-label">Request Status:</label>
						</div>
						<div class="col-xs-10 col-sm-10 col-md-10">
							<div class="radio">
								<label class="radio-inline">
									<input type="radio" id="approved_yes" name="approved" value="1"<?php if($requestArray['approved'] == 1) echo 'checked'; ?> >Approved</input>
								</label>
								<label class="radio-inline">
									<input type="radio" id="approved_no" name="approved" value="0" <?php if($requestArray['approved'] == 0) echo 'checked'; ?> >Denied</input>
								</label>
								<label class="radio-inline">
									<input type="radio" id="approved_waiting" name="approved" value="2"
									<?php
										if($requestArray['approved'] == 2)
										{
									?>
											checked
									<?php
										}
										elseif(isset($_REQUEST['action']) && $_REQUEST['action'] == 'add')
										{
									?>
											checked
									<?php
										}
									?>
										>Waiting
									</input>
								</label>
							</div>
						</div>
					</div>

					<div class="form-group">
                              <div class="col-xs-12 col-sm-2 col-md-2">
                                   <label for="ApproveDate">Approve Date:</label>
                              </div>
                              <div class="col-xs-6 col-sm-4 col-md-5">
                                   <input class="form-control todayDate date-picker" type="text" id="ApproveDate" name="ApproveDate" value="<?= $dfs->ChangeDateFormatUS($requestArray['ApproveDate'], TRUE); ?>" placeholder="MM/DD/YYYY"/>
                              </div>
						<div class="col-xs-6 col-sm-4 col-md-4 pull-left">
							<button type="button" class="btn btn-primary todays-date" data-input-id="ApproveDate">Add Today's Date</button>
						</div>
                         </div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-12 alert alert-success">
							WARNING!!! Only change request to filled if all samples have been removed.
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2">
							<label for="requestFilled" class="form-control-label">Request Filled:</label>
						</div>
						<div class="col-xs-10 col-sm-10 col-md-10">
							<div class="radio">
								<label class="radio-inline">
									<input type="radio" id="requestFilled_yes" name="requestFilled" value="1"<?php if($requestArray['requestFilled'] == '1') echo 'selected checked'; ?> >Yes</input>
								</label>
								<label class="radio-inline">
									<input type="radio" id="requestFilled_no" name="requestFilled" value="0" <?php if($requestArray['requestFilled'] == '0') echo 'selected checked'; ?>
										<?php
											if($requestArray['requestFilled'] == '0')
											{
										?>
												checked
										<?php
											}
											elseif(isset($_REQUEST['action']) && $_REQUEST['action'] == 'add')
											{
										?>
												checked
										<?php
											}
										?>
										>No
									</input>
								</label>
							</div>
						</div>
					</div>

					<input type="hidden" id="commentID" name="commentID" value="<?= $tubeArray['commentID']; ?>"/>



               		<div class="row form-group">
                    	<label for="comment" class="col-sm-4 form-control-label" id="commentLabel">Comments:</label>
     					<div class="col-xs-10 col-sm-10 col-md-10">
     						<textarea class="form-control" rows="3" id="comment"maxlength="255" name="comment" value=""/></textarea>
     					</div>
						<div class="col-xs-2 col-sm-2 col-md-2">
							<button type="button" id="comment<?= $requestArray['reqID'];?>" class="btn btn-primary btn-lg add_comment_button" data-comment-type="request" data-comment-id="<?= $requestArray['reqID'];?>">
								<span class="glyphicon glyphicon-comment"></span>
							</button>
						</div>
               		</div>

				</fieldset>

				<?php
					if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update')
					{
				?>
						<div class="col-xs-12 col-sm-12 col-md-12 alert alert-success">
							Below you will find the request
						</div>
				<?php

					}

				?>

                    <fieldset id="new-request-fieldset">
					<?php
						if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update')
						{
					?>
							<legend>Requestor's Information</legend>
					<?php

						}
						else
						{
					?>
							<legend>Add a Request for a Sample</legend>
					<?php
						}
					?>

                         <div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="piName" class="form-control-label">PI Name:<span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="piName" name="piName" maxlength="50" value="<?= $requestArray['piName'];?>"required
							<?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?>/>
						</div>
					</div>
                         <div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="piEmail" class="form-control-label">PI email:<span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="email" class="form-control" id="piEmail" name="piEmail" maxlength="50" value="<?= $requestArray['piEmail'];?>"required <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?> />
						</div>
					</div>
                         <div class="form-group row">
                              <div class="col-xs-12 col-sm-4 col-md-4">
                                   <label for="labContactName" class="form-control-label">Lab Contact Name:</label>
                              </div>
                              <div class="col-xs-12 col-sm-8 col-md-8 pull-left">
                                   <input type="text" class="form-control" id="labContactName" maxlength="50" name="labContactName" value="<?= $requestArray['labContactName'];?>" <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?>/>
                              </div>
                         </div>
                         <div class="form-group row">
                              <div class="col-xs-12 col-sm-4 col-md-4">
                                   <label for="labContactEmail" class="form-control-label">Lab Contact email:</label>
                              </div>
                              <div class="col-xs-12 col-sm-8 col-md-8 pull-left">
                                   <input type="email" class="form-control" id="labContactEmail" maxlength="50" name="labContactEmail" value="<?= $requestArray['labContactEmail'];?>" <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?>/>
                              </div>
                         </div>
                         <div class="form-group row">
                              <div class="col-xs-12 col-sm-4 col-md-4">
                                   <label for="labContactPhone" class="form-control-label">Lab Contact phone number:</label>
                              </div>
                              <div class="col-xs-12 col-sm-8 col-md-8 pull-left">
                                   <input type="tel" class="form-control" id="labContactPhone" name="labContactPhone" value="<?= $requestArray['labContactPhone'];?>" <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?> />
                              </div>
                         </div>
                    </fieldset>
                    <fieldset id="requested-sample-fieldset">
                         <legend>Requested Samples</legend>
                         <div class="form-group">
                              <div class="col-xs-12 col-sm-4 col-md-4">
                                   <label for="requestDate">Request Date:<span class="required-field">*</span></label>
                              </div>
                              <div class="col-xs-6 col-sm-4 col-md-4">
                                   <input class="form-control todayDate date-picker" type="text" id="requestDate" name="requestDate" value="<?= $dfs->ChangeDateFormatUS($requestArray['requestDate'], TRUE); ?>" required <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?> placeholder="MM/DD/YYYY"/>
                              </div>
						<div class="col-xs-6 col-sm-4 col-md-4 pull-left">
							<button type="button" class="btn btn-primary todays-date" data-input-id="requestDate">Add Today's Date</button>
						</div>
                         </div>
					<div class="form-group">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="requestors_irb_num">Requestors IRB #:<span class="required-field">*</span></label>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-4">
							<input class="form-control todayDate" type="text"  id="requestors_irb_num" name="requestors_irb_num" value="<?= $requestArray['requestors_irb_num']; ?>" required <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?> maxlength="25"/>
						</div>
					</div>
                         <div class="col-xs-12 col-sm-12 col-md-12 alert alert-success">
                              A new request is needed for each type of sample requested
                         </div>
                         <div class="row form-group">
                              <div class="col-xs-12 col-sm-4 col-md-4">
                                   <label for="sampleType">Sample Type:<span class="required-field">*</span></label>
                              </div>
                              <div class="col-xs-12 col-sm-8 col-md-8 pull-left">

                                   <select class="form-control" id="sampleType" name="sampleType" required <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?>>
                                        <option disabled selected value> -- select an option -- </option>
                                        <option value="Antibody" <?php if($requestArray['sampleType'] == 'Antibody') echo 'selected'; ?>>Antibody</option>
                                        <option value="Bacterial_Cultures" <?php if($requestArray['sampleType'] == 'Bacterial_Cultures') echo 'selected'; ?>>Bacterial Cultures</option>
                                        <option value="Blood" <?php if($requestArray['sampleType'] == 'Blood') echo 'selected'; ?>>Blood</option>
                                        <option value="Body_fluids" <?php if($requestArray['sampleType'] == 'Body_fluids') echo 'selected'; ?>>Body fluids</option>
                                        <option value="Bone" <?php if($requestArray['sampleType'] == 'Bone') echo 'selected'; ?>>Bone</option>
                                        <option value="Cell_Lines" <?php if($requestArray['sampleType'] == 'Cell_Lines') echo 'selected'; ?>>Cell lines</option>
										<option value="Cell_Pellet" <?php if($requestArray['sampleType'] == 'Cell_Pellet') echo 'selected'; ?>>Cell Pellet</option>
                                        <option value="Chemical" <?php if($requestArray['sampleType'] == 'Chemical') echo 'selected'; ?>>Chemical</option>
                                        <option value="Cytokine" <?php if($requestArray['sampleType'] == 'Cytokine') echo 'selected'; ?>>Cytokine</option>
                                        <option value="DNA" <?php if($requestArray['sampleType'] == 'DNA') echo 'selected'; ?>>DNA</option>
										<option value="PBMC" <?php if($requestArray['sampleType'] == 'PBMC') echo 'selected'; ?>>PBMC</option>
                                        <option value="Plasma" <?php if($requestArray['sampleType'] == 'Plasma') echo 'selected'; ?>>Plasma</option>
                                        <option value="RNA" <?php if($requestArray['sampleType'] == 'RNA') echo 'selected'; ?>>RNA</option>
                                        <option value="Serum" <?php if($requestArray['sampleType'] == 'Serum') echo 'selected'; ?>>Serum</option>
                                        <option value="Urine" <?php if($requestArray['sampleType'] == 'Urine') echo 'selected'; ?>>Urine</option>
                                        <option value="Virus" <?php if($requestArray['sampleType'] == 'Virus') echo 'selected'; ?>>Virus</option>
                                        <option value="Whole_Tissue" <?php if($requestArray['sampleType'] == 'Whole_Tissue') echo 'selected'; ?>>Whole Tissue</option>

                                   </select>
                              </div>
                         </div>
                         <div class="form-group row">
                              <div class="col-xs-12 col-sm-4 col-md-4">
                                   <label for="quantiy" class="form-control-label">Quanity Requested:<span class="required-field">*</span></label>
                              </div>
                              <div class="col-xs-12 col-sm-4 col-md-4 pull-left">
                                   <input type="number" class="form-control" id="quantiy" name="quantiy" min="1" value="<?= $requestArray['quantiy'];?>"required  <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?>/>
                              </div>
                              <div class="col-xs-12 col-sm-1 col-md-1">
                                   <label for="quantiyUnits" class="form-control-label">Units:<span class="required-field">*</span></label>
                              </div>
                              <div class="col-xs-12 col-sm-3 col-md-3 pull-left">
                                   <select class="form-control" required name="quantiyUnits" <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?> >
								<option disabled selected value> -- select an option -- </option>
                                        <option value="ul" <?php if($requestArray['quantiyUnits'] == 'ul') echo 'selected'; ?> >ul</option>
                                        <option value="ml" <?php if($requestArray['quantiyUnits'] == 'ml') echo 'selected'; ?> >ml</option>
                                        <option value="million cells" <?php if($requestArray['quantiyUnits'] == 'million cells') echo 'selected'; ?> >million cells</option>
                                        <option value="ng/ul" <?php if($requestArray['quantiyUnits'] == 'ng/ul') echo 'selected'; ?> >ug</option>
							</select>
                              </div>
                         </div>
					<div class="form-group row">
                              <div class="col-xs-12 col-sm-4 col-md-4">
                                   <label for="sampleIDsRequested" class="form-control-label">Sample ID's Requested:<span class="required-field">*</span></label>
                              </div>
                              <div class="col-xs-12 col-sm-8 col-md-8 pull-left">
                                   <textarea type="text" class="form-control" id="sampleIDsRequested" name="sampleIDsRequested" maxlength="255" style="height:100px;" value="<?= $requestArray['sampleIDsRequested'];?>"required <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?>/><?= $requestArray['sampleIDsRequested'];?></textarea>
                              </div>
                         </div>
                    </fieldset>

				<fieldset id="purposed-assay-fieldset">
                         <legend>Purposed Assay</legend>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<label for="researchAbstract" class="form-control-label">
								Please provide a research abstract describing the research project, the experimental system, the research goals, and how the sample(s) will be used to facilitate the research project. <span class="required-field">*</span>
							</label>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12">
							<textarea type="text" class="form-control" id="researchAbstract" name="researchAbstract" style="height:100px;" maxlength="255" value="<?= $requestArray['researchAbstract'];?>"required <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?>/><?= $requestArray['researchAbstract'];?></textarea>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<label for="refs" class="form-control-label">
								Please describe your purposed assay and give any references.
							</label>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12">
							<textarea type="text" class="form-control" id="refs" name="refs" style="height:100px;" value="<?= $requestArray['refs'];?>"  <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?>/><?= $requestArray['refs'];?></textarea>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<label for="controls" class="form-control-label">
								Please describe your controls
							</label>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12">
							<textarea type="text" class="form-control" id="controls" name="controls" style="height:100px;" maxlength="255" value="<?= $requestArray['controls'];?>" <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?> /><?= $requestArray['controls'];?></textarea>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<label for="assayWorking" class="form-control-label">
								Please provide evidence of the successful completion of the assay on control samples
							</label>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12">
							<textarea type="text" class="form-control" id="assayWorking" name="assayWorking" style="height:100px;" maxlength="255" value="<?= $requestArray['assayWorking'];?>" <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?> /><?= $requestArray['assayWorking'];?></textarea>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<label for="refillReason" class="form-control-label">
								If this is a refill request, please provide the reason that a refill is required
							</label>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12">
							<textarea type="text" class="form-control" id="refillReason" name="refillReason" style="height:100px;"maxlength="255" <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?> value="<?= $requestArray['refillReason'];?>"  /><?= $requestArray['refillReason'];?></textarea>
						</div>
					</div>
				</fieldset>
				<fieldset id="contribution-data-fieldset">
                         <legend>Contribution of Data</legend>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<label for="dataContribution" class="form-control-label">
								Since we have a limited quantity of each sample, contribution of any data collected on these samples will greatly increase future research of these samples. Would you be willing to contribute your data to the database to assist in further research on these samples?<span class="required-field">*</span>
							</label>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12">
							<div class="radio">
								<label class="radio-inline">
									<input type="radio" id="data_yes" name="data_contribution" required value="1"<?php if($requestArray['data_contribution'] == '1') echo 'selected checked'; ?> <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?> >Yes</input>
								</label>
								<label class="radio-inline">
									<input type="radio" id="data_no" name="data_contribution" required value="0" <?php if($requestArray['data_contribution'] == '0') echo 'selected checked'; ?> <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update') echo "readonly='true'"; ?> >No</input>
								</label>


							</div>
						</div>
					</div>
				</fieldset>


				<input type="hidden" id="reqID" name="reqID" value="<?= $requestArray['reqID'];?>"/>

				<input type="hidden" id="userID" name="userID" value="<?= $requestArray['userID']; ?>"/>

				<input type="hidden" id="form_checked" name="form_checked" value="0"/>
				<?php
					if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update')
					{
				?>
						<button type="submit" id="CreateRequestSubmit" name="CreateRequestSubmit" value="Submit"  class="btn btn-primary btn-lg">Update Status of Request</button>

				<?php

					}
					else
					{
				?>
						<button type="submit" id="CreateRequestSubmit" name="CreateRequestSubmit" value="Submit"  class="btn btn-primary btn-lg">Make Sample Request</button>
				<?php
					}
				?>



               </form>
          </div>
     </div>
</div>
