<div class='container-fluid'>
	<div class="row">
		<!-- add a new irb -->
		<div class="col-xs-12 col-sm-12 col-md-6">

			<form name='create_irb' id="create_irb" class='form-horizontal' method='post'>
                    <div class="alert alert-success" role="alert">
                         This form is used to add a new IRB.  IRB's are linked to human samples while adding a tube.  This form will add more options to the drop down list IRB Project Title while adding a tube.
                    </div>
				<fieldset id='make-group-fieldset'>
					<legend id='new_legend' class='show'>
						Make a IRB
					</legend>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="irbProjectTitle" class="form-control-label">IRB Project Title <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="irbProjectTitle" name="irbProjectTitle" maxlength="255" value="<?= $irbArray['irbProjectTitle'];?>"required/>
						</div>
					</div>
                         <div class="form-group row">
                              <div class="col-xs-12 col-sm-4 col-md-4">
                                   <label for="irbPI" class="form-control-label">IRB Principle Investigator <span class="required-field">*</span></label>
                              </div>
                              <div class="col-xs-12 col-sm-8 col-md-8 pull-left">
                                   <input type="text" class="form-control" id="irbPI" name="irbPI" maxlength="100" value="<?= $irbArray['irbPI'];?>"required/>
                              </div>
                         </div>
                         <div class="form-group row">
                              <div class="col-xs-12 col-sm-4 col-md-4">
                                   <label for="irbNum" class="form-control-label">IRB Number</label>
                              </div>
                              <div class="col-xs-12 col-sm-8 col-md-8 pull-left">
                                   <input type="text" class="form-control" id="irbNum" name="irbNum" maxlength="20" value="<?= $irbArray['irbNum'];?>"/>
                              </div>
                         </div>

					<!-- hiddens -->
						<!-- form check input  -->
						<input type="hidden" id="form_checked" name="form_checked" value="0"/>
						<input type="hidden" id="irbID" name="irbID" value="<?= $irbArray['irbID'];?>"/>

					<!-- hiddens -->

					<div class="row"style="display:inline;">
						<div class="col-xs-9 col-sm-10 col-md-10">
							<button type="submit" id="CreateIRBSubmit" name="CreateIRBSubmit" value="Submit"  class="btn btn-primary btn-lg"><span id="new_item" class="show">Add an IRB</span><span id="update_item" class="hide">Update IFB</span></button>
						</div>
						<div class="col-xs-3 col-sm-2 col-md-2">
							<button id="cancel" class="btn btn-primary btn-lg">
								<a href="?page=home">Cancel</a>
							</button>
						</div>

					</div>
				</fieldset>
			</form>
		</div>
		<!-- freezers already added -->
		<div class="col-xs-12 col-sm-12 col-md-6">
			<fieldset id='make-group-fieldset'></p>
				<legend>IRB's Already Added</legend>
                    <div style="overflow-x:auto;">
                         <table class="QueryInfo dataTable" id="IRB-table">
                              <thead>
                                   <tr>
                                        <th style="display:none;">IRB ID</th>
                                        <th>IRB Project Title</th>
                                        <th>IRB Priciple Investigator</th>
                                        <th>IRB Number</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                        for($i=0; $i<sizeof($irbArray); $i++)
                                        {
                                   ?>
                                             <tr id="irb_<?=$irbArray[$i]['irbID']; ?>" >
                                                  <td style="display:none;"><?=$irbArray[$i]['irbID']; ?></td>
                                                  <td><?=$irbArray[$i]['irbProjectTitle']; ?></td>
                                                  <td><?=$irbArray[$i]['irbPI']; ?></td>
                                                  <td><?=$irbArray[$i]['irbNum']; ?></td>
                                             </tr>

                                   <?php
                                        }
                                   ?>
                              </tbody>
                         </table>
                    </div>

			</fieldset>

		</div>

	</div>
</div>
