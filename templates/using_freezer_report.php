<div class="col-xs-12 col-sm-12 col-md-12">
     <fieldset id-"freezer-user-fieldset">

          <legend>Get all the users of in freezers</legend>

          <!-- Add request remove log table here -->
          <div class="row">
			<div style="overflow-x:auto;margin-top:20px;">
				<table class="QueryInfo dataTable" id="freezer-user-table">
					<thead>
						<tr>
                                   <th style="display:none;">freezerID</th>
							<th>Freezer Name</th>
							<th>All Members</th>
                                   <th>Room Number</th>
                                   <th>Temp</th>
						</tr>
					</thead>
                         <tbody>
                              <?php
                                   for ($i = 0; $i < sizeof($freezerUsers); $i++)
                                   {
                              ?>
                                   <tr>
                                        <td style="display:none;"><?= $freezerUsers[$i]['freezerID']; ?></td>
                                        <td><?= $freezerUsers[$i]['freezerName']; ?></td>

                                        <td>
                                             <?php

                                                       if ($freezerUsers[$i]['groupID'] == null)
                                                       {
                                                            echo $freezerUsers[$i]['userName'];
                                                       }
                                                       else
                                                       {
                                                            echo $freezerUsers[$i]['allMembers'];
                                                       }

                                             ?>

                                             </td>
                                        <td><?= $freezerUsers[$i]['roomNum']; ?></td>
                                        <td><?= $freezerUsers[$i]['temp']; ?></td>
                                   </tr>
                              <?php
                                   }

                              ?>
                         </tbody>
                    </table>
               </div>
          </div>
     </fieldset>
</div>
<!-- All Users -->
<div class="col-xs-12 col-sm-12 col-md-12">
     <fieldset id-"user-table-fieldset">

          <legend>Users information</legend>

          <div class="row">
               <div class="col-md-12">
               <?php
                    require_once('templates/lists/user_list.php');
               ?>
               </div>
          </div>
     </fieldset>
</div>
