<html>
	<head>
		<title><?= SITE_TITLE; ?></title>
		<link rel="stylesheet" type="text/css" href="css/qunit.css">
		<link rel="stylesheet" type="text/css" href="css/libraries.min.css?ver=<?= TURBO_VERISION; ?>">
		<link rel='stylesheet' href='css/style.css?ver=<?= TURBO_VERISION; ?>'>
		<link rel="icon" href="img/Snowflake.ico">

		<meta charset="UTF-8">

	</head>
	<header>
		<div class="container-fluid TopName"><?= SITE_TITLE; ?></div>
	</header>
	<nav class="navbar navbar-inverse" >

  		<div class="container-fluid">
    		<div class="navbar-header">
      			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
		        		<span class="icon-bar"></span>
		        		<span class="icon-bar"></span>
		        		<span class="icon-bar"></span>
      			</button>
	    	</div>
    		<div class="collapse navbar-collapse" id="myNavbar">
      			<ul class="nav navbar-nav">
	        			<li class="active">
						<a href="?page=home<?php if (defined('USER_ID')) echo '&userID= '.USER_ID; ?>">
							<img src="img/house.png" style="height:23px;width:23px;margin-right:5px;">
							Home
						</a>



					</li>
	          			<li class="dropdown" id="add-nav-drop-down">
			          		<a class="dropdown-toggle" data-toggle="dropdown" href="#">Add<span class="caret"></span></a>
	        		  			<ul class="dropdown-menu" style="width:256px;">
			            			<li class="snowflake-icon">
									<a href="?page=freezer_form">Add a Freezer</a>
								</li>
			            			<li class="box-open-icon">
									<a href="?page=box_form">Add a Box</a>
								</li>
			            			<li class="test-tube-icon">
									<a href="?page=tube_form&action=add">Add a Tube

									</a>
								</li>
								<li class="divider"></li>
								<li class="group-icon">
									<a href="?page=group_form&action=add<?php if (defined('USER_ID')) echo '&userID= '.USER_ID; ?>">Add a Group

									</a>
								</li>
								<?php

									if (isset($roleList) && defined('USER_ID'))
									{
										// because $roleList is a nested array in_array is not useful so a for loop is used
										for($i=0; $i < sizeof($roleList); $i++)
										{
											if($roleList[$i]['roleName'] === 'Check Out Unique Numbers')
											{
								?>
												<li class="numbers-icon">
													<a href="?page=unique_home<?php if (defined('USER_ID')) echo '&userID= '.USER_ID; ?>">Check Out Unique Numbers

													</a>
												</li>
								<?php
											}
										}
									}
								?>
								<?php
									if (isset($roleList))
									{
										for($i=0; $i < sizeof($roleList); $i++)
										{
											if($roleList[$i]['roleName'] === 'Sample Request' || $roleList[$i]['roleName'] === 'Add IRBs' || $roleList[$i]['roleName'] === 'IRB Questions and Answers')
											{
								?>
												<li class="divider"></li>
												<li class="dropdown-header">Human Sample Section
													<span class="glyphicon glyphicon-arrow-down pull-right"></span>
												</li>
												<li class="divider"></li>
								<?php
												break;
											}
										}
									}
								?>
								<?php
									if (isset($roleList))
									{
										for($i=0; $i < sizeof($roleList); $i++)
										{
											if($roleList[$i]['roleName'] === 'Sample Request')
											{
								?>
								<li class="request-icon">
									<a href="?page=req_form&action=add">Add a Sample Request
									</a>
								</li>
								<?php
											}
										}
									}
								?>
								<?php
									if (isset($roleList))
									{
										for($i=0; $i < sizeof($roleList); $i++)
										{
											if($roleList[$i]['roleName'] === 'Add IRBs')
											{
								?>
								<li class="irb-icon">
									<a href="?page=irb_form&action=add"; ?>Add an IRB
									</a>
								</li>
								<?php
											}
										}
									}
								?>
								<?php
									if (isset($roleList))
									{
										for($i=0; $i < sizeof($roleList); $i++)
										{
											if($roleList[$i]['roleName'] === 'IRB Questions and Answers')
											{
								?>
								<li><a href="?page=irb_sample_restrictions&action=add"; ?>Add an IRB Question &amp; answer</a></li>
								<?php
											}
										}
									}
								?>
	          				</ul>
		        			</li>
		    		      	<li class="dropdown" id="update-nav-drop-down">
	          				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Update<span class="caret"></span></a>
			          		<ul class="dropdown-menu" style="width:200px;">
		    	        				<li class="snowflake-icon">
									<a href="?page=freezer_change&action=update">Update a Freezer</a>
								</li>
		        	    				<li class="box-open-icon">
									<a href="?page=box_change&action=update">Update a Box

									</a>
								</li>
		            				<li class="test-tube-icon">
									<a href="?page=tube_change&action=update">Update a Tube

									</a>
								</li>
								<li class="divider"></li>
								<li class="group-icon">
									<a href="?page=group_change&action=update">Update a Group

									</a>
								</li>
								<?php
									if (isset($roleList))
									{
										for($i=0; $i < sizeof($roleList); $i++)
										{
											if($roleList[$i]['roleName'] === 'Sample Request' )
											{
								?>
								<li class="divider"></li>
								<li class="dropdown-header">Human Sample Section
									<span class="glyphicon glyphicon-arrow-down pull-right"></span>
								</li>
								<?php
												break;
											}
										}
									}
								?>

								<?php
									if (isset($roleList))
									{
										for($i=0; $i < sizeof($roleList); $i++)
										{
											if($roleList[$i]['roleName'] === 'Sample Request')
											{
								?>
								<li class="request-icon">
									<a href="?page=req_change&action=update">Update a Request

									</a>
								</li>
								<?php
											}
										}
									}
								?>
	          				</ul>
			        		</li>
		    		      	<li class="dropdown" id="remove-nav-drop-down">
	          				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								Remove
								<span class="caret"></span>
							</a>
	          				<ul class="dropdown-menu" style="width:200px;">
			            			<li class="snowflake-icon">
									<a href="?page=freezer_change&action=delete">
										Remove a Freezer

									</a>
								</li>
				            		<li class="box-open-icon">
									<a href="?page=box_change&action=delete">
										Remove a Box

									</a>
								</li>
			    	        			<li class="test-tube-icon">
									<a href="?page=remove_tube_form&action=deleteTube">
										Remove a Tube

									</a>
								</li>
	          				</ul>
		        			</li>
						<li class="dropdown" id="reports-nav-drop-down">
	          				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								Reports
								<span class="caret"></span>
							</a>
	          				<ul class="dropdown-menu" style="width:310px;">
			            			<li>
									<a href="?page=box_info&action=report">Box Info</a>
								</li>
								<li>
									<a href="?page=using_freezer_report">
										Who is using each freezer?
									</a>
								</li>

								<?php
									if (isset($roleList))
									{
										for($i=0; $i < sizeof($roleList); $i++)
										{
											if($roleList[$i]['roleName'] === 'Sample Request' )
											{
								?>
								<li class="divider"></li>
								<li class="dropdown-header">Human Sample Section
									<span class="glyphicon glyphicon-arrow-down pull-right"></span>
								</li>
								<li class="divider"></li>
								<?php
												break;
											}
										}
									}
								?>
								<?php
									if (isset($roleList))
									{
										for($i=0; $i < sizeof($roleList); $i++)
										{
											if($roleList[$i]['roleName'] === 'Sample Request' )
											{
								?>
								<li class="request-icon">
									<a href="?page=report_request_filled&action=report">
										Requests Filled/Request Remove Log

									</a>
								</li>

								<?php
												break;
											}
										}
									}
								?>

	          				</ul>
		        			</li>
					</li>
				</ul>
				<?php

					if($page != 'login')
					{
				?>
						<ul class="nav navbar-nav navbar-right">
					     	<li><a href="utils/logout.php" style="padding-top:5px;padding-bottom:5px;"><img src="img/log_out.png" style="margin-left:5px;height:45px;width:30px;"> Logout</a></li>
						<!-- Find if the user has admin rights -->
						<?php
							if (isset($roleList))
							{
								for($i=0; $i < sizeof($roleList); $i++)
								{
									if($roleList[$i]['roleName'] === 'Admin')
									{
						?>
									<li><a href="?page=admin&userID=<?= USER_ID; ?>"><img src="img/Admin.png" style="margin-left:5px;height:23px;width:23px;"> admin</a></li>
						<?php
									}
								}
							}
						?>

						</ul>
				<?php
					}
				?>
    		</div>
    	</div>
  	</nav>

	<main>
		<div class="error" id="loginError"></div>
		<div class="userUpdate" ></div>
		<form id="danger" class="form-horizontal" method="post">
			<?php
			// print_r($roleList);
				if(isset($message))
				{
			?>
					<div class="alert alert-danger">
						<?= $message; ?>
					</div>
			<?php
				}
			?>
		</form>
		<?php

			require_once('templates/'.$page.'.php');

		?>
		<!-- insert modals here -->
		<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="modalInfoTitle"></h4>
					</div>
					<div class="modal-body" id="modal_body_id">
						<ul id='modal_body_id_ul'></ul>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="modal-box-location-info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="modalInfoTitle">Location of Box Info</h4>
					</div>
					<div class="modal-body" id="modal_body_id">
						<ul id='modal_body_id_ul'></ul>
						<?php
							require_once('templates/location_box_tips.php');
						?>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>


		<!-- Jquery Dialogs -->
		<!-- All the confirm windows -->
		    	<div id='ConfirmId' title='Update'></div>

		    	<div id='AlertId' title='Ooops!'></div>


		<!-- qunit -->
		<div id="qunit_testing" class="hide">
			<div id="qunit"></div>
			<div id="qunit-fixture"></div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="row">
				<span class="required-field">*</span> - Required Fields
			</div>
			<div class="row">
				<span class="unique-field">*</span> - Unique Fields
			</div>
		</div>
	</main>
	<footer>

		<script type="text/javascript" src="js/libraries/libraries.js?ver=<?= TURBO_VERISION; ?>"></script>
		<script type="text/javascript" src="js/libraries/qunit.js"></script>
		<script data-main="js/main" src="js/libraries/require.js?ver=<?= TURBO_VERISION; ?>"></script>

	</footer>
</html>
