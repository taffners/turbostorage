<div class='container-fluid' id="form_container" >

	<div class="row" style="padding-bottom:50px;">

          <a href="?page=unique_form" role="button" id="getNewNums" name="getNewNums"  class="btn btn-primary btn-lg">Get New Numbers</a>

     </div>

     <div class="row" id="recent_nums">

          <?php
               if(isset($_REQUEST['nums']))
               {
          ?>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                         <fieldset id='make-group-fieldset'></p>
                              <legend id='new_nums_legend' class='show'>
                                   Numbers Just Checked Out
                              </legend>
                              <div style="overflow-x:auto;">
                                   <table class="QueryInfo" id="FreezerInfo">
                                        <thead>
                                             <tr>
                                                  <th></th>
                                                  <th>Number</th>

                                             </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                                  $nums =  explode(',', $_REQUEST['nums']);

                                                  for($i=0;$i<sizeof($nums)-1;$i++)
                                                  {
                                             ?>
                                                       <tr>
                                                            <td><?php echo $i+1; ?>
                                                            <td><?php echo $nums[$i]; ?></td>
                                                       </tr>

                                             <?php
                                                  }
                                             ?>
                                        </tbody>
                                   </table>
                              </div>

          <?php
               }
          ?>
                         </fieldset>
                    </div>

          <div class="col-xs-12 col-sm-6 col-md-6">
               <fieldset id='make-group-fieldset'></p>
                    <legend id='new_nums_legend' class='show'>
                         Numbers You Previously Checked Out
                    </legend>

				<div class="row" style="margin-bottom:10px;">
					<div class="col-xs-offset-8 col-sm-offset-8 col-md-offset-8 col-xs-1 col-sm-1 col-md-1" style="padding-top:2px;">
						<u>Filters:</u>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3">
						<button type="button" class="btn btn-info btn-xs" id="unique-number-yes-filter">Yes</button>
						<button type="button" class="btn btn-default btn-xs" id="unique-number-no-filter">No</button>
						<button type="button" class="btn btn-success btn-xs" id="unique-number-all-filter">all</button>
					</div>
				</div>


                    <div style="overflow-x:auto;">
                         <table class="QueryInfo" id="FreezerInfo">
                              <thead>
                                   <tr>
                                        <th></th>
                                        <th>Number</th>
                                        <th>Time</th>
                                        <th>Used</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                        for($i=0;$i<sizeof($all_unique_nums);$i++)
                                        {
                                   ?>

                                             <tr
										<?php
											if ($all_unique_nums[$i]['used'] == '1')
											{
										?>
												class="yes-button-val"
										<?php
											}
											elseif ($all_unique_nums[$i]['used'] == '0')
											{
										?>
												class="no-button-val"
										<?php
											}
										?>
									>
                                                  <td><?php echo $i+1; ?>
                                                  <td><?php echo $all_unique_nums[$i]['uniqueID']; ?></td>
                                                  <td><?php echo $all_unique_nums[$i]['creationTime']; ?></td>
                                                  <td class="used_check_buttons">
                                                       <div class="radio" >
                                                            <label class="radio-inline">
                                                                 <input type="radio" id="used_no_<?php echo $all_unique_nums[$i]['uniqueID']; ?>" name="used<?php echo $all_unique_nums[$i]['uniqueID']; ?>" value="0" <?php if($all_unique_nums[$i]['used'] == '0') echo 'selected checked'; ?> >No</input>
                                                            </label>
                                                            <label class="radio-inline">
                                                                 <input type="radio" id="used_yes_<?php echo $all_unique_nums[$i]['uniqueID']; ?>" name="used<?php echo $all_unique_nums[$i]['uniqueID']; ?>" value="1"<?php if($all_unique_nums[$i]['used'] == '1') echo 'selected checked'; ?> >Yes</input>
                                                            </label>

                                                       </div>

                                                  </td>
                                             </tr>

                                   <?php
                                        }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </fieldset>
          </div>
     </div>
</div>
