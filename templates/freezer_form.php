<div class='container-fluid'>
	<div class="row">
		<!-- add a new freezer -->
		<div class="col-xs-12 col-sm-12 col-md-6">

			<form name='create_freezer' id="create_freezer" class='form-horizontal' method='post'>
				<fieldset id='make-group-fieldset'>
					<legend id='new_legend' class='show'>
						Make a New Freezer
					</legend>
					<legend id='update_legend' class='hide'>
						Update Freezer
					</legend>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="freezerName" class="form-control-label">Freezer Name: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="freezerName" name="freezerName" maxlength="32" value="<?= $freezerArray['freezerName'];?>"required/>
						</div>
					</div>
					<div class="row">
						<div class="alert alert-success" role="alert">
							<u>Note</u>: Only intergers are allowed in room number
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="roomNum" class="form-control-label">Room Number: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control" id="roomNum" name="roomNum" min="1" value="<?= $freezerArray['roomNum'];?>"required/>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="temp" class="form-control-label">Freezer Temperature: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<select id="temp" name="temp" class="form-control">

								<option value="4" <?php if($freezerArray['temp'] == '4') echo 'selected="true"'; ?>>
									4 &deg;C
								</option>
								<option value="-20" <?php if($freezerArray['temp'] == '-20') echo 'selected="true"'; ?>>
									-20 &deg;C
								</option>
								<option value="-80" <?php if($freezerArray['temp'] == '-80') echo 'selected="true"'; ?>>
									-80 &deg;C
								</option>
								<option value="-196" <?php if($freezerArray['temp'] == '-196') echo 'selected="true"'; ?>>
									-196 &deg;C
								</option>

							</select>

							<!-- <input type="number" placeholder="Enter Temp in &deg;C" class="form-control" id="temp" name="temp" value="<?= $freezerArray['temp'];?>" max="4"required/> -->
						</div>
					</div>

					<div class="row">
						<div class="alert alert-success" role="alert">
							<u>Note</u>: If Freezer is a liquid Nitrogen tank style freezer shelf number equals 1.
						</div>
					</div>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="numShelf" class="form-control-label">Number Of Shelves: <span class="required-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="number" class="form-control" id="numShelf" name="numShelf" value="<?= $freezerArray['numShelf'];?>" min="1" required/>
						</div>
					</div>


					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="comment" class="form-control-label" id="commentLabel">Comments:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<textarea class="form-control" rows="3" id="comment" name="comment" maxlength="255"/></textarea>
						</div>
					</div>
					<!-- hiddens -->
						<!-- form check input  -->
						<input type="hidden" id="form_checked" name="form_checked" value="0"/>
						<input type="hidden" id="commentID" name="commentID" value="<?= $freezerArray['commentID'];?>"/>
						<input type="hidden" id="freezerID" name="freezerID" value="<?= $freezerArray['freezerID'];?>"/>
					<!-- hiddens -->

					<div class="row"style="display:inline;">
						<div class="col-xs-9 col-sm-10 col-md-10">
							<button type="submit" id="CreateFreezerSubmit" name="CreateFreezerSubmit" value="Submit"  class="btn btn-primary btn-lg"><span id="new_item" class="show">Add a Freezer</span><span id="update_item" class="hide">Update Freezer</span></button>
						</div>
						<div class="col-xs-3 col-sm-2 col-md-2">
							<button id="cancel" class="btn btn-primary btn-lg">
								<a href="?page=home">Cancel</a>
							</button>
						</div>

					</div>
				</fieldset>
			</form>
		</div>
		<!-- freezers already added -->
		<div class="col-xs-12 col-sm-12 col-md-6">
			<fieldset id='make-group-fieldset'></p>
				<legend>Freezers already added</legend>
			<?php

				require_once('templates/lists/freezer_list.php');
			?>
			</fieldset>

		</div>

	</div>
</div>
