<div class='container-fluid'>
	<div class="row">
		<!-- add a new irb -->
		<div class="col-xs-12 col-sm-12 col-md-12">

			<form name='create_sample_restriction' id="create_sample_restriction" class='form-horizontal' method='post'>
                    <div class="alert alert-success" role="alert">
                         This form is used to add IRB Questions &amp; Answers. The purpose is to keep track of what type of research can be performed using the sample.
                         <br><br>
                         <b>Below is an example of an IRB question &amp; answer which is very broad.</b>
                         <ul>
                              <li>
                                   Does this project involve any research on genes or genetic testing/research?
                              </li>
                              <li>
                                   DNA and RNA may be sequenced from the patient's banked samples. Analysis may involve DNA sequencing (i.e. targeted regions, whole-exome, whole-genome, genotyping, ChIP-SEQ, etc), RNA sequencing (i.e. whole-transcriptome analysis, microarray, qPCR, etc.), proteomics, and profilling modified nucleotides (i.e. methylation, hydroxymethylation, inosilation, etc)
                              </li>
                         </ul>
                         <br>
                         <b>However, the Answer could have been more more restrictive.  Here's an example one below to the same question above.</b>
                         <ul>
                              <li>
                                   Yes. Whole Exome sequencing of some samples. Data may be submitted to dbGap
                              </li>
                         </ul>
                         <br>
                         <b>This is how this form works</b>
                         <ul>
                              <li>Every question is linked to an IRB &amp; the date the question was asked</li>
                              <li>Once a Date the Consent was Approved by IRB is entered it will be linked to the IRB question closet to it</li>
                         </ul>

                         <!-- IRB Approval Information -->
                    </div>
                    <fieldset id='make-group-fieldset'>
					<legend id='new_legend' class='show'>
						Add an IRB Question &amp; Answer
					</legend>
                         <div class="row form-group">
                              <div class="col-xs-12 col-sm-12 col-md-12">
                                   <label for="IRBQuestion">Question Asked By the IRB:</label>
                                   <textarea class="form-control" name="IRBQuestion" rows="3" id="IRBQuestion" ><?= $irb_questions['IRBQuestion']; ?></textarea>
                              </div>
                         </div>
                         <div class="row form-group">
                              <div class="col-xs-12 col-sm-12 col-md-12">
                                   <label for="consentDate">Date IRB Asked Question:</label>
                              </div>

                              <div class="col-xs-12 col-sm-12 col-md-12">
                                   <input class="form-control date-picker" type="text" id="consentDate" name="consentDate" value="<?= $dfs->ChangeDateFormatUS($irb_questions['consentDate'], TRUE); ?>" placeholder="MM/DD/YYYY"/>
                              </div>
                         </div>
                         <div class="row form-group">
                              <div class="col-xs-12 col-sm-12 col-md-12">
                                   <label for="answer">Answer to the IRB Question:</label>
                                   <textarea class="form-control" name="answer" rows="3" id="answer" ><?= $irb_questions['answer']; ?></textarea>
                              </div>
                         </div>
                         <div class="row form-group">
                              <div class="col-xs-12 col-sm-8 col-md-12">
                                   <label for="irbID">IRB Project Title:</label>
                                   <select id="irbID" name="irbID" class="form-control">

                                        <!-- if this is an update use the tube array to build selection list otherwise use the irb_array -->
                                        <option value="" selected="selected"></option>
                                        <?php
                                             if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'updateTube')
                                             {
                                                  for ($i=0; $i<sizeof($irb_array); $i++)
                                                  {
                                        ?>
                                                       <option value="<?php echo $irb_array[$i]['irbID']; ?>" <?php if($tubeArray['irbID'] == $irb_array[$i]['irbID']) echo 'selected="selected"'; ?> >
                                                            <?php echo $irb_array[$i]['irbProjectTitle']; ?>
                                                       </option>
                                        <?php
                                                  }
                                             }
                                             else
                                             {
                                                  for ($i=0; $i<sizeof($irb_array); $i++)
                                                  {
                                        ?>
                                                       <option value="<?php echo $irb_array[$i]['irbID']; ?>">
                                                            <?php echo $irb_array[$i]['irbProjectTitle']; ?>
                                                       </option>
                                        <?php
                                                  }
                                             }
                                        ?>
                                   </select>
                              </div>
                         </div>
                         <div class="row"style="display:inline;">
                              <div class="col-xs-9 col-sm-10 col-md-10">
                                   <button type="submit" id="CreateIRBSubmit" name="CreateIRBSubmit" value="Submit"  class="btn btn-primary btn-lg"><span id="new_item" class="show">Add an IRB</span><span id="update_item" class="hide">Update IFB</span></button>
                              </div>
                              <div class="col-xs-3 col-sm-2 col-md-2">
                                   <button id="cancel" class="btn btn-primary btn-lg">
                                        <a href="?page=home">Cancel</a>
                                   </button>
                              </div>

                         </div>
                    </fieldset>
               </form>
