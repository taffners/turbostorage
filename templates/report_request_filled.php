<!-- requests filled -->
<div class="col-xs-12 col-sm-12 col-md-12">
     <?php
          require_once('templates/lists/req_list.php');
     ?>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
     <fieldset id-"report-samples-removed">

          <legend>Request Remove Log</legend>

          <!-- Add request remove log table here -->
          <div class="row">
			<div style="overflow-x:auto;margin-top:20px;">
				<table class="QueryInfo dataTable" id="report_request_remove_log">
					<thead>
						<tr>
							<th style="display:none;">removeID</th>
                                   <th style="display:none;">tubeID</th>
							<th>Request #</th>
							<th>freezer Name</th>
							<th>Shelf #</th>
							<th>Rack #</th>
							<th>Rack Space #</th>
                                   <th>Box Name</th>
							<th>Space Loc</th>
                                   <th>Tube Name</th>
                                   <th>Sample Type</th>
                                   <th>Sample Removed By?</th>
						</tr>
					</thead>
                         <tbody>
                              <?php

                                   for($i=0;$i<sizeof($removeLog);$i++)
                                   {
                              ?>
                                        <tr>
                                             <td style="display:none;"><?= $removeLog[$i]['removeID']; ?></td>
                                             <td style="display:none;"><?= $removeLog[$i]['tubeID']; ?></td>
                                             <td><?= $removeLog[$i]['reqID']; ?></td>
                                             <td><?= $removeLog[$i]['freezerName']; ?></td>
                                             <td><?= $removeLog[$i]['shelfNum']; ?></td>
                                             <td><?= $removeLog[$i]['rackNum']; ?></td>
                                             <td><?= $removeLog[$i]['rackSpaceNum']; ?></td>
                                             <td><?= $removeLog[$i]['boxName']; ?></td>
                                             <td><?= $removeLog[$i]['spaceLoc']; ?></td>
                                             <td><?= $removeLog[$i]['tubeName']; ?></td>
                                             <td><?= $removeLog[$i]['sampleType']; ?></td>
                                             <td><?= $removeLog[$i]['userName']; ?></td>
                                        </tr>
                              <?php
                                   }
                              ?>
                         </tbody>
				</table>

			</div>
		</div>



     </fieldset>
</div>
