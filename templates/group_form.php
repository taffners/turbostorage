<div class='container-fluid' id="form_container" >

	<div class="row">

		<div class="col-xs-12 col-sm-12 col-md-6">
			<!-- form -->
			<form id="create_group" class="form-horizontal" method="post">
				<fieldset id='make-group-fieldset'>
					<legend id="new_legend" class="show">Make a New Group</legend>
					<legend id="update_legend" class="hide">Update Group</legend>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="groupName" class="form-control-label">Group Name: <span class="required-field">*</span> <span class="unique-field">*</span></label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" id="groupName" class="form-control" name="groupName" maxlength="32" value="<?= $groupArray['groupName'];?>"/>
						</div>
					</div>
					<!-- green alert box notifying to choose everyone in a group -->
					<div class="row">
						<?php

					          if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'update')
					          {
					     ?>
								<div class="alert alert-success" role="alert">
									<h4>Members can only be added to a group not deleted.</h4>
								</div>

					     <?php
					          }
					          else
					          {
					     ?>
								<div class="alert alert-success" role="alert">
									<h4>Select At least two people to form a sharing Group.  Make sure to add yourself to the group. <span class="required-field">*</span></h4>
								</div>
					     <?php
					          }
					     ?>

					</div>
					<!-- All Users -->
					<div class="row">
						<div class="col-md-12">
						<?php
							require_once('templates/lists/user_list.php');
						?>
						</div>
					</div>


					<!-- green alert box notifying to about request form -->
					<div class="row">
						<div class="alert alert-success" role="alert">
							<h4>The Sample Request Form allows for historical tracking of the reason samples were removed, if they are approved for removal, and who requested them.  <b>The sample request form is required for biobanking.</b> However, tube information is never deleted from the database.  When samples are removed from boxes the space will just be emptied but the data will still be there.</h4>
						</div>
					</div>


					<!-- Sample Request required -->
					<div class="form-group row" style="margin-top:20px">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="reqRequiredLabel" class="form-control-label">Sample Request Form Required: <span class="required-field">*</span></label>
						</div>


						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<label class="radio-inline">
								<input type="radio" id="reqRequired_no" name="reqRequired" value="0" required <?php if($groupArray['reqRequired'] === '0') echo 'checked'; ?>>
									No
								</input>

							</label>
							<label class="radio-inline">
								<input type="radio" id="reqRequired_yes" name="reqRequired" value="1" <?php if($groupArray['reqRequired'] === '1') echo 'checked'; ?>>
									yes
								</input>

							</label>

						</div>
					</div>

				<div class="form-group row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<label for="comment" id="commentLabel" class="form-control-label">Comments:</label>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
						<textarea class="form-control" rows="3" id="comment" name="comment" value="" maxlength="255"/></textarea>
					</div>
				</div>

				<!-- hiddens -->
					<input type="hidden" id="groupID" name="groupID" value="<?= $groupArray['groupID'];?>"/>

					<input type="hidden" id="commentID" name="commentID" value=""/>

				<!-- Insert all locations of choosen cells here as hidden fields-->
				<div class="hide" id="Clicked_Users">

				</div>
				<!-- form check input  -->
					<input type="hidden" id="form_checked" name="form_checked" value="0"/>

				<div class="form-group row">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" id="CreateGroupSubmit" name="CreateGroupSubmit" value="Submit"  class="btn btn-primary btn-lg"><span id="new_item" class="show">Create Group</span><span id="update_item" class="hide">Update Group</span></button>
					</div>
				</div>
			</form>
		</div>

		<!-- groups you belong to box -->
		<div class="col-xs-12 col-sm-12 col-md-6">
			<?php
				require_once('templates/lists/group_list.php');
			?>
		</div>
	</div>
</div>
