<fieldset id='make-group-fieldset'></p>
	<legend>
		<?php
			if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update')
			{
		?>
				Update a box
		<?php
			}

			elseif (isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete')
			{
		?>
				Delete a Box Without Any Tubes In Them

		<?php
			}
		?>
	</legend>
	<div class="row">
		<?php
			if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update')
			{
		?>
				<div class="alert alert-success" role="alert">
					Click on the box row that you would like to update
				</div>
		<?php
			}

			elseif (isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete')
			{
		?>
				<div class="alert alert-Danger" role="alert">
					Click on the box row that you would like to delete.<br>
					<h1>Deletion is Permanent</h1>
				</div>

		<?php
			}
		?>

	</div>
	<div class="FormBoxCustom">
		<div class='container-fluid'>

			<?php
				require_once('templates/lists/box_list.php');
			?>
		</div>
	</div>
</fieldset>
