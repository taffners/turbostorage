
<?php
	if (isset($_REQUEST['failed_question_check']))
	{
?>
	<div class="col-xs-12 col-sm-12 col-md-12 pull-left alert alert-danger" id="failed_question_check_div">
	     <div class="col-xs-6 col-sm-6 col-md-9">
	          The answers to your secruity questions do not match our records. Try again.
	     </div>
	     <div class="col-xs-6 col-sm-6 col-md-3">
	          <img src="img/failure.png" alt="firefox" height="240" width="240">
	     </div>
	</div>
<?php
	}
	elseif(isset($_REQUEST['something_wrong']))
	{
?>
		<div class="col-xs-12 col-sm-12 col-md-12 pull-left alert alert-danger" id="failed_question_check_div">
			<div class="col-xs-6 col-sm-6 col-md-9">
				The answers to your secruity questions do not match our records. Try again.
			</div>
			<div class="col-xs-6 col-sm-6 col-md-3">
				<img src="img/failure.png" alt="firefox" height="240" width="240">
			</div>
		</div>
<?php
	}
?>

<div class="col-xs-12 col-sm-12 col-md-12 pull-left alert alert-danger hide" id="failed_email_check_div">
	<div class="col-xs-6 col-sm-6 col-md-9">
		The email you entered does not match our records. Try again.
	</div>
	<div class="col-xs-6 col-sm-6 col-md-3">
		<img src="img/failure.png" alt="firefox" height="240" width="240">
	</div>
</div>

<div class='container-fluid' id="loginContainer" >
	<div class='row' id="loginOutsideRow">;

		<div class='col-xs-offset-1 col-xs-10 col-sm-offset-1 col-sm-10 col-md-offset-3 col-md-6' id="loginCol" >
		<!-- row for description -->
			<div class="row" id="siteDescription">
				<div class='container-fluid TopName'>
					<?= SITE_TITLE; ?>
				</div>
               </div>
               <div id="all_login_fields" class="show">
				<div class="FormBoxCustom">
					<form method='post' class="form-horizontal" id="change_password_form" style='padding:10px;'>
                              <div id="get_security_quest_div" class="show">
							<p class='FormHeaderCustom'>Change Password</p>
     						<div class="form-group row">
     							<div class="col-xs-12 col-sm-12 col-md-12">
     								<label for="inputEmailAddress" class="form-control-label">Enter Your Email Address Here</label>
     							</div>
     							<div class="col-xs-12 col-sm-12 col-md-12 pull-left">
     								<input type='text' id="input_emailAddress" name='emailAddress' class="form-control" maxlength="40" placeholder='Full Email Address'>
     							</div>
     						</div>
     						<div class="form-group row">
     							<div class="col-xs-9 col-sm-10 col-md-10" >
     								<button id="get_security_quest_button" name="get_security_quest_button" class='btn btn-primary btn-lg'>Submit</button>
     							</div>
                                        <div class="col-xs-3 col-sm-2 col-md-2">
                                             <a href="?page=login"class="btn btn-primary btn-lg" id="cancel">Cancel</a>
                                        </div>
     						</div>
                              </div>
                              <div id="check_security_answers_div" class="hide">
							<p class='FormHeaderCustom'>Answer the following questions to change your password</p>
							<div class="hide">
								<input type='text' id="userID" name='userID' class="form-control" value="">
							</div>
                                   <div class="form-group row">
          						<div class="col-xs-12 col-sm-3 col-md-3">
          							<label for="securityQuestion" class="form-control-label">Security Question 1</label>
          						</div>
          						<div class="col-xs-12 col-sm-9 col-md-9 pull-left">
									<input type='text' id="securityQuestionVal" name='securityQuestion' class="form-control hide" value="" readonly>
									<input type='text' id="security_question_1_written" class="form-control" value="" disabled>
          						</div>
          					</div>

          					<div class="form-group row">
          						<div class="col-xs-12 col-sm-3 col-md-3">
          							<label for="securityAnswer" class="form-control-label">Security Answer 1</label>
          						</div>
          						<div class="col-xs-10 col-sm-7 col-md-7 pull-left">
          							<input type='password' id="securityAnswer" maxlength="32" name='securityAnswer'  class="form-control" value="">
          						</div>

          						<div class="col-xs-2 col-sm-2 col-md-2 pull-left">
          							<button type="button" id="show_security_answer_create" class="btn btn-primary btn-lg">
          								Show
          							</button>
          						</div>
          					</div>

          					<div class="form-group row">
          						<div class="col-xs-12 col-sm-3 col-md-3">
          							<label for="securityQuestion2" class="form-control-label">Security Question 2</label>
          						</div>
          						<div class="col-xs-12 col-sm-9 col-md-9 pull-left">
									<input type='text' id="securityQuestion2Val" name='securityQuestion2' class="form-control show" value="" readonly>
									<input type='text' id="security_question_2_written" class="form-control" value="" disabled>
          						</div>
          					</div>

          					<div class="form-group row">
          						<div class="col-xs-12 col-sm-3 col-md-3">
          							<label for="securityAnswer2" class="form-control-label">Security Answer 2</label>
          						</div>
          						<div class="col-xs-10 col-sm-7 col-md-7 pull-left">
          							<input type='password' id="securityAnswer2" maxlength="32" name='securityAnswer2'  class="form-control" value="">
          						</div>
                                        <div class="col-xs-2 col-sm-2 col-md-2 pull-left">
                                             <button type="button" id="show_security_answer_create2" class="btn btn-primary btn-lg">
                                                  Show
                                             </button>
                                        </div>
                                   </div>
                                   <div class="form-group row">
          						<div class="col-xs-12 col-sm-3 col-md-3">
          							<label for="password" class="form-control-label">New Password</label>
          						</div>
          						<div class="col-xs-10 col-sm-7 col-md-7 pull-left">
          							<input type='password' id="password_create" name='password' class="form-control" maxlength="60"  pattern=".{8,}" value="">
          						</div>
          						<div class="col-xs-2 col-sm-2 col-md-2 pull-left">
          							<button type="button" id="show_password_create" class="btn btn-primary btn-lg">
          								Show
          							</button>
          						</div>
          					</div>
                                   <div class="form-group row">
          						<div class="col-xs-12 col-sm-3 col-md-3">
          							<label for="password2" class="form-control-label">Confirm Password</label>
          						</div>
          						<div class="col-xs-10 col-sm-7 col-md-7 pull-left">
          							<input type='password' id="password_create2" name='password2' class="form-control" maxlength="60"  pattern=".{8,}" value="">
          						</div>
          						<div class="col-xs-2 col-sm-2 col-md-2 pull-left">
          							<button type="button" id="show_password_create2" class="btn btn-primary btn-lg">
          								Show
          							</button>
          						</div>
          					</div>
							<input type="hidden" id="form_checked" name="form_checked" value="0"/>

                                   <div class="form-group row">
                                        <div class="col-xs-9 col-sm-10 col-md-10">
                                             <button  id="check_security_answers_submit" name="check_security_answers_submit"  class='btn btn-primary btn-lg'>Submit</button>
                                        </div>
                                        <div class="col-xs-3 col-sm-2 col-md-2">
                                             <a href="?page=login"class="btn btn-primary btn-lg" id="cancel">Cancel</a>
                                        </div>
                                   </div>
                              </div>



					</form>
				</div>
               </div>
          </div>
     </div>
</div>
