<div class="row">
     <div class="alert alert-success" role="alert">
          <u>Note</u>: Shelf numbering standard is from top to bottom.
          If Freezer is a liquid Nitrogen tank style freezer shelf number equals 1.
     </div>
</div>

<?php
     if(isset($_REQUEST['page']) && $_REQUEST['page'] == 'box_form')
     {
?>
          <div class="form-group row">
               <div class="col-xs-12 col-sm-4 col-md-4">
                    <label for="numShelf" class="form-control-label">Shelf Number: <span class="required-field">*</span></label>
               </div>
               <div class="col-xs-12 col-sm-8 col-md-8 pull-left">
                    <input type="number" class="form-control boxLocField" id="numShelf" name="shelfNum" min="1" value="<?= $boxArray['shelfNum']; ?>" />
               </div>
          </div>
<?php
     }
?>
<div class="row">
     <div class="alert alert-success" role="alert">
          <u>Note</u>: Rack numbering standard is from left to right for vertical freezers.
          <br><br>
          If Freezer is a liquid Nitrogen tank style freezer number rows from farthest to closest and left to right.
          <br><br>
          It is best for boxes to be in racks.  All boxes must be in a different space (shelf, rack number, rack space number). Therefore if you pick 0 for your rack (meaning no rack) you will need to make up a unique rack space number for all boxes without a rack on that shelf.
     </div>
</div>

<?php
     if(isset($_REQUEST['page']) && $_REQUEST['page'] == 'box_form')
     {
?>
          <div class="form-group row">
               <div class="col-xs-12 col-sm-4 col-md-4">
                    <label for="rackNum" class="form-control-label">Rack Number: <span class="required-field">*</span></label>
               </div>
               <div class="col-xs-12 col-sm-8 col-md-8 pull-left">
                    <input type="number" id="rackNum"  class="form-control boxLocField" name="rackNum" min="0"  value="<?= $boxArray['rackNum']; ?>" />

               </div>
          </div>
<?php
     }
?>

<div class="row">
     <div class="alert alert-success" role="alert">
          <dt>
               <u><b>Rack Spacing Numbering Standard:</b></u>
          </dt>
          <dl>
               <div class="row">
                    <div  class="col-xs-12 col-sm-6 col-md-6">
                         <dt>
                              Tower Racks or 1 Column Racks
                         </dt>

                         <dd>
                              - The Space at the bottom of the rack is space 1
                         </dd>
                    </div>
                    <div  class="col-xs-12 col-sm-6 col-md-6">
                         Example:
                         <img src="img/rack_col.png" alt="rack matrix" height="140" >
                    </div>
               </div>
          </dl>
     </div>
     <div class="alert alert-success" role="alert">
          <dt>
               <u><b>Matrix racks</b></u>
          </dt>
          <dl>
               <div class="row">
                    <div  class="col-xs-12 col-sm-6 col-md-6">
                         <dd>
                              - Space 1 is located at the top back of the freezer
                         </dd>
                         <dd>
                              - Increase the space numbers from top to bottom of the first column
                         </dd>
                    </div>
                    <div  class="col-xs-12 col-sm-6 col-md-6">
                         Example:
                         <img src="img/rack_matrix.png" alt="rack matrix" height="70" >
                    </div>
               </div>
          </dl>
     </div>
</div>

<?php
     if(isset($_REQUEST['page']) && $_REQUEST['page'] == 'box_form')
     {
?>
          <div class="form-group row">
               <div class="col-xs-12 col-sm-4 col-md-4">
                    <label for="rackSpaceNum" class="form-control-label">Rack Space Number: <span class="required-field">*</span></label>
               </div>
               <div class="col-xs-12 col-sm-8 col-md-8 pull-left">
                    <input type="number" id="rackSpaceNum"  class="form-control" name="rackSpaceNum" min="1"  value="<?= $boxArray['rackSpaceNum']; ?>" />

               </div>
          </div>
<?php
     }
?>
