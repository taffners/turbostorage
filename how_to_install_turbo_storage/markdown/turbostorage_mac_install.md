
# Install Requirements

> * My install instructions are only local hosting of Turbo Storage.  
> * MAMP can be used for an easy way to install the required environment for Turbo Storage.
> * MAMP, Apache, MySQL, PHP 5 (MAMP)

# What is MAMP

> * The easiest way to set up Turbo Storage is using a program called MAMP.  [MAMP website](https://www.mamp.info/en/ "MAMP's Website")

> * MAMP only works on macOS and Windows.

> * According to the MAMP website: "MAMP installs a local server environment in a matter of seconds on your computer. It comes free of charge, and is easily installed. MAMP will not compromise any existing Apache installation already running on your system. You can install Apache, PHP and MySQL without starting a script or having to change any configuration files! Furthermore, if MAMP is no longer needed, just delete the MAMP folder and everything returns to its original state (i.e. MAMP does not modify any of the normal system)."

> * [Video tutorial on what MAMP is](https://www.lynda.com/WordPress-tutorials/What-MAMP/361682/370429-4.html)  This video is a good explanation of what MAMP is but ignore all the Word Press talk. I did not use Word Press for Turbo Storage

# How do you set up MAMP on MacOSX

> * I have never installed MAMP on windows therefore I will not discuss windows set up.  I imagine it is similar to MacOSX.

> * [Download MAMP](https://www.mamp.info/en/ "MAMP's Downloads")

> * Once MAMP is installed in you Mac's Applications Folder navigate to the MAMP folder.

> * Place the turbostorage folder inside the htdocs folder. (If you haven't downloaded Turbo Storage it can be downloaded at the top of the page)
>> ![Folder stucture of MAMP](img/MAMP/MAMP_Folders.png)

> * Once the turbostorage folder is placed inside the htdocs folder double click on the MAMP.app icon
>> ![Folder stucture of MAMP](img/MAMP/MAMP_Folders.png)

> * This window will appear. Press Start Servers.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_server_off.png)

> * Once your server is runing the button will turn green.  Only push this green button to turn the server off.  Do not close window otherwise your server will be turned off.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_server_on.png)

> * Now press the Open WebStart Page.  This will open in your default internet browser. This is how you interact with your server, database, and php.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_webstart_page.png)

> * Now MAMP is set up and running.  Lets install the Turbo Storage Database next.

# Install Turbo Storage Database

> * Start Server

> * Open WebStart Page
>> ![Folder stucture of MAMP](img/MAMP/MAMP_webstart_page.png)

> * Go to phpMyAdmin
>> ![Folder stucture of MAMP](img/MAMP/MAMP_webstart_page.png)

> * phpmyadmin home page.  This is how you access your database directly.  Avoid access this much since you can delete some important things here.
>> ![Folder stucture of MAMP](img/MAMP/phpmyadmin_home.png)

> * Click on the Databases tab. The tab will look like the picture below.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_database.png)

> * Type in the create database field the database name for turbostorage which is freezer. Keep the drop down menu as Collation.  Press Create.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_database_2.png)

> * Now lets add something to this newly made freezer database.  Click on the freezer tab on the right menu database section. You have now selected the database you want to add to.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_freezer_right_menu.png)

> * Click on the Import tab. The tab will look like the picture below.
>> ![Folder stucture of MAMP](img/MAMP/phpmyadmin_import.png)

> * Now you need to upload the SQL for the freezer database.  The file is called freezer.sql.  This will be found in the turbostorage folder you downloaded from the top of the page which should now be in the htdocs folder.  To add this file to your database click choose file in phpmyadmin and navigate to the freezer.sql file. Then press Go to add the sql.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_import_2.png)

> * Now all of the tables for the Turbo Storage Database are made.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_tables_added.png)

> * Next we need to either add data to this database if this is a reinstall of a database or go on to setting up PHP.

# Set up PHP

> * Go back to the original MAMP window.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_webstart_page.png)

> * Click on the Preferences tab.

> * Click the PHP tab.

> * Set Standard Version to something which starts with a 5.  We need to use PHP 5. Push ok.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_php5.png)

> * Now click Open WebStart page and click on phpinfo. Check to make sure the version is correct.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_php_check.png)

> * Ok next up add an admin user to Turbo Storage.

# Add an admin user to Turbo Storage

> * Now you can access Turbo Storage. Type localhost:8888 in your internet browser.  
>> ![Folder stucture of MAMP](img/MAMP/MAMP_localhost_8888.png)

> * If this does not work Make sure your server is on.  If it still doesn't work check your apache configuration in the phpinfo page.  Search for Hostname:Port.

> * Click on the turbostorage link.

> * Now you Turbo Storage should be working.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_turbostorage_home.png)

> * click on the Create New User button. Fill out the new user form and click create new user button.  This will give you just general access to the program.  Now we need to give at least one user admin rights.  Go back to phpmyadmin. Click on the freezer tab on the left.  all the tables will be visable.  Click on the userTable and the Browse Tab.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_admin_rights.png)

> * Under this tab you will see the user you just added.  Now take reference to what this users userID is.  Mine is 10.

> * Now Go to roleTable and find out the roleID for Admin.  Click on the roleTable on the left menu section.  Click on Browse.  Then find Admin and the roleID.  It should be 5.

> * Next add admin rights for your User.  Click on the userRoleTable on the left menu section. Click on the Insert tab. Fill out the role ID of 5, userID of 10, and createrUserID of 10. Then push Go.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_userRoleTable_insert.png)

> * Click on the Browse tab to see the new record.

> * Now you can go back to turbostorage and log in. localhost:8888/turbostorage

> * You will see now that the left hand most part of the menu bar has an admin button.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_new_user_admin_rights.png)

> * Continue to the Admin section if you would like to change page access rights for any user.

# Change page access rights

> * Just click on the check box to give access rights.  IMPORTANT! do not uncheck the admin button otherwise you will not be aloud back into this page.

# Add database data for a reinstall

> * Get your data export from sql

> * Ok next up is Set up PHP

# Start MAMP server

> * Navigate to MAMP in your Applications Folder.

> * Once MAMP is installed in you Mac's Applications Folder navigate to the MAMP folder.

>> ![Folder stucture of MAMP](img/MAMP/MAMP_Folders.png)

> * Once the turbostorage folder is placed inside the htdocs folder double click on the MAMP.app icon
>> ![Folder stucture of MAMP](img/MAMP/MAMP_Folders.png)

> * This window will appear. Press Start Servers.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_server_off.png)

> * Once your server is runing the button will turn green.  Only push this green button to turn the server off.  Do not close window otherwise your server will be turned off.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_server_on.png)

> * Now press the Open WebStart Page.  This will open in your default internet browser. This is how you interact with your server, database, and php.
>> ![Folder stucture of MAMP](img/MAMP/MAMP_webstart_page.png)


# Accessing from other computers on local network

> * Figure out your ip address.  Go to System Preferences and Network.  You will need Manually configure your ip address.  

> * My Ip is 10.0.1.162

> * So just type 10.0.1.162:8888 into the internet browser.

> * Now you will need to change your energy saving profile in system Preferences.  This is only if you are going to access turbo storage of another computer on your local network.  Make sure you put the computer Sleep to Never.  Also make sure put hard disks to sleep when possible is not checked.  
>> ![Folder stucture of MAMP](img/MAMP/MAMP_engery_saving.png)

# How to Backup Turbo Storage

# How to use your Time Machine Back up of Turbo Storage

# How to Export the data in your Turbo Storage database
