
# Install Requirements

> * My install instructions are only local hosting of Turbo Storage.
> * To run Turbo storage on a Linux operating System LAMP will need to be installed
> * Linux, Apache, MySQL, PHP 5 (LAMP)

# What is LAMP

> * LAMP stands for a web service stack containing the Linux operating system, the Apache HTTP Server, the MySQL Relational Database Management System (RDBMS), and PHP programming language. Therefore this can only be installed on Linux.

>> ![lamp.png](img/LAMP_Architecture.png =302x227)

# How to Set UP LAMP

> * All the following steps are completed in terminal unless otherwise stated.

> * **STEP 1: Install Apache**

>> Install Apache2

>>>     $ sudo apt-get update
>>>     $ sudo apt-get install apache2

>> Find your IP address mine is: 10.0.1.115

>>>     $ ifconfig eth0 | grep inet | awk '{ print $2 }'
>>>     addr:10.0.1.115

>> Check if Apache2 installed correctly by typing in chrome your ip address example:

>>>     http://10.0.1.115

>> It should look similar to the page below

>> ![default_apache.png](img/default_apache.png =590x490)

> * **STEP 2: Install MySQL**

>> To install MySQL

>>>     $ sudo apt-get install mysql-server libapache2-mod-auth-mysql php5-mysql

>> Once you have installed MySQL, we should activate it with this command:

>>>     $ sudo mysql_install_db

>> Terminal should spit out a bunch of stuff like this:

>>>       Installing MySQL system tables...
>>>       160404 14:00:16 [Warning] Using unique option prefix key_buffer instead of key_buffer_size is deprecated and will be removed in a future release. Please use the full name instead.
>>>       160404 14:00:16 [Note] /usr/sbin/mysqld (mysqld 5.5.46-0ubuntu0.14.04.2) starting as process 5415 ...
>>>       OK
>>>       Filling help tables...
>>>       160404 14:00:17 [Warning] Using unique option prefix key_buffer instead of key_buffer_size is deprecated and will be removed in a future release. Please use the full name instead.
>>>       160404 14:00:17 [Note] /usr/sbin/mysqld (mysqld 5.5.46-0ubuntu0.14.04.2) starting as process 5421 ...
>>>       OK

>>>       To start mysqld at boot time you have to copy
>>>       support-files/mysql.server to the right place for your system

>>>       PLEASE REMEMBER TO SET A PASSWORD FOR THE MySQL root USER !
>>>       To do so, start the server, then issue the following commands:

>>>       /usr/bin/mysqladmin -u root password 'new-password'
>>>       /usr/bin/mysqladmin -u root -h samantha-All-Series password 'new-password'

>>>       Alternatively you can run:
>>>       /usr/bin/mysql_secure_installation

>>>       which will also give you the option of removing the test
>>>       databases and anonymous user created by default.  This is
>>>       strongly recommended for production servers.

>>>       See the manual for more instructions.

>>>       You can start the MySQL daemon with:
>>>       cd /usr ; /usr/bin/mysqld_safe &

>>>       You can test the MySQL daemon with mysql-test-run.pl
>>>       cd /usr/mysql-test ; perl mysql-test-run.pl

>>>       Please report any problems at http://bugs.mysql.com/

>> Finish up by running the MySQL set up script:

>>>     $ sudo /usr/bin/mysql_secure_installation

>> This should also result in in a lot of things printed in terminal.  There will also be some questions.  You can see how I answered mine below.

>>> * Username and password information.

>>>> * If you **DO NOT** know PHP please follow the following criteria for username and password

>>>>> * User name set equal to root
>>>>> * Password
>>>>>> * If the host name of the computer is samantha-All-Series password should be Smt@wylab
>>>>>> * Otherwise password should be root

>>>> * If you do know PHP the username and password can be set for the PHP database connection in the config.php file.

>>>       ###### root password Smt@wylab

>>>       NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MySQL
>>>       >>>        SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

>>>       In order to log into MySQL to secure it, we'll need the current
>>>       password for the root user.  If you've just installed MySQL, and
>>>       you haven't set the root password yet, the password will be blank,
>>>       so you should just press enter here.

>>>       Enter current password for root (enter for none):
>>>       ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: NO)
>>>       Enter current password for root (enter for none):
>>>       OK, successfully used password, moving on...

>>>       Setting the root password ensures that nobody can log into the MySQL
>>>       root user without the proper authorization.

>>>       You already have a root password set, so you can safely answer 'n'.

>>>       Change the root password? [Y/n] n
>>>        ... skipping.

>>>       By default, a MySQL installation has an anonymous user, allowing anyone
>>>       to log into MySQL without having to have a user account created for
>>>       them.  This is intended only for testing, and to make the installation
>>>       go a bit smoother.  You should remove them before moving into a
>>>       production environment.

>>>       Remove anonymous users? [Y/n] Y
>>>        ... Success!

>>>       Normally, root should only be allowed to connect from 'localhost'.  This
>>>       ensures that someone cannot guess at the root password from the network.

>>>       Disallow root login remotely? [Y/n] n
>>>        ... skipping.

>>>       By default, MySQL comes with a database named 'test' that anyone can
>>>       access.  This is also intended only for testing, and should be removed
>>>       before moving into a production environment.

>>>       Remove test database and access to it? [Y/n] n
>>>        ... skipping.

>>>       Reloading the privilege tables will ensure that all changes made so far
>>>       will take effect immediately.

>>>       Reload privilege tables now? [Y/n] Y
>>>        ... Success!

>>>       Cleaning up...

>>>       All done!  If you've completed all of the above steps, your MySQL
>>>       installation should now be secure.

>>>       Thanks for using MySQL!

> * **STEP 3: Install PHP 5**

>> * To Install PHP

>>>      $ sudo apt-get install php5 libapache2-mod-php5 php5-mcrypt

>> * It may also be useful to add php to the directory index, to serve the relevant php index files:

>>>      $ sudo gedit /etc/apache2/mods-enabled/dir.conf

>>>> You might need to install gedit to run the command above this is how you install it:

>>>>      $ sudo apt-get install gedit

>> * Once the dir.conf file opens in gedit it will hopefully look like this:

>>> ![dir conf file](img/dir_conf_file.png)

> * **STEP 4: See PHP on Your Server**

>> * Although LAMP is installed, we can still take a look and see the components online by creating a quick php info page

>> * To set this up, first create a new file:

>>>>      $ sudo gedit /var/www/info.php

>> * Add in the following lines of code to the file:

>>>>      &lt;?php
>>>>           phpinfo();
>>>>      ?&gt;

>> * Save the file.

>> * Restart apache so all the changes take effect:

>>>>      $ sudo service apache2 restart

>> * Run your new file info.php on your server to check if PHP is installed

>>>>      In your internet browswer go to localhost/info.php or http://127.0.1.1/info.php

>> * If you installed everything correctly this page should look similar to the image below.

>>> ![php info](img/check_php_installed.png)

> * **STEP 5: Install phpmyadmin**

>>>>      $ sudo apt-get install phpmyadmin apache2-utils

>> * During the installation, phpMyAdmin will walk you through a basic configuration. Once the process starts up, follow these steps:

>>>>      Select Apache2 for the server
>>>>      Choose YES when asked about whether to Configure the database for phpmyadmin with dbconfig-common
>>>>      Enter your MySQL password when prompted
>>>>      Enter the password that you want to use to log into phpmyadmin
>>>>      After the installation has completed, add phpmyadmin to the apache configuration.

>> * Open apache2 config file with gedit.  You might need to install gedit first with the following command:  $ sudo apt-get install gedit

>>>>      $ sudo gedit /etc/apache2/apache2.conf

>> * Add the phpmyadmin config to the file entire line below

>>>>      Include /etc/phpmyadmin/apache.conf

# How to Install Turbo Storage?

> * [Download Turbo Storage here](https://bitbucket.org/taffners/turbostorage/downloads "Turbo Storage Repository")

> * Once downloaded unzip the folder

> * **VERY VERY IMPORTANT DO NOT SKIP THIS STEP - rename the outside folder to turbostorage.**
>> * After downloading this folder the name should be similar to: taffners-turbostorage-0c5fc58a81f9.  
>> * Make sure the name is changed to turbostorage.
> * Next install a server, database, and PHP 5.  There are two options which I will outline here.  I've thoroughly tested Turbo Storage on LAMP. I've only minimally tested on MAMP.  I do not have a windows computer so I have not tested a server set up at all on windows.
