
<fieldset id="turbo-install-mac-database">
     <legend class="show">
          Install Turbo Storage Database
     </legend>
     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <ul>
               <li class="show_turbostorage_install_mac hide"><a href="?page=turbostorage_install_mac#start-mamp-server" alt="start mamp server">Start Server</a></li>

               <li class="show_turbostorage_install_mac hide"><a href="?page=turbostorage_install_mac#open-webstart-page" alt="open webstart page">Open WebStart Page </a></li>

               <li id="open-phpmyadmin" class="show_turbostorage_install_mac hide">Go to phpMyAdmin on the webstart page. (<a href="https://en.wikipedia.org/wiki/PhpMyAdmin" alt="phpMyAdmin wiki" target="_blank">what is phpMyAdmp</a>)</li>

               <li class="list-unstyled show_turbostorage_install_mac hide"><img src="img/MAMP/MAMP_webstart_page.png" alt="webstart page"></li>

               <li class="show_turbostorage_install_linux hide"><a href="?page=turbostorage_LAMP_how_to#LAMP-turn-on-server">Start LAMP Server</a></li>

               <li class="show_turbostorage_install_linux hide">In your internet browser type IP address followed by phpmyadmin example: 10.0.1.115/phpmyadmin/
                    <ul>
                         <li class="nav-buttons"><a href="?page=turbostorage_LAMP_how_to#LAMP-access-via-local-network">find IP address</a></li>
                    </ul>
               </li>

               <li class="show_turbostorage_install_linux hide">
                    Login (based on your MySQL password settings from step 2 above)
               </li>

               <li>phpmyadmin home page should now be open.  This is how you access your database directly.  Avoid accessing this much since you can delete some important things here.</li>
               <li class="list-unstyled"><img src="img/MAMP/phpmyadmin_home.png" alt="phpMyAdmin home page"></li>

               <li>Click on the Databases tab. The tab will look like the picture below.</li>
               <li class="list-unstyled"><img src="img/MAMP/MAMP_database.png" alt="MAMP Database"></li>
               <li>Type in the create database field the database name for Turbo Storage which is <b>freezer</b>. Keep the drop down menu as Collation.  Press Create.  A new section should now be added to the left panel called freezer.</li>
               <li class="list-unstyled"><img src="img/MAMP/MAMP_freezer_right_menu.png" alt="freezer left menu"></li>
               <li>Now lets add something to this newly made freezer database.  Click on the freezer section on the left menu database section. You have now selected the database you want to add to.</li>
               <li class="list-unstyled"><img src="img/MAMP/MAMP_freezer_right_menu.png" alt="freezer left menu"></li>
               <li>Click on the Import tab. The tab will look like the picture below.</li>
               <li class="list-unstyled"><img src="img/MAMP/phpmyadmin_import.png" alt="phpmyadmin import"></li>
               <li>Now you need to upload the SQL for the freezer database.  For fresh install of Turbo Storage choose the file called freezer.sql  For reinstalls choose the file called freezer-reinstall.sql. These files are  located in the turbostorage folder.  This should now be in the htdocs folder.  To add this file to your database click choose file in phpmyadmin and navigate to the freezer.sql file. Then press Go to add the sql.</li>
               <li>Now all of the tables for the Turbo Storage Database are made.</li>
               <li class="list-unstyled"><img src="img/MAMP/MAMP_tables_added.png" alt="turbostorage tables adde          <fieldset id="turbo-install-mac-database">d to database"></li>
               <li>Next there are two options depending on if this is a fresh install or a reinstall.
                    <ul>
                         <li>Fresh install --> <a href="?page=turbostorage_install_mac#turbo-install-mac-add-admin" >add an admin user to Turbo Storage.</a> </li>
                         <li class="show_turbostorage_install_mac hide">Reinstall --> <a href="?page=turbostorage_install_mac#turbo-add-data" >add data to database</a></li>
                         <li class="show_turbostorage_install_linux hide">Reinstall --> <a href="?page=turbostorage_install_linux#turbo-add-data" >add data to database</a></li>
                    </ul>
               </li>
          </ul>
     </div>
</fieldset>

<fieldset id="turbo-install-mac-add-admin">
     <legend class="show">
          Add an admin user to Turbo Storage
     </legend>
     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <ul>
               <li class="show_turbostorage_install_mac hide">Now you can access Turbo Storage. Type localhost:8888 in your internet browser.  </li>
               <li class="list-unstyled show_turbostorage_install_mac hide"><img src="img/MAMP/MAMP_localhost_8888.png"></li>
               <li class="show_turbostorage_install_mac hide">If this does not work make sure your server is on.  If it still doesn't work check your apache configuration in the phpinfo page.  Search for Hostname:Port.</li>
               <li class="show_turbostorage_install_linux hide">
                    <a href="?page=turbostorage_LAMP_how_to#LAMP-access-via-local-network">Access LAMP via Local Network</a>
               </li>
               <li>Click on the turbostorage link.</li>
               <li>Now your Turbo Storage should be working.</li>
               <li class="list-unstyled"><img src="img/MAMP/MAMP_turbostorage_home.png"></li>
               <li>Click on the Create New User button. Fill out the new user form and click create new user button.  This will give you just general access to the program.  Now we need to give at least one user admin rights.  Go back to phpmyadmin. Click on the freezer tab on the left.  all the tables will be visable.  Click on the userTable and the Browse Tab.</li>
               <li class="list-unstyled"><img src="img/MAMP/MAMP_admin_rights.png" ></li>
               <li>Under this tab you will see the user you just added.  Now take reference to what this users userID is.  Mine is 10.</li>
               <li>Now Go to roleTable and find out the roleID for Admin.  Click on the roleTable on the left menu section.  Click on Browse.  Then find Admin and the roleID.  It should be 5.</li>
               <li>Next add admin rights for your User.  Click on the userRoleTable on the left menu section. Click on the Insert tab. Fill out the role ID of 5, userID of 10, and createrUserID of 10. Then push Go.</li>
               <li class="list-unstyled"><img src="img/MAMP/MAMP_userRoleTable_insert.png" ></li>
               <li>Click on the Browse tab to see the new record.</li>
               <li>
                    Now you can go back to turbostorage and log in.
                    <span class="show_turbostorage_install_mac hide">localhost:8888/turbostorage</span>
                    <span class="show_turbostorage_install_linux hide">ip/turbostorage</span>
               </li>
               <li>You will see now that the left hand most part of the menu bar has an admin button.</li>
               <li class="list-unstyled"><img src="img/MAMP/MAMP_new_user_admin_rights.png"></li>
               <li>Next up <a href="?page=turbostorage_how_to#turbo-install-change-access" >Change User Access Rights</a></li>
          </ul>
     </div>
</fieldset>

<fieldset id="turbo-add-data">
     <legend class="show">
          Add Data to Fresh Install.
     </legend>
     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <ul>
               <li>Make sure there is a fresh install of the freezer database. If this is not a fresh install backup data and  delete the freezer database and reinstall freezer sql.
                    <ul>
                         <li><a href="?page=turbostorage_how_to#turbostorage-delete-db" alt="delete freezer database">How to delete freezer database and reinstall</a>
                    </ul>
               </li>

               <li>After you have a fresh install of the freezer database follow these steps:</li>
               <ul>
                    <li>Click on the freezer db in the left hand panel</li>
                    <li>Click on the import tab</li>
                    <li class="list-unstyled"><img src="img/MAMP/phpmyadmin_import_data.png" alt="phpMyAdmin How to Import sql data"></li>
                    <li>Make sure the top of the page says "Importing into the database freezer"</li>
                    <li>Navigate to your data export in choose file and press go <a href="?page=turbostorage_how_to#MAMP-backup-MAMP-fieldset">You should have completed the Download SQL from phpMyAdmin here</a></li>
                    <li>Now all of your tables should contain data</li>
               </ul>

          </ul>
     </div>
</fieldset>
