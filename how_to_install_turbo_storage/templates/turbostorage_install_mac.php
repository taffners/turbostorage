<div class="container-fluid" id="turbostorage-mamp-install-container">
     <div class="row">
          <div id="mamp-install-nav" class="hidden-xs col-sm-2 col-md-2">
               <div class="affix">
                    <ul class="list-unstyled">
                         <li class="nav-buttons">
                              <a class="btn btn-primary" href="#TopName" role="button">Top Page</a>
                         </li>

                         <li class="nav-buttons">
                              <a href="?page=turbostorage" class="btn btn-primary">About Turbo Storage</a>
                         </li>

                         <li class="nav-buttons"><a href="?page=turbostorage_install_mac#turbo-install-mac-requirements" class="btn btn-primary" alt="install requirements">Install Requirements</a></li>

                         <li class="nav-buttons"><a href="?page=turbostorage_install_mac#turbo-install-mac-what-is-mamp" class="btn btn-primary">What is MAMP</a></li>

                         <li class="nav-buttons"><a href="?page=turbostorage_install_mac#turbo-install-mac-setup" class="btn btn-primary">Setup MAMP</a></li>

                         <li class="nav-buttons"><a href="?page=turbostorage_install_mac#turbo-install-mac-set-php" class="btn btn-primary">Set up PHP5</a></li>

                         <li class="nav-buttons"><a href="?page=turbostorage_install_mac#turbo-install-mac-database" class="btn btn-primary">Setup Turbo Storage Database</a></li>

                         <li class="nav-buttons"><a href="?page=turbostorage_install_mac#turbo-install-mac-add-admin" class="btn btn-primary">Add Turbo Storage Admin User</a></li>

                         <li class="nav-buttons"><a href="?page=turbostorage_install_mac#turbo-add-data" class="btn btn-primary">Add Data (for reinstall)</a></li>

                         <li class="nav-buttons"><a href="?page=turbostorage_MAMP_how_to" class="btn btn-primary">MAMP How To's After Install</a></li>
                    </ul>
               </div>
          </div>
          <div class="col-xs-12 col-sm-10 col-md-10">
               <?php
                    require_once('templates/turbostorage_download_without_data.php');
               ?>

               <fieldset id="turbo-install-mac-requirements">
                    <legend class="show">
                         Turbo Storage Install Requirements on Mac<img src="img/os_icons/mac.png" alt="Mac icon" height="50px">
                    </legend>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <ul>
                              <li>My install instructions are only local hosting of Turbo Storage.  </li>
                              <li>MAMP can be used for an easy way to install the required environment for Turbo Storage.</li>
                              <li>MAMP, Apache, MySQL, PHP 5 (MAMP)</li>
                         </ul>
                    </div>
               </fieldset>

               <fieldset id="turbo-install-mac-what-is-mamp">
                    <legend class="show">
                         What is MAMP<img src="img/MAMP/MAMP_ICON.png" alt="MAMP Icon" height="50px">
                    </legend>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <ul>
                              <li>The easiest way to set up Turbo Storage is using a program called MAMP.<a href="https://www.mamp.info/en/"  alt="MAMP website" target="_blank"> MAMP's Website</a></li>
                              <li>MAMP only works on macOS and Windows.</li>
                              <li>According to the MAMP website: "MAMP installs a local server environment in a matter of seconds on your computer. It comes free of charge, and is easily installed. MAMP will not compromise any existing Apache installation already running on your system. You can install Apache, PHP and MySQL without starting a script or having to change any configuration files! Furthermore, if MAMP is no longer needed, just delete the MAMP folder and everything returns to its original state (i.e. MAMP does not modify any of the normal system)."</li>
                              <li><a href="https://www.lynda.com/WordPress-tutorials/Installing-Running-WordPress-MAMP/361682-2.html" alt="Video tutorial on what MAMP is" target="_blank">  This video is a good explanation of what MAMP is but ignore all the Word Press talk. I did not use Word Press for Turbo Storage.</a></li>
                         </ul>
                    </div>
               </fieldset>

               <fieldset id="turbo-install-mac-setup">
                    <legend class="show">
                         How do you set up MAMP on MacOSX
                    </legend>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <ul>
                              <li>I have never installed MAMP on windows therefore I will not discuss windows set up.  I imagine it is similar to MacOSX.</li>
                              <li><a href="https://www.mamp.info/en/" alt="Download MAMP from this site" target="_blank">Download MAMP</a></li>
                              <li>Once MAMP is installed in you Mac's Applications Folder navigate to the MAMP folder.</li>
                              <li>Place the turbostorage folder inside the htdocs folder. <a href="?page=turbostorage_install_mac#turbostorage-download" alt="download turbostorage">(If you haven't downloaded Turbo Storage it can be downloaded at the top of the page)</a></li>
                              <li class="list-unstyled"><img src="img/MAMP/MAMP_Folders.png" alt="Folder stucture of MAMP"></li>
                              <li>Once the turbostorage folder is placed inside the htdocs folder double click on the MAMP.app icon.</li>
                              <li class="list-unstyled"><img src="img/MAMP/MAMP_Folders.png" alt="MAMP APP in Folder structure"></li>
                              <li id="start-mamp-server">This window will appear. Press Start Servers.</li>
                              <li class="list-unstyled"><img src="img/MAMP/MAMP_server_off.png" alt="MAMP Control Panel server off"></li>
                              <li>Once your server is running the button will turn green.  Only push this green button to turn the server off.  Do not close window otherwise your server will be turned off.</li>
                              <li class="list-unstyled"><img src="img/MAMP/MAMP_server_on.png" alt="MAMP Control Panel server on"></li>
                              <li id="open-webstart-page">Now press the Open WebStart Page.  This will open in your default internet browser. This is how you interact with your server, database, and php.</li>
                              <li class="list-unstyled"><img src="img/MAMP/MAMP_webstart_page.png" alt="MAMP webstart"></li>
                              <li><a href="?page=turbostorage_install_mac#turbo-install-mac-set-php" >Change PHP version to PHP5.</a></li>
                         </ul>
                    </div>
               </fieldset>

               <fieldset id="turbo-install-mac-set-php">
                    <legend class="show">
                         Change PHP version to PHP5
                    </legend>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <ul>
                              <li>Go back to the original MAMP window.</li>
                              <li class="list-unstyled"><img src="img/MAMP/MAMP_webstart_page.png" alt="web start"></li>
                              <li>Click on the Preferences tab.</li>
                              <li>Click the PHP tab.</li>
                              <li>Set Standard Version to something which starts with a 5.  We need to use PHP 5. Push ok.</li>
                              <li class="list-unstyled"><img src="img/MAMP/MAMP_php5.png" alt="php5"></li>
                              <li>Now click Open WebStart page and click on phpinfo. Check to make sure the version is correct.</li>
                              <li class="list-unstyled"><img src="img/MAMP/MAMP_php_check.png" alt="check php version"></li>
                              <li>Now MAMP is set up and running.  Lets <a href="?page=turbostorage_install_mac#turbo-install-mac-database" alt="install turbostorage database">install the Turbo Storage Database next.</a></li>
                         </ul>
                    </div>
               </fieldset>
               <?php
                    require_once('templates/turbostorage_install_db.php');
               ?>
          </div>
     </div>
</div>
