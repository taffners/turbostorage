<html>
	<head>
		<title><?= SITE_TITLE; ?></title>
		<link rel="stylesheet" type="text/css" href="css/libraries.min.css">
		<link rel='stylesheet' href='css/style.css'>
		<link rel="icon" href="img/DNA.ico">

		<meta charset="UTF-8">

	</head>
	<header>
		<div class='container-fluid' id='TopName'><?= SITE_TITLE; ?></div>
	</header>

	<main>
		<?php
			require_once('templates/'.$page.'.php');
		?>
	</main>
	<footer>

		<script type="text/javascript" src="js/libraries/libraries.js?ver=<?= SAMANTHAS_PROTOCOLS_VER; ?>"></script>

		<script data-main="js/main" src="js/libraries/require.js?ver=<?= SAMANTHAS_PROTOCOLS_VER; ?>"></script>


	</footer>
