<div class="container-fluid" id="turbostorage-how-to-container">
     <div class="row">
          <div id="mamp-install-nav" class="hidden-xs col-sm-2 col-md-2">
               <div class="affix">
                    <ul class="list-unstyled">
                         <li class="nav-buttons"><a class="btn btn-primary" href="#TopName" role="button">Top Page</a></li>
                         <li class="nav-buttons"><a href="?page=turbostorage" class="btn btn-primary">About Turbo Storage</a></li>
                         <li class="nav-buttons"><a href="?page=turbostorage_how_to#turbo-install-change-access" class="btn btn-primary">Change User Access Rights</a></li>
                         <li class="nav-buttons"><a href="?page=turbostorage_how_to#MAMP-backup-MAMP-fieldset" class="btn btn-primary">Backup Turbo Storage</a></li>
                    </ul>
               </div>
          </div>
          <div class="col-xs-12 col-sm-10 col-md-10">
               <fieldset id="turbo-install-change-access">
                    <legend class="show">
                         Change User Access Rights
                    </legend>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <ul>
                              <li>Go to your install of Turbo Storage</li>
                              <li>Click on the Admin button in the upper right hand corner of the tool bar (this will only be present if you have admin rights)</li>
                              <li class="list-unstyled"><img src="img/admin_button.png"></li>
                              <li>The admin page will look similar to this:</li>
                              <li class="list-unstyled"><img src="img/turbostorage_access_rights.png"></li>
                              <li>Just click on the check box corresponding to the user and the rights you would like to give access rights.  <b>IMPORTANT! do not uncheck the admin button</b> otherwise you will not be aloud back into this page.</li>

                         </ul>
                    </div>
               </fieldset>
               <fieldset id="MAMP-backup-MAMP-fieldset">
                    <legend class="show">
                         How to Backup Turbo Storage
                    </legend>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow:hidden">
                         <ul>
                              <li>2 Different Methods to back up
                                   <ul>
                                        <li>Set up Time Machine (if using a Mac)</li>
                                        <li>Download SQL from PHP myadmi     <li>Go to phpmyadmin</li>
                                                  <li>Click on freezer database on the left panel. Click on Export tab in the right panel. Shturbostorage_how_toown below.</li>
                                                  <li class="list-unstyled"><img src="img/MAMP/MAMP_export_2.png" alt="Export data from phpmyadmin"></li>

                                                  <li>Click on freezer database on the left panel.</li>
                                                  <li>Click on Export tab in the right panel.</li>
                                                  <li>Click on Custom - display all possible options under Export Methods.</li>
                                                  <li>Click on Save output to file under the Output section.</li>
                                                  <li>Add freezer+%Y_%m_%d_%H_%M_%S to file name template.</li>
                                                  <li>All Shown below.</li>
                                                  <li>Press go once all these steps are followed.</li>
                                                  <li>Save this file in an external hard drive</li>

                                                  <li class="list-unstyled"><img src="img/MAMP/MAMP_export_1.png" alt="Export data from phpmyadmin" style="width:100%;"></li>
                                                  <li class="list-unstyled"><img src="img/MAMP/MAMP_export_3.png" alt="Export data from phpmyadmin" style="width:100%;"></li>
                                                  <li class="list-unstyled"><img src="img/MAMP/MAMP_export_4.png" alt="Export data from phpmyadmin" style="width:100%;"></li>

                                             </ol>
                                        </li>
                                   </ul>
                              </li>
                         </ul>
                    </div>
               </fieldset>

               <fieldset id="turbostorage-delete-db">
                    <legend class="show">
                         How to Delete Freezer database for reinstall
                    </legend>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow:hidden">
                         <ul>
                              <li><a href="?page=turbostorage_how_to#MAMP-backup-MAMP-fieldset ">Back up Data link</a></li>
                              <li class="show_turbostorage_install_mac hide"><a href="?page=turbostorage_install_mac#turbo-install-mac-database">Go to phpMyAdmin</a></li>
                              <li>Click on the phpMyAdmin logo in the left panel. Pictured below.</li>
                              <li class="list-unstyled"><img src="img/MAMP/phpmyadmin_logo.png"></li>
                              <li>Then Click on the Databases tab</li>
                              <li>Then Click on the check box next to freezer</li>
                              <li><b>Make sure before completing this step you backed up data (step 1)</b> Push The drop button</li>
                              <li class="list-unstyled"><img src="img/phpmyadmin_delete_db.png"></li>
                              <li><a href="?page=turbostorage_install_mac#turbostorage-mamp-install-container">Install Turbo Storage Database in MAMP </a></li>
                              <li><a href="?page=turbostorage_install_linux">Install Turbo Storage Database in LAMP</a></li>
                         </ul>
                    </div>
               </fieldset>






          </div>
     </div>
</div>
