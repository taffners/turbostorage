<div class="container-fluid" id="turbostorage_mamp_ho_to">
     <div class="row" >
          <div id="mamp-how-to-nav" class="hidden-xs col-sm-2 col-md-2">
               <div class="affix">
                    <ul class="list-unstyled">
                         <li class="nav-buttons"><a href="?page=turbostorage" class="btn btn-primary">About Turbo Storage</a></li>
                         <li class="nav-buttons"><a href="?page=turbostorage_install_mac" class="btn btn-primary">Install MAMP <img src="img/MAMP/MAMP_ICON.png" alt="MAMP Icon" height="50px"></a></li>
                         <li class="nav-buttons"><a href="?page=turbostorage_MAMP_how_to#MAMP-turn-on-server-fieldset" class="btn btn-primary">Start MAMP Server</a></li>
                         <li class="nav-buttons"><a href="?page=turbostorage_MAMP_how_to#MAMP-access-via-local-network-fieldset" class="btn btn-primary">Access MAMP via Local Network</a></li>
                         <li class="nav-buttons"><a href="?page=turbostorage_how_to#MAMP-backup-MAMP-fieldset" class="btn btn-primary">Backup Turbo Storage</a></li>
                    </ul>
               </div>
          </div>
          <div class="col-xs-12 col-sm-10 col-md-10">
               <fieldset id="MAMP-turn-on-server-fieldset">
                    <legend class="show">
                         Start MAMP Server<img src="img/MAMP/MAMP_ICON.png" alt="MAMP Icon" height="50px">
                    </legend>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <ul>
                              <li>Navigate to MAMP in your applications folder</li>
                              <li class="list-unstyled"><img src="img/MAMP/MAMP_Folders.png" alt="Folder structure of MAMP"></li>
                              <li>Double Click on MAMP.app The window below will appear.  Press Start Servers.</li>
                              <li class="list-unstyled"><img src="img/MAMP/MAMP_server_off.png" alt="MAMP server off window"></li>
                              <li>Once your server is running the button will turn green.  Only push this green button to turn the server off. Do not close window otherwise your server will be turned off.</li>
                              <li class="list-unstyled"><img src="img/MAMP/MAMP_server_on.png" alt="MAMP server on window"></li>
                         </ul>

                    </div>
               </fieldset>
               <fieldset id="MAMP-access-via-local-network-fieldset">
                    <legend class="show">
                         Access MAMP via Local Network
                    </legend>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <ul>
                              <li>Figure out your IP address.  Go to System Preferences --> Network.  You will need to manually configure your IP address.</li>
                              <li>My IP is 10.0.1.162</li>
                              <li>To access this computer from another computer on my local network just type 10.0.1.162:8888 into your internet browswer.</li>
                              <li>Now you will need to change your energy saving profile in system Preferences.  This is only if you are going to access turbo storage from another computer on your local network.
                                   <ul>
                                        <li>Computer Sleep: --> Never</li>
                                        <li>Unclick put hard disks to sleep when possible</li>
                                        <li> Unclick Wake for Network access</li>
                                   </ul>
                              </li>
                              <li>Here are my settings</li>
                              <li class="list-unstyled"><img src="img/MAMP/MAMP_engery_saving.png" alt="MAMP Energy Saving settings"></li>
                         </ul>
                    </div>
               </fieldset>

          </div>
     </div>
</div>
