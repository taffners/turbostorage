<div class="container-fluid">
     <div class="row" >
          <div id="turbostorage-install-nav" class="hidden-xs col-sm-2 col-md-2">
               <div class="affix">
                    <ul class="list-unstyled">
                         <li class="nav-buttons">
                              <a class="btn btn-primary" href="#TopName" role="button">Top Page</a>
                         </li>
                         <li class="nav-buttons">
                              <a href="?page=turbostorage" class="btn btn-primary">About Turbo Storage</a>
                         </li>
                         <li class="nav-buttons">
                              <a href="?page=turbostorage_install_linux#turbostorage-linux-reqs" class="btn btn-primary">Install Requirements</a>
                         </li>
                         <li class="nav-buttons">
                              <a href="?page=turbostorage_install_linux#turbostorage-linux-lamp" class="btn btn-primary">What is Lamp</a>
                         </li>
                         <li class="nav-buttons">
                              <a href="?page=turbostorage_install_linux#turbostorage-linux-lamp-setup" class="btn btn-primary">Setup LAMP</a>
                         </li>
                         <li class="nav-buttons"><a href="?page=turbostorage_install_linux#turbo-install-mac-database" class="btn btn-primary">Setup Turbo Storage Database</a></li>

                         <li class="nav-buttons"><a href="?page=turbostorage_install_linux#turbo-install-mac-add-admin" class="btn btn-primary">Add Turbo Storage Admin User</a></li>

                         <li class="nav-buttons"><a href="?page=turbostorage_install_linux#turbo-add-data" class="btn btn-primary">Add Data (for reinstall)</a></li>

                         <li class="nav-buttons"><a href="?page=turbostorage_LAMP_how_to" class="btn btn-primary">LAMP How To's After Install</a></li>
                    </ul>
               </div>
          </div>
          <div class="col-xs-12 col-sm-10 col-md-10">
               <?php
                    require_once('templates/turbostorage_download_without_data.php');
               ?>
               <fieldset id="turbostorage-linux-reqs">
                    <legend class="show">
                         Install Requirements <img src="img/os_icons/linux.png" alt="linux penguin icon" height="50px">
                    </legend>

                    <ul>
                         <li>My install instructions are only local hosting of Turbo Storage.</li>
                         <li>To run Turbo storage on a Linux operating System LAMP will need to be installed</li>
                         <li>Linux, Apache, MySQL, PHP 5 (LAMP)</li>
                    </ul>
               </fieldset>
               <fieldset id="turbostorage-linux-lamp">
                    <legend class="show">
                         What is LAMP
                    </legend>

                    <ul>
                         <li>LAMP stands for a web service stack containing the Linux operating system, the Apache HTTP Server, the MySQL Relational Database Management System (RDBMS), and PHP programming language. Therefore this can only be installed on Linux.</li>
                         <li class="list-unstyled"><img src="img/LAMP_Architecture.png" alt="Diagram of LAMP architecture" height="227" width="302"></li>

                    </ul>
               </fieldset>
               <fieldset id="turbostorage-linux-lamp-setup">
                    <legend class="show">
                         How to Set up LAMP<img src="img/os_icons/linux.png" alt="linux penguin icon" height="50px">
                    </legend>

                    <div id="turbostorage-linux-html"></div>
                    <div class="hide" class="markdown-convert" id="turbostorage-linux-md">
                         <?php
                              require_once('markdown/turbostorage_linux_install.md');
                         ?>
                    </div>
               </fieldset>
               <?php
                    require_once('templates/turbostorage_install_db.php');
               ?>
          </div>
     </div>
</div>
