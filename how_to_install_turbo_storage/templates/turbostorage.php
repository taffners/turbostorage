<div class="container-fluid">
     <div class="row" >
          <div id="turbostorage-install-nav" class="hidden-xs col-sm-2 col-md-2">
               <div class="affix">
                    <ul class="list-unstyled">
                         <li>Install</li>

                         <li  class="nav-buttons">
                              <a href="?page=turbostorage_install_linux" class="btn btn-primary">Install on Linux
                                   <img src="img/os_icons/linux.png" alt="linux penguin icon" height="50px">
                              </a>
                         </li>

                         <li class="nav-buttons"><a href="?page=turbostorage_install_mac" class="btn btn-primary">Install on Mac <img src="img/os_icons/mac.png" alt="Mac Icon" height="50px"></a></li>
                         <li class="nav-buttons"><a href="?page=turbostorage_install_windows" class="btn btn-primary">Install on Windows<img src="img/os_icons/windows.png" alt="Windows OS icon" height="50px"></a></li>
                         <li>How To's</li>
                         <li class="nav-buttons"><a href="?page=turbostorage_LAMP_how_to" class="btn btn-primary">LAMP Stack<img src="img/lamp_icon.png" alt="Lamp" height="50px"></a></li>
                         <li class="nav-buttons"><a href="?page=turbostorage_MAMP_how_to" class="btn btn-primary">MAMP Stack<img src="img/MAMP/MAMP_ICON.png" alt="MAMP Icon" height="50px"></a></li>
                         <li class="nav-buttons"><a href="?page=turbostorage_how_to" class="btn btn-primary">General Turbo Storage How To's</a></li>
                    </ul>
               </div>
          </div>
          <div class="col-xs-12 col-sm-10 col-md-10">
               <fieldset id="General-info-fieldset">
                    <legend class="show">
                         Turbo Storage
                    </legend>

                    <div id="turbostorage-html"></div>
                    <div class="hide" id="turbostorage-md">
                         <?php
                              require_once('markdown/turbostorage_README.md');
                         ?>
                    </div>
               </fieldset>
          </div>
     </div>
</div>
