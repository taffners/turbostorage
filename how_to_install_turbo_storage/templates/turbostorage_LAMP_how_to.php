<div class="container-fluid" id="turbostorage_mamp_ho_to">
     <div class="row" >
          <div id="mamp-how-to-nav" class="hidden-xs col-sm-2 col-md-2">
               <div class="affix">
                    <ul class="list-unstyled">
                         <li class="nav-buttons"><a href="?page=turbostorage" class="btn btn-primary">About Turbo Storage</a></li>
                         <li class="nav-buttons"><a href="?page=turbostorage_install_linux" class="btn btn-primary">Install LAMP<img src="img/os_icons/linux.png" alt="linux penguin icon" height="50px"></a></li>
                         <li class="nav-buttons"><a href="?page=turbostorage_LAMP_how_to#LAMP-turn-on-server" class="btn btn-primary">Start LAMP Server</a></li>
                         <li class="nav-buttons"><a href="?page=turbostorage_LAMP_how_to#LAMP-access-via-local-network" class="btn btn-primary">Access LAMP via Local Network</a></li>
                    </ul>
               </div>
          </div>
          <div class="col-xs-12 col-sm-10 col-md-10">
               <fieldset id="LAMP-turn-on-server">
                    <legend class="show">
                         Start LAMP Server<img src="img/os_icons/linux.png" alt="linux penguin icon" height="50px">
                    </legend>

                    <ul>
                         <li>Go to terminal and restart the server type the following</li>
                         <li class="list-unstyled">
                              <span class="terminal">$ </span>
                              <span class="code"> sudo service apache2 restart </span>
                         </li>
                    </ul>

               </fieldset>
               <fieldset id="LAMP-access-via-local-network">
                    <legend class="show">
                         Access LAMP via Local Network
                    </legend>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <ul>
                              <li>Figure out your IP address by typing the following command
                                   <ul>
                                        <li class="list-unstyled">
                                             <span class="terminal">$ </span>
                                             <span class="code"> /sbin/ifconfig eth0| grep 'inet addr:' </span>
                                        </li>
                                        <li class="list-unstyled">
                                             <span class="return_code"> inet addr:10.0.1.115  Bcast:10.0.1.255  Mask:255.255.255.0 </span>
                                        </li>
                                        <li>My IP is 10.0.1.115</li>
                                   </ul>
                              </li>
                              <li>just type the address into your favorite internet browser which is on the local network</li>
                         </ul>
                    </div>
               </fieldset>

          </div>
     </div>
</div>
