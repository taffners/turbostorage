<?php

     function Zip($source, $destination, $overwrite = false)
     {
          // change php size and time restrictions
          ini_set("memory_limit",-1);

          ini_set('max_execution_time', 300); //300 seconds = 5 minutes

          if (!extension_loaded('zip') || !file_exists($source))
          {
               return false;
          }

          $zip = new ZipArchive();

          $zip->open($destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE);


          $source = str_replace('\\', '/', realpath($source));

          if (is_dir($source) === true)
          {
               $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

               // Iterate over all files and folders in directory
               foreach ($files as $file)
               {

                    $file_loc = rtrim(str_replace('\\', '/', $file.PHP_EOL));

                    $file_name = rtrim(substr($file_loc, strrpos($file_loc, '/')+1));

                    // skip "." and ".." folders
                    if ($file_name === '.' || $file_name === '..')
                    {
                         continue;
                    }

                    // Add empty directory
                    if (is_dir($file_loc) === true)
                    {
                         $dir_loc = str_replace($source . '/', '', $file_loc . '/');

                         $zip->addEmptyDir($dir_loc);
                    }
                    else if (is_file($file_loc))
                    {

                         $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                    }

               }
          }
          else if (is_file($source) === true)
          {
               $zip->addFromString(basename($source), file_get_contents($source));
          }

          // close zip
          $zip->close();

          // change php size and time restrictions back
          ini_set("memory_limit",128);
          ini_set('max_execution_time', 30); //300 seconds = 5 minutes

     }

     function _Make_Samanthas_Protocol_Zip()
     {
          $loc_zip_archive = sys_get_temp_dir().'/samanthas_protocols.zip';

          Zip(realpath('..'), $loc_zip_archive, true);

          chmod($loc_zip_archive, 0777);

          header('Content-Type: application/zip');
          header('Content-Disposition: attachment; filename="samanthas_protocols.zip"');
          header('Content-Transfer-Encoding: binary');

          header('Expires: 0');

          readfile($loc_zip_archive);
     }


     function _Make_Zip_Via_Command_line()
     {
          $loc_zip_archive = sys_get_temp_dir().'/samanthas_protocols.zip';

          exec('cd /var/www/html && zip -r '.$loc_zip_archive.' samanthas_protocols && cd -');
          chmod($loc_zip_archive, 0777);

          header('Content-Type: application/zip');
          header('Content-Disposition: attachment; filename="samanthas_protocols.zip"');

          header('Expires: 0');

          readfile($loc_zip_archive);
     }

     _Make_Zip_Via_Command_line();
?>
