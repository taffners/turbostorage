define(function()
{

     function _MarkDownToHTML(md_id, html_id)
     {
          // convert
          var  _text = $('#' + md_id).text(),
               _target = document.getElementById(html_id),
               _converter = new showdown.Converter(),
               _html = _converter.makeHtml(_text);
               _target.innerHTML = _html;
     }

     return {
          MarkDownToHTML:_MarkDownToHTML
     }

});
