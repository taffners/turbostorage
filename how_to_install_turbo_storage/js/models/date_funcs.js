define(function()
{

     // Purpose: store all functions dealing with dates

	function _GetTodaysDate()
	{
		// purpose: get today's date

		// vars
		var 	today = new Date(),
			dd = today.getDate(),
			mm = today.getMonth()+1, //January is 0!
			yyyy = today.getFullYear();

		// add a zero before days and months under ten
		if(dd < 10)
		{
		    dd = '0' + dd
		}

		if(mm < 10)
		{
		    mm = '0' + mm
		}

		today = mm+'/'+dd+'/'+yyyy;

		return today;
	}

	function _ValidateDate(in_date)
	{
		// Purpose: check if date format is MM/DD/YYYY between 1900 and 2099.  Return true if date format is correct and false if not
		var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;
		return date_regex.test(in_date);
	}

	function _ChangeDateFormatEuropean(in_date)
	{
		// Purpose: Convert date format from MM/DD/YYYY to YYYY-MM-DD

		var 	split_date = in_date.split('/'),
			new_date = split_date[2] + '-' +  split_date[0] + '-' + split_date[1];

		return new_date;
	}

	function _ChangeDateFormatUS(in_date)
	{
		// Purpose: Convert date format from YYYY-MM-DD to MM/DD/YYYY

		var 	split_date = in_date.split('-'),
			new_date = split_date[1] + '/' + split_date[2] + '/' + split_date[0]

		return new_date;
	}

	return {
		GetTodaysDate:_GetTodaysDate,
		ValidateDate:_ValidateDate,
		ChangeDateFormatEuropean:_ChangeDateFormatEuropean,
		ChangeDateFormatUS:_ChangeDateFormatUS
	}
});
