define(function()
{
     function _TogPageViewClasses(unhide_class)
     {
          // (str) -> unhide all elements with the class equal to unhide_class

          // Select everything with the unhide class
          $('.' + unhide_class).each(function()
          {
               // find full class for the selected and replace hide with empty
               var  _new_class = $(this).attr('class')
                         .replace('hide', '');

               $(this).attr('class', _new_class);
          });
     }

     return {
          TogPageViewClasses:_TogPageViewClasses
     };
});
