define(['models/utils', 'models/toggle'],function(utils, toggle)
{
     // Set up a single page application router.

     var  _routes =
                    [
                         {page: 'turbostorage_install_linux', controller:'turbostorage_install_linux'},
                         {page: 'turbostorage_install_mac', controller:'turbostorage_install_mac'},
                         {page: 'turbostorage', controller:'turbostorage'},
                    ],
          _default_page = 'turbostorage',
          _php_vars = utils.PhpVars(),
          _page = '',
          _browser_ok,
          _test_status;

     function _startRouting()
     {
          // Purpose: Set route to default and start the hash check which starts the correct controller
          _hashCheck();

          _SetLocalStorageVars();

          _ShowPageSpecficFields();
     }

     function _hashCheck()
     {
          // Purpose: Check to see the anchor part of the url

          var  _i,
               _current_route,
               _curr_page = _GetCurrPage();

          // reset php_vars to current
          _php_vars = utils.PhpVars();

          // check if hash has changed
          if (_curr_page != _page)
          {
               // if it changed find the controller of the changed hash and load that controller
               for (_i = 0; _current_route = _routes[_i++];)
               {
                    if (_curr_page === _current_route.page)
                    {
                         _loadController(_current_route.controller);
                    }
               }

               // set page to current page
               _page = _curr_page;
          }
     }

     function _ShowPageSpecficFields()
     {
          var  _page = _php_vars['page'],
               _unhide_class = 'show_' + _page;

          toggle.TogPageViewClasses(_unhide_class);
     }


     function _loadController(controller_name)
     {
          // Purpose: Change the pages which are loaded if the hash was changed via require

          require(['controllers/' + controller_name], function(controller)
          {
               controller.start(_php_vars);
          });
     }

     function _SetLocalStorageVars()
     {
          //Purpose: Set Local Storage variables

          // make sure local storage is supported
          if (typeof(Storage) !== 'undefined')
          {

          }
          else
          {
               $('#message_center')
                    .attr('class', 'alert alert-danger show')
                    .append('Sorry, your browser does not support web storage...');
          }
     }

     function _GetCurrPage()
     {
          // Purpose: Get the current page in phpVars if page is not in php_vars make page the default_page.

          // load pages where page is defined
          if ( ('page' in _php_vars) )
          {
               return _php_vars['page'];
          }

          // if page is not defined redirect to default
          else
          {
               window.location.href = "?page=" + _default_page;
          }

     }

     return {
          startRouting: _startRouting,
          hashCheck:_hashCheck,
          SetLocalStorageVars:_SetLocalStorageVars
     };

});
