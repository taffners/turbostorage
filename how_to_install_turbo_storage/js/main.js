// append a version to the end of all the script urls for cache busting.
require.config(
{
     urlArgs:"ver=v0.1"
});

require(['router','models/utils'], function(router, utils)
{
     // start the router
     router.startRouting();

});
