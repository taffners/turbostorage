define(['models/toggle'],function(toggle)
{
     function _BindEvents(php_vars)
     {
          // show all classes with a class of show-page name

          var  _page = php_vars['page'],
               _unhide_class = 'show_' + _page;

          toggle.TogPageViewClasses(_unhide_class);
     }

     function _start(php_vars)
     {
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
