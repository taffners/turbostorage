define(['models/mark_down'],function(mark_down)
{
     function _BindEvents(php_vars)
     {
          mark_down.MarkDownToHTML('turbostorage-md', 'turbostorage-html');
     }

     function _start(php_vars)
     {
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
