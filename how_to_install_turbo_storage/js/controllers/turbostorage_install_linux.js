define(['models/mark_down'],function(mark_down)
{
     function _BindEvents(php_vars)
     {
          mark_down.MarkDownToHTML('turbostorage-linux-md', 'turbostorage-linux-html');

          // show all classes with a class of show-page name

          var  _page = php_vars['page'],
               _unhide_class = 'show_' + _page;
     }

     function _start(php_vars)
     {
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
