# **Turbo Storage**

![turboStorage.png](https://bitbucket.org/repo/jLAaqo/images/1615060203-turboStorage.png)

# What is it?

> * Turbo Storage, is a browser-based inventory tracking application for tracking biological samples in freezers.  Commercial inventory software requires monthly subscriptions which are cost prohibitive for research laboratories, as a result labs resort to tedious non-computerized tracking methods.  Turbo Storage was developed as a desktop and mobile friendly  free alternative to save researchers time, money, and frustration.  

# Authors
> * Samantha Taffner - Software Development
> * Sytse Piersma - Idea Generator
> * Diana Beckman - Application tester
> * Beatrice Plougastel - Application tester
> * Wayne Yokoyama - Principal Investigator

# Version History
> * v1.6 - March 30, 2017 - Deployed version of Turbo Storage
> * v1.0 - January 30, 2017 - Started Development on new version
> * v0.15 - January 1, 2017 - Deployed for use
> * v0.1 - August 10, 2016 - started Development